<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Database\Eloquent\Collection;

class PurchaseOrderExport implements FromCollection, WithHeadings
{
    protected $purchaseOrderResult;
    
    public function __construct(Collection $purchaseOrderResult)
    {
        $this->purchaseOrderResult = $purchaseOrderResult;
    }
    
    public function collection() {
        return $this->purchaseOrderResult;
    }
    
    public function headings(): array {
        return [
            'NO',
            'Order Date',
            'Supplier',
            'QTY',
            'Sub Total',
            'Discount',
            'VAT',
            'Grand Total',
            
        ];
    }
}
