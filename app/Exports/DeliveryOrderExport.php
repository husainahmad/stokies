<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Database\Eloquent\Collection;

class DeliveryOrderExport implements FromCollection, WithHeadings
{
    protected $deliveryOrderResult;
    
    public function __construct(Collection $deliveryOrderResult)
    {
        $this->deliveryOrderResult = $deliveryOrderResult;
    }
    
    public function collection() {
        return $this->deliveryOrderResult;
    }
    
    public function headings(): array {
        return [                        
        ];
    }
}
