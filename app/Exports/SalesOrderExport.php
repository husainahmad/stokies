<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Database\Eloquent\Collection;
class SalesOrderExport implements FromCollection, WithHeadings
{
    protected $salesOrderResult;
    
    public function __construct(Collection $salesOrderResult)
    {
        $this->salesOrderResult = $salesOrderResult;
    }
    
    public function collection() {
        return $this->salesOrderResult;
    }
    
    public function headings(): array {
        return [
            'NO',
            'Sales Order Date',
            'Delivery Date',
            'Sales Person',
        ];
    }
}
