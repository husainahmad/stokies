<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Database\Eloquent\Collection;

class InvoiceExport implements FromCollection, WithHeadings
{
    protected $invoiceResult;
    
    public function __construct(Collection $invoiceResult)
    {
        $this->invoiceResult = $invoiceResult;
    }
    
    public function collection() {
        return $this->invoiceResult;
    }
    
    public function headings(): array {
        return [            
            
        ];
    }
}
