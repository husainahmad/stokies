<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalesOrdersDetails extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sales_orders_details';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'sales_orders_id',
                  'items_id',
                  'quantity',
                  'price',
                  'amount',
                  'description'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the salesOrder for this model.
     */
    public function salesOrder()
    {
        return $this->belongsTo('App\Models\SalesOrders','sales_orders_id');
    }

    /**
     * Get the item for this model.
     */
    public function item()
    {
        return $this->belongsTo('App\Models\Items','items_id');
    }


    /**
     * Get created_at in array format
     *
     * @param  string  $value
     * @return array
     */
    public function getCreatedAtAttribute($value)
    {
        return date('j/n/Y g:i A', strtotime($value));
    }

    /**
     * Get updated_at in array format
     *
     * @param  string  $value
     * @return array
     */
    public function getUpdatedAtAttribute($value)
    {
        return date('j/n/Y g:i A', strtotime($value));
    }

    public function deliveryOrderDetails()
    {
        return $this->hasMany('App\Models\DeliveryOrdersDetails');
    }
    
}
