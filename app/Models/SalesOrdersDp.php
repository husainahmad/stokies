<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalesOrdersDp extends Model
{
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sales_orders_dps';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'id',
                  'sales_orders_id',
                  'total_dp',
                  'created_date',
                  'users_id',
                  'invoices_id'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the salesOrder for this model.
     */
    public function salesOrder()
    {
        return $this->belongsTo('App\Models\SalesOrders','sales_orders_id');
    }

    /**
     * Get the user for this model.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\Auth\User','users_id');
    }
    
    /**
     * Get the user for this model.
     */
    public function invoice()
    {
        return $this->belongsTo('App\Models\Invoices','invoices_id');
    }
    
    /**
     * Get updated_at in array format
     *
     * @param  string  $value
     * @return array
     */
    public function getCreatedDateID()
    {
        return date('d/m/Y', strtotime($this->created_date));
    }



}
