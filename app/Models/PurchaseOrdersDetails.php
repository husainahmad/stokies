<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrdersDetails extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'purchase_orders_details';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'purchase_orders_id',
                  'items_id',
                  'quantity',
                  'price',
                  'amount',
                  'users_id',
                  'description'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the purchaseOrder for this model.
     */
    public function purchaseOrder()
    {
        return $this->belongsTo('App\Models\PurchaseOrder','purchase_orders_id');
    }

    /**
     * Get the item for this model.
     */
    public function item()
    {
        return $this->belongsTo('App\Models\Items','items_id');
    }

    /**
     * Get the user for this model.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User','users_id');
    }


    /**
     * Get created_at in array format
     *
     * @param  string  $value
     * @return array
     */
    public function getCreatedAtAttribute($value)
    {
        return date('j/n/Y g:i A', strtotime($value));
    }

    /**
     * Get updated_at in array format
     *
     * @param  string  $value
     * @return array
     */
    public function getUpdatedAtAttribute($value)
    {
        return date('j/n/Y g:i A', strtotime($value));
    }

}
