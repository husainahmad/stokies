<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalesOrders extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sales_orders';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'no',
                  'sales_order_date',
                  'sales_order_delivery',
                  'sales_person_id',
                  'ref_po_customer',
                  'purchase_order_date',
                  'customer_id',
                  'transport',
                  'term_billing_id',
                  'user_id',
                  'discount',
                  'total',
                  'sub_total',
                  'vat',
                  'total_quantity',
                  'total_amount',
                  'status',
                  'delivery_address',
                  'tax'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the salesPerson for this model.
     */
    public function salesPerson()
    {
        return $this->belongsTo('App\Models\SalesPersons','sales_person_id');
    }

    /**
     * Get the customer for this model.
     */
    public function customer()
    {
        return $this->belongsTo('App\Models\Customers','customer_id');
    }

    /**
     * Get the termBilling for this model.
     */
    public function termBilling()
    {
        return $this->belongsTo('App\Models\TermBillings','term_billing_id');
    }

    /**
     * Get the user for this model.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\Auth\User','user_id');
    }

    /**
     * Get created_at in array format
     *
     * @param  string  $value
     * @return array
     */
    public function getCreatedAtAttribute($value)
    {
        return date('j/n/Y g:i A', strtotime($value));
    }

    /**
     * Get updated_at in array format
     *
     * @param  string  $value
     * @return array
     */
    public function getUpdatedAtAttribute($value)
    {
        return date('j/n/Y g:i A', strtotime($value));
    }

    
    public function getPurchaseOrderID()
    {
        return date('d/m/Y', strtotime($this->purchase_order_date));
    }
    
    public function getSalesOrderDeliveryID()
    {
        return date('d/m/Y', strtotime($this->sales_order_delivery));
    }
    
    public function getSalesOrderDateID()
    {
        return date('d/m/Y', strtotime($this->sales_order_date));
    }
    
    /**
     * Get updated_at in array format
     *
     * @param  string  $value
     * @return array
     */
    public function getCreatedAtID()
    {
        return date('d/m/Y', strtotime($this->created_at));
    }

    /**
     * Get updated_at in array format
     *
     * @param  string  $value
     * @return array
     */
    public function getUpdatedAtID()
    {
        return date('d/m/Y', strtotime($this->updated_at));
    }
    
    
    /**
     * Get the user for this model.
     */
    public function getDP()
    {
        return $this->hasOne('App\Models\SalesOrdersDp');
    }
    
    public function salesOrdersDetails() 
    {
        return $this->hasMany('App\Models\SalesOrdersDetails');
    }
}
