<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrders extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'purchase_orders';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'suppliers_id',
                  'purchase_no',
                  'users_id',
                  'term_billing_id',
                  'total_quantity',
                  'total_amount',
                  'status',
                  'ordered_at',
                  'arrived_at',
                  'is_tax',
                  'sub_total',
                  'vat',
                  'discount',
                  'tc',
                  'tax'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the supplier for this model.
     */
    public function supplier()
    {
        return $this->belongsTo('App\Models\Suppliers','suppliers_id');
    }

    /**
     * Get the user for this model.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\Auth\User','users_id');
    }

    /**
     * Get the termBilling for this model.
     */
    public function termBilling()
    {
        return $this->belongsTo('App\Models\TermBillings','term_billing_id');
    }
    
    public function purchaseOrdersDetails()
    {
        return $this->hasMany('App\Models\PurchaseOrdersDetails');
    }
    
    /**
     * Get created_at in array format
     *
     * @param  string  $value
     * @return array
     */
    public function getCreatedAtAttribute($value)
    {
        return date('j/n/Y g:i A', strtotime($value));
    }

    /**
     * Get updated_at in array format
     *
     * @param  string  $value
     * @return array
     */
    public function getUpdatedAtAttribute($value)
    {
        return date('j/n/Y g:i A', strtotime($value));
    }
    
    public function getOrderAtID()
    {
        return date('d/m/Y', strtotime($this->ordered_at));
    }
    
    public function getArrivedAtID()
    {
        return date('d/m/Y', strtotime($this->arrived_at));
    }

}
