<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvoiceDeliveryOrders extends Model
{
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'invoice_delivery_orders';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'invoices_id',
                  'delivery_orders_id'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the invoice for this model.
     */
    public function invoice()
    {
        return $this->belongsTo('App\Models\Invoices','invoices_id');
    }

    /**
     * Get the deliveryOrder for this model.
     */
    public function deliveryOrder()
    {
        return $this->belongsTo('App\Models\DeliveryOrders','delivery_orders_id');
    }



}
