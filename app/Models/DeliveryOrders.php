<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryOrders extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'delivery_orders';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'customers_id',
                  'sales_orders_id',
                  'users_id',
                  'driver',
                  'pic',
                  'no',
                  'ref_po',
                  'no_pol',
                  'status',
				  'created_at'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the customer for this model.
     */
    public function customer()
    {
        return $this->belongsTo('App\Models\Customers','customers_id');
    }

    /**
     * Get the user for this model.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\Auth\User','users_id');
    }

     /**
     * Get the sales_orders for this model.
     */
    public function salesOrders()
    {
        return $this->belongsTo('App\Models\SalesOrders','sales_orders_id');
    }
    
    public function deliveryOrdersDetails() {
        return $this->hasMany('App\Models\DeliveryOrdersDetails');
    }
    

    /**
     * Get created_at in array format
     *
     * @param  string  $value
     * @return array
     */
    public function getCreatedAtAttribute($value)
    {
        return date('d/m/Y', strtotime($value));
    }

    /**
     * Get updated_at in array format
     *
     * @param  string  $value
     * @return array
     */
    public function getUpdatedAtAttribute($value)
    {
        return date('d/m/Y', strtotime($value));
    }

    
    public function unusedDO()
    {
        return $this->select('deliveryOrders.*')->join("invoiceDeliveryOrders","invoiceDeliveryOrders.delivery_orders_id","=", "deliveryOrders.id");
    }
    
    /**
     * Get updated_at in array format
     *
     * @param  string  $value
     * @return array
     */
    public function getCreatedAtID()
    {
        return date('d/m/Y', strtotime($this->created_at));
    }

    /**
     * Get updated_at in array format
     *
     * @param  string  $value
     * @return array
     */
    public function getUpdatedAtID()
    {
        return date('d/m/Y', strtotime($this->updated_at));
    }
}
