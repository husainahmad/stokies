<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IncomingInvoices extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'incoming_invoices';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'no',
                  'ship_via',
                  'purchase_orders_id',
                  'due_date',
                  'created_at',
                  'updated_at',
                  'sub_total',
                  'discount',
                  'total',
                  'vat',
                  'grand_total',
                  'users_id'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the purchaseOrder for this model.
     */
    public function purchaseOrder()
    {
        return $this->belongsTo('App\Models\PurchaseOrders','purchase_orders_id');
    }

    /**
     * Get the user for this model.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\Auth\User','users_id');
    }


    /**
     * Get created_at in array format
     *
     * @param  string  $value
     * @return array
     */
    public function getCreatedAtAttribute($value)
    {
        return date('j/n/Y g:i A', strtotime($value));
    }

    /**
     * Get updated_at in array format
     *
     * @param  string  $value
     * @return array
     */
    public function getUpdatedAtAttribute($value)
    {
        return date('j/n/Y g:i A', strtotime($value));
    }

        /**
     * Get updated_at in array format
     *
     * @param  string  $value
     * @return array
     */
    public function getDueDateID()
    {
        return date('d/m/Y', strtotime($this->due_date));
    }
    
    /**
     * Get updated_at in array format
     *
     * @param  string  $value
     * @return array
     */
    public function getCreatedAtID()
    {
        return date('d/m/Y', strtotime($this->created_at));
    }

    /**
     * Get updated_at in array format
     *
     * @param  string  $value
     * @return array
     */
    public function getUpdatedAtID()
    {
        return date('d/m/Y', strtotime($this->updated_at));
    }
    
    
    public function incomingInvoicesDetails() {
        return $this->hasMany('App\Models\IncomingInvoicesDetails');
    }
}
