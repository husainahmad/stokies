<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoices extends Model
{
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'invoices';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'no',
                  'ship_via',
                  'purchase_no',
                  'due_date',
                  'created_at',
                  'updated_at',
                  'customers_id',
                  'sub_total',
                  'discount',
                  'total',
                  'vat',
                  'grand_total',
                  'users_id',
                  'sales_persons_id',
                  'is_dp',
                  'dp'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the deliveryOrder for this model.
     */
    public function deliveryOrder()
    {
        return $this->belongsTo('App\Models\DeliveryOrders','delivery_orders_id');
    }

    /**
     * Get the customer for this model.
     */
    public function customer()
    {
        return $this->belongsTo('App\Models\Customers','customers_id');
    }

    /**
     * Get the user for this model.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\Auth\User','users_id');
    }

    /**
     * Get the salesPerson for this model.
     */
    public function salesPerson()
    {
        return $this->belongsTo('App\Models\SalesPersons','sales_persons_id');
    }

    public function invoicesDetails() {
        return $this->hasMany('App\Models\InvoicesDetails');
    }

    /**
     * Get created_at in array format
     *
     * @param  string  $value
     * @return array
     */
    public function getCreatedAtAttribute($value)
    {
        return date('d/m/Y', strtotime($value));
    }

    /**
     * Get updated_at in array format
     *
     * @param  string  $value
     * @return array
     */
    public function getUpdatedAtAttribute($value)
    {
        return date('d/m/Y', strtotime($value));
    }
    
    /**
     * Get updated_at in array format
     *
     * @param  string  $value
     * @return array
     */
    public function getDueDateID()
    {
        return date('d/m/Y', strtotime($this->due_date));
    }

    /**
     * Get updated_at in array format
     *
     * @param  string  $value
     * @return array
     */
    public function getCreatedAtID()
    {
        return date('d/m/Y', strtotime($this->created_at));
    }

    /**
     * Get updated_at in array format
     *
     * @param  string  $value
     * @return array
     */
    public function getUpdatedAtID()
    {
        return date('d/m/Y', strtotime($this->updated_at));
    }
        
}
