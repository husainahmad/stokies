<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemsStockOut extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'items_stock_outs';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'items_id',
                  'volume',
                  'customers_id',
                  'price',
                  'amount_price',
                  'users_id',
                  'reference_no'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the item for this model.
     */
    public function item()
    {
        return $this->belongsTo('App\Models\Item','items_id');
    }

    /**
     * Get the customer for this model.
     */
    public function customer()
    {
        return $this->belongsTo('App\Models\Customer','customers_id');
    }

    /**
     * Get the user for this model.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User','users_id');
    }


    /**
     * Get created_at in array format
     *
     * @param  string  $value
     * @return array
     */
    public function getCreatedAtAttribute($value)
    {
        return date('j/n/Y g:i A', strtotime($value));
    }

    /**
     * Get updated_at in array format
     *
     * @param  string  $value
     * @return array
     */
    public function getUpdatedAtAttribute($value)
    {
        return date('j/n/Y g:i A', strtotime($value));
    }

}
