<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvoicesDetails extends Model
{
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'invoices_details';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'invoices_id',
                  'items_id',
                  'quantity',
                  'price',
                  'amount',
                  'description',
                  'tax'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the invoice for this model.
     */
    public function invoice()
    {
        return $this->belongsTo('App\Models\Invoice','invoices_id');
    }

    /**
     * Get the item for this model.
     */
    public function item()
    {
        return $this->belongsTo('App\Models\Items','items_id');
    }



}
