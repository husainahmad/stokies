<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryOrdersDetails extends Model
{
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'delivery_orders_details';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'delivery_orders_id',
                  'items_id',
                  'quantity',
                  'description',
                  'sales_orders_details_id'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the deliveryOrdersDetail for this model.
     */
    public function deliveryOrders()
    {
        return $this->belongsTo('App\Models\DeliveryOrders','delivery_orders_id');
    }

    /**
     * Get the deliveryOrdersDetail for this model.
     */
    public function salesOrdersDetails()
    {
        return $this->belongsTo('App\Models\SalesOrdersDetails','sales_orders_details_id');
    }

    /**
     * Get the item for this model.
     */
    public function item()
    {
        return $this->belongsTo('App\Models\Items','items_id');
    }



}
