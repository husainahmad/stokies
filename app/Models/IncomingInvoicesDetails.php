<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IncomingInvoicesDetails extends Model
{
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'incoming_invoices_details';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'incoming_invoices_id',
                  'items_id',
                  'quantity',
                  'price',
                  'amount',
                  'description',
                  'purchase_orders_details_id',
                  'tax'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the incomingInvoice for this model.
     */
    public function incomingInvoice()
    {
        return $this->belongsTo('App\Models\IncomingInvoices','incoming_invoices_id');
    }

    /**
     * Get the item for this model.
     */
    public function item()
    {
        return $this->belongsTo('App\Models\Items','items_id');
    }



}
