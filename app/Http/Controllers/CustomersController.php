<?php

namespace App\Http\Controllers;

use App\Models\Auth\User;
use App\Models\Customers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;

class CustomersController extends Controller
{

    /**
     * Display a listing of the customers.
     *
     * @return Illuminate\View\View
     */
    public function index(Request $request)
    {
        
        $customersObjects = Customers::with('user')
                ->where('name','like', "%{$request->keyword}%")
                ->orderBy('updated_at', 'desc')->paginate(10);
        $customersObjects->appends($request->only('keyword'));         
        return view('backend.customers.index', compact('customersObjects'));
    }

    /**
     * Show the form for creating a new customers.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        $users = User::pluck('id','id')->all();
        
        return view('backend.customers.create', compact('users'));
    }

    /**
     * Store a new customers in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            $user_id = (auth()->check()) ? auth()->user()->id : null;
            $data['user_id'] =  $user_id;
            Customers::create($data);

            return redirect()->route('customers.customers.index')
                             ->with('success_message', 'Customers was successfully added!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    /**
     * Display the specified customers.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $customers = Customers::with('user')->findOrFail($id);

        return view('backend.customers.show', compact('customers'));
    }

    /**
     * Show the form for editing the specified customers.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $customers = Customers::findOrFail($id);
        $users = User::pluck('id','id')->all();

        return view('backend.customers.edit', compact('customers','users'));
    }

    /**
     * Update the specified customers in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            $customers = Customers::findOrFail($id);
            $user_id = (auth()->check()) ? auth()->user()->id : null;
            $data['user_id'] =  $user_id;
            $customers->update($data);

            return redirect()->route('customers.customers.index')
                             ->with('success_message', 'Customers was successfully updated!');

        } catch (Exception $exception) {
            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }        
    }

    /**
     * Remove the specified customers from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $customers = Customers::findOrFail($id);
            $customers->delete();

            return redirect()->route('customers.customers.index')
                             ->with('success_message', 'Customers was successfully deleted!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'name' => 'required|string|min:1|max:145',
            'address' => 'required|string|min:1|max:255',
            'phone' => 'nullable|string|min:0|max:45',
            'mobile' => 'nullable|string|min:0|max:45',
            'fax' => 'nullable|string|min:0|max:45',
            'npwp' => 'nullable|string|min:0|max:45',
            'email' => 'nullable|string|min:0|max:45',
     
        ];

        
        $data = $request->validate($rules);




        return $data;
    }

}
