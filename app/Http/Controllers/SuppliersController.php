<?php

namespace App\Http\Controllers;

use App\Models\Auth\User;
use App\Models\Suppliers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;

class SuppliersController extends Controller
{

    /**
     * Display a listing of the suppliers.
     *
     * @return Illuminate\View\View
     */
    public function index(Request $request)
    {
        $suppliersObjects = Suppliers::with('user')
                ->where('name','like', "%{$request->keyword}%")
                ->orderBy('updated_at', 'desc')->paginate(10);
        $suppliersObjects->appends($request->only('keyword'));        
        return view('backend.suppliers.index', compact('suppliersObjects'));
    }

    /**
     * Show the form for creating a new suppliers.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        $users = User::pluck('id','id')->all();
        
        return view('backend.suppliers.create', compact('users'));
    }

    /**
     * Store a new suppliers in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            $user_id = (auth()->check()) ? auth()->user()->id : null;
            $data['user_id'] =  $user_id;
            Suppliers::create($data);

            return redirect()->route('suppliers.suppliers.index')
                             ->with('success_message', 'Suppliers was successfully added!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    /**
     * Display the specified suppliers.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $suppliers = Suppliers::with('user')->findOrFail($id);

        return view('backend.suppliers.show', compact('suppliers'));
    }

    /**
     * Show the form for editing the specified suppliers.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $suppliers = Suppliers::findOrFail($id);
        $users = User::pluck('id','id')->all();

        return view('backend.suppliers.edit', compact('suppliers','users'));
    }

    /**
     * Update the specified suppliers in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            $suppliers = Suppliers::findOrFail($id);
            $user_id = (auth()->check()) ? auth()->user()->id : null;
            $data['user_id'] =  $user_id;
            $suppliers->update($data);

            return redirect()->route('suppliers.suppliers.index')
                             ->with('success_message', 'Suppliers was successfully updated!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }        
    }

    /**
     * Remove the specified suppliers from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $suppliers = Suppliers::findOrFail($id);
            $suppliers->delete();

            return redirect()->route('suppliers.suppliers.index')
                             ->with('success_message', 'Suppliers was successfully deleted!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'name' => 'required|string|min:1|max:145',
            'address' => 'required|string|min:1|max:255',
            'phone' => 'nullable|string|min:0|max:45',
            'fax' => 'nullable|string|min:0|max:45',
            'cp' => 'nullable|string|min:0|max:45',
            'npwp' => 'nullable|string|min:0|max:45',
            'email' => 'nullable|string|min:0|max:45'
        ];
        
        $data = $request->validate($rules);

        return $data;
    }

}
