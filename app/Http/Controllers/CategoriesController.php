<?php

namespace App\Http\Controllers;

use App\Models\Auth\User;
use App\Helpers\Auth\Auth;
use App\Models\Categories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;

class CategoriesController extends Controller
{

    /**
     * Display a listing of the categories.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $categoriesObjects = Categories::with('user')->orderBy('id', 'desc')->paginate(10);

        return view('backend.categories.index', compact('categoriesObjects'));
    }

    /**
     * Show the form for creating a new categories.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        $users = User::pluck('id','id')->all();
        
        return view('backend.categories.create', compact('users'));
    }

    /**
     * Store a new categories in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            $data = $this->getData($request);
            $user_id = (auth()->check()) ? auth()->user()->id : null;
            $data['user_id'] =  $user_id;
            Categories::create($data);

            return redirect()->route('categories.categories.index')
                             ->with('success_message', 'Categories was successfully added!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    /**
     * Display the specified categories.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $categories = Categories::with('user')->findOrFail($id);

        return view('backend.categories.show', compact('categories'));
    }

    /**
     * Show the form for editing the specified categories.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $categories = Categories::findOrFail($id);
        $users = User::pluck('id','id')->all();

        return view('backend.categories.edit', compact('categories','users'));
    }

    /**
     * Update the specified categories in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            $categories = Categories::findOrFail($id);
            $user_id = (auth()->check()) ? auth()->user()->id : null;
            $data['user_id'] =  $user_id;
            $categories->update($data);

            return redirect()->route('categories.categories.index')
                             ->with('success_message', 'Categories was successfully updated!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }        
    }

    /**
     * Remove the specified categories from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $categories = Categories::findOrFail($id);
            $categories->delete();

            return redirect()->route('categories.categories.index')
                             ->with('success_message', 'Categories was successfully deleted!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'name' => 'required|string|min:1|max:145',
            'description' => 'nullable|string|min:0|max:255'
     
        ];
        
        $data = $request->validate($rules);




        return $data;
    }

}
