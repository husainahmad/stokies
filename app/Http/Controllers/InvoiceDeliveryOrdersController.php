<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use Illuminate\Http\Request;
use App\Models\DeliveryOrder;
use App\Http\Controllers\Controller;
use App\Models\InvoiceDeliveryOrders;
use Exception;

class InvoiceDeliveryOrdersController extends Controller
{

    /**
     * Display a listing of the invoice delivery orders.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $invoiceDeliveryOrdersObjects = InvoiceDeliveryOrders::with('invoice','deliveryorder')->paginate(25);

        return view('invoice_delivery_orders.index', compact('invoiceDeliveryOrdersObjects'));
    }

    /**
     * Show the form for creating a new invoice delivery orders.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        $invoices = Invoice::pluck('no','id')->all();
$deliveryOrders = DeliveryOrder::pluck('ref_no','id')->all();
        
        return view('invoice_delivery_orders.create', compact('invoices','deliveryOrders'));
    }

    /**
     * Store a new invoice delivery orders in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            InvoiceDeliveryOrders::create($data);

            return redirect()->route('invoice_delivery_orders.invoice_delivery_orders.index')
                             ->with('success_message', 'Invoice Delivery Orders was successfully added!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    /**
     * Display the specified invoice delivery orders.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $invoiceDeliveryOrders = InvoiceDeliveryOrders::with('invoice','deliveryorder')->findOrFail($id);

        return view('invoice_delivery_orders.show', compact('invoiceDeliveryOrders'));
    }

    /**
     * Show the form for editing the specified invoice delivery orders.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $invoiceDeliveryOrders = InvoiceDeliveryOrders::findOrFail($id);
        $invoices = Invoice::pluck('no','id')->all();
$deliveryOrders = DeliveryOrder::pluck('ref_no','id')->all();

        return view('invoice_delivery_orders.edit', compact('invoiceDeliveryOrders','invoices','deliveryOrders'));
    }

    /**
     * Update the specified invoice delivery orders in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            $invoiceDeliveryOrders = InvoiceDeliveryOrders::findOrFail($id);
            $invoiceDeliveryOrders->update($data);

            return redirect()->route('invoice_delivery_orders.invoice_delivery_orders.index')
                             ->with('success_message', 'Invoice Delivery Orders was successfully updated!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }        
    }

    /**
     * Remove the specified invoice delivery orders from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $invoiceDeliveryOrders = InvoiceDeliveryOrders::findOrFail($id);
            $invoiceDeliveryOrders->delete();

            return redirect()->route('invoice_delivery_orders.invoice_delivery_orders.index')
                             ->with('success_message', 'Invoice Delivery Orders was successfully deleted!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'invoices_id' => 'required',
            'delivery_orders_id' => 'required',
     
        ];

        
        $data = $request->validate($rules);




        return $data;
    }

}
