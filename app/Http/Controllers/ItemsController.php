<?php

namespace App\Http\Controllers;

use App\Models\Auth\User;
use App\Models\Units;
use App\Models\Items;
use App\Models\Categories;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Exception;

class ItemsController extends Controller
{

    /**
     * Display a listing of the items.
     *
     * @return Illuminate\View\View
     */
    public function index(Request $request)
    {
        $itemsObjects = Items::with('category','user','unit')
                ->where('name','like', "%{$request->keyword}%")
                ->orderBy('updated_at', 'desc')->paginate(10);
        $itemsObjects->appends($request->only('keyword'));         
        return view('backend.items.index', compact('itemsObjects'));
    }

    /**
     * Show the form for creating a new items.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        $categories = Categories::pluck('name','id')->all();
        $users = User::pluck('id','id')->all();
        $units = Units::pluck('name','id')->all();
        
        return view('backend.items.create', compact('categories','users','units'));
    }

    /**
     * Store a new items in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            $data['users_id'] = auth()->user()->id;
            Items::create($data);

            return redirect()->route('items.items.index')
                             ->with('success_message', 'Items was successfully added!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    /**
     * Display the specified items.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $items = Items::with('category','user','unit')->findOrFail($id);

        return view('backend.items.show', compact('items'));
    }

    /**
     * Display the specified items.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function json($id)
    {
        $items = Items::with('category','price','unit')->findOrFail($id);
        
        return response()->json($items);
    }
    
    /**
     * Show the form for editing the specified items.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $items = Items::findOrFail($id);
        $categories = Categories::pluck('name','id')->all();
        $users = User::pluck('id','id')->all();
        $units = Units::pluck('name','id')->all();

        return view('backend.items.edit', compact('items','categories','users','units'));
    }

    /**
     * Update the specified items in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            $items = Items::findOrFail($id);
            $data['users_id'] = auth()->user()->id;
            
            var_dump($data);
            
            $items->update($data);

            return redirect()->route('items.items.index')
                             ->with('success_message', 'Items was successfully updated!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }        
    }

    /**
     * Remove the specified items from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $items = Items::findOrFail($id);
            $items->delete();

            return redirect()->route('items.items.index')
                             ->with('success_message', 'Items was successfully deleted!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'category_id' => 'required',
            'name' => 'required|string|min:1|max:145',
            'description' => 'required|string|min:1|max:255',
            'unit_id' => 'required',    
            'code' => 'required',    
        ];

        
        $data = $request->validate($rules);




        return $data;
    }

}
