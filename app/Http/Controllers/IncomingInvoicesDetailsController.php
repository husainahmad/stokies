<?php

namespace App\Http\Controllers;

use App\Models\Item;
use Illuminate\Http\Request;
use App\Models\IncomingInvoice;
use App\Http\Controllers\Controller;
use App\Models\IncomingInvoicesDetails;
use Exception;

class IncomingInvoicesDetailsController extends Controller
{

    /**
     * Display a listing of the incoming invoices details.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $incomingInvoicesDetailsObjects = IncomingInvoicesDetails::with('incominginvoice','item')->paginate(25);

        return view('incoming_invoices_details.index', compact('incomingInvoicesDetailsObjects'));
    }

    /**
     * Show the form for creating a new incoming invoices details.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        $incomingInvoices = IncomingInvoice::pluck('no','id')->all();
$items = Item::pluck('name','id')->all();
        
        return view('incoming_invoices_details.create', compact('incomingInvoices','items'));
    }

    /**
     * Store a new incoming invoices details in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            IncomingInvoicesDetails::create($data);

            return redirect()->route('incoming_invoices_details.incoming_invoices_details.index')
                             ->with('success_message', 'Incoming Invoices Details was successfully added!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    /**
     * Display the specified incoming invoices details.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $incomingInvoicesDetails = IncomingInvoicesDetails::with('incominginvoice','item')->findOrFail($id);

        return view('incoming_invoices_details.show', compact('incomingInvoicesDetails'));
    }

    /**
     * Show the form for editing the specified incoming invoices details.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $incomingInvoicesDetails = IncomingInvoicesDetails::findOrFail($id);
        $incomingInvoices = IncomingInvoice::pluck('no','id')->all();
$items = Item::pluck('name','id')->all();

        return view('incoming_invoices_details.edit', compact('incomingInvoicesDetails','incomingInvoices','items'));
    }

    /**
     * Update the specified incoming invoices details in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            $incomingInvoicesDetails = IncomingInvoicesDetails::findOrFail($id);
            $incomingInvoicesDetails->update($data);

            return redirect()->route('incoming_invoices_details.incoming_invoices_details.index')
                             ->with('success_message', 'Incoming Invoices Details was successfully updated!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }        
    }

    /**
     * Remove the specified incoming invoices details from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $incomingInvoicesDetails = IncomingInvoicesDetails::findOrFail($id);
            $incomingInvoicesDetails->delete();

            return redirect()->route('incoming_invoices_details.incoming_invoices_details.index')
                             ->with('success_message', 'Incoming Invoices Details was successfully deleted!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'incoming_invoices_id' => 'required',
            'items_id' => 'required',
            'quantity' => 'required|numeric|min:-2147483648|max:2147483647',
            'price' => 'required|numeric|min:-9|max:9',
            'amount' => 'required|numeric|min:-9|max:9',
            'description' => 'nullable|string|min:0|max:2000',
     
        ];

        
        $data = $request->validate($rules);




        return $data;
    }

}
