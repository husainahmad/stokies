<?php

namespace App\Http\Controllers;

use App\Models\Auth\User;
use App\Models\Customers;
use App\Models\Items;
use Illuminate\Http\Request;
use App\Models\DeliveryOrders;
use App\Models\DeliveryOrdersDetails;
use App\Models\InvoiceDeliveryOrders;
use App\Models\SalesOrdersDetails;
use App\Models\Invoices;
use App\Models\InvoicesDetails;

use App\Models\SalesOrders;
use App\Http\Controllers\Controller;
use Exception;

class DeliveryOrdersController extends Controller
{

    /**
     * Display a listing of the delivery orders.
     *
     * @return Illuminate\View\View
     */
    public function index(Request $request)
    {
        $customers = Customers::pluck('name','id')->all();
        $searchBy['keyword'] = $request->keyword;
        $searchBy['customers_id'] = $request->customers_id;
        $deliveryOrdersObjects = DeliveryOrders::with('customer','user')
                ->where('no','like', "%{$request->keyword}%")
                ->where('customers_id','like', "%{$request->customers_id}%")
                ->orderBy('no', 'desc')->paginate(10);
        $deliveryOrdersObjects->appends($request->only('keyword'));        
        $deliveryOrdersObjects->appends($request->only('customers_id'));        
        return view('backend.delivery_orders.index', compact('deliveryOrdersObjects','customers'));
    }

    /**
     * Show the form for creating a new delivery orders.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        $customers = Customers::pluck('name','id')->all();
        $users = User::pluck('id','id')->all();
        $items = Items::pluck('name','id')->all();
       
        $salesorders = SalesOrders::select('sales_orders.no','sales_orders.id')
                        ->where('sales_orders.status','0')
                        ->get(); 
        
        
        return view('backend.delivery_orders.create', compact('customers','users','items','salesorders'));
    }

    /**
     * Store a new delivery orders in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $items_id = $request['items_id'];
            $quantity = $request['quantity'];
            $desc = $request['description'];
            
            $salesorderdetailid = $request['sales_order_detail_id']; 

            $deliveryOrders = DeliveryOrders::where('no', $request['no'])->first();
            
            if ($deliveryOrders!=null) {
                return back()->withInput()
                         ->withErrors(['unexpected_error' => 'SJ no already in the system, please use another one!']);
            }

            if (isset($items_id)) {
            
                $total_quantity = 0;
                //count total
                
                foreach( $items_id as $item => $n ) {                
                    $total_quantity+=$quantity[$item]; 
                }

                $request['total_quantity'] = $total_quantity;
                $request['status'] = 0;            

                $data = $this->getData($request);

                $data['created_at'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['created_at'])));
                $data['users_id'] = auth()->user()->id;      

                $deliveryOrders = DeliveryOrders::create($data);
                
                foreach( $items_id as $item => $n ) {
                    $dataDetail = array (
                        'delivery_orders_id' => $deliveryOrders->id,
                        'items_id' => $items_id[$item],
                        'quantity' => $quantity[$item],
                        'description' => $desc[$item],
                        'sales_orders_details_id' => $salesorderdetailid[$item]
                    );

                    DeliveryOrdersDetails::create($dataDetail);                                                            
                }   
                
                return redirect()->route('delivery_orders.delivery_orders.index')
                             ->with('success_message', 'Delivery Orders was successfully added!');
            } else {                
                return redirect()->route('delivery_orders.delivery_orders.index')
                             ->with('success_message', 'Cannot save empty data!');
            }
            
            
        } catch (Exception $exception) {        
            dd($exception);
            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    /**
     * Display the specified delivery orders.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $customers = Customers::pluck('name','id')->all();
        $users = User::pluck('id','id')->all();
        $items = Items::pluck('name','id')->all();
        $salesorders = SalesOrders::pluck('no','id')->all();        
        $deliveryOrders = DeliveryOrders::with('customer','user', 'salesOrders')->findOrFail($id);
        $salesorders = SalesOrders::pluck('no','id')->all();
        return view('backend.delivery_orders.show', compact('deliveryOrders','customers','users','items','salesorders'));
    }

    /**
     * Show the form for editing the specified delivery orders.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $deliveryOrders = DeliveryOrders::findOrFail($id);
        $customers = Customers::pluck('name','id')->all();
        $users = User::pluck('id','id')->all();
        $items = Items::pluck('name','id')->all();
        $salesorders = SalesOrders::select('sales_orders.no','sales_orders.id')->leftjoin('delivery_orders', 'delivery_orders.sales_orders_id', '=', 'sales_orders.id')
                        ->where('sales_orders.status','0')
                        ->get(); 
        
        $deliveryOrdersDetails = DeliveryOrdersDetails::where('delivery_orders_id',$id)->get();
            
        return view('backend.delivery_orders.edit', compact('deliveryOrders','customers','users','items','deliveryOrdersDetails','salesorders'));
    }

    /**
     * Print the form for editing the specified delivery orders.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function pdf($id)
    {
        $deliveryOrders = DeliveryOrders::findOrFail($id);
        $customers = Customers::pluck('name','id')->all();
        $users = User::pluck('id','id')->all();
        $items = Items::pluck('name','id')->all();
        $salesorders = SalesOrders::pluck('no','id')->all();      
        $deliveryOrdersDetails = DeliveryOrdersDetails::where('delivery_orders_id',$id)->get();
            
        return view('backend.delivery_orders.print', compact('deliveryOrders','customers','users','items','deliveryOrdersDetails','salesorders'));
    }

    /**
     * Update the specified delivery orders in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $items_id = $request['items_id'];
            
            $quantity = $request['quantity'];
            $desc = $request['description'];
            $total_quantity = 0;
            
            $salesorderdetailid = $request['sales_order_detail_id']; 
            
            if (isset($items_id)) {
                //count total
            
                foreach( $items_id as $item => $n ) {                
                    $total_quantity+=$quantity[$item]; 
                }

                $request['total_quantity'] = $total_quantity;
                $request['status'] = 0;
                $data['users_id'] = auth()->user()->id;            

                $data = $this->getData($request);
                $data['created_at'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['created_at'])));
                $deliveryOrders = DeliveryOrders::findOrFail($id);
                $deliveryOrders->update($data);

                $deliveryOrdersDetails = DeliveryOrdersDetails::where('delivery_orders_id',$id)->get();

                foreach( $deliveryOrdersDetails as $item => $n ) {
                    $pod = DeliveryOrdersDetails::findOrFail($n->id);
                    $pod->delete();
                }         

                foreach( $items_id as $item => $n ) {
                    $dataDetail = array (
                        'delivery_orders_id' => $id,
                        'items_id' => $items_id[$item],
                        'quantity' => $quantity[$item],
                        'description' => $desc[$item],
                        'sales_orders_details_id' => $salesorderdetailid[$item]                    
                    );
                    DeliveryOrdersDetails::create($dataDetail);
                }  


                return redirect()->route('delivery_orders.delivery_orders.index')
                                 ->with('success_message', 'Delivery Orders was successfully updated!');
            } else {
                return redirect()->route('delivery_orders.delivery_orders.index')
                                 ->with('success_message', 'cannot update!');
            }
            

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }        
    }
    
    /**
     * Complete the specified delivery orders in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function complete($id, Request $request)
    {
        try {
            
            $data['users_id'] = auth()->user()->id;            
            $data['status'] = 1;            
            
            $deliveryOrders = DeliveryOrders::findOrFail($id);
            $deliveryOrders->update($data);
            
            return redirect()->route('delivery_orders.delivery_orders.index')
                             ->with('success_message', 'Delivery Orders was successfully updated!');

        } catch (Exception $exception) {
            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }        
    }
    
    /**
     * UnComplete the specified delivery orders in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function uncomplete($id, Request $request)
    {
        try {
            
            $data['users_id'] = auth()->user()->id;            
            $data['status'] = 0;            
            
            $deliveryOrders = DeliveryOrders::findOrFail($id);
            $deliveryOrders->update($data);
            
            return redirect()->route('delivery_orders.delivery_orders.index')
                             ->with('success_message', 'Delivery Orders was successfully updated!');

        } catch (Exception $exception) {
            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }        
    }

    /**
     * Show the form for editing the specified purchase orders.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function find(Request $request)
    {        
        
        $deliveryOrders = DeliveryOrders::where('no', $request['no'])->firstOrFail();
        
        try {
            return response()->json($deliveryOrders);
        } catch (Exception $ex) {
            return response()->json($ex);
        }        
    }
    
    /**
     * Remove the specified delivery orders from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $deliveryOrders = DeliveryOrders::findOrFail($id);
            
            $invoiceDeliveryOrders = InvoiceDeliveryOrders::where('delivery_orders_id',$id)->get();
            
            foreach( $invoiceDeliveryOrders as $itemInvoiceDelivery => $nido ) {
                
                $invoices = Invoices::where('id',$nido->invoices_id)->get();           
                
                foreach( $invoices as $itemInvoice => $ii ) {
                    $invoicesDetails = InvoicesDetails::where('invoices_id',$ii->id)->delete();
                }
                
                Invoices::where('id',$nido->invoices_id)->delete();                                                
            }
            
            InvoiceDeliveryOrders::where('delivery_orders_id',$id)->delete();
            
            DeliveryOrdersDetails::where('delivery_orders_id',$id)->delete();            
            
            $deliveryOrders->delete();

            return redirect()->route('delivery_orders.delivery_orders.index')
                             ->with('success_message', 'Delivery Orders was successfully deleted!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'customers_id' => 'required',
            'no' => 'required',
            'ref_po' => 'required|string|min:1|max:145',
            'pic' => 'required|string|min:1|max:145',
            'driver' => 'required|string|min:1|max:145',
            'sales_orders_id' => 'required',
			'created_at' => 'required',
            'no_pol' => 'required|string|min:1|max:145'
        ];
        
        $data = $request->validate($rules);

        return $data;
    }

}
