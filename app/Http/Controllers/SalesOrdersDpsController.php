<?php

namespace App\Http\Controllers;

use App\Models\Auth\User;
use Illuminate\Http\Request;
use App\Models\SalesOrdersDp;
use App\Models\SalesOrdersDetails;
use App\Models\SalesOrders;
use App\Models\Customers;
use App\Models\Invoices;
use App\Models\InvoicesDetails;

use App\Http\Controllers\Controller;
use Exception;

class SalesOrdersDpsController extends Controller
{

    /**
     * Display a listing of the sales orders dps.
     *
     * @return Illuminate\View\View
     */
    public function index(Request $request)
    {
        $customers = Customers::pluck('name','id')->all();
        $searchBy['keyword'] = $request->keyword;
        $searchBy['customer_id'] = $request->customer_id;
        $salesOrdersDps = SalesOrdersDp::select('sales_orders_dps.*')
                ->with('salesorder','user','invoice', 'invoice.invoicesDetails')
                ->leftjoin('sales_orders', 'sales_orders.id', '=', 'sales_orders_dps.sales_orders_id')
                ->where('sales_orders.no','like', "%{$request->keyword}%")
                ->where('sales_orders.customer_id','like', "%{$request->customer_id}%")
                ->orderBy('created_date', 'desc')->paginate(10);
        $salesOrdersDps->appends($request->only('keyword'));
        $salesOrdersDps->appends($request->only('customer_id'));
        return view('backend.sales_orders_dps.index', compact('salesOrdersDps','customers'));
    }

    /**
     * Show the form for creating a new sales orders dp.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        $users = User::pluck('id','id')->all();
        
        $salesOrders = SalesOrders::select('sales_orders.no','sales_orders.id')->leftjoin('sales_orders_dps', 'sales_orders_dps.sales_orders_id', '=', 'sales_orders.id')
                        ->whereNull('sales_orders_dps.sales_orders_id')
                        ->get(); 
      
        return view('backend.sales_orders_dps.create', compact('salesOrders','users'));
    }

    /**
     * Store a new sales orders dp in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
                        
            $invoice = Invoices::where('no', $request['no'])->first();
            
            if ($invoice!=null) {
                return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Invoice no already in the system, please use another one!']);
            }
            
            $request['due_date'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['due_date'])));
            $request['created_date'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['due_date'])));
            $request['created_at'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['created_at'])));
            $request['updated_at'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['created_at'])));
            
            $data_invoice = [
                'no' => $request['no'],
                'ship_via' => $request['ship_via'],
                'purchase_no' => $request['ref_po'],
                'due_date' => $request['due_date'],
                'created_at' => $request['created_at'],
                'updated_at' => $request['created_at'],
                'customers_id' => $request['customers_id'],
                'discount' => $request['discount'],
                'total' => $request['total'],
                'vat' => $request['vat'],
                'grand_total' => $request['grand_total'],
                'sales_persons_id' => $request['sales_persons_id'],
                'sub_total' => $request['sub_total'],
                'users_id' => auth()->user()->id,
                'is_dp' => 1
            ];
            
            $invoice = Invoices::create($data_invoice);
            
            //$items_id = $request['items_id'];
            $quantity = $request['quantity'];
            $price = $request['price'];
            $amount = $request['amount'];
            $description = $request['description'];
            
            foreach( $description as $desc => $n ) {
                $dataDetail = array (
                    'invoices_id' => $invoice->id,
                    'items_id' => '-1',
                    'quantity' => $quantity[$desc],
                    'price' => $price[$desc],
                    'amount' => $amount[$desc],
                    'description' => $description[$desc]
                );
                InvoicesDetails::create($dataDetail);
            }
            
            $request['created_date'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['created_at'])));
            
            $data = $this->getData($request);
            $data['users_id'] = auth()->user()->id;
            $data['created_date'] = $request['created_date'];
            $data['invoices_id'] = $invoice['id'];
            $data['total_dp'] = $request['grand_total'];
            
            $salesOrderDp = SalesOrdersDp::create($data);
            
            return redirect()->route('sales_orders_dps.sales_orders_dp.index')
                             ->with('success_message', 'Sales Orders Dp was successfully added!');

        } catch (Exception $exception) {
            dd($exception);    
            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }
    
    /**
     * Print the form for editing the specified sales orders.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function pdf($id)
    {
        $salesOrdersDp = SalesOrdersDp::with('invoice')->findOrFail($id);
        return redirect()->route('invoices.invoices.print', [$salesOrdersDp->invoices_id]);

        
   //     $salesOrders = SalesOrders::findOrFail($salesOrdersDp->sales_orders_id);
        
   //     $salesOrdersDetails = SalesOrdersDetails::where('sales_orders_id',$salesOrders->id)->get();
        
   //     return view('backend.sales_orders_dps.print', compact('salesOrders','salesOrdersDp','salesOrdersDetails'));
    }

    /**
     * Display the specified sales orders dp.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $salesOrdersDp = SalesOrdersDp::with('salesorder','user')->findOrFail($id);

        return view('backend.sales_orders_dps.show', compact('salesOrdersDp'));
    }

    /**
     * Show the form for editing the specified sales orders dp.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $salesOrdersDp = SalesOrdersDp::with('invoice','invoice.invoicesDetails')->findOrFail($id);
        
        $salesOrders = SalesOrders::select('sales_orders.no','sales_orders.id')->leftjoin('sales_orders_dps', 'sales_orders_dps.sales_orders_id', '=', 'sales_orders.id')
                        ->whereNull('sales_orders_dps.sales_orders_id')
                        ->get(); 
        $users = User::pluck('id','id')->all();
        
        $invoices = Invoices::findOrFail($salesOrdersDp->invoice->id);
        
        $invoicesDetails = InvoicesDetails::where('invoices_id',$invoices->id)->get();
        
        return view('backend.sales_orders_dps.edit', compact('salesOrdersDp','salesOrders','users', 'invoicesDetails'));
    }

    /**
     * Update the specified sales orders dp in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $request['due_date'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['due_date'])));
            $request['created_date'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['due_date'])));
            $request['created_at'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['created_at'])));
            $request['updated_at'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['created_at'])));
            
            $invoices = Invoices::findOrFail($request['invoices_id']);
            
            $data_invoice = [
                'no' => $request['no'],
                'ship_via' => $request['ship_via'],
                'purchase_no' => $request['ref_po'],
                'due_date' => $request['due_date'],
                'created_at' => $request['created_at'],
                'updated_at' => $request['created_at'],
                'customers_id' => $request['customers_id'],
                'discount' => $request['discount'],
                'total' => $request['total'],
                'vat' => $request['vat'],
                'grand_total' => $request['grand_total'],
                'sales_persons_id' => $request['sales_persons_id'],
                'sub_total' => $request['sub_total'],
                'users_id' => auth()->user()->id,
                'is_dp' => 1
            ];
            
            $invoices->update($data_invoice);
            
            $invoicesDetails = InvoicesDetails::where('invoices_id',$request['invoices_id'])->get();
            
            foreach( $invoicesDetails as $item => $n ) {
                $invoicesDetails = InvoicesDetails::findOrFail($n->id);
                $invoicesDetails->delete();
            }
            
            //$items_id = $request['items_id'];
            $quantity = $request['quantity'];
            $price = $request['price'];
            $amount = $request['amount'];
            $description = $request['description'];
                        
            foreach( $description as $desc => $n ) {
                $dataDetail = array (
                    'invoices_id' => $request['invoices_id'],
                    'items_id' => '-1',
                    'quantity' => $quantity[$desc],
                    'price' => $price[$desc],
                    'amount' => $amount[$desc],
                    'description' => $description[$desc]
                );
                
                InvoicesDetails::create($dataDetail);
            }
                        
            $data = $this->getData($request);            
            $data['users_id'] = auth()->user()->id;
            $data['created_date'] = $request['created_date'];
            $data['invoices_id'] = $invoices->id;
            $data['total_dp'] = $request['grand_total'];
            
            $salesOrdersDp = SalesOrdersDp::findOrFail($id);
            $salesOrdersDp->update($data);

            return redirect()->route('sales_orders_dps.sales_orders_dp.index')
                             ->with('success_message', 'Sales Orders Dp was successfully updated!');

        } catch (Exception $exception) {
            dd($exception);
            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }        
    }

    /**
     * Remove the specified sales orders dp from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $salesOrdersDp = SalesOrdersDp::findOrFail($id);
            $salesOrdersDp->delete();

            return redirect()->route('sales_orders_dps.sales_orders_dp.index')
                             ->with('success_message', 'Sales Orders Dp was successfully deleted!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'sales_orders_id' => 'required',
            'created_date' => 'required'
        ];

        
        $data = $request->validate($rules);




        return $data;
    }

}
