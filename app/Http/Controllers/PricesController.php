<?php

namespace App\Http\Controllers;

use App\Models\Items;
use App\Models\Auth\User;
use App\Models\Prices;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;

class PricesController extends Controller
{

    /**
     * Display a listing of the prices.
     *
     * @return Illuminate\View\View
     */
    public function index(Request $request)
    {
        $pricesObjects = Prices::select('prices.*')
                ->with('item','user')
                ->leftjoin('items', 'items.id', '=', 'prices.item_id')
                ->where('items.name','like', "%{$request->keyword}%")
                ->orderBy('updated_at', 'desc')->paginate(10);
        $pricesObjects->appends($request->only('keyword'));            
        return view('backend.prices.index', compact('pricesObjects'));
    }

    /**
     * Show the form for creating a new prices.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        $items = Items::pluck('name','id')->all();
        $users = User::pluck('id','id')->all();
        
        return view('backend.prices.create', compact('items','users'));
    }

    /**
     * Store a new prices in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            $user_id = (auth()->check()) ? auth()->user()->id : null;
            $data['user_id'] =  $user_id;
            Prices::create($data);

            return redirect()->route('prices.prices.index')
                             ->with('success_message', 'Prices was successfully added!');

        } catch (Exception $exception) {
            dd($exception);
            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    /**
     * Display the specified prices.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $prices = Prices::with('item','user')->findOrFail($id);

        return view('backend.prices.show', compact('prices'));
    }

    /**
     * Show the form for editing the specified prices.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $prices = Prices::findOrFail($id);
        $items = Items::pluck('name','id')->all();
        $users = User::pluck('id','id')->all();

        return view('backend.prices.edit', compact('prices','items','users'));
    }

    /**
     * Update the specified prices in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $data = $this->getData($request);
            $user_id = (auth()->check()) ? auth()->user()->id : null;
            $data['user_id'] =  $user_id;
            $prices = Prices::findOrFail($id);
            $prices->update($data);

            return redirect()->route('prices.prices.index')
                             ->with('success_message', 'Prices was successfully updated!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }        
    }

    /**
     * Remove the specified prices from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $prices = Prices::findOrFail($id);
            $prices->delete();

            return redirect()->route('prices.prices.index')
                             ->with('success_message', 'Prices was successfully deleted!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'purchase_price' => 'required|numeric|min:-9',
            'selling_price' => 'required|numeric|min:-9',
            'item_id' => 'required',
     
        ];

        $data = $request->validate($rules);
        return $data;
    }

}
