<?php

namespace App\Http\Controllers;

use App\Models\Auth\User;
use Illuminate\Http\Request;
use App\Models\PurchaseOrders;
use App\Models\Items;
use App\Models\Suppliers;

use App\Models\IncomingInvoices;
use App\Models\IncomingInvoicesDetails;
use App\Http\Controllers\Controller;
use Exception;

class IncomingInvoicesController extends Controller
{

    /**
     * Display a listing of the incoming invoices.
     *
     * @return Illuminate\View\View
     */
    public function index(Request $request)
    {
        $suppliers = Suppliers::pluck('name','id')->all();
        $searchBy['keyword'] = $request->keyword;
        $searchBy['suppliers_id'] = $request->suppliers_id;
        
        $incomingInvoicesObjects = IncomingInvoices::select('incoming_invoices.*')
                ->with('purchaseorder','user')
                ->leftjoin('purchase_orders', 'purchase_orders.id', '=', 'incoming_invoices.purchase_orders_id')
                ->where('no','like', "%{$request->keyword}%")
                ->where('purchase_orders.suppliers_id','like', "%{$request->suppliers_id}%")
                ->orderBy('updated_at', 'desc')->paginate(10);
        $incomingInvoicesObjects->appends($request->only('keyword'));
        $incomingInvoicesObjects->appends($request->only('suppliers_id'));
        return view('backend.incoming_invoices.index', compact('incomingInvoicesObjects','suppliers'));
    }

    /**
     * Show the form for creating a new incoming invoices.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        $purchaseOrders = PurchaseOrders::select('purchase_no','id')
        ->where('status','=','0')->get();
        $users = User::pluck('id','id')->all();
        $items = Items::pluck('name','id')->all();
        return view('backend.incoming_invoices.create', compact('purchaseOrders','users','items'));
    }

    /**
     * Store a new incoming invoices in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
                        
            $request['due_date'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['due_date'])));
            $request['created_at'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['created_at'])));
            
            $items_id = $request['items_id'];
            $quantity = $request['quantity'];
            $price = $request['price'];
            $amount = $request['amount'];
            $purchaseordersdetailsid = $request['purchase_orders_details_id'];             
            
            $data = $this->getData($request);
            
            $data['users_id'] = auth()->user()->id;
            $incomingInvoices = IncomingInvoices::create($data);            
            
            foreach( $items_id as $item => $n ) {
                $dataDetail = array (
                    'incoming_invoices_id' => $incomingInvoices->id,
                    'items_id' => $items_id[$item],
                    'quantity' => $quantity[$item],
                    'price' => $price[$item],
                    'amount' => $amount[$item],
                    'purchase_orders_details_id' => $purchaseordersdetailsid[$item]
                );
                IncomingInvoicesDetails::create($dataDetail);
            }
            
            return redirect()->route('incoming_invoices.incoming_invoices.index')
                             ->with('success_message', 'Incoming Invoices was successfully added!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    /**
     * Display the specified incoming invoices.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $incomingInvoices = IncomingInvoices::with('purchaseorder','user')->findOrFail($id);

        return view('backend.incoming_invoices.show', compact('incomingInvoices'));
    }

    /**
     * Show the form for editing the specified incoming invoices.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $incomingInvoices = IncomingInvoices::findOrFail($id);
        $incomingInvoicesDetails = IncomingInvoicesDetails::where('incoming_invoices_id',$id)->get();
        $purchaseOrders = PurchaseOrders::select('purchase_no','id')->get();             
        $users = User::pluck('id','id')->all();
        $items = Items::pluck('name','id')->all();
        return view('backend.incoming_invoices.edit', compact('incomingInvoices','incomingInvoicesDetails','purchaseOrders','users','items'));
    }

    /**
     * Update the specified incoming invoices in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            $request['due_date'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['due_date'])));
            $request['created_at'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['created_at'])));
            
            $data = $this->getData($request);
            $data['due_date'] = $request['due_date'];
            $incomingInvoices = IncomingInvoices::findOrFail($id);
            $incomingInvoices->update($data);
                        
            $items_id = $request['items_id'];
            $quantity = $request['quantity'];
            $price = $request['price'];
            $amount = $request['amount'];
            $purchaseordersdetailsid = $request['purchase_orders_details_id'];             
            
            $incomingInvoicesDetails = IncomingInvoicesDetails::where('incoming_invoices_id',$id)->get();
            
            foreach( $incomingInvoicesDetails as $item => $n ) {
                $oldIncomingInvoicesDetails = IncomingInvoicesDetails::findOrFail($n->id);
                $oldIncomingInvoicesDetails->delete();
            }
                        
            foreach( $items_id as $item => $n ) {
                $dataDetail = array (
                    'incoming_invoices_id' => $incomingInvoices->id,
                    'items_id' => $items_id[$item],
                    'quantity' => $quantity[$item],
                    'price' => $price[$item],
                    'amount' => $amount[$item],
                    'purchase_orders_details_id' => $purchaseordersdetailsid[$item]
                );
                IncomingInvoicesDetails::create($dataDetail);
            }

            return redirect()->route('incoming_invoices.incoming_invoices.index')
                             ->with('success_message', 'Incoming Invoices was successfully updated!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }        
    }

    /**
     * Remove the specified incoming invoices from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $incomingInvoices = IncomingInvoices::findOrFail($id);
            $incomingInvoices->delete();

            return redirect()->route('incoming_invoices.incoming_invoices.index')
                             ->with('success_message', 'Incoming Invoices was successfully deleted!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'no' => 'required|string|min:1|max:45',
            'ship_via' => 'nullable|string|min:0|max:145',
            'purchase_orders_id' => 'required',
            'due_date' => 'required|string|min:1',
            'created_at' => 'required|string|min:1',
            'sub_total' => 'required|numeric',
            'discount' => 'required|numeric',
            'total' => 'required|numeric',
            'vat' => 'required|numeric',
            'grand_total' => 'required|numeric'
     
        ];

        
        $data = $request->validate($rules);




        return $data;
    }

}
