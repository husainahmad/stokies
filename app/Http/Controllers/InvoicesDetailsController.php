<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\Invoice;
use Illuminate\Http\Request;
use App\Models\InvoicesDetails;
use App\Http\Controllers\Controller;
use Exception;

class InvoicesDetailsController extends Controller
{

    /**
     * Display a listing of the invoices details.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $invoicesDetailsObjects = InvoicesDetails::with('invoice','item')->paginate(25);

        return view('invoices_details.index', compact('invoicesDetailsObjects'));
    }

    /**
     * Show the form for creating a new invoices details.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        $invoices = Invoice::pluck('no','id')->all();
        $items = Item::pluck('name','id')->all();
        
        return view('invoices_details.create', compact('invoices','items'));
    }

    /**
     * Store a new invoices details in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            InvoicesDetails::create($data);

            return redirect()->route('invoices_details.invoices_details.index')
                             ->with('success_message', 'Invoices Details was successfully added!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    /**
     * Display the specified invoices details.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $invoicesDetails = InvoicesDetails::with('invoice','item')->findOrFail($id);

        return view('invoices_details.show', compact('invoicesDetails'));
    }

    /**
     * Show the form for editing the specified invoices details.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $invoicesDetails = InvoicesDetails::findOrFail($id);
        $invoices = Invoice::pluck('no','id')->all();
$items = Item::pluck('name','id')->all();

        return view('invoices_details.edit', compact('invoicesDetails','invoices','items'));
    }

    /**
     * Update the specified invoices details in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            $invoicesDetails = InvoicesDetails::findOrFail($id);
            $invoicesDetails->update($data);

            return redirect()->route('invoices_details.invoices_details.index')
                             ->with('success_message', 'Invoices Details was successfully updated!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }        
    }

    /**
     * Remove the specified invoices details from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $invoicesDetails = InvoicesDetails::findOrFail($id);
            $invoicesDetails->delete();

            return redirect()->route('invoices_details.invoices_details.index')
                             ->with('success_message', 'Invoices Details was successfully deleted!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'invoices_id' => 'required',
            'items_id' => 'required',
            'quantity' => 'required|numeric|min:-2147483648|max:2147483647',
            'price' => 'required|numeric|min:-9|max:9',
            'amount' => 'required|numeric|min:-9|max:9',
     
        ];

        
        $data = $request->validate($rules);




        return $data;
    }

}
