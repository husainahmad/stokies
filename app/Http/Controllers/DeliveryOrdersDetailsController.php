<?php

namespace App\Http\Controllers;

use App\Models\Item;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DeliveryOrders;
use App\Models\DeliveryOrdersDetails;
use App\Models\SalesOrdersDp;
use Exception;

class DeliveryOrdersDetailsController extends Controller
{

    /**
     * Display a listing of the delivery orders details.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $deliveryOrdersDetailsObjects = DeliveryOrdersDetails::with('deliveryordersdetail','item')->paginate(25);

        return view('delivery_orders_details.index', compact('deliveryOrdersDetailsObjects'));
    }

    /**
     * Show the form for creating a new delivery orders details.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        $deliveryOrdersDetails = DeliveryOrdersDetail::pluck('id','id')->all();
        $items = Item::pluck('name','id')->all();
        
        return view('delivery_orders_details.create', compact('deliveryOrdersDetails','items'));
    }

    /**
     * Store a new delivery orders details in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            DeliveryOrdersDetails::create($data);

            return redirect()->route('delivery_orders_details.delivery_orders_details.index')
                             ->with('success_message', 'Delivery Orders Details was successfully added!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }
    
    /**
     * Show the form for json the specified sales orders details.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
            
    public function json($id)
    {
        $deliveryOrders = DeliveryOrders::with('customer','user','salesOrders', 'salesOrders.termBilling')->findOrFail($id);
        
        $deliveryOrders['sales_orders_dps'] = SalesOrdersDp::where('sales_orders_id', $deliveryOrders->sales_orders_id)->get();
        
        $deliveryOrdersDetails = DeliveryOrdersDetails::where('delivery_orders_id', $id)->get();
                
        foreach ($deliveryOrdersDetails as $key => $value) {
            $deliveryOrdersDetails[$key] = $value;
            $deliveryOrdersDetails[$key]['item'] = $value->item;
            $deliveryOrdersDetails[$key]['unit'] = $value->item->unit;
            $deliveryOrdersDetails[$key]['sales_orders_details'] = $value->salesOrdersDetails;            
        }              
        
        $deliveryOrders['deliveryOrdersDetails'] = $deliveryOrdersDetails;
        return response()->json($deliveryOrders);
    }        

    /**
     * Display the specified delivery orders details.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $deliveryOrdersDetails = DeliveryOrdersDetails::with('deliveryordersdetail','item')->findOrFail($id);

        return view('delivery_orders_details.show', compact('deliveryOrdersDetails'));
    }

    /**
     * Show the form for editing the specified delivery orders details.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $deliveryOrdersDetails = DeliveryOrdersDetails::findOrFail($id);
        $deliveryOrdersDetails = DeliveryOrdersDetail::pluck('id','id')->all();
        $items = Item::pluck('name','id')->all();

        return view('delivery_orders_details.edit', compact('deliveryOrdersDetails','deliveryOrdersDetails','items'));
    }

    /**
     * Update the specified delivery orders details in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            $deliveryOrdersDetails = DeliveryOrdersDetails::findOrFail($id);
            $deliveryOrdersDetails->update($data);

            return redirect()->route('delivery_orders_details.delivery_orders_details.index')
                             ->with('success_message', 'Delivery Orders Details was successfully updated!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }        
    }

    /**
     * Remove the specified delivery orders details from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $deliveryOrdersDetails = DeliveryOrdersDetails::findOrFail($id);
            $deliveryOrdersDetails->delete();

            return redirect()->route('delivery_orders_details.delivery_orders_details.index')
                             ->with('success_message', 'Delivery Orders Details was successfully deleted!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'delivery_orders_id' => 'required',
            'items_id' => 'required',
            'quantity' => 'required|numeric|min:-2147483648|max:2147483647',
            'description' => 'nullable|string|min:0|max:255',
     
        ];

        
        $data = $request->validate($rules);




        return $data;
    }

}
