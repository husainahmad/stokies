<?php

namespace App\Http\Controllers;

use App\Models\Auth\User;
use App\Models\Units;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;

class UnitsController extends Controller
{

    /**
     * Display a listing of the units.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $unitsObjects = Units::with('user')->orderBy('updated_at', 'desc')->paginate(10);

        return view('backend.units.index', compact('unitsObjects'));
    }

    /**
     * Show the form for creating a new units.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        $users = User::pluck('id','id')->all();
        
        return view('backend.units.create', compact('users'));
    }

    /**
     * Store a new units in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            $user_id = (auth()->check()) ? auth()->user()->id : null;
            $data['user_id'] =  $user_id;
            Units::create($data);

            return redirect()->route('units.units.index')
                             ->with('success_message', 'Units was successfully added!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    /**
     * Display the specified units.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $units = Units::with('user')->findOrFail($id);

        return view('backend.units.show', compact('units'));
    }

    /**
     * Show the form for editing the specified units.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $units = Units::findOrFail($id);
        $users = User::pluck('id','id')->all();

        return view('backend.units.edit', compact('units','users'));
    }

    /**
     * Update the specified units in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            $units = Units::findOrFail($id);
            $user_id = (auth()->check()) ? auth()->user()->id : null;
            $data['user_id'] =  $user_id;
            $units->update($data);

            return redirect()->route('units.units.index')
                             ->with('success_message', 'Units was successfully updated!');

        } catch (Exception $exception) {
            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }        
    }

    /**
     * Remove the specified units from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $units = Units::findOrFail($id);
            $units->delete();

            return redirect()->route('units.units.index')
                             ->with('success_message', 'Units was successfully deleted!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'name' => 'required|string|min:1|max:145',
            'description' => 'nullable|string|min:0|max:255',
        ];

        
        $data = $request->validate($rules);




        return $data;
    }

}
