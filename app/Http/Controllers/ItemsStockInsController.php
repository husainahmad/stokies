<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\User;
use App\Models\Supplier;
use App\Models\ItemsStockIn;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;

class ItemsStockInsController extends Controller
{

    /**
     * Display a listing of the items stock ins.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $itemsStockIns = ItemsStockIn::with('item','supplier','user')->paginate(25);

        return view('items_stock_ins.index', compact('itemsStockIns'));
    }

    /**
     * Show the form for creating a new items stock in.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        $items = Item::pluck('name','id')->all();
$suppliers = Supplier::pluck('name','id')->all();
$users = User::pluck('id','id')->all();
        
        return view('items_stock_ins.create', compact('items','suppliers','users'));
    }

    /**
     * Store a new items stock in in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            ItemsStockIn::create($data);

            return redirect()->route('items_stock_ins.items_stock_in.index')
                             ->with('success_message', 'Items Stock In was successfully added!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    /**
     * Display the specified items stock in.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $itemsStockIn = ItemsStockIn::with('item','supplier','user')->findOrFail($id);

        return view('items_stock_ins.show', compact('itemsStockIn'));
    }

    /**
     * Show the form for editing the specified items stock in.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $itemsStockIn = ItemsStockIn::findOrFail($id);
        $items = Item::pluck('name','id')->all();
$suppliers = Supplier::pluck('name','id')->all();
$users = User::pluck('id','id')->all();

        return view('items_stock_ins.edit', compact('itemsStockIn','items','suppliers','users'));
    }

    /**
     * Update the specified items stock in in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            $itemsStockIn = ItemsStockIn::findOrFail($id);
            $itemsStockIn->update($data);

            return redirect()->route('items_stock_ins.items_stock_in.index')
                             ->with('success_message', 'Items Stock In was successfully updated!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }        
    }

    /**
     * Remove the specified items stock in from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $itemsStockIn = ItemsStockIn::findOrFail($id);
            $itemsStockIn->delete();

            return redirect()->route('items_stock_ins.items_stock_in.index')
                             ->with('success_message', 'Items Stock In was successfully deleted!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'items_id' => 'required',
            'volume' => 'required|numeric|min:-2147483648|max:2147483647',
            'suppliers_id' => 'required',
            'price' => 'required|numeric|min:-9|max:9',
            'amount_price' => 'required|numeric|min:-9|max:9',
            'users_id' => 'required',
            'reference_no' => 'required|numeric|min:-2147483648|max:2147483647',
     
        ];

        
        $data = $request->validate($rules);




        return $data;
    }

}
