<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\PurchaseOrder;
use App\Http\Controllers\Controller;
use App\Models\PurchaseOrdersDetails;
use Exception;

class PurchaseOrdersDetailsController extends Controller
{

    /**
     * Display a listing of the purchase orders details.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $purchaseOrdersDetailsObjects = PurchaseOrdersDetails::with('purchaseorder','item','user')->paginate(25);

        return view('purchase_orders_details.index', compact('purchaseOrdersDetailsObjects'));
    }

    /**
     * Show the form for creating a new purchase orders details.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        $purchaseOrders = PurchaseOrder::pluck('created_at','id')->all();
        $items = Item::pluck('name','id')->all();
        $users = User::pluck('id','id')->all();
        
        return view('purchase_orders_details.create', compact('purchaseOrders','items','users'));
    }

    /**
     * Store a new purchase orders details in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            PurchaseOrdersDetails::create($data);

            return redirect()->route('purchase_orders_details.purchase_orders_details.index')
                             ->with('success_message', 'Purchase Orders Details was successfully added!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    /**
     * Display the specified purchase orders details.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $purchaseOrdersDetails = PurchaseOrdersDetails::with('purchaseorder','item','user')->findOrFail($id);

        return view('purchase_orders_details.show', compact('purchaseOrdersDetails'));
    }

    /**
     * Show the form for editing the specified purchase orders details.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $purchaseOrdersDetails = PurchaseOrdersDetails::findOrFail($id);
        $purchaseOrders = PurchaseOrder::pluck('created_at','id')->all();
        $items = Item::pluck('name','id')->all();
        $users = User::pluck('id','id')->all();

        return view('purchase_orders_details.edit', compact('purchaseOrdersDetails','purchaseOrders','items','users'));
    }

    /**
     * Update the specified purchase orders details in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            $purchaseOrdersDetails = PurchaseOrdersDetails::findOrFail($id);
            $purchaseOrdersDetails->update($data);

            return redirect()->route('purchase_orders_details.purchase_orders_details.index')
                             ->with('success_message', 'Purchase Orders Details was successfully updated!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }        
    }

    /**
     * Remove the specified purchase orders details from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $purchaseOrdersDetails = PurchaseOrdersDetails::findOrFail($id);
            $purchaseOrdersDetails->delete();

            return redirect()->route('purchase_orders_details.purchase_orders_details.index')
                             ->with('success_message', 'Purchase Orders Details was successfully deleted!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'purchase_orders_id' => 'required',
            'items_id' => 'required',
            'quantity' => 'required|numeric|min:-2147483648|max:2147483647',
            'price' => 'required|numeric|min:-9|max:9',
            'amount' => 'required|numeric|min:-9|max:9',
            'users_id' => 'required',
     
        ];

        
        $data = $request->validate($rules);




        return $data;
    }

}
