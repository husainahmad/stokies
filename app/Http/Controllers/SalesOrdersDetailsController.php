<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\SalesOrder;
use Illuminate\Http\Request;
use App\Models\SalesOrders;
use App\Models\SalesOrdersDetails;
use App\Models\SalesOrdersDp;
use App\Models\DeliveryOrdersDetails;
use App\Http\Controllers\Controller;
use Exception;

class SalesOrdersDetailsController extends Controller
{

    /**
     * Display a listing of the sales orders details.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $salesOrdersDetailsObjects = SalesOrdersDetails::with('salesorder','item')->paginate(25);

        return view('sales_orders_details.index', compact('salesOrdersDetailsObjects'));
    }

    /**
     * Show the form for creating a new sales orders details.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        $salesOrders = SalesOrder::pluck('no','id')->all();
        $items = Item::pluck('name','id')->all();
        
        return view('sales_orders_details.create', compact('salesOrders','items'));
    }

    /**
     * Store a new sales orders details in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            SalesOrdersDetails::create($data);

            return redirect()->route('sales_orders_details.sales_orders_details.index')
                             ->with('success_message', 'Sales Orders Details was successfully added!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    /**
     * Display the specified sales orders details.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $salesOrdersDetails = SalesOrdersDetails::with('salesorder','item')->findOrFail($id);

        return view('sales_orders_details.show', compact('salesOrdersDetails'));
    }

    /**
     * Show the form for editing the specified sales orders details.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $salesOrdersDetails = SalesOrdersDetails::findOrFail($id);
        $salesOrders = SalesOrder::pluck('no','id')->all();
        $items = Item::pluck('name','id')->all();

        return view('sales_orders_details.edit', compact('salesOrdersDetails','salesOrders','items'));
    }
    
    /**
     * Show the form for json the specified sales orders details.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
            
    public function json($id)
    {        
        $salesOrders = SalesOrders::with('salesperson','customer','termbilling','user')->findOrFail($id);
        $salesOrdersDetails = SalesOrdersDetails::where('sales_orders_id', $id)
                            ->get();        
        
        $salesOrders['sales_orders_dps'] = SalesOrdersDp::where('sales_orders_id', $id)->get();
        
        $sodIds = array();
        
        foreach ($salesOrdersDetails as $key => $value) {            
            array_push($sodIds, $value->id);
        }
        
        $deliveryOrdersDetails = DeliveryOrdersDetails::whereIn('sales_orders_details_id', $sodIds)->get();                        
        
        foreach ($salesOrdersDetails as $key => $value) {            
            $found = false;
            $total = 0;
            foreach ($deliveryOrdersDetails as $k => $v) {
                if ($v->sales_orders_details_id == $value->id) {
                    $found = true;
                    $total = $total + $v->quantity;
                    $salesOrdersDetail[$key]['deliveryOrdersDetails'] = $v;
                }
            }            
            $salesOrdersDetail[$key] = $value;
            $salesOrdersDetail[$key]['item'] = $value->item;
            $salesOrdersDetail[$key]['unit'] = $value->item->unit;
            $salesOrdersDetail[$key]['checked'] = $found;
            if ($found) {
                $salesOrdersDetail[$key]['total_left'] = $salesOrdersDetail[$key]['quantity'] - $total;
            }            
        }
                
        $data['salesOrders'] = $salesOrders;
        $data['salesOrdersDetails'] = $salesOrdersDetails;
        
        return response()->json($data);
    }        

    /**
     * Update the specified sales orders details in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            $salesOrdersDetails = SalesOrdersDetails::findOrFail($id);
            $salesOrdersDetails->update($data);

            return redirect()->route('sales_orders_details.sales_orders_details.index')
                             ->with('success_message', 'Sales Orders Details was successfully updated!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }        
    }

    /**
     * Remove the specified sales orders details from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $salesOrdersDetails = SalesOrdersDetails::findOrFail($id);
            $salesOrdersDetails->delete();

            return redirect()->route('sales_orders_details.sales_orders_details.index')
                             ->with('success_message', 'Sales Orders Details was successfully deleted!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'sales_orders_id' => 'required',
            'items_id' => 'required',
            'quantity' => 'required|numeric|min:-2147483648|max:2147483647',
            'price' => 'required|numeric|min:-9|max:9',
     
        ];

        
        $data = $request->validate($rules);




        return $data;
    }

}
