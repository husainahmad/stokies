<?php

namespace App\Http\Controllers;

use App\Models\Auth\User;
use App\Models\SalesPersons;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;

class SalesPersonsController extends Controller
{

    /**
     * Display a listing of the sales persons.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $salesPersonsObjects = SalesPersons::with('user')->orderBy('updated_at', 'desc')->paginate(25);

        return view('backend.sales_persons.index', compact('salesPersonsObjects'));
    }

    /**
     * Show the form for creating a new sales persons.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        $users = User::pluck('id','id')->all();
        
        return view('backend.sales_persons.create', compact('users'));
    }

    /**
     * Store a new sales persons in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            $user_id = (auth()->check()) ? auth()->user()->id : null;
            $data['user_id'] =  $user_id;
            SalesPersons::create($data);

            return redirect()->route('sales_persons.sales_persons.index')
                             ->with('success_message', 'Sales Persons was successfully added!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    /**
     * Display the specified sales persons.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $salesPersons = SalesPersons::with('user')->findOrFail($id);

        return view('backend.sales_persons.show', compact('salesPersons'));
    }

    /**
     * Show the form for editing the specified sales persons.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $salesPersons = SalesPersons::findOrFail($id);
        $users = User::pluck('id','id')->all();

        return view('backend.sales_persons.edit', compact('salesPersons','users'));
    }

    /**
     * Update the specified sales persons in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $data = $this->getData($request);
            $user_id = (auth()->check()) ? auth()->user()->id : null;
            $data['user_id'] =  $user_id;
            $salesPersons = SalesPersons::findOrFail($id);
            $salesPersons->update($data);

            return redirect()->route('sales_persons.sales_persons.index')
                             ->with('success_message', 'Sales Persons was successfully updated!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }        
    }

    /**
     * Remove the specified sales persons from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $salesPersons = SalesPersons::findOrFail($id);
            $salesPersons->delete();

            return redirect()->route('sales_persons.sales_persons.index')
                             ->with('success_message', 'Sales Persons was successfully deleted!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'name' => 'required|string|min:1|max:45',
            'address' => 'required|string|min:1|max:255',
            'no_rek' => 'nullable|string|min:0|max:45',
            'phone' => 'nullable|string|min:0|max:45',
            'mobile' => 'nullable|string|min:0|max:45',
        ];

        
        $data = $request->validate($rules);




        return $data;
    }

}
