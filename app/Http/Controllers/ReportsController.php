<?php

namespace App\Http\Controllers;

use App\Models\Auth\User;
use App\Models\Customers;
use App\Models\Items;
use App\Models\Categories;
use App\Models\SalesOrders;
use App\Models\SalesOrdersDp;
use App\Models\SalesOrdersDetails;
use App\Models\SalesPersons;
use App\Models\DeliveryOrdersDetails;
use App\Models\DeliveryOrders;
use App\Models\InvoiceDeliveryOrders;
use App\Models\Invoices;
use App\Models\InvoicesDetails;
use App\Models\PurchaseOrders;
use App\Models\PurchaseOrdersDetails;
use App\Models\Suppliers;
use App\Models\TermBillings;

use App\Models\IncomingInvoices;
use App\Models\IncomingInvoicesDetails;

use App\Exports\SalesOrderExport;
use App\Exports\PurchaseOrderExport;
use App\Exports\DeliveryOrderExport;
use App\Exports\InvoiceExport;

use Maatwebsite\Excel\Facades\Excel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;
use DB;

class ReportsController extends Controller
{

    /**
     * Display a listing of the sales orders.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $salesOrdersObjects = SalesOrders::with('salesperson','customer','termbilling','user')->orderBy('updated_at', 'desc')->paginate(25);

        return view('backend.reports.index', compact('salesOrdersObjects'));
    }
    
    public function salesorder()
    {
        $salesPeople = SalesPersons::pluck('name','id')->all();
        $customers = Customers::pluck('name','id')->all();
        $items = Items::pluck('name','id')->all();
        $categories = Categories::pluck('name','id')->all();
        
        return view('backend.reports.salesorder', compact('salesPeople','customers','salesOrdersObjects','items','categories'));
    }
    
    /**
     * Store a new sales orders in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function salesorderresult(Request $request)
    {
        try {
            $request['date_period'] = $request['from_date']." - ".$request['to_date'];
            $request['from_date'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['from_date'])));
            $request['to_date'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['to_date'])));                                       
            
            $salesOrdersRequest = $request;
            
            DB::enableQueryLog();
                
            $matchThese = [
                        ['no', 'like', '%'.$request['no'].'%']
                    ];
            
            $salesOrdersRequest['customer_name'] = "All";
            
            if (!empty($request['customers_id'])) {
                $cIds = ['customer_id', '=', ''.$request['customers_id'].''];
                array_push($matchThese, $cIds);
                
                $customer = Customers::find($request['customers_id']);
                
                if ($customer!=null) {
                    $salesOrdersRequest['customer_name'] = $customer->name;
                }

            }
            
            $itemses = Items::where('category_id',$request['category_id'])->get();            
            $salesOrdersRequest['items'] = $itemses;            
            
            $category = Categories::find($request['category_id']);
            
            if ($category==null) {
                $salesOrdersRequest['category_name'] = "All";
            } else {
                $salesOrdersRequest['category_name'] = $category->name;
            }
                        
            if (!empty($itemses) && count($itemses)>0) {
                
                $itemids = array();
                foreach( $itemses as $item) { 
                    array_push($itemids, $item->id);
                }
                $salesOrdersObjects = SalesOrders::where($matchThese)                                                
                                              ->whereBetween('sales_order_date',[$request['from_date'], $request['to_date']])
                                              ->whereHas('salesOrdersDetails', function($query) use ($itemids) {
                                                  $query->whereIn('items_id', $itemids);
                                              })
                                              ->with('salesperson','customer','termbilling','user','salesOrdersDetails','salesOrdersDetails.item.unit')->orderBy('no', 'asc')->get();
                //dd(DB::getQueryLog());                                
            } else {
                $salesOrdersObjects = SalesOrders::where($matchThese)                                                
                                              ->whereBetween('sales_order_date',[$request['from_date'], $request['to_date']])
                                              ->with('salesperson','customer','termbilling','user','salesOrdersDetails','salesOrdersDetails.item.unit')->orderBy('no', 'asc')->get();
                //dd(DB::getQueryLog());    
            }    
                        
            //dd(DB::getQueryLog());
            
            if ($request['action']=='Export XLS') {
                $export = new SalesOrderExport($salesOrdersObjects);
                return Excel::download($export, 'salesOrder.xlsx');
            } else if ($request['action']=='print') {                
                return view('backend.reports.salesorderprint', compact('salesPeople','customers','salesOrdersObjects','salesOrdersRequest','items','categories'));                                               
            } else {
                $salesPeople = SalesPersons::pluck('name','id')->all();
                $customers = Customers::pluck('name','id')->all();
                $items = Items::pluck('name','id')->all();
                $categories = Categories::pluck('name','id')->all();
        
                return view('backend.reports.salesorder', compact('salesPeople','customers','salesOrdersObjects','salesOrdersRequest','items','categories'));    
            }            

        } catch (Exception $exception) {
            dd($exception);
            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }
    
    public function purchaseorder()
    {
        $suppliers = Suppliers::pluck('name','id')->all();
        $items = Items::pluck('name','id')->all();
        $categories = Categories::pluck('name','id')->all();
        return view('backend.reports.purchaseorder', compact('suppliers','items','categories'));
    }
    
    /**
     * Store a new sales orders in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function purchaseorderresult(Request $request)
    {
        try {
            $request['date_period'] = $request['from_date']." - ".$request['to_date'];
            $request['from_date'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['from_date'])));
            $request['to_date'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['to_date'])));              
            
            $purchaseOrdersRequest = $request;
            
            DB::enableQueryLog();
                
            $matchThese = [
                        ['purchase_no', 'like', '%'.$request['purchase_no'].'%']
                    ];
            
            $purchaseOrdersRequest['supplier_name'] = "All";
            
            if (!empty($request['suppliers_id'])) {
                $cIds = ['suppliers_id', '=', ''.$request['suppliers_id'].''];
                array_push($matchThese, $cIds);
                
                $suppliers = Suppliers::find($request['suppliers_id']);
                if ($suppliers!=null) {
                    $purchaseOrdersRequest['supplier_name'] = $suppliers->name;
                }                
            }                         
            
            $itemses = Items::where('category_id',$request['category_id'])->get();
            
            $purchaseOrdersRequest['items'] = $itemses;
            
            $category = Categories::find($request['category_id']);
            
            if ($category==null) {
                $purchaseOrdersRequest['category_name'] = "All";
            } else {
                $purchaseOrdersRequest['category_name'] = $category->name;
            }
            
            if (!empty($itemses) && count($itemses)>0) {
                
                $itemids = array();
                foreach( $itemses as $item) { 
                    array_push($itemids, $item->id);
                }
                
                $purchaseOrdersObjects = PurchaseOrders::where($matchThese)
                                                  ->whereHas('purchaseOrdersDetails', function($query) use ($itemids) {
                                                        $query->whereIn('items_id', $itemids);
                                                  })
                                                  ->whereBetween('ordered_at',[$request['from_date'], $request['to_date']])
                                                  ->with('supplier','purchaseOrdersDetails','purchaseOrdersDetails.item.unit')->orderBy('purchase_no')->get();                                
            } else {                    
                $purchaseOrdersObjects = PurchaseOrders::where($matchThese)
                                                  ->whereBetween('ordered_at',[$request['from_date'], $request['to_date']])
                                                  ->with('supplier','purchaseOrdersDetails.item.unit')->orderBy('purchase_no')->get();
                //dd(DB::getQueryLog());
            }            
            
            
            
            $suppliers = Suppliers::pluck('name','id')->all();
            $items = Items::pluck('name','id')->all();
            $categories = Categories::pluck('name','id')->all();
            
            if ($request['action']=='Export XLS') {
                $export = new PurchaseOrderExport($purchaseOrdersObjects);
                return Excel::download($export, 'purhcaseOrder.xlsx');
            } else if ($request['action']=='print') {
                return view('backend.reports.purchaseorderprint', compact('suppliers','items','purchaseOrdersObjects','purchaseOrdersRequest','categories'));                                
            } else {
                return view('backend.reports.purchaseorder', compact('suppliers','items','purchaseOrdersObjects','purchaseOrdersRequest','categories'));
            }            
            
        } catch (Exception $exception) {
            dd($exception);
            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }
       
    public function deliveryorder()
    {
        $salesPeople = SalesPersons::pluck('name','id')->all();
        $customers = Customers::pluck('name','id')->all();
        $items = Items::pluck('name','id')->all();
        $categories = Categories::pluck('name','id')->all();
        return view('backend.reports.deliveryorder', compact('salesPeople','customers','deliveryOrdersObjects','items','categories'));
    }      
    
    /**
     * Store a new sales orders in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function deliveryorderresult(Request $request)
    {
        try {
            $request['date_period'] = $request['from_date']." - ".$request['to_date'];
            $request['from_date'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['from_date'])));
            $request['to_date'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['to_date'])));              
            
            $deliveryOrdersRequest = $request;
                
            $matchThese = [
                        ['no', 'like', '%'.$request['no'].'%']
                    ];
            
            $deliveryOrdersRequest['customer_name'] = "All";
            
            DB::enableQueryLog();
                
            if (!empty($request['customers_id'])) {
                $cIds = ['customers_id', '=', ''.$request['customers_id'].''];
                array_push($matchThese, $cIds);
                
                $customer = Customers::find($request['customers_id']);
                if ($customer!=null) {                    
                    $deliveryOrdersRequest['customer_name'] = $customer->name;
                }
            }
            
            $itemses = Items::where('category_id',$request['category_id'])->get();
            
            $deliveryOrdersRequest['items'] = $itemses;
            
            $category = Categories::find($request['category_id']);
            if ($category==null) {
                $deliveryOrdersRequest['category_name'] = "All";
            } else {
                $deliveryOrdersRequest['category_name'] = $category->name;
            }
            
            if (!empty($itemses) && count($itemses)>0) {
                
                $itemids = array();
                foreach( $itemses as $item) { 
                    array_push($itemids, $item->id);
                }
                
                $deliveryOrdersObjects = DeliveryOrders::where($matchThese)
                                                  ->whereHas('deliveryOrdersDetails', function($query) use ($itemids) {
                                                        $query->whereIn('items_id', $itemids);
                                                  })
                                                  ->whereBetween('updated_at',[$request['from_date'], $request['to_date']])
                                                  ->with('customer','salesOrders','deliveryOrdersDetails','deliveryOrdersDetails.item.unit')->orderBy('no')->get();
                                
            } else {
                $deliveryOrdersObjects = DeliveryOrders::where($matchThese)
                                                  ->whereBetween('updated_at',[$request['from_date'], $request['to_date']])
                                                  ->with('customer','salesOrders','deliveryOrdersDetails','deliveryOrdersDetails.item.unit')->orderBy('no')->get();
            }       
            
            //dd(DB::getQueryLog());
            
            $customers = Customers::pluck('name','id')->all();
            $items = Items::pluck('name','id')->all();
            $categories = Categories::pluck('name','id')->all();
            
            if ($request['action']=='Export XLS') {
                $export = new DeliveryOrderExport($deliveryOrdersObjects);
                return Excel::download($export, 'deliveryOrder.xlsx');
            } else if ($request['action']=='print') {
                return view('backend.reports.deliveryorderprint', compact('customers','items','deliveryOrdersObjects','deliveryOrdersRequest','categories'));                
            } else {
                return view('backend.reports.deliveryorder', compact('customers','items','deliveryOrdersObjects','deliveryOrdersRequest','categories'));
            }            
                        
        } catch (Exception $exception) {
            dd($exception);
            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }
    
    public function invoice()
    {
        $salesPeople = SalesPersons::pluck('name','id')->all();
        $customers = Customers::pluck('name','id')->all();
        $items = Items::pluck('name','id')->all();
        $categories = Categories::pluck('name','id')->all();
        return view('backend.reports.invoice', compact('salesPeople','customers','invoiceObjects','items','categories'));
    }      
    
    /**
     * Store a new sales orders in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function invoiceresult(Request $request)
    {
        try {
            $request['date_period'] = $request['from_date']." - ".$request['to_date'];
            $request['from_date'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['from_date'])));
            $request['to_date'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['to_date'])));              
                
            $invoiceRequest = $request;
                
            $matchThese = [
                        ['no', 'like', '%'.$request['no'].'%']
                    ];
            
            $invoiceRequest['customer_name'] = "All";
            
            DB::enableQueryLog();
            
            if (!empty($request['customers_id'])) {
                $cIds = ['customers_id', '=', ''.$request['customers_id'].''];
                array_push($matchThese, $cIds);
                
                $customer = Customers::find($request['customers_id']);
                if ($customer!=null) {                    
                    $invoiceRequest['customer_name'] = $customer->name;
                }
            }            
                        
            $itemses = Items::where('category_id',$request['category_id'])->get();
            
            $invoiceRequest['items'] = $itemses;
            
            $category = Categories::find($request['category_id']);
            if ($category==null) {
                $invoiceRequest['category_name'] = "All";
            } else {
                $invoiceRequest['category_name'] = $category->name;
            }
            if (!empty($itemses) && count($itemses)>0) {
                
                $itemids = array();
                foreach( $itemses as $item) { 
                    array_push($itemids, $item->id);
                }
                                
                $invoiceObjects = Invoices::where($matchThese)
                                              ->whereHas('invoicesDetails', function($query) use ($itemids) {
                                                        $query->whereIn('items_id', $itemids);
                                                  })                  
                                              ->whereBetween('created_at',[$request['from_date'], $request['to_date']])
                                              ->with('customer','deliveryOrder','deliveryOrder.salesOrders','invoicesDetails')->orderBy('no')->get();                
            } else {
                $invoiceObjects = Invoices::where($matchThese)
                                                  ->whereBetween('created_at',[$request['from_date'], $request['to_date']])
                                                  ->with('customer','deliveryOrder','deliveryOrder.salesOrders','invoicesDetails')->orderBy('no')->get(); 
            }           
                                    
            //dd(DB::getQueryLog());
            
            $customers = Customers::pluck('name','id')->all();
            $items = Items::pluck('name','id')->all();
            $categories = Categories::pluck('name','id')->all();            
            
            if ($request['action']=='Export XLS') {
                $export = new DeliveryOrderExport($deliveryOrdersObjects);
                return Excel::download($export, 'invoice.xlsx');
            } else if ($request['action']=='print') {
                return view('backend.reports.invoiceprint', compact('customers','items','invoiceObjects','invoiceRequest','categories'));
            } else {
                return view('backend.reports.invoice', compact('customers','items','invoiceObjects','invoiceRequest','categories'));
            }                        
            

        } catch (Exception $exception) {
            dd($exception);
            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }
    
    public function outstanding()
    {
        $salesPeople = SalesPersons::pluck('name','id')->all();
        $customers = Customers::pluck('name','id')->all();
        $items = Items::pluck('name','id')->all();
        $categories = Categories::pluck('name','id')->all();
        
        return view('backend.reports.outstanding', compact('salesPeople','customers','salesOrdersObjects','items','categories'));
    }
    
    /**
     * Store a new sales orders in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function outstandingresult(Request $request)
    {
        try {
            $request['date_period'] = $request['from_date']." - ".$request['to_date'];
            $request['from_date'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['from_date'])));
            $request['to_date'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['to_date'])));                                       
            
            $salesOrdersRequest = $request;
            
            DB::enableQueryLog();
                
            $matchThese = [
                        ['no', 'like', '%'.$request['no'].'%']
                    ];
            
            $salesOrdersRequest['customer_name'] = "All";
            
            if (!empty($request['customers_id'])) {
                $cIds = ['customer_id', '=', ''.$request['customers_id'].''];
                array_push($matchThese, $cIds);
                
                $customer = Customers::find($request['customers_id']);
                
                if ($customer!=null) {
                    $salesOrdersRequest['customer_name'] = $customer->name;
                }

            }
            
            $category = Categories::find($request['category_id']);
            if ($category==null) {
                $salesOrdersRequest['category_name'] = "All";
            } else {
                $salesOrdersRequest['category_name'] = $category->name;
            }
            
            $itemses = Items::where('category_id',$request['category_id'])->get();
            
            $salesOrdersRequest['items'] = $itemses;
            
            if (!empty($itemses) && count($itemses)>0) {
                
                $itemids = array();
                foreach( $itemses as $item) { 
                    array_push($itemids, $item->id);
                }
                $salesOrdersObjects = SalesOrders::where($matchThese)                                                
                                              ->whereBetween('sales_order_date',[$request['from_date'], $request['to_date']])
                                              ->whereHas('salesOrdersDetails', function($query) use ($itemids) {
                                                  $query->whereIn('items_id', $itemids);
                                              })
                                              ->with('salesperson','customer','termbilling','user','salesOrdersDetails','salesOrdersDetails.deliveryOrderDetails')->orderBy('no', 'asc')->get();
                //dd(DB::getQueryLog());                                
            } else {
                $salesOrdersObjects = SalesOrders::where($matchThese)                                     
                                              ->whereBetween('sales_order_date',[$request['from_date'], $request['to_date']])
                                              ->with('salesperson','customer','termbilling','user','salesOrdersDetails','salesOrdersDetails.deliveryOrderDetails')->orderBy('no', 'asc')->get();
                //dd(DB::getQueryLog());    
            }    
                        
            //dd(DB::getQueryLog());
            //dd($salesOrdersObjects);
            
            if ($request['action']=='Export XLS') {
                $export = new SalesOrderExport($salesOrdersObjects);
                return Excel::download($export, 'salesOrder.xlsx');
            } else if ($request['action']=='print') {                
                return view('backend.reports.outstandingprint', compact('salesPeople','customers','salesOrdersObjects','salesOrdersRequest','items','categories'));                                               
            } else {
                $salesPeople = SalesPersons::pluck('name','id')->all();
                $customers = Customers::pluck('name','id')->all();
                $items = Items::pluck('name','id')->all();
                $categories = Categories::pluck('name','id')->all();
        
                return view('backend.reports.outstanding', compact('salesPeople','customers','salesOrdersObjects','salesOrdersRequest','items','categories'));    
            }            

        } catch (Exception $exception) {
            dd($exception);
            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }
    
    public function incominginvoice()
    {
        $suppliers = Suppliers::pluck('name','id')->all();
        $items = Items::pluck('name','id')->all();
        $categories = Categories::pluck('name','id')->all();
        return view('backend.reports.incominginvoice', compact('suppliers','items','categories'));;
    }      
    
    /**
     * Store a new sales orders in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function incominginvoiceresult(Request $request)
    {
        try {
            $request['date_period'] = $request['from_date']." - ".$request['to_date'];
            $request['from_date'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['from_date'])));
            $request['to_date'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['to_date'])));              
                
            $incomingInvoiceRequest = $request;
                
            $matchThese = [
                        ['no', 'like', '%'.$request['no'].'%']
                    ];
            
            $incomingInvoiceRequest['supplier_name'] = "All";
            
            DB::enableQueryLog();
            
            if (!empty($request['suppliers_id'])) {
                $cIds = ['purchase_orders.suppliers_id', '=', ''.$request['suppliers_id'].''];
                //array_push($matchThese, $cIds);
                
                $supplier = Suppliers::find($request['suppliers_id']);
                if ($supplier!=null) {                    
                    $incomingInvoiceRequest['supplier_name'] = $supplier->name;
                }
            }            
                        
            $itemses = Items::where('category_id',$request['category_id'])->get();
            
            $incomingInvoiceRequest['items'] = $itemses;
            
            $category = Categories::find($request['category_id']);
            if ($category==null) {
                $incomingInvoiceRequest['category_name'] = "All";
            } else {
                $incomingInvoiceRequest['category_name'] = $category->name;
            }
            if (!empty($itemses) && count($itemses)>0) {
                
                $itemids = array();
                foreach( $itemses as $item) { 
                    array_push($itemids, $item->id);
                }
                                
                
                if (!empty($request['suppliers_id'])) {
                    $val = $request['suppliers_id'];
                    $incomingInvoiceObjects = IncomingInvoices::where($matchThese)
                                              ->whereHas('incomingInvoicesDetails', function($query) use ($itemids) {
                                                        $query->whereIn('items_id', $itemids);
                                                  })
                                              ->whereHas('purchaseOrder', function($query) use ($val) {
                                                        $query->where('suppliers_id', $val);
                                                  })                         
                                              ->whereBetween('created_at',[$request['from_date'], $request['to_date']])
                                              ->with('purchaseOrder','purchaseOrder.supplier','incomingInvoicesDetails')->orderBy('created_at')->get();  
                } else {
                    $incomingInvoiceObjects = IncomingInvoices::where($matchThese)
                                              ->whereHas('incomingInvoicesDetails', function($query) use ($itemids) {
                                                        $query->whereIn('items_id', $itemids);
                                                  })                  
                                              ->whereBetween('created_at',[$request['from_date'], $request['to_date']])
                                              ->with('purchaseOrder','purchaseOrder.supplier','incomingInvoicesDetails')->orderBy('created_at')->get();                                                                  
                }

                
            } else {
                
                if (!empty($request['suppliers_id'])) {
                    $val = $request['suppliers_id'];
                    $incomingInvoiceObjects = IncomingInvoices::where($matchThese)
                                                 ->whereHas('purchaseOrder', function($query) use ($val) {
                                                        $query->where('suppliers_id', $val);
                                                  })                     
                                                  ->whereBetween('created_at',[$request['from_date'], $request['to_date']])
                                                  ->with('purchaseOrder','purchaseOrder.supplier','incomingInvoicesDetails')->orderBy('created_at')->get(); 
                } else {                
                    $incomingInvoiceObjects = IncomingInvoices::where($matchThese)
                                                  ->whereBetween('created_at',[$request['from_date'], $request['to_date']])
                                                  ->with('purchaseOrder','purchaseOrder.supplier','incomingInvoicesDetails')->orderBy('created_at')->get(); 
                }
            }           
                                    
            //dd(DB::getQueryLog());
            $categories = Categories::pluck('name','id')->all();
            $items = Items::pluck('name','id')->all();
            $suppliers = Suppliers::pluck('name','id')->all();            
            
            if ($request['action']=='Export XLS') {
                $export = new DeliveryOrderExport($incomingInvoiceObjects);
                return Excel::download($export, 'invoice.xlsx');
            } else if ($request['action']=='print') {
                return view('backend.reports.incominginvoiceprint', compact('suppliers','items','incomingInvoiceObjects','incomingInvoiceRequest','categories'));
            } else {
                return view('backend.reports.incominginvoice', compact('suppliers','items','incomingInvoiceObjects','incomingInvoiceRequest','categories'));
            }                        
            

        } catch (Exception $exception) {
            dd($exception);
            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }
    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'no' => 'required|string|min:1|max:45',
            'sales_order_date' => 'required|string',
            'sales_order_delivery' => 'required|string',
            'sales_person_id' => 'required',
            'ref_po_customer' => 'required|string',
            'purchase_order_date'=> 'required|string',
            'customer_id' => 'required',
            'transport' => 'required|string',
            'term_billing_id' => 'required',
            'total_quantity' => 'required|between:0,99.99',
            'total_amount' => 'required|between:0,99.99',
            'sub_total' => 'required|between:0,99.99',
            'discount' => 'required|between:0,99.99',
            'total' => 'required|between:0,99.99',
            'vat' => 'required|between:0,99.99'
        ];

        $data = $request->validate($rules);

        return $data;
    }

}
