<?php

namespace App\Http\Controllers;

use App\Models\Auth\User;
use App\Models\Customers;
use App\Models\Items;
use App\Models\SalesOrders;
use App\Models\SalesOrdersDp;
use App\Models\SalesOrdersDetails;
use App\Models\SalesPersons;
use App\Models\DeliveryOrdersDetails;
use App\Models\DeliveryOrders;
use App\Models\InvoiceDeliveryOrders;
use App\Models\Invoices;
use App\Models\InvoicesDetails;

use App\Models\TermBillings;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;
use DB;

class SalesOrdersController extends Controller
{

    /**
     * Display a listing of the sales orders.
     *
     * @return Illuminate\View\View
     */
    public function index(Request $request)
    {
        $customers = Customers::pluck('name','id')->all();
        $searchBy['keyword'] = $request->keyword;
        $searchBy['customer_id'] = $request->customer_id;
        $salesOrdersObjects = SalesOrders::with('salesperson','customer','termbilling','user')
                ->where('no','like', "%{$searchBy['keyword']}%")
                ->where('customer_id','like', "%{$searchBy['customer_id']}%")
                ->orderBy('no', 'desc')->paginate(10);
        $salesOrdersObjects->appends($request->only('keyword'));          
        $salesOrdersObjects->appends($request->only('customer_id'));          
        return view('backend.sales_orders.index', compact('salesOrdersObjects','customers','searchBy'));
    }

    /**
     * Display a listing of the sales orders.
     *
     * @return Illuminate\View\View
     */
    public function outstanding()
    {
        
        $salesOrdersObjects = SalesOrders::where('status',0)->with('salesperson','customer','termbilling','user')->orderBy('updated_at', 'desc')->paginate(25);
        $ids = array();
        
        foreach( $salesOrdersObjects as $item => $n ) {  
            array_push($ids, $n->id);
        }
        
        //DB::enableQueryLog();
        $salesOrdersObjectsDP = SalesOrdersDp::whereIn('sales_orders_id', $ids)->get();
        $deliveryOrder = DeliveryOrders::whereIn('sales_orders_id', $ids)->get();
        
        $ids_delivery = array();
        foreach( $deliveryOrder as $item => $n ) {  
            array_push($ids_delivery, $n->id);
        }  
        
        $invoiceDeliveryOrders = InvoiceDeliveryOrders::whereIn('delivery_orders_id', $ids_delivery)->get();
        
        foreach( $salesOrdersObjects as $item => $n ) {  
            $total_dp = 0;
            foreach( $salesOrdersObjectsDP as $itemDP => $ndp ) {  
                if ($ndp->sales_orders_id==$n->id) {
                    $total_dp+=$ndp->total_dp;
                }
            } 
            $total_invoice = 0;
            foreach( $deliveryOrder as $itemDO => $ndo ) { 
                if ($ndo->sales_orders_id==$n->id) {
                    foreach ($invoiceDeliveryOrders as $itemInvoice => $nin) {
                        if ($ndo->id == $nin->delivery_orders_id) {
                            $total_invoice+=$nin->invoice->grand_total;
                        }
                    }
                }
            }
            $salesOrdersObjects[$item]['total_dp'] = $total_dp;
            $salesOrdersObjects[$item]['total_invoice'] = $total_invoice;            
            
        } 
        
        return view('backend.sales_orders.outstanding', compact('salesOrdersObjects'));
    }
    /**
     * Show the form for creating a new sales orders.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        $salesPeople = SalesPersons::pluck('name','id')->all();
        $customers = Customers::pluck('name','id')->all();
        $termBillings = TermBillings::pluck('name','id')->all();
        $items = Items::pluck('name','id')->all();
        $users = User::pluck('id','id')->all();
        
        return view('backend.sales_orders.create', compact('salesPeople','customers','termBillings','users','items'));
    }

    /**
     * Store a new sales orders in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {

            $salesOrder = SalesOrders::where('no', $request['no'])->first();
            if ($salesOrder!=null) {
                return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Sales order no already in the system, please use another one!']);
            }
            
            $request['sales_order_date'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['sales_order_date'])));
            $request['sales_order_delivery'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['sales_order_delivery'])));  
            $request['purchase_order_date'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['purchase_order_date'])));  
                                    
            $items_id = $request['items_id'];
            $quantity = $request['quantity'];
            $price = $request['price'];
            $description = $request['description'];
            $amount = $request['amount'];
            
            $total_quantity = 0;
            $total_amount = 0;
            //count total
            foreach( $items_id as $item => $n ) {                
                $total_quantity+=$quantity[$item];
                $total_amount+=$price[$item];                
            }
            
            $request['total_quantity'] = $total_quantity;
            $request['total_amount'] = $request['grand_total'];
            $data['users_id'] = auth()->user()->id;
            $data['delivery_address'] = $request['delivery_address'];
            $data = $this->getData($request);
            
            $salesOrder = SalesOrders::create($data);
            
            foreach( $items_id as $item => $n ) {
                $dataDetail = array (
                    'sales_orders_id' => $salesOrder->id,
                    'items_id' => $items_id[$item],
                    'quantity' => $quantity[$item],
                    'price' => $price[$item],
                    'description' => $description[$item],
                    'amount' => $amount[$item]
                );                
                SalesOrdersDetails::create($dataDetail);
            }
            
            return redirect()->route('sales_orders.sales_orders.index')
                             ->with('success_message', 'Sales Orders was successfully added!');

        } catch (Exception $exception) {
            dd($exception);
            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    /**
     * Display the specified sales orders.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $salesOrders = SalesOrders::with('salesperson','customer','termbilling','user')->findOrFail($id);

        return view('backend.sales_orders.show', compact('salesOrders'));
    }    

    /**
     * Show the form for editing the specified sales orders.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $salesOrders = SalesOrders::findOrFail($id);
        $salesPeople = SalesPersons::pluck('name','id')->all();
        $customers = Customers::pluck('name','id')->all();
        $termBillings = TermBillings::pluck('name','id')->all();
        $items = Items::pluck('name','id')->all();
        
        $salesOrdersDetails = SalesOrdersDetails::where('sales_orders_id',$salesOrders->id)->get();
        
        return view('backend.sales_orders.edit', compact('salesOrders','salesPeople','customers','termBillings','items','salesOrdersDetails'));
    }

    /**
     * Print the form for editing the specified sales orders.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function pdf($id)
    {
        $salesOrders = SalesOrders::findOrFail($id);
        $salesPeople = SalesPersons::pluck('name','id')->all();
        $customers = Customers::pluck('name','id')->all();
        $termBillings = TermBillings::pluck('name','id')->all();
        $items = Items::pluck('name','id')->all();
        
        $salesOrdersDetails = SalesOrdersDetails::where('sales_orders_id',$salesOrders->id)->get();
        
        return view('backend.sales_orders.print', compact('salesOrders','salesPeople','customers','termBillings','items','salesOrdersDetails'));
    }
    /**
     * Update the specified sales orders in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            $request['sales_order_date'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['sales_order_date'])));
            $request['sales_order_delivery'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['sales_order_delivery'])));  
            $request['purchase_order_date'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['purchase_order_date'])));
            
            $items_id = $request['items_id'];
            $quantity = $request['quantity'];
            $price = $request['price'];
            $description = $request['description'];
            $amount = $request['amount'];
            
            $total_quantity = 0;
            $total_amount = 0;
            //count total
            foreach( $items_id as $item => $n ) {                
                $total_quantity+=$quantity[$item];
                $total_amount+=$price[$item];                
            }
            
            $request['total_quantity'] = $total_quantity;
            $request['total_amount'] = $request['grand_total'];
                                    
            $data = $this->getData($request);
            $data['users_id'] = auth()->user()->id;
            
            $salesOrders = SalesOrders::findOrFail($id);
            $salesOrders->update($data);
            
            $salesOrdersDetails = SalesOrdersDetails::where('sales_orders_id',$salesOrders->id)->get();
            
            foreach( $salesOrdersDetails as $item => $n ) {
                $salesOrdersDetails = SalesOrdersDetails::findOrFail($n->id);
                
                DeliveryOrdersDetails::where('sales_orders_details_id', $salesOrdersDetails->id)->delete();
                
                $salesOrdersDetails->delete();                                
            }
            
            foreach( $items_id as $item => $n ) {
                $dataDetail = array (
                    'sales_orders_id' => $salesOrders->id,
                    'items_id' => $items_id[$item],
                    'quantity' => $quantity[$item],
                    'price' => $price[$item],
                    'description' => $description[$item],
                    'amount' => $amount[$item]
                );                
                SalesOrdersDetails::create($dataDetail);
            }
            
            return redirect()->route('sales_orders.sales_orders.index')
                             ->with('success_message', 'Sales Orders was successfully updated!');

        } catch (Exception $exception) {
            dd($exception);
            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }        
    }

    /**
     * Remove the specified sales orders from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $salesOrders = SalesOrders::findOrFail($id);
            
            $salesOrdersDetails = SalesOrdersDetails::where('sales_orders_id',$id)->get();
            
            foreach( $salesOrdersDetails as $item => $n ) {
                $salesOrdersDetails = SalesOrdersDetails::findOrFail($n->id);                
                $salesOrdersDetails->delete();                                
            }
            
            $deliveryOrders = DeliveryOrders::where('sales_orders_id', $id)->get();
            foreach( $deliveryOrders as $item => $n ) {                
                DeliveryOrdersDetails::where('delivery_orders_id', $n->id)->delete();                                            
                $invoiceDeliveryOrders = InvoiceDeliveryOrders::where('delivery_orders_id', $n->id)->get();
                $invoice = Invoices::where('id', $invoiceDeliveryOrders->invoices_id)->get();
                InvoicesDetails::where('invoices_id', $invoice->id)->delete();  
                Invoices::where('id', $invoiceDeliveryOrders->invoices_id)->delete();                
                InvoiceDeliveryOrders::where('delivery_orders_id', $deliveryOrders->id)->delete();
                
                
            }
            
            DeliveryOrders::where('sales_orders_id', $id)->delete();
            $salesOrders->delete();            
            
            return redirect()->route('sales_orders.sales_orders.index')
                             ->with('success_message', 'Sales Orders was successfully deleted!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }
    
    /**
     * Update the specified sales orders in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function complete($id, Request $request)
    {
        try {
            $data['users_id'] = auth()->user()->id;
            $data['status'] = 1;
            
            $salesOrders = SalesOrders::findOrFail($id);
            $salesOrders->update($data);
            
            return redirect()->route('sales_orders.sales_orders.index')
                             ->with('success_message', 'Sales Orders was successfully updated!');

        } catch (Exception $exception) {
                   return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }        
    }
    
    
    /**
     * Uncomplete the specified sales orders in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function uncomplete($id, Request $request)
    {
        try {
            $data['users_id'] = auth()->user()->id;
            $data['status'] = 0;
            
            $salesOrders = SalesOrders::findOrFail($id);
            $salesOrders->update($data);
            
            return redirect()->route('sales_orders.sales_orders.index')
                             ->with('success_message', 'Sales Orders was successfully updated!');

        } catch (Exception $exception) {
                   return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }        
    }
    
    public function find(Request $request)
    {        
        
        $salesOrders = SalesOrders::where('no', $request['no'])->firstOrFail();
        
        try {
            return response()->json($salesOrders);
        } catch (Exception $ex) {
            return response()->json($ex);
        }        
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'no' => 'required|string|min:1|max:45',
            'sales_order_date' => 'required|string',
            'sales_order_delivery' => 'required|string',
            'sales_person_id' => 'required',
            'ref_po_customer' => 'required|string',
            'purchase_order_date'=> 'required|string',
            'customer_id' => 'required',
            'transport' => 'required|string',
            'term_billing_id' => 'required',
            'total_quantity' => 'required|between:0,99.99',
            'total_amount' => 'required|between:0,99.99',
            'sub_total' => 'required|between:0,99.99',
            'discount' => 'required|between:0,99.99',
            'total' => 'required|between:0,99.99',
            'vat' => 'required|between:0,99.99',
            'delivery_address' => 'string',
        ];

        $data = $request->validate($rules);

        return $data;
    }

}
