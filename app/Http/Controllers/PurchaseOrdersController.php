<?php

namespace App\Http\Controllers;

use App\Models\Auth\User;
use App\Models\Suppliers;
use App\Models\Items;
use Illuminate\Http\Request;
use App\Models\PurchaseOrders;
use App\Models\PurchaseOrdersDetails;
use App\Models\IncomingInvoices;
use App\Models\IncomingInvoicesDetails;
use App\Models\TermBillings;
use App\Http\Controllers\Controller;
use Exception;

class PurchaseOrdersController extends Controller
{

    /**
     * Display a listing of the purchase orders.
     *
     * @return Illuminate\View\View
     */
    public function index(Request $request)
    {
        $suppliers = Suppliers::pluck('name','id')->all();
        $searchBy['keyword'] = $request->keyword;
        $searchBy['suppliers_id'] = $request->suppliers_id;
        
        $purchaseOrdersObjects = PurchaseOrders::with('supplier','user')
                ->where('purchase_no','like', "%{$searchBy['keyword']}%")
                ->where('suppliers_id','like', "%{$searchBy['suppliers_id']}%")
                ->orderBy('purchase_no', 'desc')->paginate(10);
        $purchaseOrdersObjects->appends($request->only('keyword'));        
        $purchaseOrdersObjects->appends($request->only('suppliers_id'));        
        return view('backend.purchase_orders.index', compact('purchaseOrdersObjects','suppliers','searchBy'));
    }

    /**
     * Show the form for creating a new purchase orders.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        $suppliers = Suppliers::pluck('name','id')->all();
        $users = User::pluck('id','id')->all();
        $items = Items::pluck('name','id')->all();
        $termBillings = TermBillings::pluck('name','id')->all();
        return view('backend.purchase_orders.create', compact('suppliers','users', 'items', 'termBillings'));
    }

    /**
     * Store a new purchase orders in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
             
            $request['ordered_at'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['ordered_at'])));
            $request['arrived_at'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['arrived_at'])));  
        
            $purchaseOrders = PurchaseOrders::where('purchase_no', $request['purchase_no'])->first();
            if ($purchaseOrders!=null) {
                return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Purchase no already in the system, please use another one!']);
            }
            $items_id = $request['items_id'];
            $quantity = $request['quantity'];
            $price = $request['price'];
            $description = $request['description'];
            $term_billing_id = $request['term_billing_id'];
            $amount = $request['amount'];            
            
            $is_tax = 0;
            if ($request['is_tax']!=null) {
                $is_tax = 1;
            }
            
            $total_quantity = 0;
            $total_amount = 0;
            //count total
            foreach( $items_id as $item => $n ) {                
                $total_quantity+=$quantity[$item];
                $total_amount+=$price[$item];                
            }
            
            $request['total_quantity'] = $total_quantity;
            $request['total_amount'] = $total_amount;
            $request['status'] = 0;
            
            $data = $this->getData($request);
            
            $data['users_id'] = auth()->user()->id;            
            $data['is_tax'] = $is_tax;            
            $data['term_billing_id'] = $term_billing_id;      
            $data['total_amount'] = $request['grand_total'];
			
            $purchaceOrders = PurchaseOrders::create($data);
            
            foreach( $items_id as $item => $n ) {
                
                $dataDetail = array (
                    'purchase_orders_id' => $purchaceOrders->id,
                    'items_id' => $items_id[$item],
                    'quantity' => $quantity[$item],
                    'price' => $price[$item],
                    'amount' => $amount[$item],
                    'description' => $description[$item]
                );
                PurchaseOrdersDetails::create($dataDetail);
            }
            
            return redirect()->route('purchase_orders.purchase_orders.index')
                             ->with('success_message', 'Purchase Orders was successfully added!');

        } catch (Exception $exception) {
            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    /**
     * Display the specified purchase orders.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $purchaseOrders = PurchaseOrders::with('supplier','user')->findOrFail($id);

        return view('backend.purchase_orders.show', compact('purchaseOrders'));
    }

    /**
     * Show the form for editing the specified purchase orders.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $purchaseOrders = PurchaseOrders::findOrFail($id);
        $suppliers = Suppliers::pluck('name','id')->all();
        $users = User::pluck('id','id')->all();
        $items = Items::pluck('name','id')->all();
        $termBillings = TermBillings::pluck('name','id')->all();
        
        $purchaseOrdersDetails = PurchaseOrdersDetails::where('purchase_orders_id',$purchaseOrders->id)->get();
        
        return view('backend.purchase_orders.edit', compact('purchaseOrders','suppliers','users', 'items', 'termBillings', 'purchaseOrdersDetails'));
    }
    
    /**
     * Show the form for editing the specified purchase orders.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function pdf($id)
    {
        $purchaseOrders = PurchaseOrders::findOrFail($id);
        $suppliers = Suppliers::pluck('name','id')->all();
        $users = User::pluck('id','id')->all();
        $users = User::pluck('id','id')->all();
        $items = Items::pluck('name','id')->all();
        $termBillings = TermBillings::pluck('name','id')->all();
        
        $purchaseOrdersDetails = PurchaseOrdersDetails::where('purchase_orders_id',$purchaseOrders->id)->get();
        
        return view('backend.purchase_orders.print', compact('purchaseOrders','suppliers','users', 'items', 'termBillings', 'purchaseOrdersDetails'));
    }
    
        /**
     * Show the form for editing the specified purchase orders.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function find(Request $request)
    {        
        
        $purchaseOrders = PurchaseOrders::where('purchase_no', $request['purchase_no'])->firstOrFail();
        
        try {
            return response()->json($purchaseOrders);
        } catch (Exception $ex) {
            return response()->json($ex);
        }        
    }

    /**
     * Show the form for json the specified sales orders details.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
            
    public function json($id)
    {        
        $purchaseOrders = PurchaseOrders::with('supplier','user','termbilling')->findOrFail($id);
        $purchaseOrdersDetails = PurchaseOrdersDetails::where('purchase_orders_id',$purchaseOrders->id)->get();
        
        foreach ($purchaseOrdersDetails as $key => $value) {
            $purchaseOrdersDetails[$key] = $value;
            $purchaseOrdersDetails[$key]['item'] = $value->item;
            $purchaseOrdersDetails[$key]['unit'] = $value->item->unit;
        }
        
        $incomingInvoices = IncomingInvoices::where('purchase_orders_id', $purchaseOrders->id)->get();
        foreach( $incomingInvoices as $item => $n ) {
            $incomingInvoicesDetails = IncomingInvoicesDetails::where('incoming_invoices_id', $n->id)->get();
            foreach( $incomingInvoicesDetails as $incomingIDs => $m ) {                
                foreach ($purchaseOrdersDetails as $key => $value) {
                    if ($purchaseOrdersDetails[$key]['item']->id == $m->item->id) {
                        $purchaseOrdersDetails[$key]['quantity'] = $purchaseOrdersDetails[$key]['quantity'] - $m->quantity;                    
                    }                    
                }
            }
        }

        $data['purchaseOrders'] = $purchaseOrders;
        $data['purchaseOrdersDetails'] = $purchaseOrdersDetails;
                
        return response()->json($data);
    }  
    
    /**
     * Update the specified purchase orders in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
                       
            $request['ordered_at'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['ordered_at'])));
            $request['arrived_at'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['arrived_at'])));  
             
            $items_id = $request['items_id'];
            $quantity = $request['quantity'];
            $price = $request['price'];
            $description = $request['description'];
            $amount = $request['amount'];
            
            $total_quantity = 0;
            $total_amount = 0;
            //count total
            foreach( $items_id as $item => $n ) {                
                $total_quantity+=$quantity[$item];
                $total_amount+=$price[$item];                
            }
            
            $request['total_quantity'] = $total_quantity;
            $request['total_amount'] = $request['grand_total'];
            $request['status'] = 0;
            
            $request['users_id'] = auth()->user()->id;
                        
            $data = $this->getData($request);
            
            $purchaseOrders = PurchaseOrders::findOrFail($id);
            $purchaseOrders->update($data);
            
            $purchaseOrdersDetails = PurchaseOrdersDetails::where('purchase_orders_id', $id)->get();
            
            foreach( $purchaseOrdersDetails as $item => $n ) {
                $pod = PurchaseOrdersDetails::findOrFail($n->id);
                $pod->delete();
            }
            
            foreach( $items_id as $item => $n ) {
                $dataDetail = array (
                    'purchase_orders_id' => $id,
                    'items_id' => $items_id[$item],
                    'quantity' => $quantity[$item],
                    'price' => $price[$item],
                    'amount' => $amount[$item],
                    'description' => $description[$item]                
                );
                PurchaseOrdersDetails::create($dataDetail);
            }
            
            return redirect()->route('purchase_orders.purchase_orders.index')
                             ->with('success_message', 'Purchase Orders was successfully updated!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }        
    }

    /**
     * Remove the specified purchase orders from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            
            $incomingInvoices = IncomingInvoices::where('purchase_orders_id', $id)->get();
            foreach( $incomingInvoices as $item => $n ) {
                $incomingInvoicesDetails = IncomingInvoicesDetails::where('incoming_invoices_id', $n->id)->get();
                foreach( $incomingInvoicesDetails as $incomingIDs => $m ) {
                    $iid = IncomingInvoicesDetails::findOrFail($m->id);
                    $iid->delete();
                }
                
                $in = IncomingInvoices::findOrFail($n->id);
                $in->delete();
            }
                        
            $purchaseOrdersDetails = PurchaseOrdersDetails::where('purchase_orders_id', $id)->get();
            
            foreach( $purchaseOrdersDetails as $item => $n ) {
                $pod = PurchaseOrdersDetails::findOrFail($n->id);
                $pod->delete();
            }
            
            $purchaseOrders = PurchaseOrders::findOrFail($id);
            $purchaseOrders->delete();
            
            return redirect()->route('purchase_orders.purchase_orders.index')
                             ->with('success_message', 'Purchase Orders was successfully deleted!');

        } catch (Exception $exception) {
            
            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }
    
        /**
     * Update the specified purchase orders in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function complete($id, Request $request)
    {
        try {
            $data['users_id'] = auth()->user()->id;
            $data['status'] = 1;
            
            $purchaseOrders = PurchaseOrders::findOrFail($id);
            $purchaseOrders->update($data);
            
            return redirect()->route('purchase_orders.purchase_orders.index')
                             ->with('success_message', 'Purhcase Orders was successfully updated!');

        } catch (Exception $exception) {
                   return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }        
    }
    
    
    /**
     * Uncomplete the specified sales orders in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function uncomplete($id, Request $request)
    {
        try {
            $data['users_id'] = auth()->user()->id;
            $data['status'] = 0;
            
            $purchaseOrders = PurchaseOrders::findOrFail($id);
            $purchaseOrders->update($data);            
            
            return redirect()->route('purchase_orders.purchase_orders.index')
                             ->with('success_message', 'Purchase Orders was successfully updated!');

        } catch (Exception $exception) {
                   return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }        
    }
    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'suppliers_id' => 'required',
            'purchase_no' => 'required|string|min:1|max:65',
            'total_quantity' => 'required|numeric|min:0|max:100000000045',
            'total_amount' => 'required|numeric|min:0|max:10000000000045',
            'ordered_at' => 'required|string',
            'arrived_at' => 'required|string',
            'term_billing_id' => 'required|numeric',
            'vat' => 'required',
            'sub_total' => 'required',
            'discount' => 'required',
            'tc' => 'required'
        ];

        
        $data = $request->validate($rules);
        return $data;
    }

}
