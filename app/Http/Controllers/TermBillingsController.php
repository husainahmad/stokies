<?php

namespace App\Http\Controllers;

use App\Models\TermBillings;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;

class TermBillingsController extends Controller
{

    /**
     * Display a listing of the term billings.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $termBillingsObjects = TermBillings::orderBy('updated_at', 'desc')->paginate(25);

        return view('backend.term_billings.index', compact('termBillingsObjects'));
    }

    /**
     * Show the form for creating a new term billings.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        
        
        return view('backend.term_billings.create');
    }

    /**
     * Store a new term billings in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            TermBillings::create($data);

            return redirect()->route('term_billings.term_billings.index')
                             ->with('success_message', 'Term Of Payments was successfully added!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    /**
     * Display the specified term billings.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $termBillings = TermBillings::findOrFail($id);

        return view('backend.term_billings.show', compact('termBillings'));
    }

    /**
     * Show the form for editing the specified term billings.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $termBillings = TermBillings::findOrFail($id);
        

        return view('backend.term_billings.edit', compact('termBillings'));
    }

    /**
     * Update the specified term billings in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            $termBillings = TermBillings::findOrFail($id);
            $termBillings->update($data);

            return redirect()->route('term_billings.term_billings.index')
                             ->with('success_message', 'Term Of Payments was successfully updated!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }        
    }

    /**
     * Remove the specified term billings from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $termBillings = TermBillings::findOrFail($id);
            $termBillings->delete();

            return redirect()->route('term_billings.term_billings.index')
                             ->with('success_message', 'Term Of Payments was successfully deleted!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'name' => 'required|string|min:1|max:145',
            'days' => 'required|numeric',            
        ];

        
        $data = $request->validate($rules);




        return $data;
    }

}
