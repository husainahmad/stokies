<?php

namespace App\Http\Controllers;

use App\Models\Auth\User;
use App\Models\Invoices;
use App\Models\InvoicesDetails;
use App\Models\InvoiceDeliveryOrders;
use App\Models\Customers;
use App\Models\SalesPersons;
use App\Models\Items;
use Illuminate\Http\Request;
use App\Models\DeliveryOrders;
use App\Models\SalesOrders;
use App\Models\SalesOrdersDp;
use App\Http\Controllers\Controller;
use Exception;

class InvoicesController extends Controller
{

    /**
     * Display a listing of the invoices.
     *
     * @return Illuminate\View\View
     */
    public function index(Request $request)
    {
        $customers = Customers::pluck('name','id')->all();
        $searchBy['keyword'] = $request->keyword;
        $searchBy['customer_id'] = $request->customer_id;
        
        $invoicesObjects = Invoices::with('customer','user','salesperson')
                ->where('no','like', "%{$searchBy['keyword']}%")
                ->where('customers_id','like', "%{$searchBy['customer_id']}%")
                ->orderBy('no', 'desc')->paginate(10);
        
        foreach ($invoicesObjects as $key => $value) {
            $invoicesObjects[$key] = $value;
            //$invoicesObjects[$key]['invoiceDeliveryOrders'] = InvoiceDeliveryOrders::where('invoices_id',$value->id)->get();     
        }         
        
        $invoicesObjects->appends($request->only('keyword'));   
        $invoicesObjects->appends($request->only('customer_id'));   
        return view('backend.invoices.index', compact('invoicesObjects','customers','searchBy'));
    }

    /**
     * Show the form for creating a new invoices.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        $deliveryOrders = DeliveryOrders::select('delivery_orders.no','delivery_orders.id')
                        ->where('delivery_orders.status','0')
                        ->get(); 
        
        $customers = Customers::pluck('name','id')->all();
        $users = User::pluck('id','id')->all();
        $items = Items::pluck('name','id')->all();
        $salesPeople = SalesPersons::pluck('name','id')->all();
        
        return view('backend.invoices.create', compact('deliveryOrders','customers','users','salesPeople','items' ));
    }

    /**
     * Store a new invoices in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {

            $invoice = Invoices::where('no', $request['no'])->first();
            if ($invoice!=null) {
                return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Invoice no already in the system, please use another one!']);
            }
            
            $request['due_date'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['due_date'])));
            $request['created_at'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['created_at'])));
            $request['updated_at'] = date('Y-m-d H:i:s');
            
            $items_id = $request['items_id'];
            $quantity = $request['quantity'];
            $price = $request['price'];
            $amount = $request['amount'];
            
            $delivery_orders_id = $request['delivery_orders_id'];
            
            $data = $this->getData($request);
            
            $data['users_id'] = auth()->user()->id;
            $invoice = Invoices::create($data);
            
            foreach( $delivery_orders_id as $item => $n ) {
                $dataDetail = array (
                    'invoices_id' => $invoice->id,
                    'delivery_orders_id' => $delivery_orders_id[$item]  
                );
                InvoiceDeliveryOrders::create($dataDetail);
            }
            
            foreach( $items_id as $item => $n ) {
                $dataDetail = array (
                    'invoices_id' => $invoice->id,
                    'items_id' => $items_id[$item],
                    'quantity' => $quantity[$item],
                    'price' => $price[$item],
                    'amount' => $amount[$item]     
                );
                InvoicesDetails::create($dataDetail);
            }
            
            return redirect()->route('invoices.invoices.index')
                             ->with('success_message', 'Invoices was successfully added!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    /**
     * Display the specified invoices.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $invoices = Invoices::with('deliveryorder','customer','user','salesperson')->findOrFail($id);

        return view('backend.invoices.show', compact('invoices'));
    }

    /**
     * Show the form for editing the specified invoices.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $invoices = Invoices::findOrFail($id);
        
        if ($invoices->is_dp==1) {
            $salesOrdersDp = SalesOrdersDp::with('invoice')->where('invoices_id', $id)
                        ->firstOrFail();
            
            return redirect()->route('sales_orders_dps.sales_orders_dp.edit', [$salesOrdersDp->id]);            
        }
        
        $deliveryOrders = DeliveryOrders::select('delivery_orders.no','delivery_orders.id')
                        ->where('delivery_orders.status','0')
                        ->get(); 
        
        $customers = Customers::pluck('name','id')->all();
        $users = User::pluck('id','id')->all();
        $salesPeople = SalesPersons::pluck('name','id')->all();
        $items = Items::pluck('name','id')->all();

        $invoicesDetails = InvoicesDetails::where('invoices_id',$id)->get();
        
        $invoiceDeliveryOrders = InvoiceDeliveryOrders::with('deliveryOrder','invoice')->where('invoices_id',$id)->get();
        
        $sales_orders_id = 0;
        foreach ($invoiceDeliveryOrders as $key => $indeliveryOrder) {
            $sales_orders_id = $indeliveryOrder->deliveryOrder->sales_orders_id;
        }
        
        $salesOrders = SalesOrders::findOrFail($sales_orders_id);
        $salesOrdersDp = SalesOrdersDp::where('sales_orders_id', $salesOrders->id)->get();
        
        return view('backend.invoices.edit', compact('invoices','deliveryOrders','customers','users','salesPeople', 'items', 'invoicesDetails','invoiceDeliveryOrders','salesOrders','salesOrdersDp'));
    }
    
    /**
     * Show the form for editing the specified invoices.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function pdf($id)
    {
        $invoices = Invoices::findOrFail($id);
                
        $customers = Customers::pluck('name','id')->all();
        $users = User::pluck('id','id')->all();
        $salesPeople = SalesPersons::pluck('id','id')->all();
        $items = Items::pluck('name','id')->all();

        $invoicesDetails = InvoicesDetails::where('invoices_id',$id)->get();
        //dd($invoices);
        
        if ($invoices->is_dp) {
            $salesOrderDp = SalesOrdersDp::where('invoices_id',$id)->first();
            return view('backend.invoices.print', compact('invoices','invoiceDeliveryOrders','customers','users','salesPeople', 'items','salesOrderDp', 'invoicesDetails'));
        } else {
            $invoiceDeliveryOrders = InvoiceDeliveryOrders::where('invoices_id',$id)->get();        
            return view('backend.invoices.print', compact('invoices','invoiceDeliveryOrders','customers','users','salesPeople', 'items', 'invoicesDetails'));
        }        
    }

    /**
     * Update the specified invoices in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $request['due_date'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['due_date'])));
            $request['created_at'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['created_at'])));
            $request['updated_at'] = date('Y-m-d H:i:s');
            
            $items_id = $request['items_id'];
            $quantity = $request['quantity'];
            $price = $request['price'];
            $amount = $request['amount'];
            $description = $request['description'];
            
            $data = $this->getData($request);
            $data['users_id'] = auth()->user()->id;
            
            $invoices = Invoices::findOrFail($id);
            $invoices->update($data);
            
            $invoicesDetails = InvoicesDetails::where('invoices_id',$id)->get();
            
            foreach( $invoicesDetails as $item => $n ) {
                $invoicesDetails = InvoicesDetails::findOrFail($n->id);
                $invoicesDetails->delete();
            }
            
            foreach( $items_id as $item => $n ) {
                $dataDetail = array (
                    'invoices_id' => $id,
                    'items_id' => $items_id[$item],
                    'quantity' => $quantity[$item],
                    'price' => $price[$item],
                    'amount' => $amount[$item],
                    'description' => $description[$item]
                );
                InvoicesDetails::create($dataDetail);
            }
            
            return redirect()->route('invoices.invoices.index')
                             ->with('success_message', 'Invoices was successfully updated!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }        
    }

    /**
     * Remove the specified invoices from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $invoices = Invoices::findOrFail($id);            
            $invoicesDetails = InvoicesDetails::where('invoices_id',$id)->delete();            
            $invoices->delete();

            return redirect()->route('invoices.invoices.index')
                             ->with('success_message', 'Invoices was successfully deleted!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }
    
    public function find(Request $request)
    {        
        
        $invoices = Invoices::where('no', $request['no'])->firstOrFail();
        
        try {
            return response()->json($invoices);
        } catch (Exception $ex) {
            return response()->json($ex);
        }        
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'no' => 'required|string|min:1|max:45',
            'ship_via' => 'required|string|min:1|max:145',
            'purchase_no' => 'required|string|min:1|max:45',
            'due_date' => 'required',
            'created_at' => 'required',
            'updated_at' => 'required',
            'customers_id' => 'required',
            'discount' => 'required',
            'total' => 'required',
            'dp' => 'required',
            'vat' => 'required',
            'grand_total' => 'required',
            'sales_persons_id' => 'required',    
            'sub_total' => 'required'
        ];

        
        $data = $request->validate($rules);




        return $data;
    }

}
