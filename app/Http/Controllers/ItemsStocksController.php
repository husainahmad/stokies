<?php

namespace App\Http\Controllers;

use App\Models\Items;
use App\Models\Auth\User;
use App\Models\ItemsStock;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;

class ItemsStocksController extends Controller
{

    /**
     * Display a listing of the items stocks.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $itemsStocks = ItemsStock::with('item','user')->orderBy('updated_at', 'desc')->paginate(5);

        return view('backend.items_stocks.index', compact('itemsStocks'));
    }

    /**
     * Show the form for creating a new items stock.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        $items = Items::pluck('name','id')->all();
$users = User::pluck('id','id')->all();
        
        return view('backend.items_stocks.create', compact('items','users'));
    }

    /**
     * Store a new items stock in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
      $user_id = (auth()->check()) ? auth()->user()->id : null;
            $data['user_id'] =  $user_id;      
            ItemsStock::create($data);

            return redirect()->route('items_stocks.items_stock.index')
                             ->with('success_message', 'Items Stock was successfully added!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    /**
     * Display the specified items stock.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $itemsStock = ItemsStock::with('item','user')->findOrFail($id);

        return view('backend.items_stocks.show', compact('itemsStock'));
    }

    /**
     * Show the form for editing the specified items stock.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $itemsStock = ItemsStock::findOrFail($id);
        $items = Item::pluck('name','id')->all();
$users = User::pluck('id','id')->all();

        return view('backend.items_stocks.edit', compact('itemsStock','items','users'));
    }

    /**
     * Update the specified items stock in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $data = $this->getData($request);
            $user_id = (auth()->check()) ? auth()->user()->id : null;
            $data['user_id'] =  $user_id;
            $itemsStock = ItemsStock::findOrFail($id);
            $itemsStock->update($data);

            return redirect()->route('items_stocks.items_stock.index')
                             ->with('success_message', 'Items Stock was successfully updated!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }        
    }

    /**
     * Remove the specified items stock from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $itemsStock = ItemsStock::findOrFail($id);
            $itemsStock->delete();

            return redirect()->route('items_stocks.items_stock.index')
                             ->with('success_message', 'Items Stock was successfully deleted!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'stock' => 'required|numeric|min:-2147483648|max:2147483647',
            'items_id' => 'required',     
        ];

        
        $data = $request->validate($rules);




        return $data;
    }

}
