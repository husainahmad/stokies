@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Items Stock Out' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('items_stock_outs.items_stock_out.destroy', $itemsStockOut->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('items_stock_outs.items_stock_out.index') }}" class="btn btn-primary" title="Show All Items Stock Out">
                        <span class="fa fa-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('items_stock_outs.items_stock_out.create') }}" class="btn btn-success" title="Create New Items Stock Out">
                        <span class="fa fa-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('items_stock_outs.items_stock_out.edit', $itemsStockOut->id ) }}" class="btn btn-primary" title="Edit Items Stock Out">
                        <span class="fa fa-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Items Stock Out" onclick="return confirm(&quot;Delete Items Stock Out??&quot;)">
                        <span class="fa fa-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Items</dt>
            <dd>{{ optional($itemsStockOut->item)->name }}</dd>
            <dt>Volume</dt>
            <dd>{{ $itemsStockOut->volume }}</dd>
            <dt>Customers</dt>
            <dd>{{ optional($itemsStockOut->customer)->name }}</dd>
            <dt>Price</dt>
            <dd>{{ $itemsStockOut->price }}</dd>
            <dt>Amount Price</dt>
            <dd>{{ $itemsStockOut->amount_price }}</dd>
            <dt>Created At</dt>
            <dd>{{ $itemsStockOut->created_at }}</dd>
            <dt>Updated At</dt>
            <dd>{{ $itemsStockOut->updated_at }}</dd>
            <dt>Users</dt>
            <dd>{{ optional($itemsStockOut->user)->id }}</dd>
            <dt>Reference No</dt>
            <dd>{{ $itemsStockOut->reference_no }}</dd>

        </dl>

    </div>
</div>

@endsection