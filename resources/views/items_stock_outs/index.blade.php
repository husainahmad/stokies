@extends('layouts.app')

@section('content')

    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="fa fa-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 class="mt-5 mb-5">Items Stock Outs</h4>
            </div>

            <div class="btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('items_stock_outs.items_stock_out.create') }}" class="btn btn-success" title="Create New Items Stock Out">
                    <span class="fa fa-plus" aria-hidden="true"></span>
                </a>
            </div>

        </div>
        
        @if(count($itemsStockOuts) == 0)
            <div class="panel-body text-center">
                <h4>No Items Stock Outs Available!</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>Items</th>
                            <th>Volume</th>
                            <th>Customers</th>
                            <th>Price</th>
                            <th>Amount Price</th>
                            <th>Users</th>
                            <th>Reference No</th>

                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($itemsStockOuts as $itemsStockOut)
                        <tr>
                            <td>{{ optional($itemsStockOut->item)->name }}</td>
                            <td>{{ $itemsStockOut->volume }}</td>
                            <td>{{ optional($itemsStockOut->customer)->name }}</td>
                            <td>{{ $itemsStockOut->price }}</td>
                            <td>{{ $itemsStockOut->amount_price }}</td>
                            <td>{{ optional($itemsStockOut->user)->id }}</td>
                            <td>{{ $itemsStockOut->reference_no }}</td>

                            <td>

                                <form method="POST" action="{!! route('items_stock_outs.items_stock_out.destroy', $itemsStockOut->id) !!}" accept-charset="UTF-8">
                                <input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}

                                    <div class="btn-group btn-group-xs pull-right" role="group">
                                        <a href="{{ route('items_stock_outs.items_stock_out.show', $itemsStockOut->id ) }}" class="btn btn-info" title="Show Items Stock Out">
                                            <span class="fa fa-search" aria-hidden="true"></span>
                                        </a>
                                        <a href="{{ route('items_stock_outs.items_stock_out.edit', $itemsStockOut->id ) }}" class="btn btn-primary" title="Edit Items Stock Out">
                                            <span class="fa fa-pencil" aria-hidden="true"></span>
                                        </a>

                                        <button type="submit" class="btn btn-danger" title="Delete Items Stock Out" onclick="return confirm(&quot;Delete Items Stock Out?&quot;)">
                                            <span class="fa fa-trash" aria-hidden="true"></span>
                                        </button>
                                    </div>

                                </form>
                                
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            {!! $itemsStockOuts->render() !!}
        </div>
        
        @endif
    
    </div>
@endsection