@extends('layouts.app')

@section('content')

    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 class="mt-5 mb-5">Delivery Orders Details</h4>
            </div>

            <div class="btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('delivery_orders_details.delivery_orders_details.create') }}" class="btn btn-success" title="Create New Delivery Orders Details">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </a>
            </div>

        </div>
        
        @if(count($deliveryOrdersDetailsObjects) == 0)
            <div class="panel-body text-center">
                <h4>No Delivery Orders Details Available!</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>Delivery Orders Details</th>
                            <th>Items</th>
                            <th>Quantity</th>

                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($deliveryOrdersDetailsObjects as $deliveryOrdersDetails)
                        <tr>
                            <td>{{ optional($deliveryOrdersDetails->deliveryOrdersDetail)->id }}</td>
                            <td>{{ optional($deliveryOrdersDetails->item)->name }}</td>
                            <td>{{ $deliveryOrdersDetails->quantity }}</td>

                            <td>

                                <form method="POST" action="{!! route('delivery_orders_details.delivery_orders_details.destroy', $deliveryOrdersDetails->id) !!}" accept-charset="UTF-8">
                                <input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}

                                    <div class="btn-group btn-group-xs pull-right" role="group">
                                        <a href="{{ route('delivery_orders_details.delivery_orders_details.show', $deliveryOrdersDetails->id ) }}" class="btn btn-info" title="Show Delivery Orders Details">
                                            <span class="glyphicon glyphicon-open" aria-hidden="true"></span>
                                        </a>
                                        <a href="{{ route('delivery_orders_details.delivery_orders_details.edit', $deliveryOrdersDetails->id ) }}" class="btn btn-primary" title="Edit Delivery Orders Details">
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                        </a>

                                        <button type="submit" class="btn btn-danger" title="Delete Delivery Orders Details" onclick="return confirm(&quot;Delete Delivery Orders Details?&quot;)">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                        </button>
                                    </div>

                                </form>
                                
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            {!! $deliveryOrdersDetailsObjects->render() !!}
        </div>
        
        @endif
    
    </div>
@endsection