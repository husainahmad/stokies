@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Delivery Orders Details' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('delivery_orders_details.delivery_orders_details.destroy', $deliveryOrdersDetails->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('delivery_orders_details.delivery_orders_details.index') }}" class="btn btn-primary" title="Show All Delivery Orders Details">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('delivery_orders_details.delivery_orders_details.create') }}" class="btn btn-success" title="Create New Delivery Orders Details">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('delivery_orders_details.delivery_orders_details.edit', $deliveryOrdersDetails->id ) }}" class="btn btn-primary" title="Edit Delivery Orders Details">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Delivery Orders Details" onclick="return confirm(&quot;Delete Delivery Orders Details??&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Delivery Orders Details</dt>
            <dd>{{ optional($deliveryOrdersDetails->deliveryOrdersDetail)->id }}</dd>
            <dt>Items</dt>
            <dd>{{ optional($deliveryOrdersDetails->item)->name }}</dd>
            <dt>Quantity</dt>
            <dd>{{ $deliveryOrdersDetails->quantity }}</dd>
            <dt>Description</dt>
            <dd>{{ $deliveryOrdersDetails->description }}</dd>

        </dl>

    </div>
</div>

@endsection