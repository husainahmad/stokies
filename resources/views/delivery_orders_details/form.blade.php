
<div class="form-group {{ $errors->has('delivery_orders_details_id') ? 'has-error' : '' }}">
    <label for="delivery_orders_details_id" class="col-md-2 control-label">Delivery Orders Details</label>
    <div class="col-md-10">
        <select class="form-control" id="delivery_orders_details_id" name="delivery_orders_details_id" required="true">
        	    <option value="" style="display: none;" {{ old('delivery_orders_details_id', optional($deliveryOrdersDetails)->delivery_orders_details_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select delivery orders details</option>
        	@foreach ($deliveryOrdersDetails as $key => $deliveryOrdersDetail)
			    <option value="{{ $key }}" {{ old('delivery_orders_details_id', optional($deliveryOrdersDetails)->delivery_orders_details_id) == $key ? 'selected' : '' }}>
			    	{{ $deliveryOrdersDetail }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('delivery_orders_details_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('items_id') ? 'has-error' : '' }}">
    <label for="items_id" class="col-md-2 control-label">Items</label>
    <div class="col-md-10">
        <select class="form-control" id="items_id" name="items_id" required="true">
        	    <option value="" style="display: none;" {{ old('items_id', optional($deliveryOrdersDetails)->items_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select items</option>
        	@foreach ($items as $key => $item)
			    <option value="{{ $key }}" {{ old('items_id', optional($deliveryOrdersDetails)->items_id) == $key ? 'selected' : '' }}>
			    	{{ $item }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('items_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('quantity') ? 'has-error' : '' }}">
    <label for="quantity" class="col-md-2 control-label">Quantity</label>
    <div class="col-md-10">
        <input class="form-control" name="quantity" type="number" id="quantity" value="{{ old('quantity', optional($deliveryOrdersDetails)->quantity) }}" min="-2147483648" max="2147483647" required="true" placeholder="Enter quantity here...">
        {!! $errors->first('quantity', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
    <label for="description" class="col-md-2 control-label">Description</label>
    <div class="col-md-10">
        <input class="form-control" name="description" type="text" id="description" value="{{ old('description', optional($deliveryOrdersDetails)->description) }}" maxlength="255">
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>
</div>

