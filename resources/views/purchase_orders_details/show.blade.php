@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Purchase Orders Details' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('purchase_orders_details.purchase_orders_details.destroy', $purchaseOrdersDetails->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('purchase_orders_details.purchase_orders_details.index') }}" class="btn btn-primary" title="Show All Purchase Orders Details">
                        <span class="fa fa-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('purchase_orders_details.purchase_orders_details.create') }}" class="btn btn-success" title="Create New Purchase Orders Details">
                        <span class="fa fa-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('purchase_orders_details.purchase_orders_details.edit', $purchaseOrdersDetails->id ) }}" class="btn btn-primary" title="Edit Purchase Orders Details">
                        <span class="fa fa-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Purchase Orders Details" onclick="return confirm(&quot;Delete Purchase Orders Details??&quot;)">
                        <span class="fa fa-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Purchase Orders</dt>
            <dd>{{ optional($purchaseOrdersDetails->purchaseOrder)->created_at }}</dd>
            <dt>Items</dt>
            <dd>{{ optional($purchaseOrdersDetails->item)->name }}</dd>
            <dt>Quantity</dt>
            <dd>{{ $purchaseOrdersDetails->quantity }}</dd>
            <dt>Price</dt>
            <dd>{{ $purchaseOrdersDetails->price }}</dd>
            <dt>Amount</dt>
            <dd>{{ $purchaseOrdersDetails->amount }}</dd>
            <dt>Created At</dt>
            <dd>{{ $purchaseOrdersDetails->created_at }}</dd>
            <dt>Updated At</dt>
            <dd>{{ $purchaseOrdersDetails->updated_at }}</dd>
            <dt>Users</dt>
            <dd>{{ optional($purchaseOrdersDetails->user)->id }}</dd>

        </dl>

    </div>
</div>

@endsection