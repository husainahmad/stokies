
<div class="form-group {{ $errors->has('purchase_orders_id') ? 'has-error' : '' }}">
    <label for="purchase_orders_id" class="col-md-2 control-label">Purchase Orders</label>
    <div class="col-md-10">
        <select class="form-control" id="purchase_orders_id" name="purchase_orders_id" required="true">
        	    <option value="" style="display: none;" {{ old('purchase_orders_id', optional($purchaseOrdersDetails)->purchase_orders_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select purchase orders</option>
        	@foreach ($purchaseOrders as $key => $purchaseOrder)
			    <option value="{{ $key }}" {{ old('purchase_orders_id', optional($purchaseOrdersDetails)->purchase_orders_id) == $key ? 'selected' : '' }}>
			    	{{ $purchaseOrder }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('purchase_orders_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('items_id') ? 'has-error' : '' }}">
    <label for="items_id" class="col-md-2 control-label">Items</label>
    <div class="col-md-10">
        <select class="form-control" id="items_id" name="items_id" required="true">
        	    <option value="" style="display: none;" {{ old('items_id', optional($purchaseOrdersDetails)->items_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select items</option>
        	@foreach ($items as $key => $item)
			    <option value="{{ $key }}" {{ old('items_id', optional($purchaseOrdersDetails)->items_id) == $key ? 'selected' : '' }}>
			    	{{ $item }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('items_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('quantity') ? 'has-error' : '' }}">
    <label for="quantity" class="col-md-2 control-label">Quantity</label>
    <div class="col-md-10">
        <input class="form-control" name="quantity" type="number" id="quantity" value="{{ old('quantity', optional($purchaseOrdersDetails)->quantity) }}" min="-2147483648" max="2147483647" required="true" placeholder="Enter quantity here...">
        {!! $errors->first('quantity', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('price') ? 'has-error' : '' }}">
    <label for="price" class="col-md-2 control-label">Price</label>
    <div class="col-md-10">
        <input class="form-control" name="price" type="number" id="price" value="{{ old('price', optional($purchaseOrdersDetails)->price) }}" min="-9" max="9" required="true" placeholder="Enter price here...">
        {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('amount') ? 'has-error' : '' }}">
    <label for="amount" class="col-md-2 control-label">Amount</label>
    <div class="col-md-10">
        <input class="form-control" name="amount" type="number" id="amount" value="{{ old('amount', optional($purchaseOrdersDetails)->amount) }}" min="-9" max="9" required="true" placeholder="Enter amount here...">
        {!! $errors->first('amount', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('users_id') ? 'has-error' : '' }}">
    <label for="users_id" class="col-md-2 control-label">Users</label>
    <div class="col-md-10">
        <select class="form-control" id="users_id" name="users_id" required="true">
        	    <option value="" style="display: none;" {{ old('users_id', optional($purchaseOrdersDetails)->users_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select users</option>
        	@foreach ($users as $key => $user)
			    <option value="{{ $key }}" {{ old('users_id', optional($purchaseOrdersDetails)->users_id) == $key ? 'selected' : '' }}>
			    	{{ $user }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('users_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

