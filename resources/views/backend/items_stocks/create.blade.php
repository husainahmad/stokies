@extends('backend.layouts.app')
@section ('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
    @include('backend.items.includes.breadcrumb-links')
@endsection

@section('content')

    <div class="panel panel-default">

        <div class="panel-heading clearfix">
            
            <div class="btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('items_stocks.items_stock.index') }}" class="btn btn-primary" title="Show All Items Stock">
                    <span class="fa fa-th-list" aria-hidden="true"></span>
                </a>
            </div>

        </div>

        <div class="panel-body">
        
            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            <form method="POST" action="{{ route('items_stocks.items_stock.store') }}" accept-charset="UTF-8" id="create_items_stock_form" name="create_items_stock_form" class="form-horizontal">
            {{ csrf_field() }}
            @include ('backend.items_stocks.form', [
                                        'itemsStock' => null,
                                      ])

                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <input class="btn btn-primary" type="submit" value="Add">
                    </div>
                </div>

            </form>

        </div>
    </div>

@endsection


