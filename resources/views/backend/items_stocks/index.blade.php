@extends('backend.layouts.app')
@section ('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
    @include('backend.items.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="fa fa-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">
            
            <div class="pull-left">
                <h4 class="mt-5 mb-5"></h4>
            </div>
            
            <div class="btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('items_stocks.items_stock.create') }}" class="btn btn-success" title="Create New Items Stock">
                    <span class="fa fa-plus" aria-hidden="true"></span>
                </a>
            </div>

        </div>
        
        @if(count($itemsStocks) == 0)
            <div class="panel-body text-center">
                <h4>No Items Stocks Available!</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>Stock</th>
                            <th>Items</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($itemsStocks as $itemsStock)
                        <tr>
                            <td>{{ $itemsStock->stock }}</td>
                            <td>{{ optional($itemsStock->item)->name }}</td>

                            <td>

                                <form method="POST" action="{!! route('items_stocks.items_stock.destroy', $itemsStock->id) !!}" accept-charset="UTF-8">
                                <input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}

                                    <div class="btn-group btn-group-xs pull-right" role="group">
<!--                                        <a href="{{ route('items_stocks.items_stock.show', $itemsStock->id ) }}" class="btn btn-info" title="Show Items Stock">
                                            <span class="fa fa-search" aria-hidden="true"></span>
                                        </a>-->
                                        <a href="{{ route('items_stocks.items_stock.edit', $itemsStock->id ) }}" class="btn btn-primary" title="Edit Items Stock">
                                            <span class="fa fa-pencil" aria-hidden="true"></span>
                                        </a>

                                        <button type="submit" class="btn btn-danger" title="Delete Items Stock" onclick="return confirm(&quot;Delete Items Stock?&quot;)">
                                            <span class="fa fa-trash" aria-hidden="true"></span>
                                        </button>
                                    </div>

                                </form>
                                
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            {!! $itemsStocks->render() !!}
        </div>
        
        @endif
    
    </div>
    </div>
</div>
@endsection