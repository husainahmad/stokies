
<div class="form-group {{ $errors->has('stock') ? 'has-error' : '' }}">
    <label for="stock" class="col-md-2 control-label">Stock</label>
    <div class="col-md-10">
        <input class="form-control" name="stock" type="number" id="stock" value="{{ old('stock', optional($itemsStock)->stock) }}" min="-2147483648" max="2147483647" required="true" placeholder="Enter stock here...">
        {!! $errors->first('stock', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('items_id') ? 'has-error' : '' }}">
    <label for="items_id" class="col-md-2 control-label">Items</label>
    <div class="col-md-10">
        <select class="form-control" id="items_id" name="items_id" required="true">
        	    <option value="" style="display: none;" {{ old('items_id', optional($itemsStock)->items_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select items</option>
        	@foreach ($items as $key => $item)
			    <option value="{{ $key }}" {{ old('items_id', optional($itemsStock)->items_id) == $key ? 'selected' : '' }}>
			    	{{ $item }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('items_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>