@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Items Stock' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('items_stocks.items_stock.destroy', $itemsStock->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('items_stocks.items_stock.index') }}" class="btn btn-primary" title="Show All Items Stock">
                        <span class="fa fa-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('items_stocks.items_stock.create') }}" class="btn btn-success" title="Create New Items Stock">
                        <span class="fa fa-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('items_stocks.items_stock.edit', $itemsStock->id ) }}" class="btn btn-primary" title="Edit Items Stock">
                        <span class="fa fa-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Items Stock" onclick="return confirm(&quot;Delete Items Stock??&quot;)">
                        <span class="fa fa-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Stock</dt>
            <dd>{{ $itemsStock->stock }}</dd>
            <dt>Items</dt>
            <dd>{{ optional($itemsStock->item)->name }}</dd>
            <dt>Created At</dt>
            <dd>{{ $itemsStock->created_at }}</dd>
            <dt>Updated At</dt>
            <dd>{{ $itemsStock->updated_at }}</dd>
            <dt>Users</dt>
            <dd>{{ optional($itemsStock->user)->id }}</dd>

        </dl>

    </div>
</div>

@endsection