

<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
    <label for="name" class="col-md-2 control-label">Name</label>
    <div class="col-md-10">
        <input class="form-control" name="name" type="text" id="name" value="{{ old('name', optional($termBillings)->name) }}" minlength="1" maxlength="145" required="true" placeholder="Enter name here...">
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>



<div class="form-group {{ $errors->has('days') ? 'has-error' : '' }}">
    <label for="days" class="col-md-2 control-label">Days</label>
    <div class="col-md-10">
        <input class="form-control" name="days" type="text" id="days" value="{{ old('days', optional($termBillings)->days) }}" minlength="1" maxlength="145" required="true" placeholder="Enter how long due date will take...">
        {!! $errors->first('days', '<p class="help-block">:message</p>') !!}
    </div>
</div>

