@extends('backend.layouts.app')
@section ('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
    @include('backend.customers.includes.breadcrumb-links')
@endsection
@section('content')

    <div class="panel panel-default">

        <div class="panel-heading clearfix">
            <div class="btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('customers.customers.index') }}" class="btn btn-primary" title="Show All Customers">
                    <span class="fa fa-th-list" aria-hidden="true"></span>
                </a>
            </div>

        </div>

        <div class="panel-body">
        
            <form method="POST" action="{{ route('customers.customers.store') }}" accept-charset="UTF-8" id="create_customers_form" name="create_customers_form" class="form-horizontal">
            {{ csrf_field() }}
            @include ('backend.customers.form', [
                                        'customers' => null,
                                      ])

                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <input class="btn btn-primary" type="submit" value="Add">
                    </div>
                </div>

            </form>

        </div>
    </div>

@endsection


