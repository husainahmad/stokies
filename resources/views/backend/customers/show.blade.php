@extends('backend.layouts.app')
@section ('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
    @include('backend.customers.includes.breadcrumb-links')
@endsection
@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($customers->name) ? $customers->name : 'Customers' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('customers.customers.destroy', $customers->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('customers.customers.index') }}" class="btn btn-primary" title="Show All Customers">
                        <span class="fa fa-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('customers.customers.create') }}" class="btn btn-success" title="Create New Customers">
                        <span class="fa fa-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('customers.customers.edit', $customers->id ) }}" class="btn btn-primary" title="Edit Customers">
                        <span class="fa fa-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Customers" onclick="return confirm(&quot;Delete Customers??&quot;)">
                        <span class="fa fa-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Name</dt>
            <dd>{{ $customers->name }}</dd>
            <dt>Address</dt>
            <dd>{{ $customers->address }}</dd>
            <dt>Phone</dt>
            <dd>{{ $customers->phone }}</dd>
            <dt>Mobile</dt>
            <dd>{{ $customers->mobile }}</dd>
            <dt>Fax</dt>
            <dd>{{ $customers->fax }}</dd>
            <dt>Npwp</dt>
            <dd>{{ $customers->npwp }}</dd>
            <dt>Created At</dt>
            <dd>{{ $customers->created_at }}</dd>
            <dt>Updated At</dt>
            <dd>{{ $customers->updated_at }}</dd>
            <dt>Email</dt>
            <dd>{{ $customers->email }}</dd>

        </dl>

    </div>
</div>

@endsection