@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Incoming Invoices Details' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('incoming_invoices_details.incoming_invoices_details.destroy', $incomingInvoicesDetails->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('incoming_invoices_details.incoming_invoices_details.index') }}" class="btn btn-primary" title="Show All Incoming Invoices Details">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('incoming_invoices_details.incoming_invoices_details.create') }}" class="btn btn-success" title="Create New Incoming Invoices Details">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('incoming_invoices_details.incoming_invoices_details.edit', $incomingInvoicesDetails->id ) }}" class="btn btn-primary" title="Edit Incoming Invoices Details">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Incoming Invoices Details" onclick="return confirm(&quot;Delete Incoming Invoices Details??&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Incoming Invoices</dt>
            <dd>{{ optional($incomingInvoicesDetails->incomingInvoice)->no }}</dd>
            <dt>Items</dt>
            <dd>{{ optional($incomingInvoicesDetails->item)->name }}</dd>
            <dt>Quantity</dt>
            <dd>{{ $incomingInvoicesDetails->quantity }}</dd>
            <dt>Price</dt>
            <dd>{{ $incomingInvoicesDetails->price }}</dd>
            <dt>Amount</dt>
            <dd>{{ $incomingInvoicesDetails->amount }}</dd>
            <dt>Description</dt>
            <dd>{{ $incomingInvoicesDetails->description }}</dd>

        </dl>

    </div>
</div>

@endsection