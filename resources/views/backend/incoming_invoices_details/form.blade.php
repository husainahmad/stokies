
<div class="form-group {{ $errors->has('incoming_invoices_id') ? 'has-error' : '' }}">
    <label for="incoming_invoices_id" class="col-md-2 control-label">Incoming Invoices</label>
    <div class="col-md-10">
        <select class="form-control" id="incoming_invoices_id" name="incoming_invoices_id" required="true">
        	    <option value="" style="display: none;" {{ old('incoming_invoices_id', optional($incomingInvoicesDetails)->incoming_invoices_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select incoming invoices</option>
        	@foreach ($incomingInvoices as $key => $incomingInvoice)
			    <option value="{{ $key }}" {{ old('incoming_invoices_id', optional($incomingInvoicesDetails)->incoming_invoices_id) == $key ? 'selected' : '' }}>
			    	{{ $incomingInvoice }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('incoming_invoices_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('items_id') ? 'has-error' : '' }}">
    <label for="items_id" class="col-md-2 control-label">Items</label>
    <div class="col-md-10">
        <select class="form-control" id="items_id" name="items_id" required="true">
        	    <option value="" style="display: none;" {{ old('items_id', optional($incomingInvoicesDetails)->items_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select items</option>
        	@foreach ($items as $key => $item)
			    <option value="{{ $key }}" {{ old('items_id', optional($incomingInvoicesDetails)->items_id) == $key ? 'selected' : '' }}>
			    	{{ $item }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('items_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('quantity') ? 'has-error' : '' }}">
    <label for="quantity" class="col-md-2 control-label">Quantity</label>
    <div class="col-md-10">
        <input class="form-control" name="quantity" type="number" id="quantity" value="{{ old('quantity', optional($incomingInvoicesDetails)->quantity) }}" min="-2147483648" max="2147483647" required="true" placeholder="Enter quantity here...">
        {!! $errors->first('quantity', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('price') ? 'has-error' : '' }}">
    <label for="price" class="col-md-2 control-label">Price</label>
    <div class="col-md-10">
        <input class="form-control" name="price" type="number" id="price" value="{{ old('price', optional($incomingInvoicesDetails)->price) }}" min="-9" max="9" required="true" placeholder="Enter price here...">
        {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('amount') ? 'has-error' : '' }}">
    <label for="amount" class="col-md-2 control-label">Amount</label>
    <div class="col-md-10">
        <input class="form-control" name="amount" type="number" id="amount" value="{{ old('amount', optional($incomingInvoicesDetails)->amount) }}" min="-9" max="9" required="true" placeholder="Enter amount here...">
        {!! $errors->first('amount', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
    <label for="description" class="col-md-2 control-label">Description</label>
    <div class="col-md-10">
        <input class="form-control" name="description" type="text" id="description" value="{{ old('description', optional($incomingInvoicesDetails)->description) }}" maxlength="2000">
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>
</div>

