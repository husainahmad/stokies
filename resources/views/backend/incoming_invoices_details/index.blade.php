@extends('layouts.app')

@section('content')

    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 class="mt-5 mb-5">Incoming Invoices Details</h4>
            </div>

            <div class="btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('incoming_invoices_details.incoming_invoices_details.create') }}" class="btn btn-success" title="Create New Incoming Invoices Details">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </a>
            </div>

        </div>
        
        @if(count($incomingInvoicesDetailsObjects) == 0)
            <div class="panel-body text-center">
                <h4>No Incoming Invoices Details Available!</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>Incoming Invoices</th>
                            <th>Items</th>
                            <th>Quantity</th>
                            <th>Price</th>
                            <th>Amount</th>

                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($incomingInvoicesDetailsObjects as $incomingInvoicesDetails)
                        <tr>
                            <td>{{ optional($incomingInvoicesDetails->incomingInvoice)->no }}</td>
                            <td>{{ optional($incomingInvoicesDetails->item)->name }}</td>
                            <td>{{ $incomingInvoicesDetails->quantity }}</td>
                            <td>{{ $incomingInvoicesDetails->price }}</td>
                            <td>{{ $incomingInvoicesDetails->amount }}</td>

                            <td>

                                <form method="POST" action="{!! route('incoming_invoices_details.incoming_invoices_details.destroy', $incomingInvoicesDetails->id) !!}" accept-charset="UTF-8">
                                <input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}

                                    <div class="btn-group btn-group-xs pull-right" role="group">
                                        <a href="{{ route('incoming_invoices_details.incoming_invoices_details.show', $incomingInvoicesDetails->id ) }}" class="btn btn-info" title="Show Incoming Invoices Details">
                                            <span class="glyphicon glyphicon-open" aria-hidden="true"></span>
                                        </a>
                                        <a href="{{ route('incoming_invoices_details.incoming_invoices_details.edit', $incomingInvoicesDetails->id ) }}" class="btn btn-primary" title="Edit Incoming Invoices Details">
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                        </a>

                                        <button type="submit" class="btn btn-danger" title="Delete Incoming Invoices Details" onclick="return confirm(&quot;Delete Incoming Invoices Details?&quot;)">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                        </button>
                                    </div>

                                </form>
                                
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            {!! $incomingInvoicesDetailsObjects->render() !!}
        </div>
        
        @endif
    
    </div>
@endsection