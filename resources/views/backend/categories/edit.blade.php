@extends('backend.layouts.app')
@section ('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
    @include('backend.categories.includes.breadcrumb-links')
@endsection

@section('content')

    <div class="panel panel-default">
  
        <div class="panel-heading clearfix">

            <div class="btn-group btn-group-sm pull-right" role="group">

                <a href="{{ route('categories.categories.index') }}" class="btn btn-primary" title="Show All Categories">
                    <span class="fa fa-th-list" aria-hidden="true"></span>
                </a>

                <a href="{{ route('categories.categories.create') }}" class="btn btn-success" title="Create New Categories">
                    <span class="fa fa-plus" aria-hidden="true"></span>
                </a>

            </div>
        </div>

        <div class="panel-body">
            
            <form method="POST" action="{{ route('categories.categories.update', $categories->id) }}" id="edit_categories_form" name="edit_categories_form" accept-charset="UTF-8" class="form-horizontal">
            {{ csrf_field() }}
            <input name="_method" type="hidden" value="PUT">
            @include ('backend.categories.form', [
                                        'categories' => $categories,
                                      ])

                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <input class="btn btn-primary" type="submit" value="Update">
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection