@extends('backend.layouts.app')
@section ('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
    @include('backend.categories.includes.breadcrumb-links')
@endsection

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($categories->name) ? $categories->name : 'Categories' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('categories.categories.destroy', $categories->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('categories.categories.index') }}" class="btn btn-primary" title="Show All Categories">
                        <span class="fa fa-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('categories.categories.create') }}" class="btn btn-success" title="Create New Categories">
                        <span class="fa fa-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('categories.categories.edit', $categories->id ) }}" class="btn btn-primary" title="Edit Categories">
                        <span class="fa fa-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Categories" onclick="return confirm(&quot;Delete Categories??&quot;)">
                        <span class="fa fa-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Name</dt>
            <dd>{{ $categories->name }}</dd>
            <dt>Description</dt>
            <dd>{{ $categories->description }}</dd>
            <dt>Create At</dt>
            <dd>{{ $categories->create_at }}</dd>
            <dt>Update At</dt>
            <dd>{{ $categories->update_at }}</dd>
            <dt>User</dt>
            <dd>{{ optional($categories->user)->id }}</dd>

        </dl>

    </div>
</div>

@endsection