@extends('backend.layouts.app')
@section ('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
    @include('backend.suppliers.includes.breadcrumb-links')
@endsection

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($suppliers->name) ? $suppliers->name : 'Suppliers' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('suppliers.suppliers.destroy', $suppliers->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('suppliers.suppliers.index') }}" class="btn btn-primary" title="Show All Suppliers">
                        <span class="fa fa-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('suppliers.suppliers.create') }}" class="btn btn-success" title="Create New Suppliers">
                        <span class="fa fa-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('suppliers.suppliers.edit', $suppliers->id ) }}" class="btn btn-primary" title="Edit Suppliers">
                        <span class="fa fa-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Suppliers" onclick="return confirm(&quot;Delete Suppliers??&quot;)">
                        <span class="fa fa-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Name</dt>
            <dd>{{ $suppliers->name }}</dd>
            <dt>Address</dt>
            <dd>{{ $suppliers->address }}</dd>
            <dt>Phone</dt>
            <dd>{{ $suppliers->phone }}</dd>
            <dt>Fax</dt>
            <dd>{{ $suppliers->fax }}</dd>
            <dt>Cp</dt>
            <dd>{{ $suppliers->cp }}</dd>
            <dt>Npwp</dt>
            <dd>{{ $suppliers->npwp }}</dd>
            <dt>Created At</dt>
            <dd>{{ $suppliers->created_at }}</dd>
            <dt>Updated At</dt>
            <dd>{{ $suppliers->updated_at }}</dd>
            <dt>Email</dt>
            <dd>{{ $suppliers->email }}</dd>

        </dl>

    </div>
</div>

@endsection