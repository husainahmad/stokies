
<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
    <label for="name" class="col-md-2 control-label">Name</label>
    <div class="col-md-10">
        <input class="form-control" name="name" type="text" id="name" value="{{ old('name', optional($suppliers)->name) }}" minlength="1" maxlength="145" required="true" placeholder="Enter name here...">
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
    <label for="address" class="col-md-2 control-label">Address</label>
    <div class="col-md-10">        
        <textarea id="address" name="address" rows="6" class="form-control" placeholder="">{{ old('address', optional($suppliers)->address) }}</textarea>
        {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
    <label for="phone" class="col-md-2 control-label">Phone</label>
    <div class="col-md-10">
        <input class="form-control" name="phone" type="text" id="phone" value="{{ old('phone', optional($suppliers)->phone) }}" maxlength="45" placeholder="Enter phone here...">
        {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('fax') ? 'has-error' : '' }}">
    <label for="fax" class="col-md-2 control-label">Fax</label>
    <div class="col-md-10">
        <input class="form-control" name="fax" type="text" id="fax" value="{{ old('fax', optional($suppliers)->fax) }}" maxlength="45" placeholder="Enter fax here...">
        {!! $errors->first('fax', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('cp') ? 'has-error' : '' }}">
    <label for="cp" class="col-md-2 control-label">Cp</label>
    <div class="col-md-10">
        <input class="form-control" name="cp" type="text" id="cp" value="{{ old('cp', optional($suppliers)->cp) }}" maxlength="45" placeholder="Enter cp here...">
        {!! $errors->first('cp', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('npwp') ? 'has-error' : '' }}">
    <label for="npwp" class="col-md-2 control-label">Npwp</label>
    <div class="col-md-10">
        <input class="form-control" name="npwp" type="text" id="npwp" value="{{ old('npwp', optional($suppliers)->npwp) }}" maxlength="45" placeholder="Enter npwp here...">
        {!! $errors->first('npwp', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
    <label for="email" class="col-md-2 control-label">Email</label>
    <div class="col-md-10">
        <input class="form-control" name="email" type="text" id="email" value="{{ old('email', optional($suppliers)->email) }}" maxlength="45" placeholder="Enter email here...">
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>

