@extends('backend.layouts.app')
@section ('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
    @include('backend.delivery_orders.includes.breadcrumb-links')
@endsection
@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Delivery Orders' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('delivery_orders.delivery_orders.destroy', $deliveryOrders->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('delivery_orders.delivery_orders.index') }}" class="btn btn-primary" title="Show All Delivery Orders">
                        <span class="fa fa-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('delivery_orders.delivery_orders.create') }}" class="btn btn-success" title="Create New Delivery Orders">
                        <span class="fa fa-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('delivery_orders.delivery_orders.edit', $deliveryOrders->id ) }}" class="btn btn-primary" title="Edit Delivery Orders">
                        <span class="fa fa-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Delivery Orders" onclick="return confirm(&quot;Delete Delivery Orders??&quot;)">
                        <span class="fa fa-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Customers</dt>
            <dd>{{ optional($deliveryOrders->customer)->name }}</dd>
            <dt>Ref No</dt>
            <dd>{{ $deliveryOrders->ref_no }}</dd>
            <dt>Users</dt>
            <dd>{{ optional($deliveryOrders->user)->id }}</dd>
            <dt>Created At</dt>
            <dd>{{ $deliveryOrders->created_at }}</dd>
            <dt>Updated At</dt>
            <dd>{{ $deliveryOrders->updated_at }}</dd>

        </dl>

    </div>
</div>

@endsection