
<div class="row">
    <div class="col-sm-6">        
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">                                        
                    <div class="form-group row {{ $errors->has('sales_orders_id') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label"  for="sales_orders_id" class="col-md-12 control-label">Refensi SO</label>
                        <div class="col-md-9">                            
                            <select class="form-control sales-orders" id="sales_orders_id" name="sales_orders_id" required="true">
                                    <option value="" style="display: none;" {{ old('sales_orders_id', optional($deliveryOrders)->sales_orders_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select ref SO</option>
                                    @if ((optional($deliveryOrders)->sales_orders_id) !== null)
                                        <option value="{{ optional($deliveryOrders)->sales_orders_id }}" selected>
                                            {{ optional($deliveryOrders)->salesOrders->no }}
                                        </option>
                                    @endif
                                    @foreach ($salesorders as $key => $salesorder)
                                        <option value="{{ $salesorder->id }}">
                                            {{ $salesorder->no }}
                                        </option>
                                    @endforeach
                            </select>

                            {!! $errors->first('customers_id', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>                                           
            </div>   
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('customers_id') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label"  for="customers_id" class="col-md-2 control-label">Customers</label>
                        <div class="col-md-9">
                            <select class="form-control customers_id" id="customers_id" name="customers_id" required="true">
                                        <option value="" style="display: none;" {{ old('customers_id', optional($deliveryOrders)->customers_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select customers</option>
                                    @foreach ($customers as $key => $customer)
                                                <option value="{{ $key }}" {{ old('customers_id', optional($deliveryOrders)->customers_id) == $key ? 'selected' : '' }}>
                                                    {{ $customer }}
                                                </option>
                                            @endforeach
                            </select>

                            {!! $errors->first('customers_id', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>                
            </div>    
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('no') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label"  for="no" class="col-md-2 control-label">No</label>
                        <div class="col-md-9">
                            <input class="form-control" name="no" type="text" id="no" value="{{ old('no', optional($deliveryOrders)->no == '' ? 'SJ-ABL/'.date('y').'/' : optional($deliveryOrders)->no) }}" minlength="1" maxlength="45" required="true" placeholder="Enter ref no here...">
                            {!! $errors->first('no', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>                  
                </div>
            </div>    
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('created_at') ? 'has-error' : '' }}">
                        <label for="created_at" class="col-md-3 col-form-label">Date</label>
                        <div class="col-md-9">
                            <span class="input-group">
                                <span class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                            </span>
                            <input class="form-control" name="created_at" type="text" id="created_at" data-provide="datepicker" value="{{ old('created_at', optional($deliveryOrders)->created_at) == '' ? date('d/m/Y') : old('created_at', optional($deliveryOrders)->created_at)  }}" minlength="1" required="true" placeholder="Enter date here...">
                            {!! $errors->first('created_at', '<p class="help-block">:message</p>') !!}
                        </div>                        
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    <div class="col-sm-6">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">                    
                    <div class="form-group row {{ $errors->has('ref_po') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label"  for="ref_po" class="col-md-12 control-label">Refensi PO</label>
                        <div class="col-md-9">
                            <input class="form-control" name="ref_po" type="text" id="ref_po" value="{{ old('ref_po', optional($deliveryOrders)->ref_po) }}" minlength="1" maxlength="45" required="true" placeholder="Enter ref PO here...">
                            {!! $errors->first('ref_po', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>                                           
            </div>    
            
            <div class="row">
                <div class="col-sm-12">                    
                    <div class="form-group row {{ $errors->has('pic') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label"  for="pic" class="col-md-12 control-label">PIC</label>
                        <div class="col-md-9">
                            <input class="form-control" name="pic" type="text" id="pic" value="{{ old('pic', optional($deliveryOrders)->pic) }}" minlength="1" maxlength="45" required="true" placeholder="Enter pic here...">
                            {!! $errors->first('pic', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('driver') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label"  for="driver" class="col-md-2 control-label">Driver</label>
                        <div class="col-md-9">
                            <input class="form-control" name="driver" type="text" id="driver" value="{{ old('driver', optional($deliveryOrders)->driver) }}" minlength="1" maxlength="45" required="true" placeholder="Enter driver here...">
                            {!! $errors->first('driver', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
            </div>            
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('no_pol') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label"  for="no_pol" class="col-md-2 control-label">NO POL</label>
                        <div class="col-md-9">
                            <input class="form-control" name="no_pol" type="text" id="no_pol" value="{{ old('no_pol', optional($deliveryOrders)->no_pol) }}" minlength="1" maxlength="45" required="true" placeholder="Enter nopol here...">
                            {!! $errors->first('no_pol', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
            </div>            
        </div>
    </div>
</div>




