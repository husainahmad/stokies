@extends('backend.layouts.app')
@section ('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
    @include('backend.delivery_orders.includes.breadcrumb-links')
@endsection

@section('content')

    <div class="panel panel-default">
  
        <div class="panel-heading clearfix">

            <div class="btn-group btn-group-sm pull-right" role="group">

                <a href="{{ route('delivery_orders.delivery_orders.index') }}" class="btn btn-primary" title="Show All Delivery Orders">
                    <span class="fa fa-th-list" aria-hidden="true"></span>
                </a>

                <a href="{{ route('delivery_orders.delivery_orders.create') }}" class="btn btn-success" title="Create New Delivery Orders">
                    <span class="fa fa-plus" aria-hidden="true"></span>
                </a>

            </div>
        </div>

        <div class="panel-body">

            <form method="POST" action="{{ route('delivery_orders.delivery_orders.update', $deliveryOrders->id) }}" id="edit_delivery_orders_form" name="edit_delivery_orders_form" accept-charset="UTF-8" class="form-horizontal">
            {{ csrf_field() }}
            <input name="_method" type="hidden" value="PUT">
            @include ('backend.delivery_orders.form', [
                                        'deliveryOrders' => $deliveryOrders,
                                      ])
                <div class="card">
                    <div class="card-body">                        
                        <div class="table-responsive">  
                            <table class="table table-bordered" id="dynamic_field">  
                                <thead>
                                  <tr>
                                   <th>Item</th>                                   
                                   <th>Quantity</th>
                                   <th>Unit</th>
                                   <th>Actions</th>
                                 </tr>
                              </thead>
                              <tbody>
                                  <?php $i = 0; ?>
                                  @foreach ($deliveryOrdersDetails as $deliveryOrdersDetails)
                
                                  <tr id="row{{$deliveryOrdersDetails->sales_orders_details_id}}">
                                      <input type="hidden" name="sales_order_detail_id[]" value="{{$deliveryOrdersDetails->sales_orders_details_id}}">
                                      <td>
                                          <select class="form-control items-selection" id="items_id[]" name="items_id[]" required="true"  row-index="{{ $i }}">                                               
                                            <option>Please select</option>
                                            @foreach ($items as $key => $item)
                                                @if ($key==$deliveryOrdersDetails->items_id)  
                                                <option value="{{ $key }}" selected>
                                                @else
                                                <option value="{{ $key }}">
                                                @endif
                                                    {{ $item }}
                                                </option>
                                            @endforeach
                                          </select>
                                      </td>
                                      <td width="15%">
                                          <input class="form-control text-count" name="quantity[]" type="number" id="quantity[]" value="{{$deliveryOrdersDetails->quantity}}" min="1" max="2147483647" required="true" placeholder="Enter">
                                      </td>
                                      <td width="10%">
                                          <input class="form-control item-units" name="units[]" type="text" id="units[]" value="{{$deliveryOrdersDetails->item->unit->name}}" placeholder="Enter description here...">                                   
                                      </td>
                                      <td>
                                          <button type="button" class="btn btn-danger-item btn-delete-item-do" title="Delete Item" id="{{$deliveryOrdersDetails->sales_orders_details_id}}" >
                                                 <span class="fa fa-trash" aria-hidden="true"></span>
                                          </button>
                                      </td>           
                                    </tr>
                                  <?php $i++; ?>
                                   @endforeach
                              </tbody>                              
                            </table>                              
                        </div>
                    </div>
                </div>   
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <input class="btn btn-primary" type="submit" value="Update">
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection

@push('after-scripts')    
        <script src="/js/bootstrap-datepicker.min.js"></script>
        <link media="all" type="text/css" rel="stylesheet" href="/css/bootstrap-datepicker.css">
        <link href='/css/select2.min.css' rel='stylesheet' type='text/css'>
        <script src='/js/select2.min.js' type='text/javascript'></script>
        <script>
            var i={{ $i }};

            $(function() {     
                $("#customers_id").select2();      
                $("#sales_orders_id").select2();  
                $('#created_at').datepicker({
                    todayBtn: "linked",
                    language: "en",
                    autoclose: true,
                    todayHighlight: true,
                    format: 'dd/mm/yyyy'
                });

                $( ".items-selection" ).change(function() {
                    var index = $(".items-selection").index(this);                    
                    onItemSelectionChange($(this).val(), index);
                });
                
                $(document).on('click', '.btn-delete-item-so', function(){  
                    var button_id = $(this).attr("id");   
                    $('#row'+button_id+'').remove();  
                });
                
                $('.btn-add-more-item').click(function(){  
                   $('#dynamic_field').append('<tr id="row'+ i +'">'+
                                         '<td>'+
                                         '    <select class="form-control items-selection" id="items_id[]" name="items_id[]" required="true" row-index="'+i+'">'+ optionValues +                                                
                                         '    </select>'+
                                         '</td>'+
                                         '<td>'+
                                         '    <input class="form-control text-count" name="quantity[]" type="number" id="quantity[]" value="" min="1" max="2147483647" required="true" placeholder="Enter">'+
                                         '</td>'+
                                         '<td>'+
                                         '    <input class="form-control text-description" name="description[]" type="text" id="description[]" value="" placeholder="Enter amount here...">'+
                                         '</td>'+
                                         '<td width="10%">'+
                                         '    <button type="button" class="btn btn-danger-item btn-delete-item" title="Delete Item" id="'+ i +'" >'+
                                         '      <span class="fa fa-trash" aria-hidden="true"></span>'+
                                         '    </button>'+
                                         '</td>'+
                                     '</tr>');  
                    $(".text-count").bind('keyup mouseup', function () {
                        countTotal();        
                   });   
                    $(".items-selection").change(function() {                     
                        var index = $(".items-selection").index(this);                    
                        onItemSelectionChange($(this).val(), index);
                    });
                    return false;
               });  
               $(document).on('click', '.btn-delete-item', function(){  
                    var button_id = $(this).attr("id");   
                    $('#row'+button_id+'').remove();  
               });

               $(".text-count").bind('keyup mouseup', function () {
                    countTotal();        
               });
               
               $( ".sales-orders" ).change(function() {
                    var index = $(".sales-orders").index(this);                    
                    onOrderRefSOSelectionChange($(this).val(), index);
                });
               
               function onOrderRefSOSelectionChange(value, index) {
                    console.log(index);               
                    console.log(value);               

                    $.ajax({
                         type: "GET",
                         url: "{{ route('sales_orders_details.sales_orders_details.index') }}/show/json/" + value,
                         success: function( response ) {
                             console.log(response);
                             $('#dynamic_field tbody').empty();

                             $.each(response, function(i, item) {

                                 if (!item.checked) {
                                    $('#dynamic_field').append('<tr id="row'+ item.item.id +'">'+
                                             '<input type="hidden" name="items_id[]" id="items_id[]" value="'+item.item.id+'">'+
                                             '<input type="hidden" name="sales_order_detail_id[]" value="'+item.id+'">'+
                                             '<td>'+ item.item.name +
                                                 '</td>'+
                                                 '<!--<td width="30%">'+
                                                 '    <input class="form-control" name="description[]" type="text" id="description[]" value="'+item.description+'" >'+
                                                 '</td>--><input class="form-control" name="description[]" type="hidden" id="description[]" value="-">'+
                                                 '<td width="15%">'+
                                                 '    <input class="form-control text-count" name="quantity[]" type="number" id="quantity[]" value="'+item.quantity+'" min="0" max="2147483647" required="true" placeholder="Enter">'+
                                                 '</td>'+
                                                 '<td width="10%">'+
                                                 '    <input class="form-control item-units" name="units[]" type="text" id="units[]" value="'+item.unit.name+'" required="true" placeholder="Enter">'+
                                                 '</td>'+
                                                 '<td width="5%">'+
                                                 '    <button type="button" class="btn btn-danger-item btn-delete-item-so" title="Delete Item" id="'+ item.item.id +'" >'+
                                                 '      <span class="fa fa-trash" aria-hidden="true"></span>'+
                                                 '    </button>'+
                                                 '</td>'+
                                         '</tr>'); 
                                }

                                $(document).on('click', '.btn-delete-item-so', function(){  
                                    var button_id = $(this).attr("id");   
                                    $('#row'+button_id+'').remove();  
                                });

                             });
                             countTotal();
                         }
                     });

                }
               function onItemSelectionChange(value, index) {
                   console.log(index);               
                   $.each($('.item-price'), function() {
                        console.log($(".item-price").index(this));  
                        if ($(".item-price").index(this)===index) {                        
                            $.ajax({
                                type: "GET",
                                url: "{{ route('items.items.index') }}/show/json/" + value,
                                success: function( response ) {
                                    console.log(response.price);
                                    $(".item-price").eq(index).val(response.price.purchase_price);
                                    countTotal();
                                }
                            });
                        }
                   });
               }

               function countTotal() {
                    var totalQuantity = 0;
                    var totalAmount = 0;
                    var values = $("input[name='quantity[]']").map(function(){
                        return $(this).val();
                    }).get();

                    console.log(isNaN(values));
                    for (var i = 0, len = values.length; i < len; i++) {
                        try {   
                            var qty = isNaN(parseInt(values[i])) ? 0 : parseInt(values[i]);
                            totalQuantity += qty;                                            
                        } catch (err) {
                            totalQuantity += 0;
                        }                   
                    }
                    console.log(totalQuantity); 
                    $('#totalQuantity').text(totalQuantity);
                    return false;
               }           
            });
            var optionValues = '<option>Please select</option>';
            @foreach ($items as $key => $item)
                optionValues += '<option value="{{ $key }}">'+
                    '{{ $item }}'+
                '</option>';
            @endforeach

        </script>
@endpush