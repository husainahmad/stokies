@extends('backend.layouts.app')
@section ('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
    @include('backend.delivery_orders.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        
        @if(Session::has('success_message'))
            <div class="alert alert-success">
                <span class="fa fa-ok"></span>
                {!! session('success_message') !!}

                <button type="button" class="close" data-dismiss="alert" aria-label="close">
                    <span aria-hidden="true">&times;</span>
                </button>

            </div>
        @endif

        <div class="panel panel-default">

            <div class="panel-heading clearfix">
                
                <form action="{{ url()->current() }}">
                    <div class="col-md-offset-1 col-md-2 pull-left" role="group">                
                            <input type="text" name="keyword" class="form-control" placeholder="Search SJ No...">                                    
                    </div>
                    <div class="col-md-offset-1 col-sm-5 pull-left" >
                    <div class="form-group row {{ $errors->has('customers_id') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label"  for="customers_id" class="col-md-2 control-label">Customers</label>
                        <div class="col-md-9">
                            <select class="form-control customers_id" id="customers_id" name="customers_id" >
                                        <option value="" style="display: none;" disabled selected>Select customers</option>
                                    @foreach ($customers as $key => $customer)
                                                <option value="{{ $key }}" >
                                                    {{ $customer }}
                                                </option>
                                            @endforeach
                            </select>

                            {!! $errors->first('customers_id', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>                
                    <div class="col-md-offset-3 col-md-1 pull-left">
                        <button type="submit" class="btn btn-primary">
                                Search
                            </button>   
                        <br/><br/>
                        
                    </div>
                    </form>    
                    <div class="col-md-offset-4 btn-group btn-group-sm pull-right" role="group">
                        <a href="{{ route('delivery_orders.delivery_orders.create') }}" class="btn btn-success" title="Create New Delivery Orders">
                            <span class="fa fa-plus" aria-hidden="true"></span>
                        </a>
                    </div>

            </div>

            @if(count($deliveryOrdersObjects) == 0)
                <div class="panel-body text-center">
                    <h4>No Delivery Orders Available!</h4>
                </div>
            @else
            <div class="panel-body panel-body-with-table">
                <div class="table-responsive">

                    <table class="table table-striped ">
                        <thead>
                            <tr>
                                <th>No</th>
								<th>Date</th>
                                <th>Customers</th>
                                <th>Ref PO</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($deliveryOrdersObjects as $deliveryOrders)
                            <tr>
                                <td>{{ $deliveryOrders->no }}</td>              
								<td>{{ $deliveryOrders->created_at }}</td>              
                                <td>{{ optional($deliveryOrders->customer)->name }}</td>
                                <td>{{ $deliveryOrders->ref_po }}</td>                       
                                <td>

                                    

                                        <div class="btn-group btn-group-xs pull-right" role="group">
                                            <a target="_blank" href="{{ route('delivery_orders.delivery_orders.print', $deliveryOrders->id ) }}" class="btn btn-info" title="Print Delivery Orders">
                                                <span class="fa fa-print" aria-hidden="true"></span>
                                            </a>
<!--                                            <a href="{{ route('delivery_orders.delivery_orders.show', $deliveryOrders->id ) }}" class="btn btn-info" title="Show Delivery Orders">
                                                <span class="fa fa-search" aria-hidden="true"></span>
                                            </a>-->
                                            <a href="{{ route('delivery_orders.delivery_orders.edit', $deliveryOrders->id ) }}" class="btn btn-primary" title="Edit Delivery Orders">
                                                <span class="fa fa-pencil" aria-hidden="true"></span>
                                            </a>
                                            @if ($deliveryOrders->status==0)
                                            <form method="POST" action="{!! route('delivery_orders.delivery_orders.complete', $deliveryOrders->id) !!}" accept-charset="UTF-8">
                                                <input name="_method" type="hidden" value="PUT">
                                            {{ csrf_field() }}
                                                    <button type="submit" class="btn btn-success" title="Complete" onclick="return confirm(&quot;Complete Process?&quot;)">
                                                        <span class="fa fa-flag" aria-hidden="true"></span>
                                                    </button>
                                            </form>
                                            @else
                                            <form method="POST" action="{!! route('delivery_orders.delivery_orders.uncomplete', $deliveryOrders->id) !!}" accept-charset="UTF-8">
                                                <input name="_method" type="hidden" value="PUT">
                                            {{ csrf_field() }}
                                                    <button type="submit" class="btn btn-success" title="Uncomplete" onclick="return confirm(&quot;Uncomplete Process?&quot;)">
                                                        <span class="fa fa-expand" aria-hidden="true"></span>
                                                    </button>
                                            </form>
                                            @endif
                                            <form method="POST" action="{!! route('delivery_orders.delivery_orders.destroy', $deliveryOrders->id) !!}" accept-charset="UTF-8">
                                                <input name="_method" value="DELETE" type="hidden">
                                                {{ csrf_field() }}
                                            <button type="submit" class="btn btn-danger" title="Delete Delivery Orders" onclick="return confirm(&quot;Delete Delivery Orders?&quot;)">
                                                <span class="fa fa-trash" aria-hidden="true"></span>
                                            </button>
                                                </form>
                                        </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>

            <div class="panel-footer">
                {!! $deliveryOrdersObjects->render() !!}
            </div>

            @endif

        </div>
    </div>    
</div>    
@endsection