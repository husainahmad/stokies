<!-- Load paper.css for happy printing -->
<link rel="stylesheet" href="../../css/paper.css">
<!--<link rel="stylesheet" href="../../fonts/Dotimatrix_5/stylesheet.css">-->
<!-- Set page size here: A5, A4 or A3 -->
<!-- Set also "landscape" if you need -->
<style>
    body {
        display: block; 
        width: 21cm;
        height: 27.7cm;        
        margin-top: 50px;
        margin-left: 20px;
        margin-right: 20px;
        margin-bottom: 20px;        
        padding: 0;
        background-color: #FAFAFA;
        font-size: smaller;
        font-family: Consolas;
    }
    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }
    .page {
        width: 21cm;
        min-height: 27.7cm;
        padding: 2cm;
        margin: 1cm auto;
        border: 1px #D3D3D3 solid;
        border-radius: 5px;
        background: white;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }
    .subpage {
        padding: 1cm;
        border: 5px red solid;
        height: 256mm;
        outline: 2cm #FFEAEA solid;
    }
    table th, table td {
        border-bottom: 1px solid #000;
        border-right: 1px solid #000;
        padding: 5px 10px 5px 5px;
    }
    @page {
        size: A4;
        margin: 0;
    }
    @media print {
        .page {
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
        }
    }
</style>
    <div class="book">
        <br/>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td width="60%" style='border: 0px; vertical-align: top'>                                        
                    <table width="100%" cellpadding="0" cellspacing="0" style='border: 0px solid #000;'>
                        <tr>
                            <td style='border: 0px; padding: 0px;'>                                                                
                                <table width="100%" cellpadding="0" cellspacing="0">                        
                                    <tr>
                                        <td width="25%" style='border: 0px; padding: 0px;'>
                                            <img src="/img/logo.jpg">
                                        </td>
                                        <td style='border: 0px; padding: 0px; font-size: small; vertical-align: middle'>
                                            <strong><div style="font-size: large">PT. ARGA BAJA LESTARI</div></strong><br>  
                                            <strong>Steel & Material Building Distributor</strong><br>
                                            Jl. Raya Jatiwaringin No. 24B Jati Cempaka<br/>
                                            Pondok Gede. Bekasi 17416<br/>
                                            Telp. <strong>021.84994366</strong><br/>
                                            Fax. <strong>021.84979581</strong><br/>
                                            e-Mail: argabajalestari@yahoo.com<br/>
                                        </td>
                                    </tr>
                                </table>    
                                <br/>
                            </td>
                        </tr>
                        
                    </table>
                </td>
                <td width="40%" style='border: 0px; vertical-align: top; padding: 0px;'>
                    <h2 align="center">{{ !empty($title) ? $title : 'SURAT JALAN' }}</h2>
                    <div align="center"><strong>NO : {{ $deliveryOrders->no }}</strong></div>
                    <br>
                    <table width="100%" cellpadding="0" cellspacing="0">                        
                        <tr>
                            <td style='border: 1px solid #000;padding: 10px;'>
                                Tanggal : {{ $deliveryOrders->created_at }}<br>
                                Kepada YTH : <br/><br/>
                                <strong>{{ optional($deliveryOrders->customer)->name }} </strong><br/>
                                <?php echo nl2br(optional($deliveryOrders->salesOrders)->delivery_address) ?><br>
                                {{ optional($deliveryOrders->customer)->phone }} <br/>
                            </td>
                        </tr>
                    </table>                    
                </td>                
            </tr>
            
            <tr>
                <td style="border: 0px; vertical-align: bottom" >
                    <table width="100%" cellpadding="0" cellspacing="0">                                                                                                              
                        <tr>
                            <td width="25%" style="border: 0px;padding: 0px; vertical-align: bottom">
                               <strong>PO NO</strong>
                            </td>
                            <td width="1%" style="border: 0px;padding: 0px;">&nbsp;:&nbsp;</td>
                            <td width="74%" style="border: 0px;padding: 0px;">
                                {{ $deliveryOrders->ref_po }}
                            </td>
                        </tr>                                                                        
                        <tr>
                            <td width="25%" style="border: 0px;padding: 0px;">
                               <strong>NOPOL</strong>
                            </td>
                            <td width="1%" style="border: 0px;padding: 0px;">&nbsp;:&nbsp;</td>
                            <td width="74%" style="border: 0px;padding: 0px;">
                                {{ $deliveryOrders->no_pol }}
                            </td>
                        </tr>                                     
                    </table>
                </td>
            </tr>
        </table>

        <table width="100%" cellpadding="0" cellspacing="0" style="padding: 10px">  
            <thead>
              <tr>
               <th style='border-left: 1px solid #000;border-top: 1px solid #000;'>No</th>     
               <th style='border-top: 1px solid #000;'>Item</th>                                          
               <th style='border-top: 1px solid #000;'>Quantity</th>               
               <th style='border-top: 1px solid #000;'>Unit</th>
             </tr>
          </thead>
          <tbody>
              <?php $i = 0; ?>
              @foreach ($deliveryOrdersDetails as $deliveryOrdersDetail)
              @if($deliveryOrdersDetail->quantity>0)
              <tr id="row{{ $i }}">
                  <td width="5%" align="right" style="border-left: 1px solid #000;">     
                      {{ $i+1 }}
                  </td> 
                  <td align="left" >
                      {{ optional($deliveryOrdersDetail->item)->name }}
                  </td>
                  <td width="10%" align="right">     
                      {{ number_format($deliveryOrdersDetail->quantity,2,'.',',') }}
                  </td>       
                  <td width="10%" align="left">
                      {{ optional($deliveryOrdersDetail->item)->unit->name }}    
                  </td>                                                      
              </tr>              
              <?php $i++; ?>
              @endif
               @endforeach
          </tbody>          
        </table>  
        <table width="100%" cellpadding="0" cellspacing="0" style="border: 0px; border-bottom: 0px; border-right: 0px; ">
            <tr>
                <td width="30%" style="border: 0px;">                    
                    <strong>Tanda Terima</strong>
                </td>
                <td width="30%" style="border: 0px; text-align: center">                    
                    <strong>Sopir,</strong>
                </td>
                <td width="30%" style="border: 0px; text-align: center" >
                    <strong>Hormat Kami</strong>
                </td>                
            </tr>            
            <tr>
                <td width="30%" style="border: 0px;">                    
                    <br/><br/><br/>
                </td>
                <td width="30%" style="border: 0px;">                    
                </td>
                <td width="30%" style="border: 0px;">
                    
                </td>                
            </tr>
            
            <tr>
                <td width="30%" style="border: 0px;">                    
                </td>
                <td width="30%" style="border: 0px; text-align: center">                    
                    <strong>{{ $deliveryOrders->driver }}</strong>
                </td>
                <td width="30%" style="border: 0px; text-align: center">
                    <strong>Asy'ari</strong>
                </td>                
            </tr>            
        </table>      
        <p></p>
        <p></p>
        
    </div>
<script>
    window.print();
</script>
