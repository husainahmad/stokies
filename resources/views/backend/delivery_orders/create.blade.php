@extends('backend.layouts.app')
@section ('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
    @include('backend.delivery_orders.includes.breadcrumb-links')
@endsection
@section('content')

    <div class="panel panel-default">

        <div class="panel-heading clearfix">
            <div class="btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('delivery_orders.delivery_orders.index') }}" class="btn btn-primary" title="Show All Delivery Orders">
                    <span class="fa fa-th-list" aria-hidden="true"></span>
                </a>
            </div>
        </div>

        <div class="panel-body">
        
            <form method="POST" action="{{ route('delivery_orders.delivery_orders.store') }}" accept-charset="UTF-8" id="create_delivery_orders_form" name="create_delivery_orders_form" class="form-horizontal">
            {{ csrf_field() }}
            @include ('backend.delivery_orders.form', [
                                        'deliveryOrders' => null,
                                      ])
                <div class="card">
                    <div class="card-body">                        
                        <div class="table-responsive">  
                            <table class="table table-bordered" id="dynamic_field">  
                                <thead>
                                  <tr>
                                   <th>Item</th>                                   
<!--                                   <th>Description</th>    -->
                                   <th width="15%">Quantity</th>
                                   <th width="10%">Unit</th>
                                   <th width="10%">Actions</th>
                                 </tr>
                              </thead>
                              <tbody>                                  
                              </tbody>                              
                            </table>                                  
                        </div>
                    </div>
                </div>                       
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <input class="btn btn-primary" type="submit" value="Add">
                    </div>
                </div>

            </form>

        </div>
    </div>

@endsection

@push('after-scripts')    
        <script src="/js/bootstrap-datepicker.js"></script>
        <link media="all" type="text/css" rel="stylesheet" href="/css/bootstrap-datepicker.css">
        <link href='/css/select2.min.css' rel='stylesheet' type='text/css'>
        <script src='/js/select2.min.js' type='text/javascript'></script>
    <script>
        var i=1;
        
        $(function() {                 
                                        
            $("#customers_id").select2();      
            $("#sales_orders_id").select2();      

            $("#no").change(function(){
                $.ajax({
                    type: "GET",
                    url: "{{ route('delivery_orders.delivery_orders.find', null ) }}no=" + $("#no").val(),
                    success: function( response ) {
                        alert('SJ No telah terdaftar, mohon diganti!')
                    }
                });
            });

            $('#created_at').datepicker({
                todayBtn: "linked",
                language: "en",
                autoclose: true,
                todayHighlight: true,
                format: 'dd/mm/yyyy'
            });
            
            $( ".items-selection" ).change(function() {
                var index = $(".items-selection").index(this);                    
                onItemSelectionChange($(this).val(), index);
            });
            
            $( ".sales-orders" ).change(function() {
                var index = $(".sales-orders").index(this);                    
                onOrderRefSOSelectionChange($(this).val(), index);
            });
                        
           $(document).on('click', '.btn-delete-item', function(){  
                var button_id = $(this).attr("id");   
                $('#row'+button_id+'').remove();  
           });
           
           $(".text-count").bind('keyup mouseup', function () {
                countTotal();        
           }); 
           
           function onItemSelectionChange(value, index) {
               console.log(index);               
               $.each($('.item-price'), function() {
                    console.log($(".item-price").index(this));  
                    if ($(".item-price").index(this)===index) {                        
                        $.ajax({
                            type: "GET",
                            url: "{{ route('items.items.index') }}/show/json/" + value,
                            success: function( response ) {
                                console.log(response.price);
                                $(".item-price").eq(index).val(response.price.purchase_price);
                                $(".item-units").eq(index).val(response.unit.name);
                                countTotal();
                            }
                        });
                    }
               });
           }
           
           function onOrderRefSOSelectionChange(value, index) {
               console.log(index);               
               console.log(value);               
               
               $.ajax({
                    type: "GET",
                    url: "{{ route('sales_orders_details.sales_orders_details.index') }}/show/json/" + value,
                    success: function( response ) {
                        console.log(response);
                        $('#dynamic_field tbody').empty();
                        $("#customers_id").val(response.salesOrders.customer.id);
                        $("#ref_po").val(response.salesOrders.ref_po_customer);
                        $("#discount").val(response.salesOrders.discount);
                        
                        $total_dp = 0;
                        $.each(response.salesOrders.sales_orders_dps, function(i, sales_order_dp) {
                            $total_dp+=sales_order_dp.total_dp;
                        });
                        $("#total_dp").val($total_dp);


                        $.each(response.salesOrdersDetails, function(i, item) {
                            console.log(item);
                            var total_left = item.quantity;
                            if (item.total_left>0) {
                                total_left = item.total_left;
                            }
                            if ((!item.checked) || (item.total_left>0)) {
                                $('#dynamic_field').append('<tr id="row'+ item.item.id +'">'+
                                         '<input type="hidden" name="items_id[]" id="items_id[]" value="'+item.item.id+'">'+
                                         '<input type="hidden" name="sales_order_detail_id[]" value="'+item.id+'">'+
                                         '<td>'+ item.item.name +
                                             '</td>'+
                                             '<!--<td width="30%">'+
                                             '    <input class="form-control" name="description[]" type="text" id="description[]" value="'+item.description+'" >'+
                                             '</td>--><input class="form-control" name="description[]" type="hidden" id="description[]" value="-">'+
                                             '<td width="15%">'+
                                             '    <input class="form-control text-count" name="quantity[]" type="number" id="quantity[]" value="'+total_left+'" min="0" max="2147483647" required="true" placeholder="Enter">'+
                                             '</td>'+
                                             '<td width="10%">'+
                                             '    <input class="form-control item-units" name="units[]" type="text" id="units[]" value="'+item.unit.name+'" required="true" placeholder="Enter">'+
                                             '</td>'+
                                             '<td width="5%">'+
                                             '    <button type="button" class="btn btn-danger-item btn-delete-item" title="Delete Item" id="'+ item.item.id +'" >'+
                                             '      <span class="fa fa-trash" aria-hidden="true"></span>'+
                                             '    </button>'+
                                             '</td>'+
                                     '</tr>'); 
                            } 
                                                       
                            $(document).on('click', '.btn-delete-item', function(){  
                                var button_id = $(this).attr("id");   
                                $('#row'+button_id+'').remove();  
                            });
                        });
                        countTotal();
                    }
                });
                        
           }
           
           function countTotal() {
                var totalQuantity = 0;
                var totalAmount = 0;
                var values = $("input[name='quantity[]']").map(function(){
                    return $(this).val();
                }).get();

                var prices = $("input[name='price[]']").map(function(){
                    return $(this).val();
                }).get();

                console.log(isNaN(values));
                for (var i = 0, len = values.length; i < len; i++) {
                    try {   
                        var qty = isNaN(parseInt(values[i])) ? 0 : parseInt(values[i]);
                        totalQuantity += qty;
                        var price = isNaN(parseFloat(prices[i])) ? 0 : parseFloat(prices[i]);
                        var amount = price*qty;
                        $(".text-count-amount").eq(i).val(amount);                        
                    } catch (err) {
                        totalQuantity += 0;
                    }                   
                }
                console.log(totalQuantity); 
                $('#totalQuantity').text(totalQuantity);

                var valueAmountPrice = $("input[name='amount[]']").map(function(){
                    return $(this).val();
                }).get();

                var totalAmountPrice = 0;
                for (var i = 0, len = valueAmountPrice.length; i < len; i++) {
                    try {   
                        totalAmountPrice += isNaN(parseFloat(valueAmountPrice[i])) ? 0 : parseFloat(valueAmountPrice[i]);
                    } catch (err) {
                        totalAmountPrice += 0;
                    }                   
                }
                /*
                console.log(totalAmountPrice); 
                $('#sub_total').val(totalAmountPrice);

                var discount = $('#discount').val();
                console.log(discount);
                var total = totalAmountPrice-discount;
                $('#total').val(total);
                var vat = total*0.11;
                $('#vat').val(vat);

                var grand_total = total-vat;
                $('#grand_total').val(grand_total);
                */
                return false;
           }           
        });
        var optionValues = '<option value="">Please select</option>';
        @foreach ($items as $key => $item)
            optionValues += '<option value="{{ $key }}">'+
                '{{ $item }}'+
            '</option>';
        @endforeach
        
    </script>
@endpush
