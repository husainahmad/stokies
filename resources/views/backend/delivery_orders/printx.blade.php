<!-- Load paper.css for happy printing -->
<link rel="stylesheet" href="dist/paper.css">

<!-- Set page size here: A5, A4 or A3 -->
<!-- Set also "landscape" if you need -->
<style>
    body {
        margin: 0;
        padding: 0;
        background-color: #FAFAFA;
        font: 12pt "Tahoma";
    }
    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }
    .page {
        width: 21cm;
        min-height: 29.7cm;
        padding: 2cm;
        margin: 1cm auto;
        border: 1px #D3D3D3 solid;
        border-radius: 5px;
        background: white;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }
    .subpage {
        padding: 1cm;
        border: 5px red solid;
        height: 256mm;
        outline: 2cm #FFEAEA solid;
    }
    table th, table td {
        border-bottom: 1px solid #000;
        border-right: 1px solid #000;
        padding: 5px 10px 5px 5px;
    }
    @page {
        size: A4;
        margin: 0;
    }
    @media print {
        .page {
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
        }
    }
</style>
    <div class="book">
        <p>&nbsp;</p>
        <p valign="center">
        <h2 align="center">{{ !empty($title) ? $title : 'Delivery Orders' }}</h2>
        </p>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td width="15%" style='border-left: 1px solid #000;border-top: 1px solid #000;'>
                    Customer
                </td>
                <td width="1%" style='border-top: 1px solid #000;'>:</td>
                <td width="40%" style='border-top: 1px solid #000;'>
                    {{ optional($deliveryOrders->customer)->name }}
                </td>
                <td width="15%" style='border-top: 1px solid #000;'>
                    Ref No
                </td>
                <td width="1%" style='border-top: 1px solid #000;'>:</td>
                <td width="40%" style='border-top: 1px solid #000;'>
                    {{ $deliveryOrders->ref_no }}
                </td>
            </tr>
            <tr>
                <td width="15%" style="border-left: 1px solid #000;">
                    Created At
                </td>
                <td width="1%">:</td>
                <td width="40%">
                    {{ $deliveryOrders->created_at }}
                </td>
                <td width="25%">
                   No
                </td>
                <td width="1%">:</td>
                <td width="40%">
                    {{ $deliveryOrders->no }}
                </td>
            </tr>            
        </table>      
        <p></p>
        <table width="100%" cellpadding="0" cellspacing="0">  
            <thead>
              <tr>
               <th style='border-left: 1px solid #000;border-top: 1px solid #000;'>Item</th>
               <th style='border-top: 1px solid #000;'>Quantity</th>
               <th style='border-top: 1px solid #000;'>Description</th>
             </tr>
          </thead>
          <tbody>
              <?php $i = 0; ?>
              @foreach ($deliveryOrdersDetails as $deliveryOrdersDetails)
              <tr id="row{{ $i }}">
                  <td width="30%" style="border-left: 1px solid #000;">
                      {{ optional($deliveryOrdersDetails->item)->name }}
                  </td>
                  <td width="10%" align="right">
                      {{ $deliveryOrdersDetails->quantity }}
                  </td>
                  <td width="40%" align="right">
                      {{ $deliveryOrdersDetails->description }}                           
                  </td>    
              </tr>
              <?php $i++; ?>
               @endforeach
          </tbody>          
        </table>    
    </div>
<script>
    window.print();
</script>
