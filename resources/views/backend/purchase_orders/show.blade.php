@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Purchase Orders' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('purchase_orders.purchase_orders.destroy', $purchaseOrders->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('purchase_orders.purchase_orders.index') }}" class="btn btn-primary" title="Show All Purchase Orders">
                        <span class="fa fa-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('purchase_orders.purchase_orders.create') }}" class="btn btn-success" title="Create New Purchase Orders">
                        <span class="fa fa-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('purchase_orders.purchase_orders.edit', $purchaseOrders->id ) }}" class="btn btn-primary" title="Edit Purchase Orders">
                        <span class="fa fa-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Purchase Orders" onclick="return confirm(&quot;Delete Purchase Orders??&quot;)">
                        <span class="fa fa-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Suppliers</dt>
            <dd>{{ optional($purchaseOrders->supplier)->name }}</dd>
            <dt>Created At</dt>
            <dd>{{ $purchaseOrders->created_at }}</dd>
            <dt>Updated At</dt>
            <dd>{{ $purchaseOrders->updated_at }}</dd>
            <dt>Purchase No</dt>
            <dd>{{ $purchaseOrders->purchase_no }}</dd>
            <dt>Users</dt>
            <dd>{{ optional($purchaseOrders->user)->id }}</dd>
            <dt>Total Quantity</dt>
            <dd>{{ $purchaseOrders->total_quantity }}</dd>
            <dt>Total Amount</dt>
            <dd>{{ $purchaseOrders->total_amount }}</dd>
            <dt>Status</dt>
            <dd>{{ $purchaseOrders->status }}</dd>
            <dt>Ordered At</dt>
            <dd>{{ $purchaseOrders->ordered_at }}</dd>

        </dl>

    </div>
</div>

@endsection