<br/>
<br/>
<div class="row">
    <div class="col-sm-6">        
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('suppliers_id') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label" for="suppliers_id">Suppliers</label>       
                        <div class="col-md-9">
                            <select class="form-control" id="suppliers_id" name="suppliers_id" required="true">
                                        <option value="" style="display: none;" {{ old('suppliers_id', optional($purchaseOrders)->suppliers_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select suppliers</option>
                                    @foreach ($suppliers as $key => $supplier)
                                                <option value="{{ $key }}" {{ old('suppliers_id', optional($purchaseOrders)->suppliers_id) == $key ? 'selected' : '' }}>
                                                    {{ $supplier }}
                                                </option>
                                            @endforeach
                            </select>

                            {!! $errors->first('suppliers_id', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('ordered_at') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label" for="ordered_at">Ordered At</label>
                        <div class="col-md-9">
                            <div class="input-group">
                                <span class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                </span>
                            <input class="form-control" name="ordered_at" type="text" id="ordered_at" data-provide="datepicker" value="{{ old('ordered_at', optional($purchaseOrders)->getOrderAtID()) }}" minlength="1" required="true" placeholder="Enter ordered at here...">
                                {!! $errors->first('ordered_at', '<p class="help-block">:message</p>') !!}                        
                            </div>
                        </div>    
                    </div>                    
                </div>
            </div>
             <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('arrived_at') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label" for="arrived_at">Arrived At</label>
                        <div class="col-md-9">
                            <div class="input-group">
                                <span class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                </span>
                            <input class="form-control" name="arrived_at" type="text" id="arrived_at" data-provide="datepicker" value="{{ old('arrived_at', optional($purchaseOrders)->getArrivedAtID()) }}" minlength="1" required="true" placeholder="Enter arrived at here...">
                                {!! $errors->first('arrived_at', '<p class="help-block">:message</p>') !!}      
                            </div> 
                        </div>    
                    </div>                     
                </div>                 
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('tc') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label" for="tc">Terms & Condition</label>
                        <div class="col-md-9">
                            <textarea id="tc" name="tc" rows="6" class="form-control" placeholder="" required="true">{{ old('tc', optional($purchaseOrders)->tc) }}</textarea>
                        </div>                          
                    </div> 
                </div>
            </div>            
            
        </div>
    </div>
    <div class="col-sm-6">        
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('purchase_no') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label" for="purchase_no">Purchase No</label>
                        <div class="col-md-9">
                            <input class="form-control" name="purchase_no" type="text" id="purchase_no" value="{{ old('purchase_no', optional($purchaseOrders)->purchase_no) == '' ? 'PO-ABL/'.date('y').'/' : old('purchase_no', optional($purchaseOrders)->purchase_no) }}" minlength="1" maxlength="45" required="true" placeholder="Enter purchase no here...">
                                {!! $errors->first('purchase_no', '<p class="help-block">:message</p>') !!}                        
                        </div>
                    </div>                                          
                </div>
            </div>  
            
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('term_billing_id') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label" for="term_billing_id">TOC</label>
                        <div class="col-md-9">
                            <select class="form-control" id="term_billing_id" name="term_billing_id" required="true">
                                       <option value="" style="display: none;" {{ old('term_billing_id', optional($purchaseOrders)->term_billing_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select term billing</option>
                                   @foreach ($termBillings as $key => $termBilling)
                                               <option value="{{ $key }}" {{ old('term_billing_id', optional($purchaseOrders)->term_billing_id) == $key ? 'selected' : '' }}>
                                                   {{ $termBilling }}
                                               </option>
                                           @endforeach
                           </select>
                           {!! $errors->first('term_billing_id', '<p class="help-block">:message</p>') !!}
                        </div>                        
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('is_tax') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label" for="arrived_at">Tax</label>
                        <div class="col-md-9">
                            <div class="input-group">
                                {{ 
                                    html()->label(
                                    html()->checkbox('is_tax', optional($purchaseOrders)->is_tax == 1 ? 'checked' : '')                                        
                                          ->class('switch-input')
                                          ->id('is_tax').'<span class="switch-label"></span><span class="switch-handle"></span>')
                                          ->class('switch switch-sm switch-3d switch-primary')
                                          ->for('is_tax') }}   
                            </div>  
                        </div>                          
                    </div> 
                </div>
            </div> 
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('tax') ? 'has-error' : '' }}">                        
                            <label class="col-md-3 col-form-label" for="tax">Tax Persen</label>
                        <div class="col-md-9">
                            <input class="form-control" name="tax" type="text" id="tax" value="{{ old('tax', optional($purchaseOrders)->tax) == '' ? '0.11' : old('tax', optional($purchaseOrders)->tax) }}" minlength="1" maxlength="45" required="true" placeholder="Enter...">
                                {!! $errors->first('tax', '<p class="help-block">:message</p>') !!}                        
                        </div>                    
                    </div> 
                </div>
            </div>                        
        </div>
    </div>
    <div class="col-sm-6">                
    </div>
</div>