<!-- Load paper.css for happy printing -->
<link rel="stylesheet" href="../../css/paper.css">

<!-- Set page size here: A5, A4 or A3 -->
<!-- Set also "landscape" if you need -->
<style>
    body {
        width: 21cm;
        height: 27.7cm;
        margin-top: 10px;
        margin-left: 50px;
        margin-right: 50px;
        margin-bottom: 10px; 
        padding: 0;
        background-color: #FAFAFA;
        color: #000;
        font: 14pt "Tahoma";
    }
    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }
    .page {
        width: 21cm;
        min-height: 27.7cm;
        padding: 2cm;
        margin: 1cm auto;
        border: 1px #D3D3D3 solid;
        border-radius: 5px;
        background: white;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }
    .subpage {
        padding: 1cm;
        border: 5px red solid;
        height: 256mm;
        outline: 2cm #FFEAEA solid;
    }
    hr.style2 {
	border-top: 3px double #4286f4;
    }
    table th, table td {
        border-bottom: 1px solid #000;
        border-right: 1px solid #000;
        padding: 5px 5px 5px 5px;
    }
    #container {
        min-height:100%;
        position:relative;
    }
    #header {
    }
    #body {
        padding-bottom:250px;   /* Height of the footer */
    }
    #footer {
        position:absolute;
        bottom:110;
        width:100%;
        height:100px;   /* Height of the footer */
     }
    @page {
        size: A4;
        margin: 0;
    }
    @media print {
        .page {
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
        }
    }
</style>
    <div id="container">
        <div id="header">
            <table width="100%" cellpadding="0" cellspacing="0">                        
                <tr>
                    <td style='border: 0px; padding: 0px; vertical-align: top;' align="center">
                        <img src="/img/logo2.jpg">
                    </td>
                </tr>
                <tr>
                    <td style='border: 0px; padding: 0px; vertical-align: top;' align="center">
                        <br>
                    </td>
                </tr>
                <tr>
                    <td style='border: 0px; padding: 0px; vertical-align: top;' align="center">
                        <h2 align="center">{{ !empty($title) ? $title : 'PURCHASE ORDER' }}</h2>
                    </td>
                </tr>
            </table> 
        </div>    
        <div id="body">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="50%" style='border: 0px;vertical-align: top;'>                                        
                        <table width="100%" cellpadding="0" cellspacing="0" style='border: 0px solid #000;'>
                            <tr>
                                <td style="border: 0px;">
                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td width="25%" style='border: 0px;padding: 0px;'>
                                                <strong>NO</strong>
                                            </td>
                                            <td width="1%" style="border: 0px; padding: 0px;">:&nbsp;</td>
                                            <td width="74%" style='border: 0px;padding: 0px;'>
                                                <strong>{{ $purchaseOrders->purchase_no }}</strong>
                                            </td>                                        
                                        </tr>
                                        <tr>
                                            <td width="25%" style="border: 0px;padding: 0px;">
                                                DATE
                                            </td>
                                            <td width="1%" style="border: 0px; padding: 0px;">:&nbsp;</td>
                                            <td width="74%" style="border: 0px; padding: 0px;">
                                                <?php echo date_format(date_create($purchaseOrders->ordered_at), 'd/m/Y') ?>
                                            </td>                                        
                                        </tr>   
                                        <tr>
                                            <td width="25%" style="border: 0px; padding: 0px;">
                                                ARRIVED
                                            </td>
                                            <td width="1%" style="border: 0px; padding: 0px;">:&nbsp;</td>
                                            <td width="74%" style="border: 0px; padding: 0px;">
                                                <?php echo date_format(date_create($purchaseOrders->arrived_at), 'd/m/Y') ?>
                                            </td>                                        
                                        </tr>      
                                        <tr>
                                            <td width="25%" style="border: 0px; padding: 0px;">
                                                <strong>TOC</strong>
                                            </td>
                                            <td width="1%" style="border: 0px; padding: 0px;">:&nbsp;</td>
                                            <td width="74%" style="border: 0px; padding: 0px;">
                                                <strong>
                                                    {{ optional($purchaseOrders->termBilling)->name }}
                                                </strong>
                                            </td>                                        
                                        </tr>      
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="50%" style='border: 0px; vertical-align: top;'>
                        <table width="100%" cellpadding="0" cellspacing="0">                        
                            <tr>
                                <td style='border: 1px solid #000;padding: 10px;'>                                
                                    <table style='border: 0px solid #000;'>
                                        <tbody>
                                            <tr>
                                                <td width="5%" style='border: 0px solid #000; padding: 0px;'><strong>TO</strong></td>
                                                <td width="2%" style='border: 0px solid #000; padding: 0px;'>:</td>
                                                <td style='border: 0px solid #000; padding: 0px;'>
                                                    <strong>{{ optional($purchaseOrders->supplier)->name }} </strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="5%" style='border: 0px solid #000; padding: 0px;'></td>
                                                <td width="2%" style='border: 0px solid #000; padding: 0px;'></td>
                                                <td style='border: 0px solid #000; padding: 0px;'>
                                                    <?php echo nl2br(optional($purchaseOrders->supplier)->address) ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="5%" style='border: 0px solid #000; padding: 0px;'>TELP</td>
                                                <td width="2%" style='border: 0px solid #000; padding: 0px;'>:</td>
                                                <td style='border: 0px solid #000; padding: 0px;'>
                                                    {{ optional($purchaseOrders->supplier)->phone }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="5%" style='border: 0px solid #000; padding: 0px;'>FAX</td>
                                                <td width="2%" style='border: 0px solid #000; padding: 0px;'>:</td>
                                                <td style='border: 0px solid #000; padding: 0px;'>
                                                    {{ optional($purchaseOrders->supplier)->fax }} 
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="5%" style='border: 0px solid #000; padding: 0px;'><strong>UP</strong></td>
                                                <td width="2%" style='border: 0px solid #000; padding: 0px;'>:</td>
                                                <td style='border: 0px solid #000; padding: 0px;'>
                                                    <strong><?php echo nl2br(optional($purchaseOrders->supplier)->cp) ?></strong>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>                                
                                </td>
                            </tr>
                        </table>                    
                    </td>                
                </tr>
            </table>
            <br/>
            <table width="100%" cellpadding="0" cellspacing="0">  
                <thead>
                  <tr>
                   <th style='border-left: 1px solid #000;border-top: 1px solid #000;'>No</th>               
                   <th style='border-top: 1px solid #000;'>Item</th>
                   <th style='border-top: 1px solid #000;'>Quantity</th>
                   <th style='border-top: 1px solid #000;'>Unit</th>
                   <th style='border-top: 1px solid #000;'>Price<br>(Rp)</th>
                   <th style='border-top: 1px solid #000;'>Amount<br>(Rp)</th>
                 </tr>
              </thead>
              <tbody>
                  <?php $i = 0; ?>
                  @foreach ($purchaseOrdersDetails as $purchaseOrdersDetails)
                  <tr id="row{{ $i }}">
                      <td width="5%" align="center" style="border-left: 1px solid #000;">                      
                          {{ $i+1 }}
                      </td>
                      <td width="30%" align="left">
                          {{ optional($purchaseOrdersDetails->item)->name }}
                      </td>                  
                      <td width="10%" align="right">
                          {{ number_format($purchaseOrdersDetails->quantity,2,'.',',') }}
                      </td>
                      <td width="10%" align="center">
                          {{ optional($purchaseOrdersDetails->item)->unit->name }}
                      </td>
                      <td width="15%" align="right">
                           {{ number_format($purchaseOrdersDetails->price,2,'.',',') }}
                      </td>
                      <td width="15%" align="right">
                            {{ number_format($purchaseOrdersDetails->amount,2,'.',',')  }}                                                     
                      </td>    
                  </tr>
                  <?php $i++; ?>
                   @endforeach
              </tbody>
              <tfoot>

                    <tr>
                        <td colspan="4" style="border-right: 1px solid #000; border-bottom: 0px; border-top: 0px;"></td>
                        <td style="border: 0px; border-right: 1px solid #000; border-bottom: 1px solid #000; text-align: left"><span style="font-weight: bold;">Sub Total</span></td>
                        <td colspan="1" style="text-align: right">
                            <strong> {{ number_format($purchaseOrders->sub_total,0,'.',',') }}</strong>
                        </td>
                    </tr>      
                    <tr>
                        <td colspan="4" style="border-right: 1px solid #000; border-bottom: 0px; border-top: 0px;"></td>
                        <td style="border: 0px; border-right: 1px solid #000; border-bottom: 1px solid #000; text-align: left"><span style="font-weight: bold;">Discount</span></td>
                        <td colspan="1" style="text-align: right">
                            <strong> {{ number_format($purchaseOrders->discount,0,'.',',') }}</strong>
                        </td>
                    </tr>   
                    <tr>
                        <td colspan="4" style="border-right: 1px solid #000; border-bottom: 0px; border-top: 0px;"></td>
                        <td style="border: 0px; border-right: 1px solid #000; border-bottom: 1px solid #000; text-align: left"><span style="font-weight: bold;">VAT</span></td>
                        <td colspan="1" style="text-align: right">  
                            <strong> {{ number_format($purchaseOrders->vat,0,'.',',') }}</strong>
                        </td>
                    </tr>                                   
                    <tr>
                        <td colspan="4" style="border-right: 1px solid #000; border-bottom: 0px; border-top: 0px;"></td>
                        <td style="border: 0px; border-right: 1px solid #000; border-bottom: 1px solid #000; text-align: left"><span style="font-weight: bold;">Grand Total</span></td>
                        <td colspan="1" style="text-align: right">
                            <strong> {{ number_format($purchaseOrders->total_amount,0,'.',',') }}</strong>
                        </td>
                    </tr>                          
                </tfoot>
            </table>  
        </div>
        <div id="footer" style="font-size: larger">
            <table width="100%" cellpadding="0" cellspacing="0" style="border: 0px; border-bottom: 0px; border-right: 0px; ">
                <tr>
                    <td colspan="2" width="100%" style="border: 0px;vertical-align: text-top;"> 
                        <strong>
                            Terms & Conditions
                        </strong>
                        <br>
                        <div>
                        <?php echo nl2br($purchaseOrders->tc) ?>
                        </div>    
                    </td>
                </tr>     
                <tr>
                    <td colspan="2" width="100%" style="border: 0px;vertical-align: text-top;">    
                        <br>
                        Demikianlah permintaan order dari kami, atas perhatian dan kerjasamanya, kami ucapkan terima kasih.
                        <br><br>
                    </td>                                    
                </tr>    

                <tr>
                    <td width="40%" style="border: 0px; font-size: medium;" valign="top">   
                        <strong>Hormat Kami</strong>
                    </td>

                    <td width="60%" style="border: 0px; vertical-align: text-top; text-align: left; font-size: medium;" valign="top">
                            <table style="border: 0px;" cellpadding="0" cellspacing="0" valign="top" >
                                <tr>
                                    <td width="25%" style="border: 0px;" valign="top"><strong>No. NPWP</strong></td>
                                    <td style="border: 0px;" valign="top"><strong>:</strong></td>
                                    <td style="border: 0px;"><strong>90.131.150.6-447.000</strong></td>
                                </tr>
                                <tr>
                                    <td style="border: 0px;" valign="top"><strong>Alamat</strong></td>
                                    <td style="border: 0px;" valign="top"><strong>:</strong></td>
                                    <td style="border: 0px; "><strong>Jl. Wadas Raya Ujung No.60 <br>RT.007 RW.003, Jati Cempaka, Pondok Gede, Kota Bekasi - Jawa Barat</strong></td>
                                </tr>
                            </table>    
                    </td>                
                </tr>     
                <tr>
                    <td width="40%" style="border: 0px; font-size: medium;"  valign="bottom">    
                        <br>
                        <strong>Rusdi</strong>
                    </td>

                    <td width="60%" style="border: 0px; vertical-align: text-top; text-align: left" >                    
                    </td>                
                </tr>     


            </table>      
        </div>
    </div>
<script>
    window.print();
</script>
