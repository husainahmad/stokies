@extends('backend.layouts.app')
@section ('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
    @include('backend.purchase_orders.includes.breadcrumb-links')
@endsection
@section('content')

    <div class="panel panel-default">

        <div class="panel-heading clearfix">            

            <div class="btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('purchase_orders.purchase_orders.index') }}" class="btn btn-primary" title="Show All Purchase Orders">
                    <span class="fa fa-th-list" aria-hidden="true"></span>
                </a>
            </div>

        </div>

        <div class="panel-body">
        
            <form method="POST" action="{{ route('purchase_orders.purchase_orders.store') }}" accept-charset="UTF-8" id="create_purchase_orders_form" name="create_purchase_orders_form" class="form-horizontal">
            {{ csrf_field() }}
            @include ('backend.purchase_orders.form', [
                                        'purchaseOrders' => null,
                                      ])

                <div class="card">
                    <div class="card-body">                        
                        <div class="table-responsive">  
                            <table class="table table-bordered" id="dynamic_field">  
                                <thead>
                                  <tr>
                                   <th>Item</th>
                                   <th>Quantity</th>
                                   <th>Unit</th>
                                   <th>Price</th>
                                   <th>Amount</th>
<!--                                   <th>Description</th>-->
                                   <th>Actions</th>
                                 </tr>
                              </thead>
                              <tbody>
                                  <tr id="row0">
                                      <td>
                                          <select class="form-control items-selection" id="items_id[]" name="items_id[]" required="true"  row-index="0">                                               
                                            <option>Please select</option>  
                                            @foreach ($items as $key => $item)
                                                <option value="{{ $key }}">
                                                    {{ $item }}
                                                </option>
                                            @endforeach
                                          </select>
                                      </td>                                      
                                      <td width="15%">
                                          <input class="form-control text-count" name="quantity[]" type="number" id="quantity[]" data-n-bracket="(,)" value="0" step="any" required="true" placeholder="Enter">
                                      </td>
                                      <td width="10%">
                                           <input class="form-control item-units" name="units[]" type="text" id="units[]" required="true" placeholder="Enter">
                                      </td>
                                      <td width="15%">
                                          <input class="form-control text-count item-price" name="price[]" type="number" id="price[]" value="0" step="any"  required="true"  placeholder="Enter price here...">                                   
                                      </td>
                                      <td width="15%">
                                          <input class="form-control text-count-amount" name="amount[]" type="number" id="amount[]" value="0" step="any" required="true"  placeholder="Enter amount here...">                                   
                                      </td>    
<!--                                      <td width="20%">
                                          <input class="form-control" name="description[]" type="text" id="description[]" value="-" placeholder="Enter description here...">                                   
                                      </td>    -->
                                      <input class="form-control" name="description[]" type="hidden" id="description[]" value="-">
                                      <td width="10%">
                                          <button type="button" class="btn btn-danger-item btn-delete-item" title="Delete Item" id="0" >
                                            <span class="fa fa-trash" aria-hidden="true"></span>
                                          </button>
                                      </td>
                                  </tr>
                              </tbody>
                              <tfoot>
                                  <tr>
                                      <td colspan="2"><span style="font-weight: bold;">Total</span></td>
                                      <td><span style="font-weight: bold;" id="totalQuantity"></span></td>
                                      <td></td>
                                      <td></td>
                                      <td>
                                          <button type="button" class="btn btn-add-more-item" title="Add more item">
                                            <span class="fa fa-plus" aria-hidden="true"></span>
                                          </button>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td colspan="3"></td>
                                      <td><span style="font-weight: bold;">Sub Total</span></td>
                                      <td colspan="2">
                                          <input class="form-control" name="sub_total" type="number" step="any" id="sub_total" value="0" min="1" required="true" placeholder="" >
                                      </td>
                                  </tr>  
                                  <tr>
                                      <td colspan="3"></td>
                                      <td><span style="font-weight: bold;">Discount</span></td>
                                      <td colspan="2">
                                          <input class="form-control" name="discount" type="number" step="any" id="discount" value="0" min="0" required="true" placeholder="" >
                                      </td>
                                  </tr>    
                                  <tr>
                                      <td colspan="3"></td>
                                      <td><span style="font-weight: bold;">Total</span></td>
                                      <td colspan="2">
                                          <input class="form-control" name="total" type="number" step="any" id="total" value="0" required="true" placeholder="" >
                                      </td>
                                  </tr>      
                                  <tr>
                                      <td colspan="3"></td>
                                      <td><span style="font-weight: bold;">VAT</span></td>
                                      <td colspan="2">
                                          <input class="form-control" name="vat" type="number" id="vat" step="any" value="0" required="true" placeholder="" >
                                      </td>
                                  </tr>                                   
                                  <tr>
                                      <td colspan="3"></td>
                                      <td><span style="font-weight: bold;">Grand Total</span></td>
                                      <td colspan="2">
                                          <input class="form-control" name="grand_total" type="number" step="any" id="grand_total" value="0" required="true" placeholder="" >
                                      </td>
                                  </tr>                                        
                              </tfoot>
                            </table>                              
                        </div>
                    </div>
                </div>                              
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <input class="btn btn-primary" type="submit" value="Add">
                    </div>
                </div>

            </form>

        </div>
    </div>

@endsection        
@push('after-scripts')    
        <script src="/js/bootstrap-datepicker.js"></script>
        <link media="all" type="text/css" rel="stylesheet" href="/css/bootstrap-datepicker.css">
        <link href='/css/select2.min.css' rel='stylesheet' type='text/css'>
        <script src='/js/select2.min.js' type='text/javascript'></script>
        <script>
            var i=1;

            $(function() {        
                $("#suppliers_id").select2();                              
                $(".items-selection").select2();      
                $("#purchase_no").change(function(){
                    $.ajax({
                        type: "GET",
                        url: "{{ route('purchase_orders.purchase_orders.find', null ) }}purchase_no=" + $("#purchase_no").val(),
                        success: function( response ) {
                            alert('Purchase No telah terdaftar, mohon diganti!')
                        }
                    });
                });
                
                $("#is_tax").change(function() {
                    countTotal();
                });
                
                $('#arrived_at').datepicker({
                    todayBtn: "linked",
                    language: "en",
                    autoclose: true,
                    todayHighlight: true,
                    format: 'dd/mm/yyyy'
                });
                $('#ordered_at').datepicker({
                    todayBtn: "linked",
                    language: "en",
                    autoclose: true,
                    todayHighlight: true,
                    format: 'dd/mm/yyyy'
                });
                $( ".items-selection" ).change(function() {
                    var index = $(".items-selection").index(this);                    
                    onItemSelectionChange($(this).val(), index);
                });
                $('.btn-add-more-item').click(function(){  
                   $('#dynamic_field').append('<tr id="row'+ i +'">'+
                                         '<td>'+
                                         '    <select class="form-control items-selection" id="items_id'+i+'" name="items_id[]" required="true" row-index="'+i+'">'+ optionValues +                                                
                                         '    </select>'+
                                         '</td>'+
                                         '<td width="15%">'+
                                         '    <input class="form-control text-count" name="quantity[]" step="any" type="number" id="quantity[]" value="0" step="any" required="true" placeholder="Enter">'+
                                         '</td>'+
                                         '<td width="10%">'+
                                         '    <input class="form-control item-units" name="units[]" step="any" type="text" id="units[]" value="" required="true" placeholder="Enter">'+
                                         '</td>'+
                                         '<td width="15%">'+
                                         '    <input class="form-control text-count item-price" name="price[]" step="any" type="number" id="price[]" value="0" step="any" required="true"  placeholder="Enter price here...">'+
                                         '</td>'+
                                         '<td width="15%">'+
                                         '    <input class="form-control text-count-amount" name="amount[]" step="any" type="number" id="amount[]" value="0" step="any" required="true"  placeholder="Enter amount here...">'+
                                         '</td>'+
                                         '<!--<td width="20%">'+
                                         '    <input class="form-control" name="description[]" type="input" id="description[]" value="-" placeholder="Enter description here...">'+
                                         '</td>--><input class="form-control" name="description[]" type="hidden" id="description[]" value="-">'+
                                         '<td width="10%">'+
                                         '    <button type="button" class="btn btn-danger-item btn-delete-item" title="Delete Item" id="'+ i +'" >'+
                                         '      <span class="fa fa-trash" aria-hidden="true"></span>'+
                                         '    </button>'+
                                         '</td>'+
                                     '</tr>');  
                    $(".text-count").bind('keyup mouseup', function () {
                        countTotal();        
                    });   
                    $(".items-selection").change(function() {                     
                        var index = $(".items-selection").index(this);                    
                        onItemSelectionChange($(this).val(), index);
                    });
                    $("#items_id"+i).select2();  
                    i++;    
                    return false;
               });  
               $(document).on('click', '.btn-delete-item', function(){  
                    var button_id = $(this).attr("id");   
                    $('#row'+button_id+'').remove();  
               });

               $(".text-count").bind('keyup mouseup', function () {
                    countTotal();        
               }); 

               function onItemSelectionChange(value, index) {
                   console.log(index);               
                   $.each($('.item-price'), function() {
                        console.log($(".item-price").index(this));  
                        if ($(".item-price").index(this)===index) {                        
                            $.ajax({
                                type: "GET",
                                url: "{{ route('items.items.index') }}/show/json/" + value,
                                success: function( response ) {
                                    console.log(response.price);
                                    $(".item-price").eq(index).val(response.price.purchase_price);
                                    $(".item-units").eq(index).val(response.unit.name);
                                    countTotal();
                                }
                            });
                        }
                   });
               }

               function countTotal() {
                    var totalQuantity = 0;
                    var totalAmount = 0;
                    var values = $("input[name='quantity[]']").map(function(){
                        return $(this).val();
                    }).get();

                    var prices = $("input[name='price[]']").map(function(){
                        return $(this).val();
                    }).get();

                    console.log(isNaN(values));
                    for (var i = 0, len = values.length; i < len; i++) {
                        try {   
                            var qty = isNaN(parseInt(values[i])) ? 0 : parseInt(values[i]);
                            totalQuantity += qty;
                            var price = isNaN(parseFloat(prices[i])) ? 0 : parseFloat(prices[i]);
                            var amount = price*qty;
                            $(".text-count-amount").eq(i).val(amount);                        
                        } catch (err) {
                            totalQuantity += 0;
                        }                   
                    }
                    console.log(totalQuantity); 
                    $('#totalQuantity').text(totalQuantity);

                    var valueAmountPrice = $("input[name='amount[]']").map(function(){
                        return $(this).val();
                    }).get();

                    var totalAmountPrice = 0;
                    for (var i = 0, len = valueAmountPrice.length; i < len; i++) {
                        try {   
                            totalAmountPrice += isNaN(parseFloat(valueAmountPrice[i])) ? 0 : parseFloat(valueAmountPrice[i]);
                        } catch (err) {
                            totalAmountPrice += 0;
                        }                   
                    }
                    console.log(totalAmountPrice); 
                    $('#sub_total').val(totalAmountPrice);

                    var discount = $('#discount').val();
                    
                    console.log(discount);
                    var total = totalAmountPrice-discount;
                    $('#total').val(total);
                    var vat = total*$('#tax').val();
                    console.log("tax:"+$('#tax').val());

                    $('#vat').val(0);
                    
                    if ($("#is_tax").is(":checked")) {
                        $('#vat').val(vat);                    
                    } else {
                        vat = 0;
                    }
                    console.log(vat);
                    
                    var grand_total = total+vat;
                    $('#grand_total').val(grand_total);
                    return false;
               }           
            });
            var optionValues = '<option>Please select</option>';
            @foreach ($items as $key => $item)
                optionValues += '<option value="{{ $key }}">'+
                    '{{ $item }}'+
                '</option>';
            @endforeach

        </script>
@endpush