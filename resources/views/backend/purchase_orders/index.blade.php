@extends('backend.layouts.app')
@section ('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
    @include('backend.purchase_orders.includes.breadcrumb-links')
@endsection
@section('content')
<div class="card">
    <div class="card-body">
        
    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="fa fa-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">
            <form action="{{ url()->current() }}">
            <div class="col-md-offset-1 col-md-3 pull-left" role="group">                
                    <input type="text" name="keyword" class="form-control" placeholder="Search PO NO...">                                    
            </div>
            <div class="col-md-offset-2 col-md-4 pull-left">
                <div class="form-group row {{ $errors->has('suppliers_id') ? 'has-error' : '' }}">
                    <label class="col-md-3 col-form-label" for="suppliers_id">Suppliers</label>       
                    <div class="col-md-9">
                        <select class="form-control" id="suppliers_id" name="suppliers_id" >
                                    <option value="" style="display: none;" disabled selected>Select suppliers</option>
                                @foreach ($suppliers as $key => $supplier)
                                            <option value="{{ $key }}" >
                                                {{ $supplier }}
                                            </option>
                                        @endforeach
                        </select>

                        {!! $errors->first('suppliers_id', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            </div>    
            <div class="col-md-offset-3" col-md-1 pull-left>
                <button type="submit" class="btn btn-primary">
                        Search
                    </button>    
            </div>
            </form>
            <div class="col-md-offset-4 btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('purchase_orders.purchase_orders.create') }}" class="btn btn-success" title="Create New Purchase Orders">
                    <span class="fa fa-plus" aria-hidden="true"></span>
                </a>            
            </div>

        </div>
        
        @if(count($purchaseOrdersObjects) == 0)
            <div class="panel-body text-center">
                <h4>No Purchase Orders Available!</h4>
            </div>
        @else
        
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>Suppliers</th>
                            <th>Purchase No</th>
                            <th>Users</th>
                            <th>Total Quantity</th>
                            <th>Total Amount</th>
                            <th>Ordered At</th>

                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($purchaseOrdersObjects as $purchaseOrders)
                        <tr>
                            <td>{{ optional($purchaseOrders->supplier)->name }}</td>
                            <td>{{ $purchaseOrders->purchase_no }}</td>
                            <td>{{ optional($purchaseOrders->user)->id }}</td>
                            <td>{{ number_format($purchaseOrders->total_quantity,0,'.',',') }}</td>
                            <td>{{ number_format($purchaseOrders->total_amount,0,'.',',') }}</td>
                            <td>{{ date_format(date_create($purchaseOrders->ordered_at), 'd/m/Y') }}</td>

                            <td>                                
                                    <div class="btn-group btn-group-xs pull-right" role="group">
                                        <a target="_blank" href="{{ route('purchase_orders.purchase_orders.print', $purchaseOrders->id ) }}" class="btn btn-info" title="Print Purchase Orders">
                                            <span class="fa fa-print" aria-hidden="true"></span>
                                        </a>
<!--                                        <a href="{{ route('purchase_orders.purchase_orders.show', $purchaseOrders->id ) }}" class="btn btn-info" title="Show Purchase Orders">
                                            <span class="fa fa-search" aria-hidden="true"></span>
                                        </a>-->
                                        <a href="{{ route('purchase_orders.purchase_orders.edit', $purchaseOrders->id ) }}" class="btn btn-primary" title="Edit Purchase Orders">
                                            <span class="fa fa-pencil" aria-hidden="true"></span>
                                        </a>

                                        @if ($purchaseOrders->status==0)
                                        <form method="POST" action="{!! route('purchase_orders.purchase_orders.complete', $purchaseOrders->id) !!}" accept-charset="UTF-8">
                                            <input name="_method" type="hidden" value="PUT">
                                        {{ csrf_field() }}
                                                <button type="submit" class="btn btn-success" title="Complete" onclick="return confirm(&quot;Complete Process?&quot;)">
                                                    <span class="fa fa-flag" aria-hidden="true"></span>
                                                </button>
                                        </form>
                                        @else
                                        <form method="POST" action="{!! route('purchase_orders.purchase_orders.uncomplete', $purchaseOrders->id) !!}" accept-charset="UTF-8">
                                            <input name="_method" type="hidden" value="PUT">
                                        {{ csrf_field() }}
                                                <button type="submit" class="btn btn-uncomplete" title="Uncomplete" onclick="return confirm(&quot;Uncomplete Process?&quot;)">
                                                    <span class="fa fa-expand" aria-hidden="true"></span>
                                                </button>
                                        </form>
                                        @endif
                                        <form method="POST" action="{!! route('purchase_orders.purchase_orders.destroy', $purchaseOrders->id) !!}" accept-charset="UTF-8">
                                          <input name="_method" value="DELETE" type="hidden">
                                            {{ csrf_field() }}
    
                                        <button type="submit" class="btn btn-danger" title="Delete Purchase Orders" onclick="return confirm(&quot;Delete Purchase Orders?&quot;)">
                                            <span class="fa fa-trash" aria-hidden="true"></span>
                                        </button>                                        
                                        </form>    
                                    </div>                               
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            {!! $purchaseOrdersObjects->render() !!}
        </div>
        
        @endif
    
    </div>
    </div>
</div>
@endsection