@extends('backend.layouts.app')
@section ('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
    @include('backend.invoices.includes.breadcrumb-links')
@endsection


@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Invoices' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('invoices.invoices.destroy', $invoices->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('invoices.invoices.index') }}" class="btn btn-primary" title="Show All Invoices">
                        <span class="fa fa-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('invoices.invoices.create') }}" class="btn btn-success" title="Create New Invoices">
                        <span class="fa fa-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('invoices.invoices.edit', $invoices->id ) }}" class="btn btn-primary" title="Edit Invoices">
                        <span class="fa fa-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Invoices" onclick="return confirm(&quot;Delete Invoices??&quot;)">
                        <span class="fa fa-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>No</dt>
            <dd>{{ $invoices->no }}</dd>
            <dt>Ship Via</dt>
            <dd>{{ $invoices->ship_via }}</dd>
            <dt>Purchase No</dt>
            <dd>{{ $invoices->purchase_no }}</dd>
            <dt>Delivery Orders</dt>
            <dd>{{ optional($invoices->deliveryOrder)->ref_no }}</dd>
            <dt>Created At</dt>
            <dd>{{ $invoices->created_at }}</dd>
            <dt>Due Date</dt>
            <dd>{{ $invoices->due_date }}</dd>
            <dt>Customers</dt>
            <dd>{{ optional($invoices->customer)->name }}</dd>
            <dt>Sub Total</dt>
            <dd>{{ $invoices->sub_total }}</dd>
            <dt>Discount</dt>
            <dd>{{ $invoices->discount }}</dd>
            <dt>Total</dt>
            <dd>{{ $invoices->total }}</dd>
            <dt>Vat</dt>
            <dd>{{ $invoices->vat }}</dd>
            <dt>Grand Total</dt>
            <dd>{{ $invoices->grand_total }}</dd>
            <dt>Users</dt>
            <dd>{{ optional($invoices->user)->id }}</dd>
            <dt>Sales Persons</dt>
            <dd>{{ optional($invoices->salesPerson)->id }}</dd>

        </dl>

    </div>
</div>

@endsection