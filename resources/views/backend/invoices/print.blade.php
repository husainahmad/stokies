<!-- Load paper.css for happy printing -->
<link rel="stylesheet" href="../../css/paper.css">
<!--<link rel="stylesheet" href="../../fonts/Dotimatrix_5/stylesheet.css">-->
<!-- Set page size here: A5, A4 or A3 -->
<!-- Set also "landscape" if you need -->
<style>    
    body {
        width: 21cm;
        height: 27.7cm;        
        display: block; 
        margin-top: 50px;
        margin-left: 20px;
        margin-right: 20px;
        margin-bottom: 20px;        
        padding: 0;
        background-color: #FAFAFA;
        font-size: smaller;
        color: #000;
        font-family: Consolas;
    }
    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }
    .page {
        width: 21cm;
        min-height: 27.7cm;
        padding: 2cm;
        margin: 1cm auto;
        border: 1px #D3D3D3 solid;
        border-radius: 5px;
        background: white;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }
    .subpage {
        padding: 1cm;
        border: 5px red solid;
        height: 256mm;
        outline: 2cm #FFEAEA solid;
    }
    
    table th, table td {
        border-bottom: 1px solid #000;
        border-right: 1px solid #000;
        padding: 5px 10px 5px 5px;
    }
    @page {
        size: A4;
        margin: 0;
    }
    @media print {
        .page {
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
            display: block;
        }
        
    }
</style>
    <div>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td width="60%" style='border: 0px;'>                                        
                    <table width="100%" cellpadding="0" cellspacing="0" style='border: 0px solid #000;'>
                        <tr>
                            <td style='border: 0px; padding: 0px;'>                                                                
                                <table width="100%" cellpadding="0" cellspacing="0">                        
                                    <tr>
                                        <td width="25%" style='border: 0px; padding: 0px;'>
                                            <img src="/img/logo.jpg">
                                        </td>
                                        <td style='border: 0px; padding: 0px; vertical-align: middle; font-size: smaller'>
                                            <strong><div style="font-size: x-large">PT. ARGA BAJA LESTARI</div></strong>                                                                                        
                                            Jl. Raya Jatiwaringin No. 24B Jati Cempaka<br/>
                                            Pondok Gede. Bekasi 17416<br/>
                                            Telp. 021.84994366<br/>
                                            Fax. 021.84979581<br/>
                                            e-Mail: argabajalestari@yahoo.com<br/>
                                        </td>
                                    </tr>
                                </table>    
                                <br>
                            </td>
                        </tr>
                        <tr>
                            <td style="border: 0px;">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td width="25%" style='border: 0px;padding: 0px;'>
                                            <strong>NO</strong>
                                        </td>
                                        <td width="1%" style="border: 0px; padding: 0px;">:&nbsp;</td>
                                        <td width="74%" style='border: 0px;padding: 0px;'>
                                            <strong>{{ $invoices->no }}</strong>
                                        </td>                                        
                                    </tr>
                                    <tr>
                                        <td width="25%" style="border: 0px;padding: 0px;">
                                            <strong>DATE</strong>
                                        </td>
                                        <td width="1%" style="border: 0px; padding: 0px;">:&nbsp;</td>
                                        <td width="74%" style="border: 0px; padding: 0px;">
                                            {{ $invoices->created_at }}
                                        </td>                                        
                                    </tr>   
                                    <tr>
                                        <td width="25%" style="border: 0px; padding: 0px;">
                                            <strong>DUE DATE</strong>
                                        </td>
                                        <td width="1%" style="border: 0px; padding: 0px;">:&nbsp;</td>
                                        <td width="74%" style="border: 0px; padding: 0px;">
                                            <?php echo date_format(date_create($invoices->due_date), 'd/m/Y') ?>
                                        </td>                                        
                                    </tr>    
                                    <tr>
                                        <td width="25%" style="border: 0px; padding: 0px;">
                                            <strong>SHIP VIA</strong>
                                        </td>
                                        <td width="1%" style="border: 0px; padding: 0px;">:&nbsp;</td>
                                        <td width="74%" style="border: 0px; padding: 0px;">
                                            {{ $invoices->ship_via }}
                                        </td>                                        
                                    </tr> 
                                    <tr>
                                        <td width="25%" style="border: 0px; padding: 0px;">
                                           <strong>PO NO</strong>
                                        </td>
                                        <td width="1%" style="border: 0px; padding: 0px;">:&nbsp;</td>
                                        <td width="74%" style="border: 0px; padding: 0px;">
                                            {{ $invoices->purchase_no }}
                                        </td>
                                    </tr>                               
                                    
                                    <tr>
                                        <td width="25%" style="border: 0px; padding: 0px;">
                                           <strong>SJ NO</strong>
                                        </td>
                                        <td width="1%" style="border: 0px; padding: 0px;">:&nbsp;</td>
                                        <td width="74%" style="border: 0px; padding: 0px;">
                                            @if(isset($invoiceDeliveryOrders))
                                                @foreach ($invoiceDeliveryOrders as $invoiceDeliveryOrder)
                                                    {{ $invoiceDeliveryOrder->deliveryOrder->no }}<br/>
                                                @endforeach
                                            @endif
                                        </td>
                                    </tr> 
                                    
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td width="40%" style='border: 0px; vertical-align: top;'>
                    <h2 align="center">{{ !empty($title) ? $title : 'INVOICE' }}</h2>
                    <br/>
                    <table width="100%" cellpadding="0" cellspacing="0">                        
                        <tr>
                            <td style='border: 1px solid #000;padding: 10px;'>
                                CUSTOMER : <br/><br/>
                                <strong>{{ optional($invoices->customer)->name }} </strong><br/>
                                <?php echo nl2br(optional($invoices->customer)->address) ?><br>
                                {{ optional($invoices->customer)->phone }} <br/>
                            </td>
                        </tr>
                    </table>                    
                </td>                
            </tr>
        </table>
        <table width="100%" cellpadding="0" cellspacing="0" style="padding: 5px;">  
            <thead>
              <tr>
               <th style='border-top: 1px solid #000;border-left: 1px solid #000;'>No</th>     
               <th style='border-top: 1px solid #000;'>Item</th>           
               <th style='border-top: 1px solid #000;'>QTY</th>
               <th style='border-top: 1px solid #000;'>Unit</th>               
               <th style='border-top: 1px solid #000;'>Price<br>(Rp)</th>
               <th style='border-top: 1px solid #000;'>Amount<br>(Rp)</th>
             </tr>
          </thead>
          <tbody>
              <?php $i = 0; ?>
              @foreach ($invoicesDetails as $invoicesDetail)
              <tr id="row{{ $i }}">
                  <td width="5%" align="right" style="border-left: 1px solid #000;">     
                      {{ $i+1 }}
                  </td> 
                  <td align="left" >
                      @php
                        if ($invoices->is_dp == 1) {
                            echo $invoicesDetail->description;
                        } else {
                            echo optional($invoicesDetail->item)->name;
                        }
                      @endphp
                      
                  </td>                     
                  <td width="10%" align="right">     
                      {{ number_format($invoicesDetail->quantity,2,'.',',') }}
                  </td>
                  <td width="10%" align="center">
                      @php
                        if ($invoices->is_dp == 1) {
                            echo '-';
                        } else {
                            echo optional($invoicesDetail->item)->unit->name;
                        }
                      @endphp
                  </td>
                  <td width="15%" align="right">     
                      {{ number_format($invoicesDetail->price,2,'.',',') }}
                  </td>  
                  <td width="15%" align="right">     
                      {{ number_format($invoicesDetail->amount,2,'.',',') }}
                  </td>                    
              </tr>
              <?php $i++; ?>
               @endforeach
          </tbody>          
          <tfoot>
                <tr>                    
                    <td colspan="4" style="border: 0px;text-align: left;padding-top: 20px;">
                        <span style="font-weight: bold;">SALES PERSON</span> : {{ optional($invoices->salesPerson)->name }}
                    </td>
                    <td style="border: 0px; border-right: 1px solid #000; text-align: left"><span style="font-weight: bold;">Sub Total</span></td>
                    <td style="text-align: right">                        
                        <strong>{{ number_format(round($invoices->sub_total),0,'.',',') }}</strong>
                    </td>
                </tr>                                  
                <tr>
                    <td colspan="4" style="border: 0px;"></td>
                    <td style="border: 0px; border-right: 1px solid #000; text-align: left"><span style="font-weight: bold;">Discount</span></td>
                    <td style="text-align: right">             
                        <strong>{{ number_format(round($invoices->discount),0,'.',',') }}</strong>
                    </td>
                </tr>
                @if (!empty($invoices->dp))
                <tr>
                    <td colspan="4" style="border: 0px;"></td>
                    <td style="border: 0px; border-right: 1px solid #000; text-align: left"><span style="font-weight: bold;">DP</span></td>
                    <td style="text-align: right">             
                        <strong>{{ number_format(round($invoices->dp),0,'.',',') }}</strong>
                    </td>
                </tr>
                @endif
                <tr>
                    <td colspan="4" style="border: 0px;"></td>
                    <td style="border: 0px; border-right: 1px solid #000; text-align: left"><span style="font-weight: bold;">Total</span></td>
                    <td style="text-align: right">        
                        <strong>{{ number_format(round($invoices->total),0,'.',',') }}</strong>
                    </td>
                </tr>      
                <tr>
                    <td colspan="4" style="border: 0px;"></td>
                    <td style="border: 0px; border-right: 1px solid #000; text-align: left"><span style="font-weight: bold;">VAT</span></td>
                    <td style="text-align: right">  
                        <strong>{{ number_format(round($invoices->vat),0,'.',',') }}</strong>
                    </td>
                </tr>  
                <tr>
                    <td colspan="4" style="border: 0px;"></td>
                    <td style="border: 0px; border-right: 1px solid #000; text-align: left"><span style="font-weight: bold;">Grand Total</span></td>
                    <td style="text-align: right">
                        <strong>{{ number_format(round($invoices->grand_total),0,'.',',') }}</strong>
                    </td>
                </tr>                      
            </tfoot>
        </table>  
        <br>
        <table width="100%" cellpadding="0" cellspacing="0" style="padding: 10px;">  
            <tr>
                <td style="border-left: 1px solid #000;border-top: 1px solid #000;"><strong>{{__('labels.general.said') }} : {{strtoupper(Terbilang::make(round($invoices->grand_total,0)))}} {{__('labels.general.currency') }}</strong></td>
            </tr>
        </table>    
        <br>
        <table width="100%" cellpadding="0" cellspacing="0" style="border: 0px; border-bottom: 0px; border-right: 0px; ">
            <tr>
                <td width="60%" style="border: 0px; text-align: left; vertical-align: top;">     
                    <strong>NOTES : <br/></strong>
                    PLEASE TRANSFER YOUR PAYMENT TO OUR ACCOUNT<br/>                        
                        <ul>                            
                            <li>
                                BNI <span style="padding-left:2.8em">:</span> Cabang Daan Mogot<br>
                                NO.REK <span style="padding-left:1.1em">:</span> 0798330927
                            </li>   
                            <li>
                                MANDIRI <span style="padding-left:0.5em">:</span> Cabang Bekasi Jatiwaringin<br>
                                NO.REK <span style="padding-left:1.1em">:</span> 167-00-0289822-8
                            </li>                         
                            <li>
                                BCA <span style="padding-left:2.8em">:</span> Cabang Kalimalang<br>
                                NO.REK <span style="padding-left:1.1em">:</span> 2308937700
                            </li>
                            <li>A/N PT. ARGA BAJA LESTARI        </li>
                        </ul>                                           
                    
                </td>
                <td width="40%" style="border: 0px; text-align: center; vertical-align: top;" >
                    <strong>PT. ARGA BAJA LESTARI</strong>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>                    
                    <strong>Aenul Khuriyah</strong><br>
                    Finance Manager
                </td>              
            </tr>    
            
        </table>      
        <p></p>
        <p></p>
        
    </div>
<script>
    window.print();
</script>

