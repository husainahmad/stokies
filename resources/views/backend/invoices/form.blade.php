<div class="row">
    <div class="col-sm-6">        
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('no') ? 'has-error' : '' }}">
                        <label for="no" class="col-md-3 col-form-label">No</label>
                        <div class="col-md-9">
                            <input class="form-control" name="no" type="text" id="no" value="{{ old('no', optional($invoices)->no) == '' ? 'INV-ABL/'.date('y').'/' : old('no', optional($invoices)->no) }}" minlength="1" maxlength="45" required="true" placeholder="Enter no here...">
                            {!! $errors->first('no', '<p class="help-block">:message</p>') !!} 
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('ship_via') ? 'has-error' : '' }}">
                        <label for="ship_via" class="col-md-3 col-form-label">Ship Via</label>
                        <div class="col-md-9">
                            <input class="form-control" name="ship_via" type="text" id="ship_via" value="{{ old('ship_via', optional($invoices)->ship_via) }}" minlength="1" maxlength="145" required="true" placeholder="Enter ship via here...">
                            {!! $errors->first('ship_via', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('created_at') ? 'has-error' : '' }}">
                        <label for="created_at" class="col-md-3 col-form-label">Date</label>
                        <div class="col-md-9">
                            <span class="input-group">
                                <span class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                            </span>
                            <input class="form-control" name="created_at" type="text" id="created_at" data-provide="datepicker" value="{{ old('created_at', optional($invoices)->created_at) == '' ? date('d/m/Y') : old('created_at', optional($invoices)->created_at)  }}" minlength="1" required="true" placeholder="Enter date here...">
                            {!! $errors->first('created_at', '<p class="help-block">:message</p>') !!}
                        </div>
                        
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('due_date') ? 'has-error' : '' }}">
                        <label for="due_date" class="col-md-3 col-form-label">Due Date</label>
                        <div class="col-md-9">
                            <span class="input-group">
                                <span class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                            </span>
                            <input class="form-control" name="due_date" type="text" id="due_date" data-provide="datepicker" value="{{ old('due_date', optional($invoices)->due_date) == '' ? date('d/m/Y') : old('due_date', date('d/m/Y', strtotime(optional($invoices)->due_date)))  }}" minlength="1" required="true" placeholder="Enter due date here...">
                            {!! $errors->first('due_date', '<p class="help-block">:message</p>') !!}
                        </div>
                        
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    <div class="col-sm-6">        
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('purchase_no') ? 'has-error' : '' }}">
                        <label for="purchase_no" class="col-md-3 col-form-label">Purchase No</label>
                        <div class="col-md-9">
                            <input class="form-control" name="purchase_no" type="text" id="purchase_no" value="{{ old('purchase_no', optional($invoices)->purchase_no) }}" minlength="1" maxlength="45" required="true" placeholder="Enter purchase no here...">
                            {!! $errors->first('purchase_no', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>                
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('sales_persons_id') ? 'has-error' : '' }}">
                        <label for="sales_persons_id" class="col-md-3 col-form-label">Sales Persons</label>
                        <div class="col-md-9">
                            <select class="form-control" id="sales_persons_id" name="sales_persons_id" required="true">
                                        <option value="" style="display: none;" {{ old('sales_persons_id', optional($invoices)->sales_persons_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select sales persons</option>
                                    @foreach ($salesPeople as $key => $salesPerson)
                                                <option value="{{ $key }}" {{ old('sales_persons_id', optional($invoices)->sales_persons_id) == $key ? 'selected' : '' }}>
                                                    {{ $salesPerson }}
                                                </option>
                                            @endforeach
                            </select>

                            {!! $errors->first('sales_persons_id', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('customers_id') ? 'has-error' : '' }}">
                        <label for="customers_id" class="col-md-3 col-form-label">Customers</label>
                        <div class="col-md-9">
                            <select class="form-control" id="customers_id" name="customers_id" required="true">
                                        <option value="" style="display: none;" {{ old('customers_id', optional($invoices)->customers_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select customers</option>
                                    @foreach ($customers as $key => $customer)
                                                <option value="{{ $key }}" {{ old('customers_id', optional($invoices)->customers_id) == $key ? 'selected' : '' }}>
                                                    {{ $customer }}
                                                </option>
                                            @endforeach
                            </select>

                            {!! $errors->first('customers_id', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>























