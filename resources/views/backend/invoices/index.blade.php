@extends('backend.layouts.app')
@section ('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
    @include('backend.invoices.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="fa fa-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">
            
            <form action="{{ url()->current() }}">
            <div class="col-md-offset-1 col-md-2 pull-left" role="group">                
                    <input type="text" name="keyword" class="form-control" placeholder="Search No Invoice...">                                    
            </div>
            <div class="col-md-offset-2 col-sm-5 pull-left">
                <div class="form-group row {{ $errors->has('customer_id') ? 'has-error' : '' }}">
                    <label class="col-md-3 col-form-label"  for="customer_id">Customer</label>
                    <div class="col-md-9">
                        <select class="form-control" id="customer_id" name="customer_id" >
                                <option value="" style="display: none;" disabled selected>Select customer</option>
                                @foreach ($customers as $key => $customer)
                                            <option value="{{ $key }}" >
                                                {{ $customer }}
                                            </option>
                                        @endforeach
                        </select>

                        {!! $errors->first('customer_id', '<p class="help-block">:message</p>') !!}
                    </div>

                </div>
            </div>    
            <div class="col-md-offset-3 col-md-1 pull-left">
                <button type="submit" class="btn btn-primary">
                        Search
                    </button>    
                <br/><br/>
            </div>
            </form>
            
            <div class="col-md-offset-4 btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('invoices.invoices.create') }}" class="btn btn-success" title="Create New Invoices">
                    <span class="fa fa-plus" aria-hidden="true"></span>
                </a>
            </div>

        </div>
        
        @if(count($invoicesObjects) == 0)
            <div class="panel-body text-center">
                <h4>No Invoices Available!</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Ship Via</th>
                            <th>Purchase No</th>
                            <th>Date</th>
                            <th>Due Date</th>
                            <th>Customers</th>
                            <th>Grand Total</th>
                            <th>Sales Persons</th>

                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($invoicesObjects as $invoices)
                        <tr>
                            <td>{{ $invoices->no }}</td>
                            <td>{{ $invoices->ship_via }}</td>
                            <td>{{ $invoices->purchase_no }}</td>
<!--                            <td>{{ optional($invoices->invoiceDeliveryOrders)->no }}</td>-->
                            <td>{{ $invoices->created_at }}</td>
                            <td>{{ date('d/m/Y', strtotime($invoices->due_date)) }}</td>
                            <td>{{ optional($invoices->customer)->name }}</td>                            
                            <td>{{  number_format($invoices->grand_total,0,'.',',') }}</td>
                            <td>{{ optional($invoices->salesPerson)->name }}</td>

                            <td>

                                <form method="POST" action="{!! route('invoices.invoices.destroy', $invoices->id) !!}" accept-charset="UTF-8">
                                <input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}

                                    <div class="btn-group btn-group-xs pull-right" role="group">
                                        <a target="_blank" href="{{ route('invoices.invoices.print', $invoices->id ) }}" class="btn btn-info" title="Show Invoices">
                                            <span class="fa fa-print" aria-hidden="true"></span>
                                        </a>
<!--                                        <a href="{{ route('invoices.invoices.show', $invoices->id ) }}" class="btn btn-info" title="Show Invoices">
                                            <span class="fa fa-search" aria-hidden="true"></span>
                                        </a>-->
                                        <a href="{{ route('invoices.invoices.edit', $invoices->id ) }}" class="btn btn-primary" title="Edit Invoices">
                                            <span class="fa fa-pencil" aria-hidden="true"></span>
                                        </a>

                                        <button type="submit" class="btn btn-danger" title="Delete Invoices" onclick="return confirm(&quot;Delete Invoices?&quot;)">
                                            <span class="fa fa-trash" aria-hidden="true"></span>
                                        </button>
                                    </div>

                                </form>
                                
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
            <div class="panel-footer">
                {!! $invoicesObjects->render() !!}
            </div>
        @endif
    
    </div>
    </div>
</div>
@endsection