@extends('backend.layouts.app')
@section ('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
    @include('backend.invoices.includes.breadcrumb-links')
@endsection


@section('content')

    <div class="panel panel-default">

        <div class="panel-heading clearfix">            
            <div class="btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('invoices.invoices.index') }}" class="btn btn-primary" title="Show All Invoices">
                    <span class="fa fa-th-list" aria-hidden="true"></span>
                </a>
            </div>

        </div>

        <div class="panel-body">
        
            <form method="POST" action="{{ route('invoices.invoices.store') }}" accept-charset="UTF-8" id="create_invoices_form" name="create_invoices_form" class="form-horizontal">
            {{ csrf_field() }}
            @include ('backend.invoices.form', [
                                        'invoices' => null,
                                      ])
                <div class="card">
                    <div class="card-body">                        
                        <div class="table-responsive">  
                            <table class="table table-bordered" id="dynamic_field_do">  
                                <thead>
                                  <tr>
                                   <th>DO NO</th>                                   
                                   <th>Customer</th>       
                                   <th>Refrensi PO</th>
                                   <th>Refrensi SO</th>
                                   <th>Action</th>
                                 </tr>
                              </thead>
                              <tbody>    
                                  <tr id="row-do-0">
                                      <td width="10%">
                                          <select class="form-control delivery-order-selection" id="delivery_orders_id[]" name="delivery_orders_id[]" required="true">
                                                <option value="">Select delivery orders</option>
                                                    @foreach ($deliveryOrders as $key => $deliveryOrder)
                                                        <option value="{{ $deliveryOrder->id }}" >
                                                            {{ $deliveryOrder->no }}
                                                </option>
                                                    @endforeach
                                         </select>

                                        {!! $errors->first('delivery_orders_id[]', '<p class="help-block">:message</p>') !!}

                                      </td>
                                      <td width="10%">
                                          <input class="form-control text-customer" name="customer[]" type="text" id="customer[]" value="" required="true" >
                                      </td>
                                      <td width="10%">
                                          <input class="form-control text-ref-po" name="ref_po[]" type="text" id="ref_po[]" value="" required="true" >
                                      </td>
                                       <td width="10%">
                                           <input class="form-control text-ref-so" name="ref_do[]" type="text" id="ref_do[]" value="" required="true" >
                                      </td>   
                                      <td width="10%">
                                          <button type="button" class="btn btn-danger-do btn-delete-do" name="btn-delete[]" id="btn-delete[]" title="Delete DO" id-row="0" index="0" >
                                            <span class="fa fa-trash" aria-hidden="true"></span>
                                          </button>
                                      </td>
                                  </tr>
                              </tbody>
                              <tfoot>
                                  <tr>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>
                                          <button type="button" class="btn btn-add-more-do" title="Add more item">
                                            <span class="fa fa-plus" aria-hidden="true"></span>
                                          </button>
                                      </td>
                                  </tr>
                              </tfoot>
                            </table>                                  
                        </div>
                                                
                        <div class="table-responsive">  
                            <table class="table table-bordered" id="dynamic_field">  
                                <thead>
                                  <tr>
                                   <th>Item</th>
                                   <th>Quantity</th>
                                   <th>Unit</th>
                                   <th>Price</th>
                                   <th>Amount</th>
<!--                                   <th>Description</th>-->
                                 </tr>
                              </thead>
                              <tbody>                                  
                              </tbody>
                              <tfoot>
                                  <tr>
                                      <td colspan="2"><span style="font-weight: bold;">Total</span></td>
                                      <td><span style="font-weight: bold;" id="totalQuantity"></span></td>
                                      <td></td>
                                      <td></td>
                                  </tr>
                                  <tr>
                                      <td colspan="2"></td>
                                      <td><span style="font-weight: bold;">Sub Total</span></td>
                                      <td colspan="2">
                                          <input class="form-control" name="sub_total" type="number" id="sub_total" value="0" step="any" min="1" required="true"  placeholder="">
                                      </td>
                                  </tr>
                                  <tr>
                                      <td colspan="2"></td>
                                      <td><span style="font-weight: bold;">Discount</span></td>
                                      <td colspan="2">
                                          <input class="form-control text-count" name="discount" type="number" id="discount" value="0" step="any" required="true"  placeholder="">
                                      </td>
                                  </tr>
                                  <tr>
                                      <td colspan="2"></td>
                                      <td><span style="font-weight: bold;">DP</span></td>
                                      <td colspan="2">
                                          <input class="form-control text-count" name="dp" type="number" id="dp" value="0" step="any" required="true"  placeholder="">
                                      </td>
                                  </tr>
                                  <tr>
                                      <td colspan="2"></td>
                                      <td><span style="font-weight: bold;">Total</span></td>
                                      <td colspan="2">
                                          <input class="form-control" name="total" type="number" id="total" value="0" step="any" required="true"   placeholder="">
                                      </td>
                                  </tr>      
                                  <tr>
                                      <td colspan="2"></td>
                                      <td><span style="font-weight: bold;">VAT</span></td>
                                      <td colspan="2">
                                          <input class="form-control" name="vat" type="number" id="vat" value="0" step="any" required="true"   placeholder="">
                                      </td>
                                  </tr>                                   
                                  <tr>
                                      <td colspan="2"></td>
                                      <td><span style="font-weight: bold;">Grand Total</span></td>
                                      <td colspan="2">
                                          <input class="form-control" name="grand_total" type="number" id="grand_total" value="0" step="any" required="true"   placeholder="">
                                      </td>
                                  </tr>                                   
                              </tfoot>
                            </table>                              
                        </div>
                    </div>
                </div>         
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <input class="btn btn-primary" type="submit" value="Add">
                    </div>
                </div>

            </form>

        </div>
    </div>
<input class="form-control" name="hid_days" type="hidden" id="hid_days" value="0">
@endsection

@push('after-scripts')    
        <script src="/js/bootstrap-datepicker.js"></script>
        
        <link media="all" type="text/css" rel="stylesheet" href="/css/bootstrap-datepicker.css">
        <script>
            var i=1;
            var total_dp = new Array(100);
            total_dp[0] = 0;
            var tax = 0.1;
            $(function() {           
                
                $("#no").change(function(){
                    $.ajax({
                        type: "GET",
                        url: "{{ route('invoices.invoices.find', null ) }}no=" + $("#no").val(),
                        success: function( response ) {
                            alert('No telah terdaftar, mohon diganti!')
                        }
                    });
                });
                
                $('#created_at').datepicker({
                    todayBtn: "linked",
                    language: "en",
                    autoclose: true,
                    todayHighlight: true,
                    format: 'dd/mm/yyyy'
                });
                                
                $('#due_date').datepicker({
                    todayBtn: "linked",
                    language: "en",
                    autoclose: true,
                    todayHighlight: true,
                    format: 'dd/mm/yyyy'
                });                
                
                $( ".items-selection" ).change(function() {
                    var index = $(".items-selection").index(this);                    
                    onItemSelectionChange($(this).val(), index);
                });
                
                $( ".delivery-order-selection" ).change(function() {
                    var index = $(".delivery-order-selection").index(this);                    
                    onOrderRefDOSelectionChange($(this).val(), index);
                });
                
                $("#created_at").change(function(){
                    var newDt = stringToDate($("#created_at").val(),"dd/MM/yyyy","/");
                     console.log("Current Date :" + newDt + "<br/>");
                     // add 5 days to the current date
                     newDt.setDate(newDt.getDate() + $("#hid_days").val());
                     console.log("Due Date :" + newDt + "<br/>");

                     var dd = newDt.getDate();
                     var mm = newDt.getMonth() + 1; //January is 0!

                     var yyyy = newDt.getFullYear();
                     if (dd < 10) {
                          dd = '0' + dd;
                     } 
                     if (mm < 10) {
                          mm = '0' + mm;
                     } 
                     var due_date = dd + '/' + mm + '/' + yyyy;

                     $("#due_date").val(due_date);
                });
                
                $('.btn-add-more-do').click(function(){      
                   
                   $('#dynamic_field_do').append('<tr id="row-do-'+ i +'">'+
                                '<td>'+
                                '    <select class="form-control delivery-order-selection" id="delivery_orders_id[]" name="delivery_orders_id[]" required="true" row-index="'+i+'">'+ optionValuesDO +
                                '    </select>'+
                                '</td>'+
                                '<td>'+
                                '    <input class="form-control text-customer" name="customer[]" type="text" id="customer[]" value="" required="true" >'+
                                '</td>'+
                                '<td>'+
                                '    <input class="form-control text-ref-po" name="ref_po[]" type="text" id="ref_po[]" value="" required="true">'+
                                '</td>'+
                                '<td>'+
                                '    <input class="form-control text-ref-so" name="ref_so[]" type="text" id="ref_so[]" value="" required="true">'+
                                '</td>'+
                                '<td width="10%">'+
                                '    <button type="button" class="btn btn-delete-do" title="Delete DO" name="btn-delete[]" id="btn-delete[]" id-row="'+ i +'" >'+
                                '      <span class="fa fa-trash" aria-hidden="true"></span>'+
                                '    </button>'+
                                '</td>'+
                            '</tr>');  
                    total_dp[i] = 0;
                    i++;                    
                    
                    $(".text-count").bind('keyup mouseup', function () {
                        countTotal();        
                    });   
                   
                    $( ".delivery-order-selection" ).change(function() {
                        var index = $(".delivery-order-selection").index(this);                    
                        onOrderRefDOSelectionChange($(this).val(), index);
                    });
                    
                    $( ".btn-delete-do" ).click(function() {
                        var index = $(".btn-delete-do").index(this);       
                        console.log(index);
                        
                        var button_id = $(this).attr("id-row");   
                        
                        $('#row-do-'+button_id+'').remove();  
                        $('#row'+button_id+'').remove();  
                        $('.parentIndex'+button_id).remove();  

                        total_dp[index] = 0;
                        console.log(total_dp);
                        
                        var t_dp = 0;
                        $.each(total_dp, function(i, tot_dp) {  
                            console.log(tot_dp);
                            if (tot_dp!==undefined) 
                            t_dp+=tot_dp;
                        });
                        
                        $("#total_dp").val(t_dp);
                        countTotal();
                    });                   
                    return false;
               });                 
               
               $(".text-count").bind('keyup mouseup', function () {
                    countTotal();        
               });
               
               function onOrderRefDOSelectionChange(value, index) {
                    
                    $.ajax({
                         type: "GET",
                         url: "{{ route('delivery_orders_details.delivery_orders_details.index') }}/show/json/" + value,
                         success: function( response ) {
                             console.log(response);
                             //$('#dynamic_field tbody').empty();
                             
                            $("#purchase_no").val(response.ref_po);
                            $("#customers_id").val(response.customer.id);
                            $("#sales_persons_id").val(response.sales_orders.sales_person_id);
                            $("#ship_via").val(response.sales_orders.transport);
                            tax = response.sales_orders.tax;
                            console.log(response.sales_orders.tax);
                            if (response.sales_orders.term_billing!=undefined) {
                                //due_date
                                //create_at
                                
                                $("#hid_days").val(response.sales_orders.term_billing.days);
                                
                                 var newDt = stringToDate($("#created_at").val(),"dd/MM/yyyy","/");
                                 console.log("Current Date :" + newDt + "<br/>");
                                 // add 5 days to the current date
                                 newDt.setDate(newDt.getDate() + response.sales_orders.term_billing.days);
                                 console.log("Due Date :" + newDt + "<br/>");
                                 
                                 var dd = newDt.getDate();
                                 var mm = newDt.getMonth() + 1; //January is 0!

                                 var yyyy = newDt.getFullYear();
                                 if (dd < 10) {
                                      dd = '0' + dd;
                                 } 
                                 if (mm < 10) {
                                      mm = '0' + mm;
                                 } 
                                 var due_date = dd + '/' + mm + '/' + yyyy;

                                 $("#due_date").val(due_date);
                            }
                             try {
                                 $('.text-customer').each(function(i) {
                                    if (i === index) {
                                        $(this).val(response.customer.name);
                                    }
                                 });

                                 $('.text-ref-po').each(function(i) {
                                    if (i === index) {
                                        $(this).val(response.ref_po);
                                    }
                                 });

                                 $('.text-ref-so').each(function(i) {
                                    if (i === index) {
                                        $(this).val(response.sales_orders.no);
                                    }
                                 });
                             } catch (err) {
                             }                               
                             
                             $("#discount").val(response.sales_orders.discount);
                             
                             if (response.sales_orders_dps.length>0) {
                                 $.each(response.sales_orders_dps, function(i, sales_order_dp) {
                                    total_dp[index] = sales_order_dp.total_dp; 
                                });
                             } else {
                                 total_dp[index] = 0;
                             }
                             
                             console.log(total_dp);
                             
                             var t_dp = 0;
                             $.each(total_dp, function(i, tot_dp) {  
                                 console.log(tot_dp);
                                 if (tot_dp!==undefined) 
                                 t_dp+=tot_dp;
                             });
                             
                             $("#total_dp").val(t_dp);
                             
                             $('.parentIndex'+index).remove();  
                             
                             $.each(response.deliveryOrdersDetails, function(i, order) {
                                  console.log(order);
                                 $('#dynamic_field').append('<tr id="row'+ index + '" class="parentIndex'+ index +'">'+
                                          '<input type="hidden" name="items_id[]" id="items_id[]" value="' + order.item.id +'">' +
                                          '<td width="30%">'+ order.item.name +
                                              '</td>'+
                                              '<td width="10%">'+
                                              '    <input class="form-control text-count" name="quantity[]" type="number" step="any" id="quantity[]" value="'+order.quantity+'" required="true"  placeholder="Enter">'+
                                              '</td>'+
                                              '<td width="20%">'+
                                              '    <input class="form-control item-units" name="units[]" type="text" step="any" id="units[]" value="'+order.unit.name+'" required="true" placeholder="Enter">'+
                                              '</td>'+
                                              '<td width="10%">'+
                                              '    <input class="form-control text-count item-price" name="price[]" step="any" type="number" id="price[]" value="'+order.sales_orders_details.price+'" required="true"  placeholder="Enter price here...">'+
                                              '</td>'+
                                              '<td width="10%">'+
                                              '    <input class="form-control text-count-amount" name="amount[]" step="any" type="number" id="amount[]" value="'+(order.sales_orders_details.price*order.quantity) +'" required="true"  placeholder="Enter amount here...">'+
                                              '</td>'+
                                              '<!--<td width="30%">'+
                                              '    <input class="form-control" name="description[]" type="text" id="description[]" value="'+(order.sales_orders_details.description) +'" placeholder="Enter description here...">'+
                                              '</td--><input class="form-control" name="description[]" type="hidden" id="description[]" value="-">'+
                                      '</tr>'); 

                             });
                             countTotal();
                         }
                     });
               }
               
               function onItemSelectionChange(value, index) {
                   console.log(index);               
                   $.each($('.item-price'), function() {
                        console.log($(".item-price").index(this));  
                        if ($(".item-price").index(this)===index) {                        
                            $.ajax({
                                type: "GET",
                                url: "{{ route('items.items.index') }}/show/json/" + value,
                                success: function( response ) {
                                    console.log(response);
                                    $(".item-price").eq(index).val(response.price.purchase_price);
                                    $(".item-units").eq(index).val(response.unit.name);
                                    countTotal();
                                }
                            });
                        }
                   });
               }

               function stringToDate(_date,_format,_delimiter) {
                    var formatLowerCase=_format.toLowerCase();
                    var formatItems=formatLowerCase.split(_delimiter);
                    var dateItems=_date.split(_delimiter);
                    var monthIndex=formatItems.indexOf("mm");
                    var dayIndex=formatItems.indexOf("dd");
                    var yearIndex=formatItems.indexOf("yyyy");
                    var month=parseInt(dateItems[monthIndex]);
                    month-=1;
                    var formatedDate = new Date(dateItems[yearIndex],month,dateItems[dayIndex]);
                    return formatedDate;
               }
               
               function countTotal() {
                    var totalQuantity = 0;
                    var totalAmount = 0;
                    
                    try { 
                        var values = $("input[name='quantity[]']").map(function(){
                            return $(this).val();
                        }).get();

                        var prices = $("input[name='price[]']").map(function(){
                            return $(this).val();
                        }).get();

                        console.log(isNaN(values));
                        for (var i = 0, len = values.length; i < len; i++) {
                            try {   
                                var qty = isNaN(parseInt(values[i])) ? 0 : parseInt(values[i]);
                                totalQuantity += qty;
                                var price = isNaN(parseFloat(prices[i])) ? 0 : parseFloat(prices[i]);
                                var amount = price*qty;
                                $(".text-count-amount").eq(i).val(amount);                        
                            } catch (err) {
                                totalQuantity += 0;
                            }                   
                        }
                        console.log(totalQuantity); 

                        var valueAmountPrice = $("input[name='amount[]']").map(function(){
                            return $(this).val();
                        }).get();

                        var totalAmountPrice = 0;
                        for (var i = 0, len = valueAmountPrice.length; i < len; i++) {
                            try {   
                                totalAmountPrice += isNaN(parseFloat(valueAmountPrice[i])) ? 0 : parseFloat(valueAmountPrice[i]);
                            } catch (err) {
                                totalAmountPrice += 0;
                            }                   
                        }
                    } catch (err) {
                        totalQuantity += 0;
                    } 
                    
                    $('#totalQuantity').text(totalQuantity);

                    console.log(totalAmountPrice); 
                    $('#sub_total').val(totalAmountPrice);
                    
                    var discount = $('#discount').val();
                    var dp = $('#dp').val();
                    console.log(discount);
                    var total = totalAmountPrice-discount-dp;
                    $('#total').val(total);
                    var vat = total*tax;
                    console.log(tax);
                    $('#vat').val(vat);
                    
                    var grand_total = total+vat;
                    $('#grand_total').val(grand_total);
                    return false;
               }           
            });
            var optionValues = '<option>Please select </option>';
            @foreach ($items as $key => $item)
                optionValues += '<option value="{{ $key }}">'+
                    '{{ $item }}'+
                '</option>';
            @endforeach
 
            var optionValuesDO = '<option>Select delivery order</option>';
            @foreach ($deliveryOrders as $key => $deliveryOrder)
                optionValuesDO += '<option value="{{ $deliveryOrder->id }}">'+
                    '{{ $deliveryOrder->no }}'+
                '</option>';
            @endforeach
            
        </script>
@endpush

