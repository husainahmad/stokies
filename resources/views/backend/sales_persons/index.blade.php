@extends('backend.layouts.app')
@section ('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
    @include('backend.sales_persons.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="fa fa-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">
            <div class="pull-left">
                <h4 class="mt-5 mb-5"></h4>
            </div>
            <div class="btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('sales_persons.sales_persons.create') }}" class="btn btn-success" title="Create New Sales Persons">
                    <span class="fa fa-plus" aria-hidden="true"></span>
                </a>
            </div>

        </div>
        
        @if(count($salesPersonsObjects) == 0)
            <div class="panel-body text-center">
                <h4>No Sales Persons Available!</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Address</th>
                            <th>No Rek</th>
                            <th>Phone</th>
                            <th>Mobile</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($salesPersonsObjects as $salesPersons)
                        <tr>
                            <td>{{ $salesPersons->name }}</td>
                            <td>{{ $salesPersons->address }}</td>
                            <td>{{ $salesPersons->no_rek }}</td>
                            <td>{{ $salesPersons->phone }}</td>
                            <td>{{ $salesPersons->mobile }}</td>

                            <td>

                                <form method="POST" action="{!! route('sales_persons.sales_persons.destroy', $salesPersons->id) !!}" accept-charset="UTF-8">
                                <input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}

                                    <div class="btn-group btn-group-xs pull-right" role="group">
<!--                                        <a href="{{ route('sales_persons.sales_persons.show', $salesPersons->id ) }}" class="btn btn-info" title="Show Sales Persons">
                                            <span class="fa fa-search" aria-hidden="true"></span>
                                        </a>-->
                                        <a href="{{ route('sales_persons.sales_persons.edit', $salesPersons->id ) }}" class="btn btn-primary" title="Edit Sales Persons">
                                            <span class="fa fa-pencil" aria-hidden="true"></span>
                                        </a>

                                        <button type="submit" class="btn btn-danger" title="Delete Sales Persons" onclick="return confirm(&quot;Delete Sales Persons?&quot;)">
                                            <span class="fa fa-trash" aria-hidden="true"></span>
                                        </button>
                                    </div>

                                </form>
                                
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            {!! $salesPersonsObjects->render() !!}
        </div>
        
        @endif
    
    </div>
    </div>
</div>
@endsection