@extends('backend.layouts.app')
@section ('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
    @include('backend.sales_persons.includes.breadcrumb-links')
@endsection

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($salesPersons->name) ? $salesPersons->name : 'Sales Persons' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('sales_persons.sales_persons.destroy', $salesPersons->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('sales_persons.sales_persons.index') }}" class="btn btn-primary" title="Show All Sales Persons">
                        <span class="fa fa-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('sales_persons.sales_persons.create') }}" class="btn btn-success" title="Create New Sales Persons">
                        <span class="fa fa-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('sales_persons.sales_persons.edit', $salesPersons->id ) }}" class="btn btn-primary" title="Edit Sales Persons">
                        <span class="fa fa-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Sales Persons" onclick="return confirm(&quot;Delete Sales Persons??&quot;)">
                        <span class="fa fa-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Name</dt>
            <dd>{{ $salesPersons->name }}</dd>
            <dt>Address</dt>
            <dd>{{ $salesPersons->address }}</dd>
            <dt>No Rek</dt>
            <dd>{{ $salesPersons->no_rek }}</dd>
            <dt>Phone</dt>
            <dd>{{ $salesPersons->phone }}</dd>
            <dt>Mobile</dt>
            <dd>{{ $salesPersons->mobile }}</dd>
            <dt>Created At</dt>
            <dd>{{ $salesPersons->created_at }}</dd>
            <dt>Updated At</dt>
            <dd>{{ $salesPersons->updated_at }}</dd>

        </dl>

    </div>
</div>

@endsection