
<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
    <label for="name" class="col-md-2 control-label">Name</label>
    <div class="col-md-10">
        <input class="form-control" name="name" type="text" id="name" value="{{ old('name', optional($salesPersons)->name) }}" minlength="1" maxlength="45" required="true" placeholder="Enter name here...">
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
    <label for="address" class="col-md-2 control-label">Address</label>
    <div class="col-md-10">
        <input class="form-control" name="address" type="text" id="address" value="{{ old('address', optional($salesPersons)->address) }}" minlength="1" maxlength="255" required="true" placeholder="Enter address here...">
        {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('no_rek') ? 'has-error' : '' }}">
    <label for="no_rek" class="col-md-2 control-label">No Rek</label>
    <div class="col-md-10">
        <input class="form-control" name="no_rek" type="text" id="no_rek" value="{{ old('no_rek', optional($salesPersons)->no_rek) }}" maxlength="45" placeholder="Enter no rek here...">
        {!! $errors->first('no_rek', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
    <label for="phone" class="col-md-2 control-label">Phone</label>
    <div class="col-md-10">
        <input class="form-control" name="phone" type="text" id="phone" value="{{ old('phone', optional($salesPersons)->phone) }}" maxlength="45" placeholder="Enter phone here...">
        {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('mobile') ? 'has-error' : '' }}">
    <label for="mobile" class="col-md-2 control-label">Mobile</label>
    <div class="col-md-10">
        <input class="form-control" name="mobile" type="text" id="mobile" value="{{ old('mobile', optional($salesPersons)->mobile) }}" maxlength="45" placeholder="Enter mobile here...">
        {!! $errors->first('mobile', '<p class="help-block">:message</p>') !!}
    </div>
</div>

