
<div class="form-group {{ $errors->has('purchase_price') ? 'has-error' : '' }}">
    <label for="purchase_price" class="col-md-2 control-label">Purchase Price</label>
    <div class="col-md-10">
        <input class="form-control" name="purchase_price" type="number" id="purchase_price" value="{{ old('purchase_price', optional($prices)->purchase_price) }}" min="-9" required="true" placeholder="Enter purchase price here...">
        {!! $errors->first('purchase_price', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('selling_price') ? 'has-error' : '' }}">
    <label for="selling_price" class="col-md-2 control-label">Selling Price</label>
    <div class="col-md-10">
        <input class="form-control" name="selling_price" type="number" id="selling_price" value="{{ old('selling_price', optional($prices)->selling_price) }}" min="-9" required="true" placeholder="Enter selling price here...">
        {!! $errors->first('selling_price', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('item_id') ? 'has-error' : '' }}">
    <label for="item_id" class="col-md-2 control-label">Item</label>
    <div class="col-md-10">
        <select class="form-control" id="item_id" name="item_id" required="true">
        	    <option value="" style="display: none;" {{ old('item_id', optional($prices)->item_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select item</option>
        	@foreach ($items as $key => $item)
			    <option value="{{ $key }}" {{ old('item_id', optional($prices)->item_id) == $key ? 'selected' : '' }}>
			    	{{ $item }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('item_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

