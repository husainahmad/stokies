@extends('backend.layouts.app')
@section ('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
    @include('backend.prices.includes.breadcrumb-links')
@endsection
@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Prices' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('prices.prices.destroy', $prices->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('prices.prices.index') }}" class="btn btn-primary" title="Show All Prices">
                        <span class="fa fa-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('prices.prices.create') }}" class="btn btn-success" title="Create New Prices">
                        <span class="fa fa-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('prices.prices.edit', $prices->id ) }}" class="btn btn-primary" title="Edit Prices">
                        <span class="fa fa-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Prices" onclick="return confirm(&quot;Delete Prices??&quot;)">
                        <span class="fa fa-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Purchase Price</dt>
            <dd>{{ $prices->purchase_price }}</dd>
            <dt>Selling Price</dt>
            <dd>{{ $prices->selling_price }}</dd>
            <dt>Item</dt>
            <dd>{{ optional($prices->item)->name }}</dd>
            <dt>Created At</dt>
            <dd>{{ $prices->created_at }}</dd>
            <dt>Updated At</dt>
            <dd>{{ $prices->updated_at }}</dd>

        </dl>

    </div>
</div>

@endsection