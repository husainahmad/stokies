@extends('backend.layouts.app')
@section ('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
    @include('backend.prices.includes.breadcrumb-links')
@endsection

@section('content')

    <div class="panel panel-default">
  
        <div class="panel-heading clearfix">

            <div class="btn-group btn-group-sm pull-right" role="group">

                <a href="{{ route('prices.prices.index') }}" class="btn btn-primary" title="Show All Prices">
                    <span class="fa fa-th-list" aria-hidden="true"></span>
                </a>

                <a href="{{ route('prices.prices.create') }}" class="btn btn-success" title="Create New Prices">
                    <span class="fa fa-plus" aria-hidden="true"></span>
                </a>

            </div>
        </div>

        <div class="panel-body">

            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            <form method="POST" action="{{ route('prices.prices.update', $prices->id) }}" id="edit_prices_form" name="edit_prices_form" accept-charset="UTF-8" class="form-horizontal">
            {{ csrf_field() }}
            <input name="_method" type="hidden" value="PUT">
            @include ('backend.prices.form', [
                                        'prices' => $prices,
                                      ])

                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <input class="btn btn-primary" type="submit" value="Update">
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection