@extends('backend.layouts.app')
@section ('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
    @include('backend.prices.includes.breadcrumb-links')
@endsection
@section('content')
<div class="card">
    <div class="card-body">
    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="fa fa-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">
            <form action="{{ url()->current() }}">
            <div class="col-md-offset-2 col-md-3 pull-left" role="group">                
                    <input type="text" name="keyword" class="form-control" placeholder="Search No Invoice...">                                    
            </div>
            <div class="col-md-offset-3 col-md-1 pull-left">
                <button type="submit" class="btn btn-primary">
                        Search
                    </button>    
                <br/><br/>
            </div>
            </form>
            
            <div class="col-md-offset-4 btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('prices.prices.create') }}" class="btn btn-success" title="Create New Prices">
                    <span class="fa fa-plus" aria-hidden="true"></span>
                </a>
            </div>

        </div>
        
        @if(count($pricesObjects) == 0)
            <div class="panel-body text-center">
                <h4>No Prices Available!</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>Purchase Price</th>
                            <th>Selling Price</th>
                            <th>Item</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($pricesObjects as $prices)
                        <tr>
                            <td>{{ $prices->purchase_price }}</td>
                            <td>{{ $prices->selling_price }}</td>
                            <td>{{ optional($prices->item)->name }}</td>

                            <td>

                                <form method="POST" action="{!! route('prices.prices.destroy', $prices->id) !!}" accept-charset="UTF-8">
                                <input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}

                                    <div class="btn-group btn-group-xs pull-right" role="group">
<!--                                        <a href="{{ route('prices.prices.show', $prices->id ) }}" class="btn btn-info" title="Show Prices">
                                            <span class="fa fa-search" aria-hidden="true"></span>
                                        </a>-->
                                        <a href="{{ route('prices.prices.edit', $prices->id ) }}" class="btn btn-primary" title="Edit Prices">
                                            <span class="fa fa-pencil" aria-hidden="true"></span>
                                        </a>

                                        <button type="submit" class="btn btn-danger" title="Delete Prices" onclick="return confirm(&quot;Delete Prices?&quot;)">
                                            <span class="fa fa-trash" aria-hidden="true"></span>
                                        </button>
                                    </div>

                                </form>
                                
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            {!! $pricesObjects->render() !!}
        </div>
        
        @endif
    
    </div>
    </div>
</div>
@endsection