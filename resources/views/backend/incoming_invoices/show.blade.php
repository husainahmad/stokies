@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Incoming Invoices' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('incoming_invoices.incoming_invoices.destroy', $incomingInvoices->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('incoming_invoices.incoming_invoices.index') }}" class="btn btn-primary" title="Show All Incoming Invoices">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('incoming_invoices.incoming_invoices.create') }}" class="btn btn-success" title="Create New Incoming Invoices">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('incoming_invoices.incoming_invoices.edit', $incomingInvoices->id ) }}" class="btn btn-primary" title="Edit Incoming Invoices">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Incoming Invoices" onclick="return confirm(&quot;Delete Incoming Invoices??&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>No</dt>
            <dd>{{ $incomingInvoices->no }}</dd>
            <dt>Ship Via</dt>
            <dd>{{ $incomingInvoices->ship_via }}</dd>
            <dt>Purchase Orders</dt>
            <dd>{{ optional($incomingInvoices->purchaseOrder)->created_at }}</dd>
            <dt>Created At</dt>
            <dd>{{ $incomingInvoices->created_at }}</dd>
            <dt>Due Date</dt>
            <dd>{{ $incomingInvoices->due_date }}</dd>
            <dt>Sub Total</dt>
            <dd>{{ $incomingInvoices->sub_total }}</dd>
            <dt>Discount</dt>
            <dd>{{ $incomingInvoices->discount }}</dd>
            <dt>Total</dt>
            <dd>{{ $incomingInvoices->total }}</dd>
            <dt>Vat</dt>
            <dd>{{ $incomingInvoices->vat }}</dd>
            <dt>Grand Total</dt>
            <dd>{{ $incomingInvoices->grand_total }}</dd>
            <dt>Users</dt>
            <dd>{{ optional($incomingInvoices->user)->id }}</dd>
            <dt>Updated At</dt>
            <dd>{{ $incomingInvoices->updated_at }}</dd>

        </dl>

    </div>
</div>

@endsection