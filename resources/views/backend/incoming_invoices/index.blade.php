@extends('backend.layouts.app')
@section ('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
    @include('backend.incoming_invoices.includes.breadcrumb-links')
@endsection
@section('content')

<div class="card">
    <div class="card-body">
    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">
            <form action="{{ url()->current() }}">
            <div class="col-md-offset-2 col-md-3 pull-left" role="group">                
                    <input type="text" name="keyword" class="form-control" placeholder="Search Inovice No...">                                    
            </div>
            <div class="col-md-offset-2 col-md-4 pull-left">
                <div class="form-group row {{ $errors->has('suppliers_id') ? 'has-error' : '' }}">
                    <label class="col-md-3 col-form-label" for="suppliers_id">Suppliers</label>       
                    <div class="col-md-9">
                        <select class="form-control" id="suppliers_id" name="suppliers_id" >
                                    <option value="" style="display: none;" disabled selected>Select suppliers</option>
                                @foreach ($suppliers as $key => $supplier)
                                            <option value="{{ $key }}" >
                                                {{ $supplier }}
                                            </option>
                                        @endforeach
                        </select>

                        {!! $errors->first('suppliers_id', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            </div>      
            <div class="col-md-offset-3 col-md-1 pull-left">
                <button type="submit" class="btn btn-primary">
                        Search
                    </button>    
                <br/><br/>
            </div>
            </form>    
            <div class="col-md-offset-4 btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('incoming_invoices.incoming_invoices.create') }}" class="btn btn-success" title="Create New Incoming Invoices">
                    <span class="fa fa-plus" aria-hidden="true"></span>
                </a>
            </div>

        </div>
        
        @if(count($incomingInvoicesObjects) == 0)
            <div class="panel-body text-center">
                <h4>No Incoming Invoices Available!</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>No PO</th>
                            <th>Date</th>
                            <th>Due Date</th>
                            <th>Supplier</th>                            
                            <th>Sub Total</th>
                            <th>Discount</th>
                            <th>Total</th>
                            <th>Vat</th>
                            <th>Grand Total</th>

                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($incomingInvoicesObjects as $incomingInvoices)
                        <tr>
                            <td>{{ $incomingInvoices->no }}</td>
                            <td>{{ $incomingInvoices->purchaseorder->purchase_no }}</td>
                            <td>{{ DateTime::createFromFormat('d/m/Y g a', optional($incomingInvoices)->created_at)->format('d/m/Y') }}</td>
                            <td>{{ date('d/m/Y', strtotime(optional($incomingInvoices)->due_date)) }}</td>
                            <td>{{ optional($incomingInvoices->purchaseOrder)->supplier->name }}</td>                            
                            <td>{{ number_format($incomingInvoices->sub_total,0,'.',',') }}</td>
                            <td>{{ number_format($incomingInvoices->discount,0,'.',',') }}</td>
                            <td>{{ number_format($incomingInvoices->total,0,'.',',') }}</td>
                            <td>{{ number_format($incomingInvoices->vat,0,'.',',') }}</td>
                            <td>{{ number_format($incomingInvoices->grand_total,0,'.',',') }}</td>

                            <td>

                                <form method="POST" action="{!! route('incoming_invoices.incoming_invoices.destroy', $incomingInvoices->id) !!}" accept-charset="UTF-8">
                                <input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}

                                    <div class="btn-group btn-group-xs pull-right" role="group">
                                        <a href="{{ route('incoming_invoices.incoming_invoices.show', $incomingInvoices->id ) }}" class="btn btn-info" title="Show Incoming Invoices">
                                            <span class="fa fa-print" aria-hidden="true"></span>
                                        </a>
                                        <a href="{{ route('incoming_invoices.incoming_invoices.edit', $incomingInvoices->id ) }}" class="btn btn-primary" title="Edit Incoming Invoices">
                                            <span class="fa fa-pencil" aria-hidden="true"></span>
                                        </a>

                                        <button type="submit" class="btn btn-danger" title="Delete Incoming Invoices" onclick="return confirm(&quot;Delete Incoming Invoices?&quot;)">
                                            <span class="fa fa-trash" aria-hidden="true"></span>
                                        </button>
                                    </div>

                                </form>
                                
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            {!! $incomingInvoicesObjects->render() !!}
        </div>
        
        @endif
    
    </div>
    
    
</div>
</div> 
@endsection
   