<br>
<br>
<div class="row">            
    <div class="col-sm-6">     
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('no') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label" for="no" class="col-md-2 control-label">No</label>
                        <div class="col-md-9">
                            <input class="form-control" name="no" type="text" id="no" value="{{ old('no', optional($incomingInvoices)->no) }}" minlength="1" maxlength="45" required="true" placeholder="Enter no here...">
                            {!! $errors->first('no', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>                
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('purchase_orders_id') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label" for="purchase_orders_id" class="col-md-2 control-label">PO</label>
                        <div class="col-md-9">
                            <select class="form-control purchase-orders" id="purchase_orders_id" name="purchase_orders_id" required="true">
                                        <option value="" style="display: none;" {{ old('purchase_orders_id', optional($incomingInvoices)->purchase_orders_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select purchase orders</option>
                                            @foreach ($purchaseOrders as $key => $purchaseOrder)
                                                <option value="{{ $purchaseOrder->id }}" {{ old('purchase_orders_id', optional($incomingInvoices)->purchase_orders_id) == $purchaseOrder->id ? 'selected' : '' }}>
                                                    {{ $purchaseOrder->purchase_no }} 
                                                </option>
                                            @endforeach
                            </select>

                            {!! $errors->first('purchase_orders_id', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>                
            </div>                        
        </div>
    </div>    
    <div class="col-sm-6">   
        <div class="card-body">
            <div class="row">    
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('ship_via') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label" for="ship_via" class="col-md-2 control-label">Ship Via</label>
                        <div class="col-md-9">
                            <input class="form-control" name="ship_via" type="text" id="ship_via" 
                                   value="{{ old('ship_via', optional($incomingInvoices)->ship_via) }}" maxlength="145" placeholder="Enter ship via here...">
                            {!! $errors->first('ship_via', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>                
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('created_at') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label" for="created_at" class="col-md-2 control-label">Date</label>                        
                        <div class="col-md-9">
                            <div class="input-group">
                                <span class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                            </span>
                            <input class="form-control" name="created_at" type="text" id="created_at" 
                                   data-provide="datepicker" value="{{ old('created_at', 
                                               optional($incomingInvoices)->created_at) == '' ? date('d/m/Y') : 
                                               old('created_at', DateTime::createFromFormat('d/m/Y g a', optional($incomingInvoices)->created_at)->format('d/m/Y')) }}"
                                   minlength="1" required="true" placeholder="Enter due date here...">
                            </div>
                            <small class="text-muted">ex. 99/99/9999</small>                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('due_date') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label" for="due_date" class="col-md-2 control-label">Due Date</label>                        
                        <div class="col-md-9">            
                            <div class="input-group">
                                <span class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                            </span>
                            <input class="form-control" name="due_date" type="text" id="due_date" 
                                   data-provide="datepicker" value="{{ old('due_date', optional($incomingInvoices)->due_date) == '' ? date('d/m/Y') : old('due_date', date('d/m/Y', strtotime(optional($incomingInvoices)->due_date)))  }}" 
                                   minlength="1" required="true" placeholder="Enter due date here...">
                                {!! $errors->first('due_date', '<p class="help-block">:message</p>') !!}
                            </div>
                            <small class="text-muted">ex. 99/99/9999</small>
                        </div>
                    </div>
                </div>                
            </div>
        </div>    
    </div>        
</div>


