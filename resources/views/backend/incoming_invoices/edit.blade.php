@extends('backend.layouts.app')
@section ('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
    @include('backend.incoming_invoices.includes.breadcrumb-links')
@endsection
@section('content')
<div class="card">
    <div class="card-body">
        

        <div class="panel panel-default">

            <div class="panel-heading clearfix">

                <div class="btn-group btn-group-sm pull-right" role="group">

                    <a href="{{ route('incoming_invoices.incoming_invoices.index') }}" class="btn btn-primary" title="Show All Incoming Invoices">
                        <span class="fa fa-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('incoming_invoices.incoming_invoices.create') }}" class="btn btn-success" title="Create New Incoming Invoices">
                        <span class="fa fa-plus" aria-hidden="true"></span>
                    </a>

                </div>
            </div>

            <div class="panel-body">

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

                <form method="POST" action="{{ route('incoming_invoices.incoming_invoices.update', $incomingInvoices->id) }}" id="edit_incoming_invoices_form" name="edit_incoming_invoices_form" accept-charset="UTF-8" class="form-horizontal">
                {{ csrf_field() }}
                <input name="_method" type="hidden" value="PUT">
                @include ('backend.incoming_invoices.form', [
                                            'incomingInvoices' => $incomingInvoices,
                                          ])
                    <div class="card">
                    <div class="card-body">                        
                        <div class="table-responsive">  
                            <table class="table table-bordered" id="dynamic_field">  
                                <thead>
                                  <tr>
                                   <th>Item</th>
                                   <th>Quantity</th>
                                   <th>Unit</th>
                                   <th>Price</th>
                                   <th>Amount</th>
                                 </tr>
                              </thead>
                              <tbody>
                                  
                                  <?php $i = 0; ?>
                                  @foreach ($incomingInvoicesDetails as $incomingInvoicesDetail)
                                  
                                  <tr id="row{{ $i }}">
                                     <input type="hidden" name="purchase_orders_details_id[]" value="{{$incomingInvoicesDetail->purchase_orders_details_id}}">   
                                      <td>
                                          <select class="form-control items-selection" id="items_id[]" name="items_id[]" required="true"  row-index="{{ $i }}">                                               
                                              <option value="">Please select</option>                                                
                                                <?php $unit="";?>
                                            @foreach ($items as $key => $item)
                                            
                                                @if ($key==$incomingInvoicesDetail->items_id)  
                                                <option value="{{ $key }}" selected>
                                                @else
                                                <option value="{{ $key }}">
                                                @endif
                                                    {{ $item }}
                                                </option>
                                            @endforeach                                            
                                          </select>
                                      </td>                                      
                                      <td width="10%">
                                          <input class="form-control text-count" name="quantity[]" type="number" step="any" id="quantity[]" value="{{$incomingInvoicesDetail->quantity}}" min="0" max="2147483647" required="true" placeholder="Enter">
                                      </td>
                                      <td width="15%">
                                           <input class="form-control text-count item-units" name="units[]" type="text" id="units[]" value="{{ optional($incomingInvoicesDetail->item)->name}}" min="0" max="2147483647" required="true" placeholder="Enter">
                                      </td>
                                      <td width="15%">
                                          <input class="form-control text-count item-price" name="price[]" step="any" type="number" id="price[]" value="{{$incomingInvoicesDetail->price}}" min="0" max="99999999" required="true" placeholder="Enter price here...">                                   
                                      </td>
                                      <td width="15%">
                                          <input class="form-control text-count-amount" name="amount[]" step="any" type="number" id="amount[]" value="{{$incomingInvoicesDetail->amount}}" min="0" max="99999999999" required="true" placeholder="Enter amount here...">                                   
                                      </td>    
                                  </tr>
                                <?php $i++; ?>
                                   @endforeach  
                              </tbody>
                              <tfoot>
                                  
                                  <tr>
                                      <td colspan="3"></td>
                                      <td><span style="font-weight: bold;">Sub Total</span></td>
                                      <td colspan="2">
                                          <input class="form-control" name="sub_total" type="number" id="sub_total" step="any" value="{{$incomingInvoices->sub_total}}" required="true" placeholder="Enter sub total here...">
                                      </td>
                                  </tr>     
                                  <tr>
                                      <td colspan="3"></td>
                                      <td><span style="font-weight: bold;">Discount</span></td>
                                      <td colspan="2">
                                          <input class="form-control text-count" name="discount" type="number" id="discount" value="{{$incomingInvoices->discount}}" step="any" required="true" placeholder="Enter discount here...">
                                      </td>
                                  </tr>      
                                  <tr>
                                      <td colspan="3"></td>
                                      <td><span style="font-weight: bold;">Total</span></td>
                                      <td colspan="2">
                                          <input class="form-control" name="total" type="number" id="total" value="{{$incomingInvoices->total}}" step="any" required="true" placeholder="Enter total here...">
                                      </td>
                                  </tr>                                   
                                  <tr>
                                      <td colspan="3"></td>
                                      <td><span style="font-weight: bold;">VAT</span></td>
                                      <td colspan="2">
                                         <input class="form-control" name="vat" type="number" id="vat" value="{{$incomingInvoices->vat}}" step="any" required="true" placeholder="Enter vat here...">
                                      </td>
                                  </tr>                                        
                                  <tr>
                                      <td colspan="3"></td>
                                      <td><span style="font-weight: bold;">Grand Total</span></td>
                                      <td colspan="2">
                                         <input class="form-control" name="grand_total" type="number" step="any"  id="grand_total" value="{{$incomingInvoices->grand_total}}" required="true" placeholder="Enter grand total here...">
                                         <input class="form-control" name="is_tax" type="hidden" id="is_tax" value="0" >
                                      </td>
                                  </tr>                                        
                              </tfoot>
                            </table>                              
                        </div>
                    </div>
                </div> 
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <input class="btn btn-primary" type="submit" value="Update">
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
<input class="form-control" name="hid_days" type="hidden" id="hid_days" value="0">

@endsection
@push('after-scripts')    
<script src="/js/bootstrap-datepicker.js"></script>
<link media="all" type="text/css" rel="stylesheet" href="/css/bootstrap-datepicker.css">
<link href='/css/select2.min.css' rel='stylesheet' type='text/css'>
<script src='/js/select2.min.js' type='text/javascript'></script>
<script>
    var i=1;
    var tax = 0.11;
    
    $(document).ready(function() {
        

        countTotal();        
         
         $('#due_date').datepicker({
            todayBtn: "linked",
            language: "en",
            autoclose: true,
            todayHighlight: true,
            format: 'dd/mm/yyyy'
        });
        
        $('#created_at').datepicker({
            todayBtn: "linked",
            language: "en",
            autoclose: true,
            todayHighlight: true,
            format: 'dd/mm/yyyy'
        });
        
        $("#created_at").change(function(){
            var newDt = stringToDate($("#created_at").val(),"dd/MM/yyyy","/");
             console.log("Current Date :" + newDt + "<br/>");
             // add 5 days to the current date
             newDt.setDate(newDt.getDate() + $("#hid_days").val());
             console.log("Due Date :" + newDt + "<br/>");

             var dd = newDt.getDate();
             var mm = newDt.getMonth() + 1; //January is 0!

             var yyyy = newDt.getFullYear();
             if (dd < 10) {
                  dd = '0' + dd;
             } 
             if (mm < 10) {
                  mm = '0' + mm;
             } 
             var due_date = dd + '/' + mm + '/' + yyyy;

             $("#due_date").val(due_date);
        });
        
        $( ".purchase-orders" ).change(function() {
            var index = $(".purchase-orders").index(this);                    
            onOrderPOSelectionChange($(this).val(), index);
        });
        
        $(".text-count").bind('keyup mouseup', function () {
            countTotal();        
        });
        
        function countTotal() {
            var totalQuantity = 0;
            var totalAmount = 0;

            try { 
                var values = $("input[name='quantity[]']").map(function(){
                    return $(this).val();
                }).get();

                var prices = $("input[name='price[]']").map(function(){
                    return $(this).val();
                }).get();

                console.log(isNaN(values));
                for (var i = 0, len = values.length; i < len; i++) {
                    try {   
                        var qty = isNaN(parseInt(values[i])) ? 0 : parseInt(values[i]);
                        totalQuantity += qty;
                        var price = isNaN(parseFloat(prices[i])) ? 0 : parseFloat(prices[i]);
                        var amount = price*qty;
                        $(".text-count-amount").eq(i).val(amount);                        
                    } catch (err) {
                        totalQuantity += 0;
                    }                   
                }
                console.log(totalQuantity); 

                var valueAmountPrice = $("input[name='amount[]']").map(function(){
                    return $(this).val();
                }).get();

                var totalAmountPrice = 0;
                for (var i = 0, len = valueAmountPrice.length; i < len; i++) {
                    try {   
                        totalAmountPrice += isNaN(parseFloat(valueAmountPrice[i])) ? 0 : parseFloat(valueAmountPrice[i]);
                    } catch (err) {
                        totalAmountPrice += 0;
                    }                   
                }
            } catch (err) {
                totalQuantity += 0;
            } 

            $('#totalQuantity').text(totalQuantity);

            console.log(totalAmountPrice); 
            $('#sub_total').val(totalAmountPrice);

            var discount = $('#discount').val();
            console.log(discount);
            var total = totalAmountPrice-discount;
            $('#total').val(total);
            
            if ($("#is_tax").val()==="1") {
                var vat = total*tax;
                $('#vat').val(vat);

                var grand_total = total+vat;
                $('#grand_total').val(grand_total);
            } else {
                var vat = 0;
                $('#vat').val(vat);

                var grand_total = total+vat;
                $('#grand_total').val(grand_total);
            }
            return false;
        }
        
        function stringToDate(_date,_format,_delimiter) {
            var formatLowerCase=_format.toLowerCase();
            var formatItems=formatLowerCase.split(_delimiter);
            var dateItems=_date.split(_delimiter);
            var monthIndex=formatItems.indexOf("mm");
            var dayIndex=formatItems.indexOf("dd");
            var yearIndex=formatItems.indexOf("yyyy");
            var month=parseInt(dateItems[monthIndex]);
            month-=1;
            var formatedDate = new Date(dateItems[yearIndex],month,dateItems[dayIndex]);
            return formatedDate;
        }

         function onOrderPOSelectionChange(value, index) {                    
            $.ajax({
                 type: "GET",
                 url: "{{ route('purchase_orders.purchase_orders.index') }}/show/json/" + value,
                 success: function( response ) {
                     console.log(response);
                     $('#dynamic_field tbody').empty();
                     try {                        
                     } catch (err) {
                     }                               
                    $("#discount").val(response.purchaseOrders.discount);
                     $("#sub_total").val(response.purchaseOrders.sub_total);
                     $("#total").val(response.purchaseOrders.sub_total - response.purchaseOrders.discount);
                     $("#grand_total").val(response.purchaseOrders.total_amount);
                     $("#vat").val(response.purchaseOrders.vat);
					 $("#is_tax").val(response.purchaseOrders.is_tax);    

                    if (response.purchaseOrders.termbilling!=undefined) {

                        $("#hid_days").val(response.purchaseOrders.termbilling.days);

                        var newDt = stringToDate($("#created_at").val(),"dd/MM/yyyy","/");              

                        console.log("Current Date :" + newDt + "<br/>");
                         // add 5 days to the current date
                        newDt.setDate(newDt.getDate() + response.purchaseOrders.termbilling.days);
                        console.log("Due Date :" + newDt + "<br/>");

                        var dd = newDt.getDate();
                        var mm = newDt.getMonth() + 1; //January is 0!

                        var yyyy = newDt.getFullYear();
                        if (dd < 10) {
                                dd = '0' + dd;
                        } 
                        if (mm < 10) {
                                mm = '0' + mm;
                        } 
                        var due_date = dd + '/' + mm + '/' + yyyy;

                        $("#due_date").val(due_date);
                     }
					
                     $.each(response.purchaseOrdersDetails, function(i, order) {
                            if (order.quantity>0) {
                                $('#dynamic_field').append('<tr id="row'+ index + '" class="parentIndex'+ index +'">'+
                                    '<input type="hidden" name="items_id[]" id="items_id[]" value="' + order.item.id +'">' +
                                    '<td width="30%">'+ order.item.name +
                                      '</td>'+
                                      '<td width="10%">'+
                                      '    <input class="form-control text-count" name="quantity[]" type="number" id="quantity[]" step="any" value="'+order.quantity+'" required="true" placeholder="Enter">'+
                                      '</td>'+
                                      '<td width="15%">'+
                                      '    <input class="form-control item-units" name="units[]" type="text" id="units[]" value="'+order.unit.name+'" required="true" placeholder="Enter">'+
                                      '</td>'+
                                      '<td width="15%">'+
                                      '    <input class="form-control text-count item-price" name="price[]" type="number" step="any" id="price[]" value="'+order.price+'" required="true" placeholder="Enter price here...">'+
                                      '</td>'+
                                      '<td width="15%">'+
                                      '    <input class="form-control text-count-amount" name="amount[]" type="number" step="any" id="amount[]" value="'+order.amount +'" required="true" placeholder="Enter amount here...">'+
                                      '</td>'+
                                      '</tr>'); 
                                $(".text-count").bind('keyup mouseup', function () {
                                    countTotal();        
                                }); 
                            }                                
                     });
                     //countTotal();
                 }
             });
        }
        $("#purchase_orders_id").select2();            
        $(".items-selection").select2();  
    });
</script>    
@endpush