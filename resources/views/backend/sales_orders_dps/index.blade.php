@extends('backend.layouts.app')
@section ('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
    @include('backend.sales_orders_dps.includes.breadcrumb-links')
@endsection
@section('content')

<div class="card">
    <div class="card-body">
    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="fa fa-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">
            <form action="{{ url()->current() }}">
            <div class="col-md-offset-1 col-md-3 pull-left" role="group">                
                    <input type="text" name="keyword" class="form-control" placeholder="Search SO No...">                                    
            </div>
            <div class="col-md-offset-2 col-sm-5 pull-left">
                    <div class="form-group row {{ $errors->has('customer_id') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label"  for="customer_id">Customer</label>
                        <div class="col-md-9">
                            <select class="form-control" id="customer_id" name="customer_id" >
                                    <option value="" style="display: none;" disabled selected>Select customer</option>
                                    @foreach ($customers as $key => $customer)
                                                <option value="{{ $key }}" >
                                                    {{ $customer }}
                                                </option>
                                            @endforeach
                            </select>

                            {!! $errors->first('customer_id', '<p class="help-block">:message</p>') !!}
                        </div>
                        
                    </div>
                </div>    
            <div class="col-md-offset-3 col-md-1 pull-left">
                <button type="submit" class="btn btn-primary">
                        Search
                    </button>   
                <br/><br/>
            </div>
            </form>    
            <div class="col-md-offset-4 btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('sales_orders_dps.sales_orders_dp.create') }}" class="btn btn-success" title="Create New Sales Orders Dp">
                    <span class="fa fa-plus" aria-hidden="true"></span>
                </a>
            </div>

        </div>
        
        @if(count($salesOrdersDps) == 0)
            <div class="panel-body text-center">
                <h4>No Sales Orders Down Payments Available!</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>Sales Orders</th>
                            <th>Invoice No</th>
                            <th>Customer</th>
                            <th>Total Dp</th>
                            <th>Created Date</th>

                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($salesOrdersDps as $salesOrdersDp)
                        <tr>
                            <td>{{ optional($salesOrdersDp->salesOrder)->no }}</td>
                            <td>{{ optional($salesOrdersDp->invoice)->no }}</td>
                            <td>{{ optional($salesOrdersDp->salesOrder)->customer->name }}</td>
                            <td>{{ $salesOrdersDp->total_dp }}</td>
                            <td>{{ $salesOrdersDp->created_date }}</td>

                            <td>

                                <form method="POST" action="{!! route('sales_orders_dps.sales_orders_dp.destroy', $salesOrdersDp->id) !!}" accept-charset="UTF-8">
                                <input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}

                                    <div class="btn-group btn-group-xs pull-right" role="group">
                                        <a target="_blank" href="{{ route('sales_orders_dps.sales_orders_dp.print', $salesOrdersDp->id ) }}" class="btn btn-info" title="Print Sales Orders">
                                            <span class="fa fa-print" aria-hidden="true"></span>
                                        </a>
                                        <a href="{{ route('sales_orders_dps.sales_orders_dp.edit', $salesOrdersDp->id ) }}" class="btn btn-primary" title="Edit Sales Orders Dp">
                                            <span class="fa fa-pencil" aria-hidden="true"></span>
                                        </a>

                                        <button type="submit" class="btn btn-danger" title="Delete Sales Orders Dp" onclick="return confirm(&quot;Delete Sales Orders Dp?&quot;)">
                                            <span class="fa fa-trash" aria-hidden="true"></span>
                                        </button>
                                    </div>

                                </form>
                                
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            {!! $salesOrdersDps->render() !!}
        </div>
        
        @endif
    
    </div>
    </div>
</div>
@endsection