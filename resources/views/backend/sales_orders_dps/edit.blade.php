@extends('backend.layouts.app')
@section ('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
    @include('backend.sales_orders_dps.includes.breadcrumb-links')
@endsection

@section('content')

<div class="card">
    <div class="card-body">
        
        <div class="panel panel-default">

            <div class="panel-heading clearfix">
                <div class="btn-group btn-group-sm pull-right" role="group">

                    <a href="{{ route('sales_orders_dps.sales_orders_dp.index') }}" class="btn btn-primary" title="Show All Sales Orders Dp">
                        <span class="fa fa-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('sales_orders_dps.sales_orders_dp.create') }}" class="btn btn-success" title="Create New Sales Orders Dp">
                        <span class="fa fa-plus" aria-hidden="true"></span>
                    </a>

                </div>
            </div>

            <div class="panel-body">

                <form method="POST" action="{{ route('sales_orders_dps.sales_orders_dp.update', $salesOrdersDp->id) }}" id="edit_sales_orders_dp_form" name="edit_sales_orders_dp_form" accept-charset="UTF-8" class="form-horizontal">
                {{ csrf_field() }}
                <input name="_method" type="hidden" value="PUT">
                <input name="invoices_id" type="hidden" value="{{ $salesOrdersDp!=null ? $salesOrdersDp->invoices_id : '0' }}">                
                
                 @include ('backend.sales_orders_dps.form', [
                                            'salesOrdersDp' => $salesOrdersDp,
                                          ])
                    
                    <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">  
                            <table class="table table-bordered" id="dynamic_field">  
                                <thead>
                                  <tr>
                                   <th>Item</th>
                                   <th>Quantity</th>
                                   <th>Price</th>
                                   <th>Amount</th>
                                   <th>Action</th>
                                 </tr>
                              </thead>
                              <tbody>     
                                  
                                  <?php $i = 0; ?>
                                  @foreach ($invoicesDetails as $invoicesDetails)
                                  
                                  <tr id="row{{ $i }}"> 
                                      <td>                                          
                                          <textarea class="form-control" name="description[]" id="description[]" required="true" placeholder="Enter description here...">{{$invoicesDetails->description}}</textarea>
                                      </td>
                                      <td width="15%">
                                          <input class="form-control text-count" name="quantity[]" step="any" type="number" id="quantity[]" value="{{$invoicesDetails->quantity}}" min="0" max="2147483647" required="true" placeholder="Enter">
                                      </td>
                                      <td width="15%">                                          
                                          <input class="form-control text-count item-price" name="price[]" step="any" type="number" id="price[]" value="{{ $invoicesDetails->price }}" min="0" max="9999999999" required="true" placeholder="Enter price here...">
                                      </td>
                                      <td width="15%">                                          
                                          <input class="form-control text-count-amount" name="amount[]" type="number" step="any" id="amount[]" value="{{ $invoicesDetails->price * $invoicesDetails->quantity }}" min="0" max="999999999999" required="true" placeholder="Enter amount here...">
                                      </td>  
                                      <td width="5%">
                                          <button type="button" class="btn btn-danger-item btn-delete-item" title="Delete Item" id="0" >
                                            <span class="fa fa-trash" aria-hidden="true"></span>
                                          </button>
                                      </td>
                                  </tr>
                                  <?php $i++; ?>
                                  @endforeach
                              </tbody>
                              <tfoot>
                                  <tr>
                                      <td colspan="2"><span style="font-weight: bold;">Total</span></td>
                                      <td><span style="font-weight: bold;" id="totalQuantity"></span></td>
                                      <td></td>
                                      <td>
                                          <button type="button" class="btn btn-add-more-item" title="Add more item">
                                            <span class="fa fa-plus" aria-hidden="true"></span>
                                          </button>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td colspan="2"></td>
                                      <td><span style="font-weight: bold;">Sub Total</span></td>
                                      <td colspan="3">
                                          <input class="form-control text-count" name="sub_total" type="number" step="any" id="sub_total" value="{{$salesOrdersDp->invoice->sub_total}}" min="1" required="true"  placeholder="">
                                      </td>
                                  </tr>                                  
                                  <tr>
                                      <td colspan="2"></td>
                                      <td><span style="font-weight: bold;">Discount</span></td>
                                      <td colspan="3">
                                          <input class="form-control text-count" name="discount" type="number" id="discount" step="any" value="{{$salesOrdersDp->invoice->discount}}"  required="true" placeholder="">
                                      </td>
                                  </tr>
                                  <tr>
                                      <td colspan="2"></td>
                                      <td><span style="font-weight: bold;">Total</span></td>
                                      <td colspan="3">
                                          <input class="form-control text-count" name="total" type="number" id="total" value="{{$salesOrdersDp->invoice->total}}" step="any" required="true"  placeholder="">
                                      </td>
                                  </tr>      
                                  <tr>
                                      <td colspan="2"></td>
                                      <td><span style="font-weight: bold;">VAT</span></td>
                                      <td colspan="3">
                                          <input class="form-control" name="vat" type="number" id="vat" value="{{$salesOrdersDp->invoice->vat}}"  step="any" required="true" placeholder="">
                                      </td>
                                  </tr>                                   
                                  <tr>
                                      <td colspan="2"></td>
                                      <td><span style="font-weight: bold;">Grand Total</span></td>
                                      <td colspan="3">
                                          <input class="form-control" name="grand_total" type="number" id="grand_total" step="any" value="{{$salesOrdersDp->invoice->grand_total}}" required="true"  placeholder="">
                                      </td>
                                  </tr>                                   
                              </tfoot>
                            </table>                              
                        </div>
                    </div>                    
                </div> 
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <input class="btn btn-primary" type="submit" value="Update">
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection

@push('after-scripts')    

        <script src="/js/bootstrap-datepicker.js"></script>        
        <link media="all" type="text/css" rel="stylesheet" href="/css/bootstrap-datepicker.css">
        <link href='/css/select2.min.css' rel='stylesheet' type='text/css'>
        <script src='/js/select2.min.js' type='text/javascript'></script>
        <script>
            var i=1;                                    
            
           $(function() {           
                $("#sales_orders_id").select2();
                   
                countTotal();        
                
                $('#due_date').datepicker({
                    todayBtn: "linked",
                    language: "en",
                    autoclose: true,
                    todayHighlight: true,
                    format: 'dd/mm/yyyy'
                });
                
                $('#created_at').datepicker({
                    todayBtn: "linked",
                    language: "en",
                    autoclose: true,
                    todayHighlight: true,
                    format: 'dd/mm/yyyy'
                });
            
                $("#sales_orders_id" ).change(function() {
                    var index = $(".sales_orders_id").index(this);                    
                    onOrderRefSOSelectionChange($(this).val(), index);
                });
                
                $(".text-count").bind('keyup mouseup', function () {
                    countTotal();        
                });
                
                $('.btn-add-more-item').click(function(){  
                    $('#dynamic_field').append('<tr id="row'+ i +'">'+
                            '<td>'+
                                '<textarea class="form-control" name="description[]" id="description[]" rows="4" required="true" placeholder="Enter description here..."></textarea>'+
                                '</td>'+
                                '<td width="15%">'+
                                '    <input class="form-control text-count" name="quantity[]" type="number" id="quantity[]" value="0" min="1" max="2147483647" required="true"  placeholder="Enter">'+
                                '</td>'+
                                '<td width="15%">'+
                                '    <input class="form-control text-count item-price" name="price[]" type="number" step="any" id="price[]" value="0" min="0" max="9999999999" required="true"  placeholder="Enter price here...">'+
                                '</td>'+
                                '<td  width="15%">'+
                                '    <input class="form-control text-count-amount" name="amount[]" type="number" step="any" id="amount[]" value="0" min="0" max="999999999999" required="true"  placeholder="Enter amount here...">'+
                                '</td>'+                                
                                '<td width="5%">'+
                                '    <button type="button" class="btn btn-danger-item btn-delete-item" title="Delete Item" id="'+ i +'" >'+
                                '      <span class="fa fa-trash" aria-hidden="true"></span>'+
                                '    </button>'+
                                '</td>'+
                        '</tr>');                  
                     $(".text-count").bind('keyup mouseup', function () {
                         countTotal();        
                    });    
                     $(".items-selection").change(function() {                     
                         var index = $(".items-selection").index(this);                    
                         onItemSelectionChange($(this).val(), index);
                     });
                     return false;
                });  
                
                $(document).on('click', '.btn-delete-item', function(){  
                    var button_id = $(this).attr("id");   
                    $('#row'+button_id+'').remove();  
                    countTotal();        
                });
           });
                      
            function onOrderRefSOSelectionChange(value, index) {
                console.log(index);               
                console.log(value);               

                $.ajax({
                     type: "GET",
                     url: "{{ route('sales_orders_details.sales_orders_details.index') }}/show/json/" + value,
                     success: function( response ) {
                         console.log(response);
                         
                         $("#customer_name").val(response.salesOrders.customer.name);
                         $("#ref_po").val(response.salesOrders.ref_po_customer);
                         $("#discount").val(response.salesOrders.discount);
                         $("#customers_id").val(response.salesOrders.customer_id);
                         $("#ship_via").val(response.salesOrders.transport);
                         $("#sales_persons_id").val(response.salesOrders.sales_person_id);                         
                         $("#tax").val(response.salesOrders.tax);  

                        var newDt = stringToDate($("#created_at").val(),"dd/MM/yyyy","/");
                        console.log("Current Date :" + newDt + "<br/>");
                        // add 5 days to the current date
                        newDt.setDate(newDt.getDate() + response.salesOrders.termbilling.days);
                        console.log("Due Date :" + newDt + "<br/>");

                        var dd = newDt.getDate();
                        var mm = newDt.getMonth() + 1; //January is 0!

                        var yyyy = newDt.getFullYear();
                        if (dd < 10) {
                             dd = '0' + dd;
                        } 
                        if (mm < 10) {
                             mm = '0' + mm;
                        } 
                        var due_date = dd + '/' + mm + '/' + yyyy;

                        $("#due_date").val(due_date);                         
                         
                         $.each(response.salesOrdersDetails, function(i, item) {
                             
                         });
                         countTotal();
                     }
                 });

            }
            
            function stringToDate(_date,_format,_delimiter) {
                var formatLowerCase=_format.toLowerCase();
                var formatItems=formatLowerCase.split(_delimiter);
                var dateItems=_date.split(_delimiter);
                var monthIndex=formatItems.indexOf("mm");
                var dayIndex=formatItems.indexOf("dd");
                var yearIndex=formatItems.indexOf("yyyy");
                var month=parseInt(dateItems[monthIndex]);
                month-=1;
                var formatedDate = new Date(dateItems[yearIndex],month,dateItems[dayIndex]);
                return formatedDate;
           }

            function countTotal() {
                 var totalQuantity = 0;
                 var totalAmount = 0;
                 var values = $("input[name='quantity[]']").map(function(){
                     return $(this).val();
                 }).get();

                 var prices = $("input[name='price[]']").map(function(){
                     return $(this).val();
                 }).get();

                 console.log(isNaN(values));
                 for (var i = 0, len = values.length; i < len; i++) {
                     try {   
                         var qty = isNaN(parseInt(values[i])) ? 0 : parseInt(values[i]);
                         totalQuantity += qty;
                         var price = isNaN(parseFloat(prices[i])) ? 0 : parseFloat(prices[i]);
                         var amount = price*qty;
                         $(".text-count-amount").eq(i).val(amount);                        
                     } catch (err) {
                         totalQuantity += 0;
                     }                   
                 }
                 console.log(totalQuantity); 
                 $('#totalQuantity').text(totalQuantity);

                 var valueAmountPrice = $("input[name='amount[]']").map(function(){
                     return $(this).val();
                 }).get();

                 var totalAmountPrice = 0;
                 for (var i = 0, len = valueAmountPrice.length; i < len; i++) {
                     try {   
                         totalAmountPrice += isNaN(parseFloat(valueAmountPrice[i])) ? 0 : parseFloat(valueAmountPrice[i]);
                     } catch (err) {
                         totalAmountPrice += 0;
                     }                   
                 }
                 
                 console.log(totalAmountPrice); 
                 $('#sub_total').val(totalAmountPrice);

                 var discount = $('#discount').val();
                 var total_dp = 0;
                 console.log(discount);
                 var total = totalAmountPrice-discount-total_dp;
                 $('#total').val(total);
                 var vat = total*$('#tax').val();
                 $('#vat').val(vat);

                 var grand_total = total+vat;
                 $('#grand_total').val(grand_total);
                 return false;
            }
           
           
        </script>
@endpush

