@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Sales Orders Dp' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('sales_orders_dps.sales_orders_dp.destroy', $salesOrdersDp->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('sales_orders_dps.sales_orders_dp.index') }}" class="btn btn-primary" title="Show All Sales Orders Dp">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('sales_orders_dps.sales_orders_dp.create') }}" class="btn btn-success" title="Create New Sales Orders Dp">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('sales_orders_dps.sales_orders_dp.edit', $salesOrdersDp->id ) }}" class="btn btn-primary" title="Edit Sales Orders Dp">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Sales Orders Dp" onclick="return confirm(&quot;Delete Sales Orders Dp??&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Sales Orders</dt>
            <dd>{{ optional($salesOrdersDp->salesOrder)->no }}</dd>
            <dt>Total Dp</dt>
            <dd>{{ $salesOrdersDp->total_dp }}</dd>
            <dt>Created Date</dt>
            <dd>{{ $salesOrdersDp->created_date }}</dd>
            <dt>Users</dt>
            <dd>{{ optional($salesOrdersDp->user)->id }}</dd>

        </dl>

    </div>
</div>

@endsection