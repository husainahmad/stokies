<!-- Load paper.css for happy printing -->
<link rel="stylesheet" href="../../css/paper.css">
<link rel="stylesheet" href="../../fonts/1979_dot_matrix_regular/stylesheet.css">
<!-- Set page size here: A5, A4 or A3 -->
<!-- Set also "landscape" if you need -->
<style>
    body {
        width: 21cm;
        height: 27.7cm;        
        display: block; 
        margin-top: 50px;
        margin-left: 20px;
        margin-right: 20px;
        margin-bottom: 20px;        
        padding: 0;
        background-color: #FAFAFA;
        font-size: smaller;
        color: #000;
        font-family: Consolas;
    }
    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }
    .page {
        width: 21cm;
        min-height: 27.7cm;
        padding: 2cm;
        margin: 1cm auto;
        border: 1px #D3D3D3 solid;
        border-radius: 5px;
        background: white;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }
    .subpage {
        padding: 1cm;
        border: 5px red solid;
        height: 256mm;
        outline: 2cm #FFEAEA solid;
    }
    table th, table td {
        border-bottom: 1px solid #000;
        border-right: 1px solid #000;
        padding: 5px 10px 5px 5px;
    }
    @page {
        size: A4;
        margin: 0;
    }
    @media print {
        .page {
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
            display: block; 
        }
    }
</style>

    <div class="book">        
        <table width="100%" cellpadding="0" cellspacing="0">            
            <tr>
                <td width="60%" style='border: 0px;'>                                        
                    <table width="100%" cellpadding="0" cellspacing="0" style='border: 0px;'>
                        <tr>
                            <td style='border: 0px; padding: 0px;'>                                                                
                                <table width="100%" cellpadding="0" cellspacing="0">                        
                                    <tr>
                                        <td width="25%" style='border: 0px; padding: 0px;'>
                                            <img src="/img/logo.jpg">
                                        </td>
                                        <td style='border: 0px; padding: 0px; vertical-align: middle; font-size: smaller'>
                                            <strong><div style="font-size: large">PT. ARGA BAJA LESTARI</div></strong>                                            
                                            <strong>Steel & Material Building Distributor</strong><br>
                                            Jl. Raya Jatiwaringin No. 24B Jati Cempaka<br/>
                                            Pondok Gede. Bekasi 17416<br/>
                                            Telp. 021.84994366<br/>
                                            Fax. 021.84979581<br/>
                                            e-Mail: argabajalestari@yahoo.com<br/>
                                        </td>
                                    </tr>
                                </table>    
                                <br>
                            </td>
                        </tr>
                        <tr>
                            <td style="border: 0px;">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td width="25%" style='border: 0px; padding: 0px;'>
                                            <strong>NO</strong>
                                        </td>
                                        <td width="1%" style='border: 0px; padding: 0px;'>:&nbsp;</td>
                                        <td width="74%" style='border: 0px; padding: 0px;'>
                                            <strong>{{ optional($salesOrdersDp->invoice)->no }}</strong>
                                        </td>                                        
                                    </tr>
                                    <tr>
                                        <td width="25%" style="border: 0px; padding: 0px;">
                                            <strong>DATE</strong>
                                        </td>
                                        <td width="1%" style="border: 0px; padding: 0px;">:&nbsp;</td>
                                        <td width="74%" style="border: 0px; padding: 0px;">
                                            <?php echo date_format(date_create($salesOrders->sales_order_date), 'd/m/Y') ?>
                                        </td>                                        
                                    </tr>   
                                    <tr>
                                        <td width="25%" style="border: 0px; padding: 0px;;">
                                            <strong>DUE DATE</strong>
                                        </td>
                                        <td width="1%" style="border: 0px; padding: 0px;">:&nbsp;</td>
                                        <td width="74%" style="border: 0px; padding: 0px;">
                                            <?php echo date_format(date_create($salesOrdersDp->created_date), 'd/m/Y') ?>
                                        </td>                                        
                                    </tr>    
                                    <tr>
                                        <td width="25%" style="border: 0px; padding: 0px;">
                                            <strong>TRANSPORT</strong>
                                        </td>
                                        <td width="1%" style="border: 0px; padding: 0px;">:&nbsp;</td>
                                        <td width="74%" style="border: 0px; padding: 0px;">
                                            {{ $salesOrders->transport }}
                                        </td>                                        
                                    </tr> 
                                    <tr>
                                        <td width="25%" style="border: 0px; padding: 0px;">
                                           <strong>PO NO</strong>
                                        </td>
                                        <td width="1%" style="border: 0px; padding: 0px;">:&nbsp;</td>
                                        <td width="74%" style="border: 0px; padding: 0px;">
                                            {{ $salesOrders->ref_po_customer }}
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td width="40%" style='border: 0px; vertical-align: top;'>
                    <h2 align="center">{{ !empty($title) ? $title : 'INVOICE' }}</h2>
                    <br/>
                    <table width="100%" cellpadding="0" cellspacing="0">                        
                        <tr>
                            <td style='border: 1px solid #000;padding: 10px;'>
                                CUSTOMER : <br/><br/>
                                <strong>{{ optional($salesOrders->customer)->name }} </strong><br/>
                                 <?php echo nl2br(optional($salesOrders->customer)->address) ?><br>
                                {{ optional($salesOrders->customer)->phone }} <br/>
                            </td>
                        </tr>
                    </table>                    
                </td>                
            </tr>
        </table>
        <table width="100%" cellpadding="0" cellspacing="0" style="padding: 10px;">  
            <thead>
              <tr>
               <th style='border-left: 1px solid #000;border-top: 1px solid #000;'>No</th>     
               <th style='border-top: 1px solid #000;'>Item</th>                        
               <th style='border-top: 1px solid #000;'>QTY</th>
               <th style='border-top: 1px solid #000;'>Unit</th>
               <th style='border-top: 1px solid #000;'>Price<br>(Rp)</th>
               <th style='border-top: 1px solid #000;'>Amount<br>(Rp)</th>
             </tr>
          </thead>
          <tbody>
              <tr>
                  <td colspan="4" align="center" style="border-left: 1px solid #000;">     
                      <strong>DP</strong>
                  </td>                      
                  <td align="center" style="text-align: right">     
                      {{ number_format($salesOrdersDp->total_dp,2,'.',',') }}
                  </td>                      
                  <td align="center" style="text-align: right">     
                      {{ number_format($salesOrdersDp->total_dp,2,'.',',') }}
                  </td>                      
              </tr>
              <?php $i = 0; ?>
              @foreach ($salesOrdersDetails as $salesOrdersDetails)
              <tr id="row{{ $i }}">
                  <td width="5%" align="right" style="border-left: 1px solid #000;">     
                      {{ $i+1 }}
                  </td> 
                  <td width="30%" align="left" >
                      {{ optional($salesOrdersDetails->item)->name }}
                  </td>                  
                  <td width="10%" align="right">     
                      {{ number_format($salesOrdersDetails->quantity,2,'.',',') }}
                  </td>
                  <td width="10%" align="left">
                      {{ optional($salesOrdersDetails->item)->unit->name }}    
                  </td>   
                  <td width="15%" align="right">     
                      {{ number_format($salesOrdersDetails->price,2,'.',',') }}
                  </td>  
                  <td width="15%" align="right">     
                      {{ number_format($salesOrdersDetails->amount,2,'.',',') }}
                  </td>  
                  
              </tr>
              <?php $i++; ?>
               @endforeach
          </tbody>          
          <tfoot>
                <tr>
                    <td colspan="4" style="border: 0px;"></td>
                    <td style="border: 0px; border-right: 1px solid #000; text-align: left"><span style="font-weight: bold;">Sub Total</span></td>
                    <td style="text-align: right">
                        {{ number_format(round($salesOrdersDp->total_dp),0,'.',',') }}
                    </td>
                </tr>   
                <tr>
                    <td colspan="4" style="border: 0px;"></td>
                    <td style="border: 0px; border-right: 1px solid #000; text-align: left"><span style="font-weight: bold;">Total</span></td>
                    <td style="text-align: right">
                        {{ number_format(round($salesOrdersDp->total_dp),0,'.',',') }}
                    </td>
                </tr> 
                   
                <tr>
                    <td colspan="4" style="border: 0px;"></td>
                    <td style="border: 0px; border-right: 1px solid #000; text-align: left"><span style="font-weight: bold;">VAT</span></td>
                    <td style="text-align: right">  
                        {{ number_format(round($salesOrdersDp->total_dp * $salesOrdersDp->salesOrders->tax),0,'.',',') }}
                    </td>
                </tr>  
                <tr>
                    <td colspan="4" style="border: 0px;"></td>
                    <td style="border: 0px; border-right: 1px solid #000; text-align: left"><span style="font-weight: bold;">Grand Total</span></td>
                    <td style="text-align: right">
                        {{ number_format(round($salesOrdersDp->total_dp + ($salesOrdersDp->total_dp * $salesOrdersDp->salesOrders->tax)),0,'.',',') }}
                    </td>
                </tr>                      
            </tfoot>
        </table>  

        <table width="100%" cellpadding="0" cellspacing="0" style="padding: 10px;">  
            <tr>
                <td style="border-left: 1px solid #000;border-top: 1px solid #000;">{{__('labels.general.said') }} : {{strtoupper(Terbilang::make(round($salesOrdersDp->total_dp + ($salesOrdersDp->total_dp * 0.11))))}} {{__('labels.general.currency') }} </td>
            </tr>
        </table> 
        <table width="100%" cellpadding="0" cellspacing="0" style="border: 0px; border-bottom: 0px; border-right: 0px; ">
            <tr>
                <td width="60%" style="border: 0px; text-align: left; vertical-align: top;">     
                    <strong>NOTES : <br/></strong>
                    PLEASE TRANSFER YOUR PAYMENT TO OUR ACCOUNT<br/>                    
                        <ul>                            
                            <li>
                                BNI <span style="padding-left:2.8em">:</span> Cabang Daan Mogot<br>
                                NO.REK <span style="padding-left:1.1em">:</span> 0798330927
                            </li>   
                            <li>
                                MANDIRI <span style="padding-left:0.5em">:</span> Cabang Bekasi Jatiwaringin<br>
                                NO.REK <span style="padding-left:1.1em">:</span> 167-00-0289822-8
                            </li>                         
                            <li>
                                BCA <span style="padding-left:2.8em">:</span> Cabang Kalimalang<br>
                                NO.REK <span style="padding-left:1.1em">:</span> 2308937700
                            </li>
                            <li>A/N PT. ARGA BAJA LESTARI        </li>
                        </ul>                          
                </td>
                <td width="40%" style="border: 0px; text-align: center; vertical-align: top;" >
                    <strong>PT. ARGA BAJA LESTARI</strong>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>                    
                    <strong>Aenul Khuriyah</strong><br>
                    Finance Manager
                </td>     
            </tr>            
        </table>      
    </div>
<script>
    window.print();
</script>
