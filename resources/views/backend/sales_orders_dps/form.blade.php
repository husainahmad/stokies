<br/>
<br/>
<input name="customers_id" id="customers_id" type="hidden" value="{{ $salesOrdersDp!=null ? ($salesOrdersDp->invoice !=null ? $salesOrdersDp->invoice->customers_id : '') : '' }}">
<input name="sales_persons_id" id="sales_persons_id" type="hidden" value="{{ $salesOrdersDp!=null ? ($salesOrdersDp->invoice!=null ? $salesOrdersDp->invoice->sales_persons_id : '') : '' }}" >
<div class="row">
    <div class="col-sm-6">        
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('no') ? 'has-error' : '' }}">
                        <label for="no" class="col-md-3 control-label">No</label>
                        <div class="col-md-9">
                            <input class="form-control" name="no" type="text" id="no" value="{{ old('no', $salesOrdersDp!=null ? (optional($salesOrdersDp)->invoice!=null ? optional($salesOrdersDp)->invoice->no : 'INV-ABL/'.date('y').'/') : 'INV-ABL/'.date('y').'/') }}" minlength="1" maxlength="45" required="true" placeholder="Enter no here...">
                            {!! $errors->first('no', '<p class="help-block">:message</p>') !!} 
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('sales_orders_id') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label"  for="sales_orders_id" class="col-md-10 control-label">Sales Orders</label>
                        <div class="col-md-9">
                            <select class="form-control" id="sales_orders_id" name="sales_orders_id" required="true">
                                <option value="" style="display: none;" {{ old('sales_orders_id', optional($salesOrdersDp)->sales_orders_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select sales orders</option>

                                @if ((optional($salesOrdersDp)->sales_orders_id) !== null)
                                    <option value="{{ optional($salesOrdersDp)->sales_orders_id }}" selected>
                                        {{ optional($salesOrdersDp)->salesOrder->no }}
                                    </option>
                                @endif

                                @foreach ($salesOrders as $key => $salesOrder)
                                    <option value="{{ $salesOrder->id }}">
                                        {{ $salesOrder->no }}
                                    </option>
                                @endforeach
                            </select>

                            {!! $errors->first('sales_orders_id', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('created_at') ? 'has-error' : '' }}">
                        <label for="created_at" class="col-md-3 col-form-label">Date</label>
                        <div class="col-md-9">
                            <span class="input-group">
                                <span class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                            </span>
                            <input class="form-control" name="created_at" type="text" id="created_at" data-provide="datepicker" 
                                   value="{{ old('created_at', 
                                               $salesOrdersDp!=null ? 
                                               (optional($salesOrdersDp)->invoice!=null ? optional($salesOrdersDp)->invoice->created_at : date('d/m/Y'))
                                               : date('d/m/Y'))  }}" minlength="1" required="true" placeholder="Enter date here...">
                            {!! $errors->first('created_at', '<p class="help-block">:message</p>') !!}
                        </div>
                        
                    </div>
                </div>
            </div>
            
            <div class="row">              
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('due_date') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label"  for="due_date" class="col-md-10 control-label">Due Date</label>
                        <div class="col-md-9">
                            <span class="input-group">
                                <span class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                            </span>
                            <input class="form-control" name="due_date" type="text" id="due_date" data-provide="datepicker"  
                                   value="{{ old('due_date', $salesOrdersDp!=null ? 
                                                     (optional($salesOrdersDp)->invoice!=null ? date('d/m/Y', strtotime(optional($salesOrdersDp)->invoice->due_date)) : date('d/m/Y'))
                                                              : date('d/m/Y'))  }}" minlength="1" required="true" placeholder="Enter created date here...">
                            {!! $errors->first('due_date', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <div class="col-sm-6">        
        <div class="card-body">
            <div class="row">                
                <div class="col-sm-12">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"  for="customers_id" class="col-md-6 control-label">Customers</label>
                        <div class="col-md-9">                            
                            <input class="form-control" name="customer_name" type="text" id="customer_name" 
                                   value="{{ $salesOrdersDp!=null ? $salesOrdersDp->salesOrder->customer->name : '' }}" required="false" >
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">                
                <div class="col-sm-12">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"  for="ref_po" class="col-md-6 control-label">PO Customers</label>
                        <div class="col-md-9">                            
                            <input class="form-control" name="ref_po" type="text" id="ref_po" value="{{ $salesOrdersDp!=null ? $salesOrdersDp->salesOrder->ref_po_customer : '' }}" required="false" >
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('ship_via') ? 'has-error' : '' }}">
                        <label for="ship_via" class="col-md-3 control-label">Ship Via</label>
                        <div class="col-md-9">
                            <input class="form-control" name="ship_via" type="text" id="ship_via" 
                                   value="{{ old('ship_via', $salesOrdersDp!=null ? (optional($salesOrdersDp)->invoice!=null ? (optional($salesOrdersDp)->invoice->ship_via) : '') : '')  }}" minlength="1" maxlength="145" required="false" placeholder="Enter ship via here...">
                            {!! $errors->first('ship_via', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('tax') ? 'has-error' : '' }}">
                        <label for="tax" class="col-md-3 control-label">Tax Persen</label>
                        <div class="col-md-9">
                            <input class="form-control" name="tax" type="text" id="tax" 
                                    value="{{ old('tax', $salesOrdersDp!=null ? (optional($salesOrdersDp)->salesOrder!=null ? (optional($salesOrdersDp)->salesOrder->tax) : '0.11') : '0.11')  }}" minlength="1" maxlength="145" required="false" placeholder="Enter...">
                                {!! $errors->first('tax', '<p class="help-block">:message</p>') !!}
                        </div>    

                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>



