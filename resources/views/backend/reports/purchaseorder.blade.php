@extends('backend.layouts.app')
@section ('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
    @include('backend.sales_orders.includes.breadcrumb-links')
@endsection
@section('content')

    <div class="panel panel-default">

        <div class="panel-heading clearfix">


        </div>

        <div class="panel-body">
        
            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
            
            <br><br>
            <br>
            <form method="POST" action="{{ route('reports.reports.purchase_order.result') }}" accept-charset="UTF-8" id="create_purchase_orders_form" name="create_purchase_orders_form" class="form-horizontal">
            {{ csrf_field() }}
            @if(isset($purchaseOrdersRequest))
                @include ('backend.reports.purchaseform', [
                                        'purchaseOrdersRequest' => $purchaseOrdersRequest,
                                      ])            
            @else
                    @include ('backend.reports.purchaseform', [
                                        'purchaseOrdersRequest' => null,
                                      ])            
            @endif
            
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <input class="btn btn-primary" type="submit" name="action" value="Search">
                        <input class="btn btn-primary" type="button" name="btnPrint" id="btnPrint" value="Print">
                    </div>
                </div>

            </form>
            @isset($purchaseOrdersObjects)
                @if(count($purchaseOrdersObjects) == 0)
                <div class="panel-body text-center">
                    <h4>No Purchase Orders Available!</h4>
                </div>
                @else
                <div class="panel-body panel-body-with-table">
                    <div class="table-responsive">

                        <table class="table table-striped ">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Order Date</th>
                                    <th>Supplier</th>
                                    <th>QTY</th>
                                    <th>Sub Total</th>
                                    <th>Discount</th>
                                    <th>Vat</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($purchaseOrdersObjects as $po)
                                <tr>
                                    <td>{{ $po->purchase_no }}</td>
                                    <td>{{ date('d/m/Y', strtotime($po->ordered_at)) }}</td>
                                    <td>{{ optional($po->supplier)->name }}</td>
                                    <td align="right">{{ $po->total_quantity }}</td>
                                    <td align="right">{{ number_format($po->sub_total,0,'.',',') }}</td>
                                    <td align="right">{{ number_format($po->discount,0,'.',',') }}</td>
                                    <td align="right">{{ number_format($po->vat,0,'.',',') }}</td>
                                    <td align="right">{{ number_format($po->total_amount,0,'.',',') }}</td>                                    
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>


                @endif
            @endisset
            
        </div>
    </div>

@endsection

@push('after-scripts')    
        <script src="/js/bootstrap-datepicker.min.js"></script>
        <link media="all" type="text/css" rel="stylesheet" href="/css/bootstrap-datepicker.css">

    <script>
        var i=1;
        
        $(function() {           
            
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
            $('#from_date').datepicker({
                todayBtn: "linked",
                language: "en",
                autoclose: true,
                todayHighlight: true,
                format: 'dd/mm/yyyy'
            });
            
            $('#to_date').datepicker({
                todayBtn: "linked",
                language: "en",
                autoclose: true,
                todayHighlight: true,
                format: 'dd/mm/yyyy'
            });
                       
            $('#btnPrint').click(function() {                
                $.ajax({
                    url: "{{ route('reports.reports.purchase_order.result') }}",
                    type: "GET",
                    data: {
                        "_token" : $('meta[name="csrf-token"]').attr('content'),
                        "from_date" : $("#from_date").val(),
                        "to_date" : $("#to_date").val(),
                        "category_id" : $("#category_id").val(),
                        "suppliers_id" : $("#suppliers_id").val(),
                        "purchase_no" : $("#purchase_no").val(),
                        "action" : "print"
                    },
                    success: function(response) {
                        var w = window.open();
                        $(w.document.body).html(response);
                    },
                    error: function() {                        
                    }
                });
            });                                   
                       
        });
        
        
    </script>
@endpush
