<!-- Load paper.css for happy printing -->
<link rel="stylesheet" href="dist/paper.css">

<!-- Set page size here: A5, A4 or A3 -->
<!-- Set also "landscape" if you need -->
<style>
    body {
        margin: 0;
        padding: 0;
        background-color: #FAFAFA;
        font: 12pt "Tahoma";
    }
    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }
    .page {
        width: 21cm;
        min-height: 29.7cm;
        padding: 2cm;
        margin: 1cm auto;
        border: 1px #D3D3D3 solid;
        border-radius: 5px;
        background: white;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }
    .subpage {
        padding: 1cm;
        border: 5px red solid;
        height: 256mm;
        outline: 2cm #FFEAEA solid;
    }
    table th, table td {
        border-bottom: 1px solid #000;
        border-right: 1px solid #000;
        padding: 5px 10px 5px 5px;
    }
    @page {
        size: A4;
        margin: 0;
    }
    @media print {
        .page {
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
        }
    }
</style>
    <div class="book">
        <br/>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td width="60%" style='border: 0px;'>                                        
                    <table width="100%" cellpadding="0" cellspacing="0" style='border: 0px solid #000;'>
                        <tr>
                            <td style='border: 0px; padding: 0px;'>                                                                
                                <table width="100%" cellpadding="0" cellspacing="0">                        
                                    <tr>
                                        <td width="25%" style='border: 0px; padding: 0px;'>
                                            <img src="/img/logo.jpg">
                                        </td>
                                        <td style='border: 0px; padding: 0px; font-size: small; vertical-align: middle'>
                                            <strong><div style="font-size: large">PT. ARGA BAJA LESTARI</div></strong>                                            
                                            Jl. Raya Jatiwaringin No. 24B Jati Cempaka<br/>
                                            Pondok Gede. Bekasi 17411<br/>
                                            Telp. 021.84994366 - Fax. 021.84979581<br/>
                                            e-Mail: argabaja@yahoo.com<br/>
                                        </td>
                                    </tr>
                                </table>    
                                <br/>
                            </td>
                        </tr>
                        <tr>
                            <td style="border: 0px;">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td width="35%" style='border: 0px; padding: 0px;'>
                                            <strong>NO</strong>
                                        </td>
                                        <td width="1%" style='border: 0px; padding: 0px;'>:</td>
                                        <td width="65%" style='border: 0px; padding: 0px;'>
                                            <strong>{{ $salesOrders->no }}</strong>
                                        </td>                                        
                                    </tr>
                                    <tr>
                                        <td width="35%" style="border: 0px; padding: 0px;">
                                            <strong>CUSTOMER PO</strong>
                                        </td>
                                        <td width="1%" style="border: 0px; padding: 0px;">:</td>
                                        <td width="65%" style="border: 0px; padding: 0px;">
                                            {{ $salesOrders->ref_po_customer }}
                                        </td>                                        
                                    </tr>   
                                    <tr>
                                        <td width="35%" style="border: 0px; padding: 0px;">
                                            <strong>PO DATE</strong>
                                        </td>
                                        <td width="1%" style="border: 0px; padding: 0px;">:&nbsp;</td>
                                        <td width="65%" style="border: 0px; padding: 0px;">
                                            {{ date_format(date_create($salesOrders->purchase_order_date), 'd-m-Y') }}
                                        </td>                                        
                                    </tr>    
                                    <tr>
                                        <td width="35%" style="border: 0px; padding: 0px;">
                                            <strong>SALES PERSON</strong>
                                        </td>
                                        <td width="1%" style="border: 0px; padding: 0px;">:&nbsp;</td>
                                        <td width="65%" style="border: 0px; padding: 0px;">
                                            {{ optional($salesOrders->salesPerson)->name }}
                                        </td>                                        
                                    </tr> 
                                    <tr>
                                        <td width="35%" style="border: 0px; padding: 0px;">
                                           <strong>TOC</strong>
                                        </td>
                                        <td width="1%" style="border: 0px; padding: 0px;">:&nbsp;</td>
                                        <td width="65%" style="border: 0px; padding: 0px;">
                                            {{ optional($salesOrders->termBilling)->name }}
                                        </td>
                                    </tr>                                                                        
                                    <tr>
                                        <td width="35%" style="border: 0px; padding: 0px;">
                                           <strong>TRANSPORT</strong>
                                        </td>
                                        <td width="1%" style="border: 0px; padding: 0px;">:&nbsp;</td>
                                        <td width="65%" style="border: 0px; padding: 0px;">
                                            {{ $salesOrders->transport }}
                                        </td>
                                    </tr> 
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td width="40%" style='border: 0px; vertical-align: top;'>
                    <h2 align="center">{{ !empty($title) ? $title : 'Sales Orders' }}</h2>
                    <br/>
                    <table width="100%" cellpadding="0" cellspacing="0">                        
                        <tr>
                            <td style='border: 1px solid #000;padding: 10px;'>
                                CUSTOMER : <br/><br/>
                                <strong>{{ optional($salesOrders->customer)->name }} </strong><br/>
                                {{ optional($salesOrders->customer)->address }} <br/>
                                {{ optional($salesOrders->customer)->phone }} <br/>
                                <br/>
                            </td>
                        </tr>
                    </table>                    
                </td>                
            </tr>
        </table>
        <br/>
        <table width="100%" cellpadding="0" cellspacing="0">  
            <thead>
              <tr>
               <th style='border-left: 1px solid #000;border-top: 1px solid #000;'>No</th>     
               <th style='border-top: 1px solid #000;'>Name</th>                                          
               <th style='border-top: 1px solid #000;'>Quantity</th>
               <th style='border-top: 1px solid #000;'>Unit</th>
               <th style='border-top: 1px solid #000;'>Price</th>
               <th style='border-top: 1px solid #000;'>Amount</th>
             </tr>
          </thead>
          <tbody>
              <?php $i = 0; ?>
              @foreach ($salesOrdersDetails as $salesOrdersDetails)
              <tr id="row{{ $i }}">
                  <td width="5%" align="right" style="border-left: 1px solid #000;">     
                      {{ $i+1 }}
                  </td>
                  <td width="30%" align="left" >
                      {{ optional($salesOrdersDetails->item)->name }}
                  </td>                  
                  <td width="10%" align="right">     
                      {{ $salesOrdersDetails->quantity }}  
                  </td>
                  <td width="10%" align="left">
                      {{ optional($salesOrdersDetails->item)->unit->name }}    
                  </td>   
                  <td width="15%" align="right">     
                      {{ $salesOrdersDetails->price }}
                  </td>  
                  <td width="20%" align="right">     
                      {{ $salesOrdersDetails->amount }}
                  </td>  
                  
              </tr>
              <?php $i++; ?>
               @endforeach
          </tbody>          
          <tfoot>
                <tr>
                    <td colspan="4" style="border: 0px;"></td>
                    <td style="border: 0px; border-right: 1px solid #000; text-align: left"><span style="font-weight: bold;">Sub Total</span></td>
                    <td style="text-align: right">
                        {{ $salesOrders->sub_total }}
                    </td>
                </tr>                                  
                <tr>
                    <td colspan="4" style="border: 0px;"></td>
                    <td style="border: 0px; border-right: 1px solid #000; text-align: left"><span style="font-weight: bold;">Discount</span></td>
                    <td style="text-align: right">             
                        {{ $salesOrders->discount }}
                    </td>
                </tr>
                <tr>
                    <td colspan="4" style="border: 0px;"></td>
                    <td style="border: 0px; border-right: 1px solid #000; text-align: left"><span style="font-weight: bold;">Total</span></td>
                    <td style="text-align: right">        
                        {{ $salesOrders->total_amount }}
                    </td>
                </tr>      
                <tr>
                    <td colspan="4" style="border: 0px;"></td>
                    <td style="border: 0px; border-right: 1px solid #000; text-align: left"><span style="font-weight: bold;">VAT</span></td>
                    <td style="text-align: right">  
                        {{ $salesOrders->vat }}
                    </td>
                </tr>                                   
                <tr>
                    <td colspan="4" style="border: 0px;"></td>
                    <td style="border: 0px; border-right: 1px solid #000; text-align: left"><span style="font-weight: bold;">Grand Total</span></td>
                    <td style="text-align: right">
                        {{ $salesOrders->total_amount }}
                    </td>
                </tr>      
                </tr>
            </tfoot>
        </table>  
        <p>&nbsp;</p>
        
        <table width="100%" cellpadding="0" cellspacing="0" style="border: 0px; border-bottom: 0px; border-right: 0px; ">
            <tr>
                <td width="50%" style="border: 0px;">                    
                </td>
                <td width="50%" style="border: 0px; text-align: center" >
                    Jakarta, {{ date('d-m-Y') }}
                </td>                
            </tr>
            
            <tr>
                <td width="50%" style="border: 0px;">                    
                </td>
                <td width="50%" style="border: 0px;">
                    
                </td>                
            </tr>
            
            <tr>
                <td width="50%" style="border: 0px;">                    
                </td>
                <td width="50%" style="border: 0px; text-align: center">
                    Sales
                </td>                
            </tr>
            
             <tr>
                <td width="50%" style="border: 0px;">                    
                </td>
                <td width="50%" style="border: 0px;">
                    &nbsp;
                </td>                
            </tr>
            
             <tr>
                <td width="50%" style="border: 0px;">                    
                </td>
                <td width="50%" style="border: 0px; text-align: center">
                    {{ optional($salesOrders->salesPerson)->name }}
                </td>                
            </tr>
            
            
        </table>      
        <p></p>
        <p></p>
        
    </div>
<script>
    window.print();
</script>
