@extends('backend.layouts.app')
@section ('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
    @include('backend.sales_orders.includes.breadcrumb-links')
@endsection
@section('content')

    <div class="panel panel-default">

        <div class="panel-heading clearfix">

            <div class="btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('sales_orders.sales_orders.index') }}" class="btn btn-primary" title="Show All Sales Orders">
                    <span class="fa fa-th-list" aria-hidden="true"></span>
                </a>
            </div>

        </div>

        <div class="panel-body">
        
            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            <form method="POST" action="{{ route('sales_orders.sales_orders.store') }}" accept-charset="UTF-8" id="create_sales_orders_form" name="create_sales_orders_form" class="form-horizontal">
            {{ csrf_field() }}
            @include ('backend.sales_orders.form', [
                                        'salesOrders' => null,
                                      ])
               <div class="card">
                    <div class="card-body">                        
                        <div class="table-responsive">  
                            <table class="table table-bordered" id="dynamic_field">  
                                <thead>
                                  <tr>
                                   <th>Item</th>                    
                                   <th>Quantity</th>
                                   <th>Unit</th>
                                   <th>Price</th>
                                   <th>Amount</th>
                                   <th>Description</th>               
                                   <th>Actions</th>
                                 </tr>
                              </thead>
                              <tbody>
                                  <tr id="row0">
                                      <td>
                                          <select class="form-control items-selection" id="items_id[]" name="items_id[]" required="true"  row-index="0">                                               
                                              <option value="">Please select</option>  
                                            @foreach ($items as $key => $item)
                                                <option value="{{ $key }}">
                                                    {{ $item }}
                                                </option>
                                            @endforeach
                                          </select>
                                      </td>

                                      <td width="10%">
                                          <input class="form-control text-count quantity" name="quantity[]" type="number" step="any" id="quantity[]" value="0" min="1" max="2147483647" required="true" placeholder="Enter">
                                      </td>
                                       <td width="10%">
                                           <input class="form-control text-count item-units" name="units[]" type="text" id="units[]" value="" min="1" max="2147483647" required="true" placeholder="Enter">
                                      </td>
                                      <td width="10%">
                                          <input class="form-control text-count item-price" name="price[]" type="number" step="any" id="price[]" value="0" placeholder="Enter here...">                                   
                                      </td>    
                                      <td width="15%">
                                          <input class="form-control text-count-amount" name="amount[]" type="number" step="any" id="amount[]" value="0" placeholder="Enter here...">
                                      </td>  
                                      <td width="10%">
                                          <input class="form-control" name="description[]" type="text" id="description[]" value="-" placeholder="">
                                      </td>
                                      <td width="5%">
                                          <button type="button" class="btn btn-danger-item btn-delete-item" title="Delete Item" id="0" >
                                            <span class="fa fa-trash" aria-hidden="true"></span>
                                          </button>
                                      </td>
                                  </tr>
                              </tbody>
                              <tfoot>
                                  <tr>
                                      <td colspan="2"><span style="font-weight: bold;">Total</span></td>
                                      <td><span style="font-weight: bold;" id="totalQuantity"></span></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>
                                          <button type="button" class="btn btn-add-more-item" title="Add more item">
                                            <span class="fa fa-plus" aria-hidden="true"></span>
                                          </button>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td colspan="4"></td>
                                      <td><span style="font-weight: bold;">Sub Total</span></td>
                                      <td colspan="2">
                                          <input class="form-control" name="sub_total" type="number" id="sub_total" value="0" min="1" step="any"  required="true" placeholder="">
                                      </td>
                                  </tr>                                  
                                  <tr>
                                      <td colspan="4"></td>
                                      <td><span style="font-weight: bold;">Discount</span></td>
                                      <td colspan="2">
                                          <input class="form-control text-count" name="discount" type="number" id="discount" step="any" value="0" required="true" placeholder="">
                                      </td>
                                  </tr>
                                  <tr>
                                      <td colspan="4"></td>
                                      <td><span style="font-weight: bold;">Total</span></td>
                                      <td colspan="2">
                                          <input class="form-control" name="total" type="number" id="total" step="any" value="0" required="true" placeholder="">
                                      </td>
                                  </tr>      
                                  <tr>
                                      <td colspan="4"></td>
                                      <td><span style="font-weight: bold;">VAT</span></td>
                                      <td colspan="2">
                                          <input class="form-control" name="vat" type="number" step="any" id="vat" value="0" required="true" placeholder="">
                                      </td>
                                  </tr>                                   
                                  <tr>
                                      <td colspan="4"></td>
                                      <td><span style="font-weight: bold;">Grand Total</span></td>
                                      <td colspan="2">
                                          <input class="form-control" name="grand_total" type="number" id="grand_total" value="" required="true" step="any" placeholder="">
                                      </td>
                                  </tr>                                        
                              </tfoot>
                            </table>                              
                        </div>
                    </div>
                </div>                
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <input class="btn btn-primary" type="submit" value="Add">
                    </div>
                </div>

            </form>

        </div>
    </div>

@endsection

@push('after-scripts')    
        <script src="/js/bootstrap-datepicker.min.js"></script>
        <link media="all" type="text/css" rel="stylesheet" href="/css/bootstrap-datepicker.css">

    <script>
        var i=1;
        
        $(function() {           
            
            $('#sales_order_date').datepicker({
                todayBtn: "linked",
                language: "it",
                autoclose: true,
                todayHighlight: true,
                format: 'dd/mm/yyyy' 
            });
            
            $('#sales_order_delivery').datepicker({
                todayBtn: "linked",
                language: "it",
                autoclose: true,
                todayHighlight: true,
                format: 'dd/mm/yyyy' 
            });
            
            $( ".items-selection" ).change(function() {
                var index = $(".items-selection").index(this);                    
                onItemSelectionChange($(this).val(), index);
            });
            $('.btn-add-more-item').click(function(){  
               $('#dynamic_field').append('<tr id="row'+ i +'">'+
                                     '<td>'+
                                         '    <select class="form-control items-selection" id="items_id[]" name="items_id[]" required="true" row-index="'+i+'">'+ optionValues +                                                
                                         '    </select>'+
                                         '</td>'+
                                         '<td width="10%">'+
                                         '    <input class="form-control text-count" name="quantity[]" type="number" id="quantity[]" value="0" min="1" max="2147483647" required="true" placeholder="Enter">'+
                                         '</td>'+
                                         '<td width="10%">'+
                                         '    <input class="form-control item-units" name="units[]" type="text" id="units[]" value="" required="true" placeholder="Enter">'+
                                         '</td>'+
                                         '<td width="10%">'+
                                         '    <input class="form-control text-count item-price" name="price[]" type="number" step="any" id="price[]" value="0" min="0" max="9999999999" required="true" placeholder="Enter price here...">'+
                                         '</td>'+
                                         '<td  width="15%">'+
                                         '    <input class="form-control text-count-amount" name="amount[]" type="number" step="any" id="amount[]" value="0" min="0" max="999999999999" required="true" placeholder="Enter amount here...">'+
                                         '</td>'+
                                         '<td width="10%">'+
                                         '    <input class="form-control" name="description[]" type="text" id="description[]" value="-">'+
                                         '</td>'+
                                         '<td width="5%">'+
                                         '    <button type="button" class="btn btn-danger-item btn-delete-item" title="Delete Item" id="'+ i +'" >'+
                                         '      <span class="fa fa-trash" aria-hidden="true"></span>'+
                                         '    </button>'+
                                         '</td>'+
                                 '</tr>');  
                $(".text-count").bind('keyup mouseup', function () {
                    countTotal();        
               });    
                $(".items-selection").change(function() {                     
                    var index = $(".items-selection").index(this);                    
                    onItemSelectionChange($(this).val(), index);
                });
                return false;
           });  
           
           $(document).on('click', '.btn-delete-item', function(){  
                var button_id = $(this).attr("id");   
                $('#row'+button_id+'').remove();  
           });
           
           $(".text-count").bind('keyup mouseup', function () {
                countTotal();        
           }); 
           
           function onItemSelectionChange(value, index) {
               console.log(index);               
               $.each($('.item-price'), function() {
                    console.log($(".item-price").index(this));  
                    if ($(".item-price").index(this)===index) {                        
                        $.ajax({
                            type: "GET",
                            url: "{{ route('items.items.index') }}/show/json/" + value,
                            success: function( response ) {
                                console.log(response.price);
                                $(".item-price").eq(index).val(response.price.selling_price);
                                $(".item-units").eq(index).val(response.unit.name);
//                                $(".item-code").eq(index).val(response.code);
                                countTotal();
                            }
                        });
                    }
               });
           }
           
           function countTotal() {
                var totalQuantity = 0;
                var totalAmount = 0;
                var values = $("input[name='quantity[]']").map(function(){
                    return $(this).val();
                }).get();

                var prices = $("input[name='price[]']").map(function(){
                    return $(this).val();
                }).get();

                console.log(isNaN(values));
                for (var i = 0, len = values.length; i < len; i++) {
                    try {   
                        var qty = isNaN(parseInt(values[i])) ? 0 : parseInt(values[i]);
                        totalQuantity += qty;
                        var price = isNaN(parseFloat(prices[i])) ? 0 : parseFloat(prices[i]);
                        var amount = price*qty;
                        $(".text-count-amount").eq(i).val(amount);                        
                    } catch (err) {
                        totalQuantity += 0;
                    }                   
                }
                console.log(totalQuantity); 
                $('#totalQuantity').text(totalQuantity);

                var valueAmountPrice = $("input[name='amount[]']").map(function(){
                    return $(this).val();
                }).get();

                var totalAmountPrice = 0;
                for (var i = 0, len = valueAmountPrice.length; i < len; i++) {
                    try {   
                        totalAmountPrice += isNaN(parseFloat(valueAmountPrice[i])) ? 0 : parseFloat(valueAmountPrice[i]);
                    } catch (err) {
                        totalAmountPrice += 0;
                    }                   
                }
                console.log(totalAmountPrice); 
                $('#sub_total').val(totalAmountPrice);

                var discount = $('#discount').val();
                console.log(discount);
                var total = totalAmountPrice-discount;
                $('#total').val(total);
                var vat = total*0.11;
                $('#vat').val(vat);

                var grand_total = total-vat;
                $('#grand_total').val(grand_total);
                return false;
           }           
        });
        var optionValues = '<option value="">Please select</option>';
        @foreach ($items as $key => $item)
            optionValues += '<option value="{{ $key }}">'+
                '{{ $item }}'+
            '</option>';
        @endforeach
        
    </script>
@endpush
