<!-- Load paper.css for happy printing -->
<link rel="stylesheet" href="dist/paper.css">

<!-- Set page size here: A5, A4 or A3 -->
<!-- Set also "landscape" if you need -->
<style>
    body {
        margin-top: 50px;
        margin-left: 50px;
        margin-right: 50px;
        margin-bottom: 20px; 
        padding: 0;
        background-color: #FAFAFA;
        font: 8pt "Tahoma";
    }
    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }
    .page {
        size: landscape;
        width: 21cm;
        min-height: 29.7cm;
        padding: 2cm;
        margin: 1cm auto;
        border: 1px #D3D3D3 solid;
        border-radius: 5px;
        background: white;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }
    .subpage {
        padding: 1cm;
        border: 5px red solid;
        height: 256mm;
        outline: 2cm #FFEAEA solid;
    }
    table th, table td {
        border-bottom: 1px solid #000;
        border-right: 1px solid #000;
        padding: 5px 10px 5px 5px;
    }
    @page {
        size: A4 landscape;
        margin: 0;
    }
    @media print {
        .page {
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
        }
    }
    
</style>
    <div class="book">
        <table width="100%" cellpadding="0" cellspacing="0">                        
            
            <tr>
                <td style='border: 0px; padding: 0px; vertical-align: top;' align="center">
                    <h2 align="center">Outstanding Report</h2>
                </td>
            </tr>
        </table> 
        
        <br/>
        
        <table class="table table-striped ">
            <thead>
                <tr>
                    <th style='border-left: 1px solid #000;border-top: 1px solid #000;'>No</th>
                    <th style='border-top: 1px solid #000;'>Sales Order Date</th>
                    <th style='border-top: 1px solid #000;'>Ref Po Customer</th>
                    <th style='border-top: 1px solid #000;'>Status</th>
                    <th style='border-top: 1px solid #000;'>Customer</th>
                    <th style='border-top: 1px solid #000;'>Total</th>
                    <th style='border-top: 1px solid #000;'>Total DP</th>
                    <th style='border-top: 1px solid #000;'>Invoice</th>
                </tr>
            </thead>
            <tbody>
            @foreach($salesOrdersObjects as $salesOrders)
                <tr>
                    <td style='border-left: 1px solid #000;'>{{ $salesOrders->no }}</td>
                    <td>{{ $salesOrders->sales_order_delivery }}</td>
                    <td>{{ $salesOrders->ref_po_customer }}</td>
                    @if($salesOrders->status==0)
                        <td>OutStanding</td>
                    @endif
                    @if($salesOrders->status==1)
                        <td>Completed</td>
                    @endif
                    <td>{{ optional($salesOrders->customer)->name }}</td>
                    <td>{{ $salesOrders->total }}</td>
                    <td>{{ $salesOrders->total_dp }}</td>
                    <td>{{ $salesOrders->total_invoice }}</td>
                    
                </tr>
            @endforeach
            </tbody>
        </table>
        
        <p></p>
    </div>
<script>
    window.print();
</script>
