@extends('backend.layouts.app')
@section ('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
    @include('backend.sales_orders.includes.breadcrumb-links')
@endsection
@section('content')

    <div class="panel panel-default">
  
        <div class="panel-heading clearfix">

            <div class="btn-group btn-group-sm pull-right" role="group">

                <a href="{{ route('sales_orders.sales_orders.index') }}" class="btn btn-primary" title="Show All Sales Orders">
                    <span class="fa fa-th-list" aria-hidden="true"></span>
                </a>

                <a href="{{ route('sales_orders.sales_orders.create') }}" class="btn btn-success" title="Create New Sales Orders">
                    <span class="fa fa-plus" aria-hidden="true"></span>
                </a>

            </div>
        </div>

        <div class="panel-body">

            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            <form method="POST" action="{{ route('sales_orders.sales_orders.update', $salesOrders->id) }}" id="edit_sales_orders_form" name="edit_sales_orders_form" accept-charset="UTF-8" class="form-horizontal">
            {{ csrf_field() }}
            <input name="_method" type="hidden" value="PUT">
            @include ('backend.sales_orders.form', [
                                        'salesOrders' => $salesOrders,
                                      ])

                <div class="card">
                    <div class="card-body">                        
                        <div class="table-responsive">  
                            <table class="table table-bordered" id="dynamic_field">  
                                <thead>
                                  <tr>
                                   <th>Item</th>
                                   <th>Quantity</th>
                                   <th>Unit</th>
                                   <th>Price</th>
                                   <th>Amount</th>
                                   <th>Description</th>
                                   <th>Actions</th>
                                 </tr>
                              </thead>
                              <tbody>
                                  <?php $i = 0; ?>
                                  @foreach ($salesOrdersDetails as $salesOrdersDetail)
                
                                  <tr id="row{{ $i }}">
                                      <td>
                                          <select class="form-control items-selection" id="items_id[]" name="items_id[]" required="true"  row-index="{{ $i }}">                                               
                                              <option value="">Please select</option>
                                            @foreach ($items as $key => $item)
                                                @if ($key==$salesOrdersDetail->items_id)  
                                                <option value="{{ $key }}" selected>
                                                @else
                                                <option value="{{ $key }}">
                                                @endif
                                                    {{ $item }}
                                                </option>
                                            @endforeach
                                          </select>
                                      </td>
                                      <td width="10%">
                                          <input class="form-control text-count" name="quantity[]" type="number" id="quantity[]" step="any" min="1" max="2147483647" required="true" value="{{ $salesOrdersDetail->quantity }}" placeholder="Enter">
                                      </td>
                                      <td width="10%">
                                           <input class="form-control item-units" name="units[]" step="any" type="text" id="units[]" value="{{ $salesOrdersDetail->item->unit->name }}" min="1" max="2147483647" required="true" placeholder="Enter">
                                      </td>
                                      <td width="10%">
                                          <input class="form-control text-count item-price" name="price[]" step="any" type="number" id="price[]" min="-9" max="99999999" required="true" placeholder="Enter price here..." value="{{ $salesOrdersDetail->price }}">                                   
                                      </td>
                                      <td width="15%">
                                          <input class="form-control text-count-amount" name="amount[]" step="any" type="number" id="amount[]" value="{{ $salesOrdersDetail->amount }}" min="-9" max="99999999999" required="true" placeholder="Enter amount here..." >                                   
                                      </td>  
                                      <td width="10%">
                                          <input class="form-control" name="description[]" type="tex" id="description[]" value="{{ $salesOrdersDetail->description }}" min="-9" max="99999999999" required="true" placeholder="Enter amount here..." >                                   
                                      </td>  
                                      <td width="5%">
                                          <button type="button" class="btn btn-danger-item btn-delete-item" title="Delete Item" id="{{ $i }}" >
                                            <span class="fa fa-trash" aria-hidden="true"></span>
                                          </button>
                                      </td>
                                  </tr>
                                  <?php $i++; ?>
                                  @endforeach
                              </tbody>
                              <tfoot>
                                  <tr>      
                                      
                                      <td><span style="font-weight: bold;">Total</span></td>
                                      <td><span style="font-weight: bold;" id="totalQuantity">{{$salesOrders->total_quantity}}</span></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>
                                          <button type="button" class="btn btn-add-more-item" title="Add more item">
                                            <span class="fa fa-plus" aria-hidden="true"></span>
                                          </button>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td colspan="4"></td>
                                      <td><span style="font-weight: bold;">Sub Total</span></td>
                                      <td colspan="2">
                                          <input class="form-control" name="sub_total" type="number" step="any" id="sub_total" value="{{$salesOrders->sub_total}}" min="1" required="true" placeholder="">
                                      </td>
                                  </tr>                                  
                                  <tr>
                                      <td colspan="4"></td>
                                      <td><span style="font-weight: bold;">Discount</span></td>
                                      <td colspan="2">
                                          <input class="form-control text-count" name="discount" step="any" type="number" id="discount" value="{{$salesOrders->discount}}" required="true" placeholder="">
                                      </td>
                                  </tr>
                                  <tr>
                                      <td colspan="4"></td>
                                      <td><span style="font-weight: bold;">Total</span></td>
                                      <td colspan="2">
                                          <input class="form-control" name="total" type="number" step="any" id="total" value="{{$salesOrders->sub_total-$salesOrders->discount}}" required="true" placeholder="">
                                      </td>
                                  </tr>      
                                  <tr>
                                      <td colspan="4"></td>
                                      <td><span style="font-weight: bold;">VAT</span></td>
                                      <td colspan="2">
                                          <input class="form-control" name="vat" type="number" id="vat" step="any" value="{{$salesOrders->vat}}" required="true" placeholder="">
                                      </td>
                                  </tr>                                   
                                  <tr>
                                      <td colspan="4"></td>
                                      <td><span style="font-weight: bold;">Grand Total</span></td>
                                      <td colspan="2">
                                          <input class="form-control" name="grand_total" type="number" step="any" id="grand_total" value="{{$salesOrders->total_amount}}" required="true" placeholder="">
                                      </td>
                                  </tr>      
                                  </tr>
                              </tfoot>
                            </table>                              
                        </div>
                    </div>
                </div>       
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <input class="btn btn-primary" type="submit" value="Update">
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection
@push('after-scripts')    
        <script src="/js/bootstrap-datepicker.min.js"></script>
        <link media="all" type="text/css" rel="stylesheet" href="/css/bootstrap-datepicker.css">

    <script>
        var i=<?=$i?>;
        
        $( document ).ready(function() {
            //countTotal();   
            
            $(".text-count").bind('keyup mouseup', function () {
                countTotal();        
            }); 
        });
        $(function() {           
            $('#sales_order_date').datepicker({
                todayBtn: "linked",
                language: "it",
                autoclose: true,
                todayHighlight: true,
                format: 'dd/mm/yyyy' 
            });
            
            $('#sales_order_delivery').datepicker({
                todayBtn: "linked",
                language: "it",
                autoclose: true,
                todayHighlight: true,
                format: 'dd/mm/yyyy' 
            });
            $( ".items-selection" ).change(function() {
                var index = $(".items-selection").index(this);                    
                onItemSelectionChange($(this).val(), index);
            });
            $('.btn-add-more-item').click(function(){  
               $('#dynamic_field').append('<tr id="row'+ i +'">'+
                                     '<td>'+
                                         '    <select class="form-control items-selection" id="items_id[]" name="items_id[]" required="true" row-index="'+i+'">'+ optionValues +                                                
                                         '    </select>'+
                                         '</td>'+
                                         '<td>'+
                                         '    <input class="form-control text-count" name="quantity[]" type="number" id="quantity[]" value="" min="0" max="2147483647" required="true" placeholder="Enter">'+
                                         '</td>'+
                                         '<td>'+
                                         '    <input class="form-control item-units" name="units[]" type="text" id="units[]" value="" required="true" placeholder="Enter">'+
                                         '</td>'+
                                         '<td>'+
                                         '    <input class="form-control text-count item-price" name="price[]" type="number" id="price[]" value="" min="0" max="9999999999" required="true" placeholder="Enter price here...">'+
                                         '</td>'+
                                         '<td>'+
                                         '    <input class="form-control text-count-amount" name="amount[]" type="number" id="amount[]" value="" min="0" max="999999999999" required="true" placeholder="Enter amount here...">'+
                                         '</td>'+
                                         '<td width="10%">'+
                                         '    <input class="form-control" name="description[]" type="text" id="description[]" value="-">'+
                                         '</td>'+
                                         '<td width="10%">'+
                                         '    <button type="button" class="btn btn-danger-item btn-delete-item" title="Delete Item" id="'+ i +'" >'+
                                         '      <span class="fa fa-trash" aria-hidden="true"></span>'+
                                         '    </button>'+
                                         '</td>'+
                                 '</tr>');  
                $(".text-count").bind('keyup mouseup', function () {
                    countTotal();        
               });    
                $(".items-selection").change(function() {                     
                    var index = $(".items-selection").index(this);                    
                    onItemSelectionChange($(this).val(), index);
                });
                countTotal(); 
                
                return false;
           });  
           $(document).on('click', '.btn-delete-item', function(){  
                var button_id = $(this).attr("id");   
                $('#row'+button_id+'').remove();  
           });
           
           $(".text-count").bind('keyup mouseup', function () {
                countTotal();        
           }); 
           
           function onItemSelectionChange(value, index) {
               console.log(index);               
               $.each($('.item-price'), function() {
                    console.log($(".item-price").index(this));  
                    if ($(".item-price").index(this)===index) {                        
                        $.ajax({
                            type: "GET",
                            url: "{{ route('items.items.index') }}/show/json/" + value,
                            success: function( response ) {
                                console.log(response.price);
                                $(".item-price").eq(index).val(response.price.purchase_price);
                                $(".item-units").eq(index).val(response.unit.name);
                                countTotal();
                            }
                        });
                    }
               });
           }                     
        });
        
        function countTotal() {
            var totalQuantity = 0;
            var totalAmount = 0;
            var values = $("input[name='quantity[]']").map(function(){
                return $(this).val();
            }).get();

            var prices = $("input[name='price[]']").map(function(){
                return $(this).val();
            }).get();

            console.log(isNaN(values));
            for (var i = 0, len = values.length; i < len; i++) {
                try {   
                    var qty = isNaN(parseInt(values[i])) ? 0 : parseInt(values[i]);
                    totalQuantity += qty;
                    var price = isNaN(parseFloat(prices[i])) ? 0 : parseFloat(prices[i]);
                    var amount = price*qty;
                    $(".text-count-amount").eq(i).val(amount);                        
                } catch (err) {
                    totalQuantity += 0;
                }                   
            }
            console.log(totalQuantity); 
            $('#totalQuantity').text(totalQuantity);

            var valueAmountPrice = $("input[name='amount[]']").map(function(){
                return $(this).val();
            }).get();

            var totalAmountPrice = 0;
            for (var i = 0, len = valueAmountPrice.length; i < len; i++) {
                try {   
                    totalAmountPrice += isNaN(parseFloat(valueAmountPrice[i])) ? 0 : parseFloat(valueAmountPrice[i]);
                } catch (err) {
                    totalAmountPrice += 0;
                }                   
            }
            console.log(totalAmountPrice); 
            $('#sub_total').val(totalAmountPrice);

            var discount = $('#discount').val();
            console.log(discount);
            var total = totalAmountPrice-discount;
            $('#total').val(total);
            var vat = total*0.11;
            $('#vat').val(vat);

            var grand_total = total-vat;
            $('#grand_total').val(grand_total);
            return false;
       } 
       
        var optionValues = '<option value="">Please select</option>';
        @foreach ($items as $key => $item)
            optionValues += '<option value="{{ $key }}">'+
                '{{ $item }}'+
            '</option>';
        @endforeach
        
    </script>
@endpush