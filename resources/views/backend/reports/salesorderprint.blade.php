<!-- Load paper.css for happy printing -->
<link rel="stylesheet" href="../../css/paper.css">
<!--<link rel="stylesheet" href="../../fonts/Dotimatrix_5/stylesheet.css">-->
<!-- Set page size here: A5, A4 or A3 -->
<!-- Set also "landscape" if you need -->
<style>    
    body {       
        display: block; 
        margin-top: 50px;
        margin-left: 30px;
        margin-right: 20px;
        margin-bottom: 20px;        
        padding: 1;
        background-color: #FAFAFA;
        font-size: xx-small;
        color: #000;
        font-family: Consolas;
    }
    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }
    .page {
        width: 27.7cm;
        min-height: 21cm;
        padding: 2cm;
        margin: 1cm auto;
        border: 1px #D3D3D3 solid;
        border-radius: 5px;
        background: white;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }
    .subpage {
        padding: 1cm;
        border: 5px red solid;
        height: 256mm;
        outline: 2cm #FFEAEA solid;
    }
    
    table th, table td {
        border-bottom: 1px solid #000;
        border-right: 1px solid #000;
        padding: 5px 10px 5px 5px;
    }
       
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    
    @page {
        size: landscape;
    }
 
</style>
    <div>
        <table width="100%" cellpadding="0" cellspacing="0" style='border: 0px solid #000; font-size:14px'>
            <tr>
                <td style='border: 0px; padding: 0px;' align="center">
                        <strong>SALES ORDER REPORT</strong><br>                                            
                    <strong>Customer : {{ $salesOrdersRequest->customer_name }}</strong><br/>                    
                    <strong>Category : {{ $salesOrdersRequest->category_name }}</strong><br/>                                            
                    Date : {{ $salesOrdersRequest->date_period }}                                   
                </td>
            </tr>
        </table>
        <table width="100%" cellpadding="0" cellspacing="0" style="padding: 0px; font-size:10px">                
            
            <tbody>
                <tr>
                    <td style='border-top: 1px solid #000;border-left: 1px solid #000;' align="center">NO</td>
                    <td style='border-top: 1px solid #000;' align="center">SO DATE</td>
                    <td style='border-top: 1px solid #000;' align="center">REF PO</td>
                    <td style='border-top: 1px solid #000;' align="center">CUSTOMER</td>
                    <td style='border-top: 1px solid #000;' align="center">ITEM</td>
                    <td style='border-top: 1px solid #000;' align="center">QTY</td>
                    <td style='border-top: 1px solid #000;' align="center">UNIT</td>
                    <td style='border-top: 1px solid #000;' align="center">SUB TOTAL</td>
                    <td style='border-top: 1px solid #000;' align="center">DISCOUNT</td>
                    <td style='border-top: 1px solid #000;' align="center">VAT</td>
                    <td style='border-top: 1px solid #000;' align="center">TOTAL</td>

                </tr>
            @php
            $total = 0;
            @endphp
            @foreach($salesOrdersObjects as $salesOrders)
                @php 
                    $total_quantity = 0; 
                    $amount = 0;
                    $vat = 0;
                @endphp
                @php 
                    $rowspan = 1; 
                @endphp
                
                @foreach($salesOrders->salesOrdersDetails as $sod)
                    @forelse($salesOrdersRequest->items as $item)
                        @if ($item->id == $sod->item->id)
                            @php 
                                $rowspan++;
                            @endphp
                            @php 
                                $amount+=$sod->amount;
                                if ($salesOrders->vat>0) {
                                    $vat += ($amount - $salesOrders->discount) * $salesOrders->tax;
                                }
                            @endphp
                        @endif
                    @empty
                        @php 
                            $rowspan++;
                        @endphp
                        @php 
                            $amount+=$sod->amount;                            
                            if ($salesOrders->vat>0) {
                                $vat = $salesOrders->vat;
                            }
                        @endphp
                    @endforelse
                @endforeach       
				
                @php
                        $total += ($amount - $salesOrders->discount) + $vat;
                @endphp
					
                <tr>
                    <td style="border-left: 1px solid #000;" valign="top" rowspan="{{ $rowspan }}">
                    @php
                            $sono = $salesOrders->no;
                            $sono = substr($salesOrders->no, strlen($salesOrders->no)-3, 3);
                    @endphp
                    {{ $sono }}
                    </td>
                    <td valign="top" rowspan="{{ $rowspan }}">{{ date('d/m/Y', strtotime($salesOrders->sales_order_date)) }}</td>
                    <td valign="top" rowspan="{{ $rowspan }}">{{ $salesOrders->ref_po_customer }}</td>
                    <td valign="top" rowspan="{{ $rowspan }}">{{ optional($salesOrders->customer)->name }}</td>
                    <td valign="top" style="border-bottom: 0px;border-top: 1px solid #000;" ></td>
                    <td valign="top" style="border-bottom: 0px;border-top: 1px solid #000;" ></td>                    
                    <td valign="top" style="border-bottom: 0px;border-top: 1px solid #000;" ></td>                    
                    <td align="right" valign="top" rowspan="{{ $rowspan }}">{{ number_format($amount,0,'.',',') }}</td>
                    <td align="right" valign="top" rowspan="{{ $rowspan }}">{{ number_format($salesOrders->discount,0,'.',',') }}</td>
                    <td align="right" valign="top" rowspan="{{ $rowspan }}">{{ number_format($vat,0,'.',',') }}</td>                    
                    <td align="right" valign="top" rowspan="{{ $rowspan }}">{{ number_format(($amount - $salesOrders->discount) + $vat,0,'.',',') }}</td>
                </tr>                    
                    @foreach($salesOrders->salesOrdersDetails as $sod)
                        @forelse($salesOrdersRequest->items as $item)
                            @if ($item->id == $sod->item->id)
                                <tr>
                                    <td align="left" valign="top" style="border-bottom: 0px;" >
                                        {{ $sod->item->name }}
                                    </td>                                    
                                    <td valign="top" align="right" style="border-top: 0px; border-bottom: 0px;">{{ $sod->quantity }}</td>                                
                                    <td valign="top" align="center" style="border-top: 0px; border-bottom: 0px;">{{ $sod->item->unit->name }}</td>                                
                                </tr>                                                     
                            @endif                                
                        @empty
                        <tr>
                            <td align="left" valign="top" style="border-bottom: 0px;" >
                            {{ $sod->item->name }}
                            </td>
                            <td align="right" valign="top" style="border-top: 0px; border-bottom: 0px;">
							{{ number_format($sod->quantity,0,'.',',') }}
							</td>
                             <td align="center" valign="top" style="border-top: 0px; border-bottom: 0px;">{{$sod->item->unit->name}}</td>
                        </tr>    
                        @endforelse
                    @endforeach                                                                    
               
            @endforeach
            <tr>
                    <td colspan="4" style="border: 0px;">&nbsp;</td>
                    <td colspan="3" style="border-bottom: 0px; border-left: 0px;border-right: 0px; border-top: 1px solid #000;">&nbsp;</td>
                    <td colspan="2" style="border: 0px;">&nbsp;</td>
                    <td style="border: 0px; border-right: 1px solid #000; text-align: left"><span style="font-weight: bold;">Total</span></td>
                    <td style="text-align: right">        
                        <strong>{{ number_format(round($total),0,'.',',') }}</strong>
                    </td>
                </tr>                      
            </tbody>
        </table>  
    </div>
<script>
    window.print();
</script>

