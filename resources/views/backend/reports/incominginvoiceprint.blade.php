<!-- Load paper.css for happy printing -->
<link rel="stylesheet" href="../../css/paper.css">
<!--<link rel="stylesheet" href="../../fonts/Dotimatrix_5/stylesheet.css">-->
<!-- Set page size here: A5, A4 or A3 -->
<!-- Set also "landscape" if you need -->
<style>    
   body {       
        display: block; 
        margin-top: 50px;
        margin-left: 30px;
        margin-right: 20px;
        margin-bottom: 20px;        
        padding: 1;
        background-color: #FAFAFA;
        font-size: xx-small;
        color: #000;
        font-family: Consolas;
    }
    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }
    .page {
        width: 27.7cm;
        min-height: 21cm;
        padding: 2cm;
        margin: 1cm auto;
        border: 1px #D3D3D3 solid;
        border-radius: 5px;
        background: white;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }
    .subpage {
        padding: 1cm;
        border: 5px red solid;
        height: 256mm;
        outline: 2cm #FFEAEA solid;
    }
    
    table th, table td {
        border-bottom: 1px solid #000;
        border-right: 1px solid #000;
        padding: 5px 10px 5px 5px;
    }
       
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    
    @page {
        size: landscape;
    }
   
    
</style>
    <div>
        <table width="100%" cellpadding="0" cellspacing="0" style='border: 0px solid #000; font-size:14px'>
            <tr>
                <td style='border: 0px; padding: 0px;' align="center">
                    <strong>INCOMING INVOICE REPORT</strong><br/>
                    <strong>Supplier : {{$incomingInvoiceRequest->supplier_name}}</strong><br/>                    
                    <strong>Category : {{$incomingInvoiceRequest->category_name}}</strong><br/>                                            
                     {{$incomingInvoiceRequest->date_period}}     
                </td>
            </tr>
        </table>
        <table width="100%" cellpadding="0" cellspacing="0" style="padding: 5px; font-size:10px">  
          <tbody>
              <tr>
                  <td style='border-top: 1px solid #000;border-left: 1px solid #000; padding: 5px 10px 5px 5px;' align="center">NO</td>     
                  <td style='border-top: 1px solid #000;padding: 5px 10px 5px 5px;' align="center">DATE</td>           
                  <td style='border-top: 1px solid #000;padding: 5px 10px 5px 5px;' align="center">DUE DATE</td>
                  <td style='border-top: 1px solid #000;padding: 5px 10px 5px 5px;' align="center">SUPPLIER</td>               
                  <td style='border-top: 1px solid #000;padding: 5px 10px 5px 5px;' align="center">ITEM</td>
                  <td style='border-top: 1px solid #000;padding: 5px 10px 5px 5px;' align="center">QTY</td>
                  <td style='border-top: 1px solid #000;padding: 5px 10px 5px 5px;' align="center">UNIT</td>
                  <td style='border-top: 1px solid #000;padding: 5px 10px 5px 5px;' align="center">SUB TOTAL<br>(RP)</td>
                  <td style='border-top: 1px solid #000;padding: 5px 10px 5px 5px;' align="center">DISCOUNT<br>(RP)</td>
                  <td style='border-top: 1px solid #000;padding: 5px 10px 5px 5px;' align="center">VAT<br>(RP)</td>
                  <td style='border-top: 1px solid #000;padding: 5px 10px 5px 5px;' align="center">TOTAL<br>(RP)</td>
                  <td style='border-top: 1px solid #000;padding: 5px 10px 5px 5px;' align="center">PO KELUAR</td>
             </tr>
              @php 
                $grand_total = 0;                     
                $rowspan = 1; 
                $sub_total = 0;
                $vat = 0;
                $total_quantity = 0;
              @endphp              
              
              @foreach($incomingInvoiceObjects as $inv)
                    
                    @php
                        $rowspan = 1; 
                        $sub_total = 0;
                        $vat = 0;
                        $total_quantity = 0;
                    @endphp    
                    @foreach(optional($inv)->incomingInvoicesDetails as $invDetail)                                
                        @forelse($incomingInvoiceRequest->items as $item)
                            @if ($item->id == $invDetail->item->id)                                
                                @php 
                                    $total_quantity+=$invDetail->quantity;
                                    $rowspan++;
                                @endphp
                                @php 
                                        $sub_total += $invDetail->amount;
                                        if ($inv->vat>0) {
                                            $vat += $invDetail->amount * $invDetail->tax;
                                        }
                                @endphp
                            @endif
                        @empty                            
                            @php 
                                $rowspan++;
                                $total_quantity+=$invDetail->quantity;
                            @endphp
                            @php 
                                $sub_total += $invDetail->amount;
                                if ($inv->vat>0) {
                                    $vat += $invDetail->amount * $invDetail->tax;
                                }
                            @endphp
                        @endforelse                            
                    @endforeach

                    <tr>
                        <td valign="top" rowspan="{{ $rowspan }}" style="border-left: 1px solid #000;padding: 5px 10px 5px 5px;">
                            @php
                                $ino = $inv->no;
                            @endphp
                            {{ $ino }}
                        </td>
                        <td valign="top" rowspan="{{ $rowspan }}" style="padding: 5px 10px 5px 5px;">{{ DateTime::createFromFormat('d/m/Y g a', optional($inv)->created_at)->format('d/m/Y') }}</td>
                        <td valign="top" rowspan="{{ $rowspan }}" style="padding: 5px 10px 5px 5px;">{{ date('d/m/Y', strtotime($inv->due_date)) }}</td>
                        <td valign="top" rowspan="{{ $rowspan }}" style="padding: 5px 10px 5px 5px;">{{ optional($inv->purchaseOrder->supplier)->name }}</td>
                        
                        <td valign="top" valign="top" style="border-bottom: 0px;border-top: 1px solid #000;" ></td>
                        <td valign="top" style="border-bottom: 0px;border-top: 1px solid #000;" ></td>                    
                        <td valign="top" style="border-bottom: 0px;border-top: 1px solid #000;" ></td>                    

                        <td valign="top" rowspan="{{ $rowspan }}" align="right" style="padding: 5px 10px 5px 5px;">{{ number_format($sub_total,0,'.',',') }}</td>
                        <td valign="top" rowspan="{{ $rowspan }}" align="right" style="padding: 5px 10px 5px 5px;">{{ number_format($inv->discount,0,'.',',') }}</td>
                        <td valign="top" rowspan="{{ $rowspan }}" align="right" style="padding: 5px 10px 5px 5px;">{{ number_format($vat,0,'.',',') }}</td>
                        <td valign="top" rowspan="{{ $rowspan }}" align="right" style="padding: 5px 10px 5px 5px;">{{ number_format($sub_total+$vat,0,'.',',') }}</td>                                    
                        <td valign="top" rowspan="{{ $rowspan }}" align="right" style="padding: 5px 10px 5px 5px;">{{ $inv->purchaseOrder->purchase_no }}</td>                                    
                    </tr>        
                    
                    @foreach(optional($inv)->incomingInvoicesDetails as $invDetail)

                        @forelse($incomingInvoiceRequest->items as $item)
                            @if ($item->id == $invDetail->item->id)
                                <tr>
                                    <td align="left" valign="top" style="border-bottom: 0px; padding: 5px 10px 5px 5px;" >
                                        {{ $invDetail->item->name }}
                                    </td>                                    
                                    <td valign="top" align="right" style="border-top: 0px; border-bottom: 0px; padding: 5px 10px 5px 5px;">{{ $invDetail->quantity }}</td>                                
                                    <td valign="top" align="center" style="border-top: 0px; border-bottom: 0px; padding: 5px 10px 5px 5px;">{{ $invDetail->item->unit->name }}</td>                                
                                </tr>   
                            @endif
                        @empty
                            <tr>
                                <td align="left" valign="top" style="border-bottom: 0px; padding: 5px 10px 5px 5px;" >
                                {{ $invDetail->item->name }}
                                </td>
                                <td align="right" valign="top" style="border-top: 0px; border-bottom: 0px; padding: 5px 10px 5px 5px;">
                                                            {{ number_format($invDetail->quantity,0,'.',',') }}
                                                            </td>
                                 <td align="center" valign="top" style="border-top: 0px; border-bottom: 0px;padding: 5px 10px 5px 5px;">{{$invDetail->item->unit->name}}</td>
                            </tr>
                        @endforelse                            
                    @endforeach
                    @php
                        $grand_total += $sub_total + $vat;
                    @endphp
              @endforeach   
              <tr>
                    <td colspan="4" style="border: 0px;"></td>
                    <td colspan="3" style="border-bottom: 0px; border-left: 0px;border-right: 0px; border-top: 1px solid #000;">&nbsp;</td>
                    <td style="border: 0px;"></td>
                    <td colspan="2" style="border: 0px; border-right: 1px solid #000; text-align: left; padding: 5px 10px 5px 5px;"><span style="font-weight: bold;">Grand Total</span></td>
                    <td style="text-align: right; padding: 5px 10px 5px 5px;">        
                        <strong>{{ number_format(round($grand_total),0,'.',',') }}</strong>
                    </td>
                    <td style="border: 0px;">                     
                        &nbsp;
                    </td>
                </tr>    
          </tbody>         
          
        </table>          
    </div>
<script>
    window.print();
</script>

