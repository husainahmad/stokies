<!-- Load paper.css for happy printing -->
<link rel="stylesheet" href="../../css/paper.css">
<!--<link rel="stylesheet" href="../../fonts/Dotimatrix_5/stylesheet.css">-->
<!-- Set page size here: A5, A4 or A3 -->
<!-- Set also "landscape" if you need -->
<style>    
   body {       
        display: block; 
        margin-top: 50px;
        margin-left: 30px;
        margin-right: 20px;
        margin-bottom: 20px;        
        padding: 1;
        background-color: #FAFAFA;
        font-size: xx-small;
        color: #000;
        font-family: Consolas;
    }
    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }
    .page {
        width: 27.7cm;
        min-height: 21cm;
        padding: 2cm;
        margin: 1cm auto;
        border: 1px #D3D3D3 solid;
        border-radius: 5px;
        background: white;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }
    .subpage {
        padding: 1cm;
        border: 5px red solid;
        height: 256mm;
        outline: 2cm #FFEAEA solid;
    }
    
    table th, table td {
        border-bottom: 1px solid #000;
        border-right: 1px solid #000;
        padding: 5px 10px 5px 5px;
    }
       
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    
    @page {
        size: landscape;
    }
   
    
</style>
    <div>
        <table width="100%" cellpadding="0" cellspacing="0" style='border: 0px solid #000; font-size:14px'>
            <tr>
                <td style='border: 0px; padding: 0px;' align="center">
                    <strong>PURCHASE ORDER REPORT</strong><br/>
                    <strong>Supplier : {{$purchaseOrdersRequest->supplier_name}}</strong><br/>                    
                    <strong>Category : {{$purchaseOrdersRequest->category_name}}</strong><br/>                                            
                     {{$purchaseOrdersRequest->date_period}}     
                </td>
            </tr>
        </table>
        <table width="100%" cellpadding="0" cellspacing="0" style="padding: 5px; font-size:10px">      
            <tbody>
                <tr>
                    <td style='border-top: 1px solid #000;border-left: 1px solid #000;' align="center">NO</td>
                    <td style='border-top: 1px solid #000;' align="center">ORDER DATE</td>
                    <td style='border-top: 1px solid #000;' align="center">SUPPLIER</td>
                    <td style='border-top: 1px solid #000;' align="center">ITEM</td>                    
                    <td style='border-top: 1px solid #000;' align="center">QTY</td>
                    <td style='border-top: 1px solid #000;' align="center">UNIT</td>
                    <td style='border-top: 1px solid #000;' align="center">SUB TOTAL</td>
                    <td style='border-top: 1px solid #000;' align="center">DISCOUNT</td>
                    <td style='border-top: 1px solid #000;' align="center">VAT</td>
                    <td style='border-top: 1px solid #000;' align="center">TOTAL</td>
                </tr>
            @php 
                $grand_total = 0;                       
            @endphp    
            @foreach($purchaseOrdersObjects as $po)
                @php 
                    $total_quantity = 0; 
                    $amount = 0;
                    $sub_total = 0;
                    $discount = 0;
                    $vat = 0;        
                    $rowspan = 1;
                    $tax = $po->tax;                    
                @endphp
                
                @foreach($po->purchaseOrdersDetails as $pod)
                    @php
                            $discount += $po->discount;
                    @endphp
                    @if (count($purchaseOrdersRequest->items)>0)
                        @foreach($purchaseOrdersRequest->items as $item)
                            @if ($item->id == $pod->item->id)
                                @php
                                    $rowspan++;
                                @endphp                                
                                @php 
                                    $sub_total += $pod->amount;									
                                    if ($po->vat>0) {
                                        $vat += $pod->amount * $tax;
                                    }
                                @endphp
                            @endif
                        @endforeach                                    
                    @else
                        @php $total_quantity+=$pod->quantity;
                        @endphp
                        @php 
                            $sub_total += $pod->amount;
                            if ($po->vat>0) {
                                $vat += $pod->amount * $tax;
                            }
                            $rowspan++;
                        @endphp
                    @endif            
                @endforeach      
                @php                    
                    $grand_total += $sub_total + $vat;
                @endphp
                <tr>
                    <td style="border-left: 1px solid #000;" valign="top" rowspan="{{ $rowspan }}">
                    @php
                        $pono = $po->purchase_no;
                        $pono = substr($po->purchase_no, strlen($po->purchase_no)-3, 3);
                    @endphp
                    {{ $pono }}
                    </td>
                    <td valign="top" rowspan="{{ $rowspan }}">{{ date('d/m/Y', strtotime($po->ordered_at)) }}</td>
                    <td valign="top" rowspan="{{ $rowspan }}">{{ optional($po->supplier)->name }}</td>
                    <td align="left" valign="top" style="border-bottom: 0px;border-top: 1px solid #000;"></td>
                    <td align="right" valign="top" style="border-bottom: 0px;border-top: 1px solid #000;"></td>                    
                    <td align="right" valign="top" style="border-bottom: 0px;border-top: 1px solid #000;"></td>                    
                    <td align="right" valign="top" rowspan="{{ $rowspan }}">{{ number_format($sub_total,0,'.',',') }}</td>
                    <td align="right" valign="top" rowspan="{{ $rowspan }}">{{ number_format($discount,0,'.',',') }}</td>
                    <td align="right" valign="top" rowspan="{{ $rowspan }}">{{ number_format($vat,0,'.',',') }}</td>
                    <td align="right" valign="top" rowspan="{{ $rowspan }}">{{ number_format($sub_total+$vat,0,'.',',') }}</td>                                    
                </tr>
                @foreach($po->purchaseOrdersDetails as $pod)
                    @if (count($purchaseOrdersRequest->items)>0)
                        @foreach($purchaseOrdersRequest->items as $item)
                            @if ($item->id == $pod->item->id)
                            <tr>
                                 <td align="left" valign="top" style="border-bottom: 0px;">
                                     {{ $pod->item->name }}
                                 </td>
                                 <td align="right" valign="top" style="border-bottom: 0px;">
                                        {{ number_format($pod->quantity,0,'.',',') }}
                                 </td>
                                 <td align="center" valign="top" style="border-bottom: 0px;">
                                    {{$pod->item->unit->name}}
                                </td>
                            </tr>                                  
                            @endif
                        @endforeach                                    
                    @else
                        <tr>
                            <td align="left" valign="top" style="border-bottom: 0px;">
                                {{ $pod->item->name }}
                            </td>
                            <td align="right" valign="top" style="border-bottom: 0px;">
                                {{ number_format($pod->quantity,0,'.',',') }}
                            </td>
                            <td align="center" valign="top" style="border-bottom: 0px;">
                                {{$pod->item->unit->name}}
                            </td>
                        </tr>
                    @endif            
                @endforeach                        
                
              @endforeach              
              <tr>
                    <td colspan="3" style="border: 0px;"></td>
                    <td colspan="3" style="border-top: 1px solid #000;border-bottom: 0px ;border-left: 0px;border-right: 0px;">&nbsp;</td>
                    <td style="border: 0px;"></td>
                    <td colspan="2" style="border: 0px; border-right: 1px solid #000; text-align: left"><span style="font-weight: bold;">Grand Total</span></td>
                    <td style="text-align: right">        
                        <strong>{{ number_format(round($grand_total),0,'.',',') }}</strong>
                    </td>
                </tr>
          </tbody>         
          
        </table>          
    </div>
<script>
    window.print();
</script>

