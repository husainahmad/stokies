<div class="row">
    <div class="col-sm-6">        
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('no') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label"  class="col-md-3 col-form-label" for="no">No</label>
                        <div class="col-md-9">
                            <input class="form-control" name="no" type="text" id="no" value="{{ old('no', optional($incomingInvoiceRequest)->no) == '' ? '' : old('no', optional($incomingInvoiceRequest)->no) }}" minlength="1" maxlength="45" placeholder="Enter no here...">
                            {!! $errors->first('no', '<p class="help-block">:message</p>') !!}
                        </div>
                        
                    </div>
                </div>                
            </div>    
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('from_date') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label"  for="from_date">From Date</label>
                        <div class="col-md-9">
                            <div class="input-group">
                                <span class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                            </span>
                                <input class="form-control" name="from_date" type="text" id="from_date" data-provide="datepicker" required="true" value="{{ old('from_date', optional($incomingInvoiceRequest)->from_date  == '' ? date("d/m/Y") : date('d/m/Y', strtotime(optional($incomingInvoiceRequest)->from_date))) }}" minlength="1" placeholder="Enter from date here...">
                                {!! $errors->first('from_date', '<p class="help-block">:message</p>') !!}
                            </div>
                            <small class="text-muted">ex. 99/99/9999</small>
                        </div>
                    </div> 
                </div>            
            </div>    
            <div class="row">
                 <div class="col-sm-12">
                     <div class="form-group row {{ $errors->has('to_date') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label"  for="to_date">To Date</label>
                        <div class="col-md-9">
                            <div class="input-group">
                                <span class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                            </span>
                            <input class="form-control" name="to_date" type="text" id="to_date" required="true" data-provide="datepicker" value="{{ old('to_date', optional($incomingInvoiceRequest)->to_date  == '' ? date("d/m/Y") : date('d/m/Y', strtotime(optional($incomingInvoiceRequest)->to_date))) }}" minlength="1" maxlength="45" placeholder="Enter to date here...">
                                {!! $errors->first('to_date', '<p class="help-block">:message</p>') !!}
                            </div>
                            <small class="text-muted">ex. 99/99/9999</small>
                        </div>
                    </div>                      
                 </div>
            </div>                                                                   
        </div>
    </div>
    <div class="col-sm-6">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('suppliers_id') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label"  for="suppliers_id">Supplier</label>
                        <div class="col-md-9">
                            <select class="form-control" id="suppliers_id" name="suppliers_id">
                                    <option value="" >Select supplier</option>
                                    @foreach ($suppliers as $key => $supplier)
                                                <option value="{{ $key }}" {{ old('suppliers_id', optional($incomingInvoiceRequest)->suppliers_id) == $key ? 'selected' : '' }}>
                                                    {{ $supplier }}
                                                </option>
                                            @endforeach
                            </select>

                            {!! $errors->first('suppliers_id', '<p class="help-block">:message</p>') !!}
                        </div>
                        
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('category_id') ? 'has-error' : '' }}">
                        <label for="category_id" class="col-md-3 control-label">Category</label>
                        <div class="col-md-9">
                            <select class="form-control" id="category_id" name="category_id" >
                                        <option value="" {{ old('category_id', optional($incomingInvoiceRequest)->category_id ?: '') == '' ? 'selected' : '' }}>Select category</option>
                                    @foreach ($categories as $key => $category)
                                                <option value="{{ $key }}" {{ old('category_id', optional($incomingInvoiceRequest)->category_id) == $key ? 'selected' : '' }}>
                                                    {{ $category }}
                                                </option>
                                            @endforeach
                            </select>

                            {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
            </div>                                       
        </div>
    </div>
</div>

