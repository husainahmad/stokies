<!-- Load paper.css for happy printing -->
<link rel="stylesheet" href="../../css/paper.css">
<!--<link rel="stylesheet" href="../../fonts/Dotimatrix_5/stylesheet.css">-->
<!-- Set page size here: A5, A4 or A3 -->
<!-- Set also "landscape" if you need -->
<style>    
    body {       
        display: block; 
        margin-top: 50px;
        margin-left: 30px;
        margin-right: 20px;
        margin-bottom: 20px;        
        padding: 1;
        background-color: #FAFAFA;
        font-size: xx-small;
        color: #000;
        font-family: Consolas;
    }
    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }
    .page {
        width: 27.7cm;
        min-height: 21cm;
        padding: 2cm;
        margin: 1cm auto;
        border: 1px #D3D3D3 solid;
        border-radius: 5px;
        background: white;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }
    .subpage {
        padding: 1cm;
        border: 5px red solid;
        height: 256mm;
        outline: 2cm #FFEAEA solid;
    }
   
    table th, table td {
        border-bottom: 1px solid #000;
        border-right: 1px solid #000;
    }      
       
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    
    @page {
        size: landscape;
    }
</style>
<div>
        <table width="100%" cellpadding="0" cellspacing="0" style='border: 0px solid #000; font-size:14px'>
            <tr>
                <td style='border: 0px; padding: 0px; ' align="center">
                    <h3>
                    <strong>INVOICE REPORT</strong><br/>                                            
                    <strong>Customer : {{$invoiceRequest->customer_name}}</strong><br/>                    
                    <strong>Category : {{$invoiceRequest->category_name}}</strong><br/>
                    Date : {{$invoiceRequest->date_period}}                    
                    </h3>
                </td>
            </tr>
        </table>
        <table width="100%" cellpadding="0" cellspacing="0" style="padding: 5px; font-size:10px">  
          
          <tbody>
              <tr>
                  <td style='border-top: 1px solid #000;border-left: 1px solid #000; padding: 5px 10px 5px 5px;' align="center">NO</td>     
                  <td style='border-top: 1px solid #000;padding: 5px 10px 5px 5px;' align="center">DATE</td>           
                  <td style='border-top: 1px solid #000;padding: 5px 10px 5px 5px;' align="center">DUE DATE</td>
                  <td style='border-top: 1px solid #000;padding: 5px 10px 5px 5px;' align="center">CUSTOMER</td>               
                  <td style='border-top: 1px solid #000;padding: 5px 10px 5px 5px;' align="center">ITEM</td>
                  <td style='border-top: 1px solid #000;padding: 5px 10px 5px 5px;' align="center">QTY</td>
                  <td style='border-top: 1px solid #000;padding: 5px 10px 5px 5px;' align="center">UNIT</td>
                  <td style='border-top: 1px solid #000;padding: 5px 10px 5px 5px;' align="center">SUB TOTAL<br>(RP)</td>
                  <td style='border-top: 1px solid #000;padding: 5px 10px 5px 5px;' align="center">DISCOUNT<br>(RP)</td>
                  <td style='border-top: 1px solid #000;padding: 5px 10px 5px 5px;' align="center">VAT<br>(RP)</td>
                  <td style='border-top: 1px solid #000;padding: 5px 10px 5px 5px;' align="center">TOTAL<br>(RP)</td>
             </tr>
              @php 
                $grand_total = 0;                     
                $rowspan = 1; 
                $sub_total = 0;
                $vat = 0;
                $total_quantity = 0;
              @endphp              
              
              @foreach($invoiceObjects as $inv)            
                    @php
                        $rowspan = 1; 
                        $sub_total = 0;
                        $vat = 0;
                        $total_quantity = 0;
                    @endphp    
                    @foreach(optional($inv)->invoicesDetails as $invDetail)     
                        @forelse($invoiceRequest->items as $item)
                            @if ($item->id == $invDetail->item->id)                                
                                @php 
                                    $total_quantity+=$invDetail->quantity;
                                    $rowspan++;
                                @endphp
                                @php 
                                        $sub_total += $invDetail->amount;
                                        if ($inv->vat>0) {
                                            $vat += ($invDetail->amount - $invDetail->discount) * $invDetail->tax;
                                        }
                                @endphp
                            @endif
                        @empty                            
                            @php 
                                $rowspan++;
                                $total_quantity+=$invDetail->quantity;
                            @endphp
                            @php 
                                $sub_total += $invDetail->amount;
                                if ($inv->vat>0) {
                                    $vat = $inv->vat;
                                }
                            @endphp
                        @endforelse                            
                    @endforeach

                    <tr>
                        <td valign="top" rowspan="{{ $rowspan }}" style="border-left: 1px solid #000;padding: 5px 10px 5px 5px;">
                            @php
                                $ino = $inv->no;
                                $ino = substr($inv->no, strlen($inv->no)-3, 3);
                            @endphp
                            {{ $ino }}
                        </td>
                        <td valign="top" rowspan="{{ $rowspan }}" style="padding: 5px 10px 5px 5px;">{{ $inv->created_at }}</td>
                        <td valign="top" rowspan="{{ $rowspan }}" style="padding: 5px 10px 5px 5px;">{{ date('d/m/Y', strtotime($inv->due_date)) }}</td>
                        <td valign="top" rowspan="{{ $rowspan }}" style="padding: 5px 10px 5px 5px;">{{ optional($inv->customer)->name }}</td>
                        
                        <td valign="top" valign="top" style="border-bottom: 0px;border-top: 1px solid #000;" ></td>
                        <td valign="top" style="border-bottom: 0px;border-top: 1px solid #000;" ></td>                    
                        <td valign="top" style="border-bottom: 0px;border-top: 1px solid #000;" ></td>                    

                        <td valign="top" rowspan="{{ $rowspan }}" align="right" style="padding: 5px 10px 5px 5px;">{{ number_format($sub_total,0,'.',',') }}</td>
                        <td valign="top" rowspan="{{ $rowspan }}" align="right" style="padding: 5px 10px 5px 5px;">{{ number_format($inv->discount,0,'.',',') }}</td>
                        <td valign="top" rowspan="{{ $rowspan }}" align="right" style="padding: 5px 10px 5px 5px;">{{ number_format($vat,0,'.',',') }}</td>
                        <td valign="top" rowspan="{{ $rowspan }}" align="right" style="padding: 5px 10px 5px 5px;">{{ number_format(($sub_total - $inv->discount - $inv->dp)+ $vat,0,'.',',') }}</td>                                    
                    </tr>        
                    
                    @foreach(optional($inv)->invoicesDetails as $invDetail)

                        @forelse($invoiceRequest->items as $item)
                            @if ($item->id == $invDetail->item->id)
                                <tr>
                                    <td align="left" valign="top" style="border-bottom: 0px; padding: 5px 10px 5px 5px;" >
                                        {{ $invDetail->item->name }}
                                    </td>                                    
                                    <td valign="top" align="right" style="border-top: 0px; border-bottom: 0px; padding: 5px 10px 5px 5px;">{{ $invDetail->quantity }}</td>                                
                                    <td valign="top" align="center" style="border-top: 0px; border-bottom: 0px; padding: 5px 10px 5px 5px;">
                                        
                                        @if ($invDetail->items_id == -1)
                                            -
                                        @else
                                            {{ $invDetail->item->unit->name }}
                                        @endif  
                                    </td>                                
                                </tr>   
                            @endif
                        @empty
                            <tr>
                                <td align="left" valign="top" style="border-bottom: 0px; padding: 5px 10px 5px 5px;" >
                                @if ($invDetail->items_id == -1)
                                    {{ $invDetail->description }}
                                @else
                                    {{ $invDetail->item->name }}
                                @endif                                
                                </td>
                                <td align="right" valign="top" style="border-top: 0px; border-bottom: 0px; padding: 5px 10px 5px 5px;">
                                                            {{ number_format($invDetail->quantity,0,'.',',') }}
                                                            </td>
                                 <td align="center" valign="top" style="border-top: 0px; border-bottom: 0px;padding: 5px 10px 5px 5px;">
                                    @if ($invDetail->items_id == -1)
                                        -
                                    @else
                                        {{ $invDetail->item->unit->name }}
                                    @endif
                                 </td>
                            </tr>
                        @endforelse                            
                    @endforeach
                    @php
                        $grand_total += ($sub_total - $inv->discount - $inv->dp)  + $vat ;
                    @endphp
              @endforeach              
                <tr>
                    <td colspan="4" style="border: 0px;"></td>
                    <td colspan="3" style="border-bottom: 0px; border-left: 0px;border-right: 0px; border-top: 1px solid #000;">&nbsp;</td>
                    <td style="border: 0px;"></td>
                    <td colspan="2" style="border: 0px; border-right: 1px solid #000; text-align: left; padding: 5px 10px 5px 5px;"><span style="font-weight: bold;">Grand Total</span></td>
                    <td colspan="4" style="text-align: right; padding: 5px 10px 5px 5px;">        
                        <strong>{{ number_format(round($grand_total),0,'.',',') }}</strong>
                    </td>
                </tr>                      
          </tbody>         
          
        </table>          
    </div>
<script>
    window.print();
</script>

