<div class="row">
    <div class="col-sm-6">        
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('no') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label"  class="col-md-3 col-form-label" for="no">No</label>
                        <div class="col-md-9">
                            <input class="form-control" name="no" type="text" id="no" value="{{ old('no', optional($salesOrders)->no) == '' ? '' : old('no', optional($salesOrders)->no) }}" minlength="1" maxlength="45" placeholder="Enter no here...">
                            {!! $errors->first('no', '<p class="help-block">:message</p>') !!}
                        </div>
                        
                    </div>
                </div>                
            </div>    
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('sales_person_id') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label"  for="sales_person_id" >Sales Person</label>
                        <div class="col-md-9">
                        <select class="form-control" id="sales_person_id" name="sales_person_id" >
                                    <option value="" style="display: none;" {{ old('sales_person_id', optional($salesOrders)->sales_person_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select sales person</option>
                                @foreach ($salesPeople as $key => $salesPerson)
                                            <option value="{{ $key }}" {{ old('sales_person_id', optional($salesOrders)->sales_person_id) == $key ? 'selected' : '' }}>
                                                {{ $salesPerson }}
                                            </option>
                                        @endforeach
                        </select>
                        {!! $errors->first('sales_person_id', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>                    
                </div>
            </div>    
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('from_date') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label"  for="from_date">From Date</label>
                        <div class="col-md-9">
                            <div class="input-group">
                                <span class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                            </span>
                                <input class="form-control" name="from_date" type="text" id="from_date" data-provide="datepicker" required="true" value="{{ old('from_date', optional($salesOrders)->from_date) }}" minlength="1" placeholder="Enter from date here...">
                                {!! $errors->first('from_date', '<p class="help-block">:message</p>') !!}
                            </div>
                            <small class="text-muted">ex. 99/99/9999</small>
                        </div>
                    </div> 
                </div>            
            </div>    
            <div class="row">
                 <div class="col-sm-12">
                     <div class="form-group row {{ $errors->has('to_date') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label"  for="to_date">To Date</label>
                        <div class="col-md-9">
                            <div class="input-group">
                                <span class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                            </span>
                            <input class="form-control" name="to_date" type="text" id="to_date" required="true" value="{{ old('to_date', optional($salesOrders)->to_date) }}" minlength="1" maxlength="45" placeholder="Enter to date here...">
                                {!! $errors->first('to_date', '<p class="help-block">:message</p>') !!}
                            </div>
                            <small class="text-muted">ex. 99/99/9999</small>
                        </div>
                    </div>                      
                 </div>
            </div>                                                                   
        </div>
    </div>
    <div class="col-sm-6">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('customer_id') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label"  for="customer_id">Customer</label>
                        <div class="col-md-9">
                            <select class="form-control" id="customer_id" name="customer_id">
                                    <option value="" style="display: none;" {{ old('customer_id', optional($salesOrders)->customer_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select customer</option>
                                    @foreach ($customers as $key => $customer)
                                                <option value="{{ $key }}" {{ old('customer_id', optional($salesOrders)->customer_id) == $key ? 'selected' : '' }}>
                                                    {{ $customer }}
                                                </option>
                                            @endforeach
                            </select>

                            {!! $errors->first('customer_id', '<p class="help-block">:message</p>') !!}
                        </div>
                        
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row{{ $errors->has('ref_po_customer') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label"  for="ref_po_customer">PO Customer</label>
                        <div class="col-md-9">
                            <input class="form-control" name="ref_po_customer" type="text" id="ref_po_customer" value="{{ old('ref_po_customer', optional($salesOrders)->ref_po_customer) }}" minlength="1" maxlength="45" placeholder="Enter ref po customer here...">
                                {!! $errors->first('ref_po_customer', '<p class="help-block">:message</p>') !!}
                        </div>    
                    </div>
                </div>
            </div>                                       
        </div>
    </div>
</div>

