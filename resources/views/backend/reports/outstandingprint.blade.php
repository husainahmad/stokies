<!-- Load paper.css for happy printing -->
<link rel="stylesheet" href="../../css/paper.css">
<!--<link rel="stylesheet" href="../../fonts/Dotimatrix_5/stylesheet.css">-->
<!-- Set page size here: A5, A4 or A3 -->
<!-- Set also "landscape" if you need -->
<style>    
    body {       
        display: block; 
        margin-top: 50px;
        margin-left: 30px;
        margin-right: 20px;
        margin-bottom: 20px;        
        padding: 1;
        background-color: #FAFAFA;
        font-size: xx-small;
        color: #000;
        font-family: Consolas;
    }
    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }
    .page {
        width: 27.7cm;
        min-height: 21cm;
        padding: 2cm;
        margin: 1cm auto;
        border: 1px #D3D3D3 solid;
        border-radius: 5px;
        background: white;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }
    .subpage {
        padding: 1cm;
        border: 5px red solid;
        height: 256mm;
        outline: 2cm #FFEAEA solid;
    }
    
    table th, table td {
        border-bottom: 1px solid #000;
        border-right: 1px solid #000;
        padding: 5px 10px 5px 5px;
        -fs-table-paginate: paginate;
    }
       
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
    
    @page {
        size: landscape;
    }
     
</style>
    <div>
        <table width="100%" cellpadding="0" cellspacing="0" style='border: 0px solid #000; font-size:14px'>
            <tr>
                <td style='border: 0px; padding: 0px;' align="center">
                        <strong>OUTSTANDING REPORT</strong><br>
                    <strong>Customer : {{ $salesOrdersRequest->customer_name }}</strong><br/>                    
                    <strong>Category : {{ $salesOrdersRequest->category_name }}</strong><br/>                                            
                    Date : {{ $salesOrdersRequest->date_period }}                 
                </td>
            </tr>
        </table>
        <table width="100%" cellpadding="0" cellspacing="0" style="padding: 5px; font-size:10px">                
            
            <tbody>
            <tr>
                <td style='border-top: 1px solid #000;border-left: 1px solid #000;' align="center">NO</td>
                <td style='border-top: 1px solid #000;' align="center">SO DATE</td>
                <td style='border-top: 1px solid #000;' align="center">REF PO</td>
                <td style='border-top: 1px solid #000;' align="center">CUSTOMER</td>
                <td style='border-top: 1px solid #000;' align="center">ITEM</td>
                <td style='border-top: 1px solid #000;' align="center">QTY</td>
                <td style='border-top: 1px solid #000;' align="center">UNIT</td>
                <td style='border-top: 1px solid #000;' align="center">TERKIRIM</td>
                <td style='border-top: 1px solid #000;' align="center">SISA</td>
                </tr>    
            @php
            $total = 0;
            @endphp
            @foreach($salesOrdersObjects as $salesOrders)
                @php 
                    $rowspan = 1;
                @endphp
                    @foreach($salesOrders->salesOrdersDetails as $sod)
                        @forelse($salesOrdersRequest->items as $item)
                            @if ($item->id == $sod->item->id)
                                @php
                                    $rowspan++;
                                @endphp
                            @endif
                        @empty                            
                            @php 
                            $rowspan++;
                            @endphp
                        @endforelse                                                        
                    @endforeach                        
                <tr>
                    <td style="border-left: 1px solid #000;" valign="top" rowspan="{{ $rowspan }}">
                    @php
                            $sono = $salesOrders->no;
                            $sono = substr($salesOrders->no, strlen($salesOrders->no)-3, 3);
                    @endphp
                    {{ $sono }}
                    </td>
                    <td valign="top" rowspan="{{ $rowspan }}">{{ date('d/m/Y', strtotime($salesOrders->sales_order_date)) }}</td>
                    <td valign="top" rowspan="{{ $rowspan }}">{{ $salesOrders->ref_po_customer }}</td>
                    <td valign="top" rowspan="{{ $rowspan }}">{{ optional($salesOrders->customer)->name }}</td>                    
                    <td valign="top" style="border-bottom: 0px;border-top: 1px solid #000;"></td>                    
                    <td valign="top" style="border-bottom: 0px;border-top: 1px solid #000;"></td>                    
                    <td valign="top" style="border-bottom: 0px;border-top: 1px solid #000;"></td>                    
                    <td valign="top" style="border-bottom: 0px;border-top: 1px solid #000;"></td>                    
                    <td valign="top" style="border-bottom: 0px;border-top: 1px solid #000;border-right: 1px solid #000;"></td>                    
                </tr>
                        
                @foreach($salesOrders->salesOrdersDetails as $sod)
                    @php $totalKirim = 0;
                    @endphp
                    @if(isset($sod->deliveryOrderDetails))
                        @foreach($sod->deliveryOrderDetails as $dod)                      
                            @forelse($salesOrdersRequest->items as $item)
                                @if ($item->id == $dod->item->id && $salesOrders->salesOrdersDetails->id == $dod->sales_orders_details_id)
                                    @php $totalKirim+=$dod->quantity;
                                    @endphp
                                @endif
                            @empty
                                @php $totalKirim+=$dod->quantity;
                                @endphp
                            @endforelse
                        @endforeach
                    @endif
                    @forelse($salesOrdersRequest->items as $item)
                        
                        @if ($item->id == $sod->item->id)
                            <tr>
                                <td align="left" valign="top" style="border-bottom: 0px;">
                                    {{ $sod->item->name }}
                                </td>
                                <td align="right" valign="top" style="border-bottom: 0px;">
									{{ number_format($sod->quantity,0,'.',',') }}
                                </td>
                                <td align="center" valign="top" style="border-bottom: 0px;">
                                    {{ $sod->item->unit->name }}
                                </td>
                                <td align="right" valign="top" style="border-bottom: 0px;">
									{{ number_format($totalKirim,0,'.',',') }}
                                </td>                                                                                
                                <td align="right" valign="top" style="border-bottom: 0px;">
								{{ number_format($sod->quantity-$totalKirim,0,'.',',') }}
								</td>                                   
                            </tr>
                        @endif
                    @empty
                        <tr>
                            <td align="left" valign="top" style="border-bottom: 0px;">
                                {{ $sod->item->name }}
                            </td>
                            <td align="right" valign="top" style="border-bottom: 0px;">
								{{ number_format($sod->quantity,0,'.',',') }}
                            </td>
                            <td align="center" valign="top" style="border-bottom: 0px;">
                                    {{ $sod->item->unit->name }}
                            </td>
                            <td align="right" valign="top" style="border-bottom: 0px;">
                                    {{ number_format($totalKirim,0,'.',',') }}
                            </td>  
                            <td align="right" valign="top" style="border-bottom: 0px;">
							{{ number_format($sod->quantity-$totalKirim,0,'.',',') }}
							</td>                                   
                        </tr>
                    @endforelse                                                        
                @endforeach                        

                @php
                $total = $total + $salesOrders->total;
                @endphp
                
            @endforeach
            <tr>
                    <td colspan="4" style="border: 0px">&nbsp;</td>                    
					<td style="border-top: 1px solid #000;border-bottom: 0px ;border-left: 0px;border-right: 0px;">&nbsp;</td>                    
					<td style="border-top: 1px solid #000;border-bottom: 0px ;border-left: 0px;border-right: 0px;">&nbsp;</td>                    
                    <td style="border-top: 1px solid #000;border-bottom: 0px ;border-left: 0px;border-right: 0px;">&nbsp;</td>                    
					<td style="border-top: 1px solid #000;border-bottom: 0px ;border-left: 0px;border-right: 0px;">&nbsp;</td>                    
					<td style="border-top: 1px solid #000;border-bottom: 0px ;border-left: 0px;border-right: 0px;">&nbsp;</td>                    
                </tr>
            </tbody>
        </table>  
    </div>
<script>
    window.print();
</script>

