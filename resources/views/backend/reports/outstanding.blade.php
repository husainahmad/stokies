@extends('backend.layouts.app')
@section ('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
    @include('backend.sales_orders.includes.breadcrumb-links')
@endsection
@section('content')

    <div class="panel panel-default">

        <div class="panel-heading clearfix">


        </div>

        <div class="panel-body">
        
            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
            
            <br><br>
            <br>
            <form method="POST" action="{{ route('reports.reports.outstanding.result') }}" accept-charset="UTF-8" id="create_outstanding_form" name="create_outstanding_form" class="form-horizontal">
            {{ csrf_field() }}
            @if(isset($salesOrdersRequest))
                @include ('backend.reports.outstandingform', [
                                        'salesOrdersRequest' => $salesOrdersRequest,
                                      ])            
            @else
                    @include ('backend.reports.outstandingform', [
                                        'salesOrdersRequest' => null,
                                      ])            
            @endif
            
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <input class="btn btn-primary" name="action" type="submit" value="Search">
                        <input class="btn btn-primary" name="btnPrint" type="button" id="btnPrint" value="Print">
                    </div>
                     <div class="col-md-offset-2 col-md-3">
                        
                    </div>   
                </div>

            </form>
            @isset($salesOrdersObjects)
                @if(count($salesOrdersObjects) == 0)
                <div class="panel-body text-center">
                    <h4>No Sales Orders Available!</h4>
                </div>
                @else
                <div class="panel-body panel-body-with-table">
                    <div class="table-responsive">

                        <table class="table table-striped ">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Sales Order Date</th>
                                    <th>Sales Order Delivery</th>
                                    <th>Ref Po Customer</th>
                                    <th>Customer</th>
                                    <th>Total</th>

                                </tr>
                            </thead>
                            <tbody>
                            @foreach($salesOrdersObjects as $salesOrders)
                                <tr>
                                    <td>{{ $salesOrders->no }}</td>
                                    <td>{{ date('d/m/Y', strtotime($salesOrders->sales_order_date)) }}</td>
                                    <td>{{ date('d/m/Y', strtotime($salesOrders->sales_order_delivery)) }}</td>
                                    <td>{{ $salesOrders->ref_po_customer }}</td>
                                    <td>{{ optional($salesOrders->customer)->name }}</td>
                                    <td align="right">{{ number_format($salesOrders->total,0,'.',',') }}</td>


                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
           
                @endif
            @endisset
            
        </div>
    </div>

@endsection

@push('after-scripts')    
        <script src="/js/bootstrap-datepicker.min.js"></script>
        <link media="all" type="text/css" rel="stylesheet" href="/css/bootstrap-datepicker.css">

    <script>
        var i=1;
        
        $(function() {           
            
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
            $('#from_date').datepicker({
                todayBtn: "linked",
                language: "en",
                autoclose: true,
                todayHighlight: true,
                format: 'dd/mm/yyyy'
            });
            
            $('#to_date').datepicker({
                todayBtn: "linked",
                language: "en",
                autoclose: true,
                todayHighlight: true,
                format: 'dd/mm/yyyy'
            });
                       
            $('#btnPrint').click(function() {                
                $.ajax({
                    url: "{{ route('reports.reports.outstanding.result') }}",
                    type: "GET",
                    data: {
                        "_token" : $('meta[name="csrf-token"]').attr('content'),
                        "from_date" : $("#from_date").val(),
                        "to_date" : $("#to_date").val(),
                        "category_id" : $("#category_id").val(),
                        "customers_id" : $("#customers_id").val(),
                        "no" : $("#no").val(),
                        "action" : "print"
                    },
                    success: function(response) {
                        var w = window.open();
                        $(w.document.body).html(response);
                    },
                    error: function() {                        
                    }
                });
            });                                                                 
        });        
        
    </script>
@endpush
