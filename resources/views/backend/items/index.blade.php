@extends('backend.layouts.app')
@section ('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
    @include('backend.items_stocks.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="fa fa-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">
            <form action="{{ url()->current() }}">
            <div class="col-md-offset-1 col-md-3 pull-left" role="group">                
                    <input type="text" name="keyword" class="form-control" placeholder="Search Name...">                                    
            </div>
            <div class="col-md-offset-2 col-md-1 pull-left">
                <button type="submit" class="btn btn-primary">
                        Search
                    </button>    
                <br/><br/>
            </div>
            </form>    
            <div class="col-md-offset-3 col-md-3 btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('items.items.create') }}" class="btn btn-success" title="Create New Items">
                    <span class="fa fa-plus" aria-hidden="true"></span>
                </a>
            </div>

        </div>
        
        @if(count($itemsObjects) == 0)
            <div class="panel-body text-center">
                <h4>No Items Available!</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>Category</th>
                            <th>Item Code</th>
                            <th>Name</th>
                            <th>Unit</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($itemsObjects as $items)
                        <tr>
                            @php
                                try {
                            @endphp
                                <td>{{ optional($items->category)->name }}</td>    
                            @php        
                                } catch (Exception $e) {
                            @endphp
                                <td>N/A</td>    
                            @php
                                }
                            @endphp
                            
                            <td>{{ $items->code }}</td>
                            <td>{{ $items->name }}</td>
                            @php
                                try {
                            @endphp
                                <td>{{ optional($items->unit)->name }}</td>   
                            @php        
                                } catch (Exception $e) {
                            @endphp
                                <td>N/A</td>    
                            @php
                                }
                            @endphp
                            

                            <td>

                                <form method="POST" action="{!! route('items.items.destroy', $items->id) !!}" accept-charset="UTF-8">
                                <input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}

                                    <div class="btn-group btn-group-xs pull-right" role="group">
<!--                                        <a href="{{ route('items.items.show', $items->id ) }}" class="btn btn-info" title="Show Items">
                                            <span class="fa fa-search" aria-hidden="true"></span>
                                        </a>-->
                                        <a href="{{ route('items.items.edit', $items->id ) }}" class="btn btn-primary" title="Edit Items">
                                            <span class="fa fa-pencil" aria-hidden="true"></span>
                                        </a>

                                        <button type="submit" class="btn btn-danger" title="Delete Items" onclick="return confirm(&quot;Delete Items?&quot;)">
                                            <span class="fa fa-trash" aria-hidden="true"></span>
                                        </button>
                                    </div>

                                </form>
                                
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            {!! $itemsObjects->render() !!}
        </div>
        
        @endif
    
    </div>
    </div>
</div>
@endsection