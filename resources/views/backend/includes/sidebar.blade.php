<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-title">
                {{ __('menus.backend.sidebar.general') }}
            </li>

            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('admin/dashboard')) }}" href="{{ route('admin.dashboard') }}"><i class="icon-speedometer"></i> {{ __('menus.backend.sidebar.dashboard') }}</a>
            </li>

            <li class="nav-title">
                {{ __('menus.backend.sidebar.system') }}
            </li>

            <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/masterdata*'), 'open') }}">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="icon-list"></i> {{ __('menus.backend.masterdata.title') }}
                </a>

                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/masterdata*')) }}" href="{{ route('customers.customers.index') }}">
                            {{ __('menus.backend.masterdata.customers.index') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/masterdata*')) }}" href="{{ route('suppliers.suppliers.index') }}">
                            {{ __('menus.backend.masterdata.suppliers.index') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/masterdata*')) }}" href="{{ route('categories.categories.index') }}">
                            {{ __('menus.backend.masterdata.categories.index') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/masterdata*')) }}" href="{{ route('units.units.index') }}">
                            {{ __('menus.backend.masterdata.units.index') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/masterdata*')) }}" href="{{ route('term_billings.term_billings.index') }}">
                            {{ __('menus.backend.masterdata.termbilling.index') }}
                        </a>
                    </li>
                </ul>
            </li>
            
            <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/masterdata*'), 'open') }}">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="icon-list"></i> {{ __('menus.backend.masterdata.items.index') }}
                </a>

                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/masterdata*')) }}" href="{{ route('items.items.index') }}">
                            {{ __('menus.backend.masterdata.items.index') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/masterdata*')) }}" href="{{ route('prices.prices.index') }}">
                            {{ __('menus.backend.masterdata.prices.index') }}
                        </a>
                    </li>
                    <!--
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/masterdata*')) }}" href="{{ route('items_stocks.items_stock.index') }}">
                            {{ __('menus.backend.masterdata.stock.index') }}
                        </a>
                    </li>
                    -->
                </ul>
            </li>
            <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/masterdata*'), 'open') }}">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="icon-list"></i> {{ __('menus.backend.masterdata.sales.index') }}
                </a>

                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/masterdata*')) }}" href="{{ route('sales_persons.sales_persons.index') }}">
                            {{ __('menus.backend.masterdata.sales-persons.index') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/masterdata*')) }}" href="{{ route('sales_orders.sales_orders.index') }}">
                            {{ __('menus.backend.masterdata.sales-orders.index') }}
                        </a>
                    </li>                                       
                </ul>
            </li>
            <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/masterdata*'), 'open') }}">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="icon-list"></i> {{ __('menus.backend.masterdata.order.index') }}
                </a>

                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/masterdata*')) }}" href="{{ route('purchase_orders.purchase_orders.index') }}">
                            {{ __('menus.backend.masterdata.purchase.index') }}
                        </a>
                    </li>                                     
                </ul>
                
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/masterdata*')) }}" href="{{ route('incoming_invoices.incoming_invoices.index') }}">
                            {{ __('menus.backend.masterdata.incoming_invoices.index') }}
                        </a>
                    </li>                                     
                </ul>
            </li>
            
            <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/masterdata*'), 'open') }}">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="icon-list"></i> {{ __('menus.backend.transactions.main') }}
                </a>
                
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/transactions*')) }}" href="{{ route('sales_orders_dps.sales_orders_dp.index') }}">
                            {{ __('menus.backend.transactions.downpayment.title') }}
                        </a>
                    </li>                                     
                </ul>

                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/transactions*')) }}" href="{{ route('delivery_orders.delivery_orders.index') }}">
                            {{ __('menus.backend.transactions.delivery_orders.title') }}
                        </a>
                    </li>                                     
                </ul>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/transactions*')) }}" href="{{ route('invoices.invoices.index') }}">
                            {{ __('menus.backend.transactions.invoices.title') }}
                        </a>
                    </li>                                     
                </ul>
            </li>
            
            <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/masterdata*'), 'open') }}">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="icon-list"></i> {{ __('menus.backend.reports.main') }}
                </a> 
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/transactions*')) }}" href="{{ route('reports.reports.purchase_order') }}">
                            {{ __('menus.backend.reports.purchase.title') }}
                        </a>
                    </li>                                     
                </ul>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/transactions*')) }}" href="{{ route('reports.reports.sales_order') }}">
                            {{ __('menus.backend.reports.sales_orders.title') }}
                        </a>
                    </li>                                     
                </ul>                
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/transactions*')) }}" href="{{ route('reports.reports.delivery_order') }}">
                            {{ __('menus.backend.reports.delivery_orders.title') }}
                        </a>
                    </li>                                     
                </ul>                
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/transactions*')) }}" href="{{ route('reports.reports.invoice') }}">
                            {{ __('menus.backend.reports.invoices.title') }}
                        </a>
                    </li>                                     
                </ul>                
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/transactions*')) }}" href="{{ route('reports.reports.outstanding') }}">
                            {{ __('menus.backend.reports.outstanding.title') }}
                        </a>
                    </li>                                     
                </ul>                
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/transactions*')) }}" href="{{ route('reports.reports.incoming_invoice') }}">
                            {{ __('menus.backend.reports.incoming_invoice.title') }}
                        </a>
                    </li>                                     
                </ul>                
            </li>
            
            @if ($logged_in_user->isAdmin())
                <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/auth*'), 'open') }}">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                        <i class="icon-user"></i> {{ __('menus.backend.access.title') }}

                        @if ($pending_approval > 0)
                            <span class="badge badge-danger">{{ $pending_approval }}</span>
                        @endif
                    </a>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="{{ route('admin.auth.user.index') }}">
                                {{ __('labels.backend.access.users.management') }}

                                @if ($pending_approval > 0)
                                    <span class="badge badge-danger">{{ $pending_approval }}</span>
                                @endif
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/role*')) }}" href="{{ route('admin.auth.role.index') }}">
                                {{ __('labels.backend.access.roles.management') }}
                            </a>
                        </li>
                    </ul>
                </li>
            @endif

            <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/log-viewer*'), 'open') }}">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="icon-list"></i> {{ __('menus.backend.log-viewer.main') }}
                </a>

                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/log-viewer')) }}" href="{{ route('log-viewer::dashboard') }}">
                            {{ __('menus.backend.log-viewer.dashboard') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/log-viewer/logs*')) }}" href="{{ route('log-viewer::logs.list') }}">
                            {{ __('menus.backend.log-viewer.logs') }}
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </nav>
</div><!--sidebar-->