@extends('backend.layouts.app')
@section ('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
    @include('backend.sales_orders.includes.breadcrumb-links')
@endsection
@section('content')
<div class="card">
    <div class="card-body">
    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="fa fa-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">
            <div class="pull-left">
                <h4 class="mt-5 mb-5"></h4>
            </div>
            <div class="btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('sales_orders.sales_orders.create') }}" class="btn btn-success" title="Create New Sales Orders">
                    <span class="fa fa-plus" aria-hidden="true"></span>
                </a>
            </div>

        </div>
        
        @if(count($salesOrdersObjects) == 0)
            <div class="panel-body text-center">
                <h4>No Sales Orders Available!</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Sales Order Date</th>
                            <th>Ref Po Customer</th>
                            <th>Status</th>
                            <th>Customer</th>
                            <th>Total</th>
                            <th>Total DP</th>
                            <th>Invoice</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($salesOrdersObjects as $salesOrders)
                        <tr>
                            <td>{{ $salesOrders->no }}</td>
                            <td>{{ $salesOrders->sales_order_delivery }}</td>
                            <td>{{ $salesOrders->ref_po_customer }}</td>
                            @if($salesOrders->status==0)
                                <td>OutStanding</td>
                            @endif
                            @if($salesOrders->status==1)
                                <td>Completed</td>
                            @endif
                            <td>{{ optional($salesOrders->customer)->name }}</td>
                            <td>{{ $salesOrders->total }}</td>
                            <td>{{ $salesOrders->total_dp }}</td>
                            <td>{{ $salesOrders->total_invoice }}</td>
                            
                            <td>

                                

                                    <div class="btn-group btn-group-xs pull-right" role="group">
                                        @if($salesOrders->status==0)
                                            <form method="POST" action="{!! route('sales_orders.sales_orders.complete', $salesOrders->id) !!}" accept-charset="UTF-8">
                                                 <input name="_method" value="PUT" type="hidden">
                                            {{ csrf_field() }}  
                                            <button type="submit" class="btn btn-success" title="Complete Sales Orders" onclick="return confirm(&quot;Complete Sales Orders?&quot;)">
                                                <span class="fa fa-bookmark" aria-hidden="true"></span>
                                            </button>
                                            </form>  
                                        @endif
                                        @if($salesOrders->status==1)
                                            <a href="#" class="btn btn-info" title="Completed Sales Orders">
                                                <span class="fa fa-check" aria-hidden="true"></span>
                                            </a>                                            
                                        @endif              

                                    </div>

                                
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            {!! $salesOrdersObjects->render() !!}
        </div>
        
        @endif
    
    </div>
    </div>
</div>

@endsection