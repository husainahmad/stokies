@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Sales Orders' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('sales_orders.sales_orders.destroy', $salesOrders->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('sales_orders.sales_orders.index') }}" class="btn btn-primary" title="Show All Sales Orders">
                        <span class="fa fa-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('sales_orders.sales_orders.create') }}" class="btn btn-success" title="Create New Sales Orders">
                        <span class="fa fa-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('sales_orders.sales_orders.edit', $salesOrders->id ) }}" class="btn btn-primary" title="Edit Sales Orders">
                        <span class="fa fa-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Sales Orders" onclick="return confirm(&quot;Delete Sales Orders??&quot;)">
                        <span class="fa fa-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>No</dt>
            <dd>{{ $salesOrders->no }}</dd>
            <dt>Sales Order Date</dt>
            <dd>{{ $salesOrders->sales_order_date }}</dd>
            <dt>Sales Order Delivery</dt>
            <dd>{{ $salesOrders->sales_order_delivery }}</dd>
            <dt>Sales Person</dt>
            <dd>{{ optional($salesOrders->salesPerson)->id }}</dd>
            <dt>Ref Po Customer</dt>
            <dd>{{ $salesOrders->ref_po_customer }}</dd>
            <dt>Customer</dt>
            <dd>{{ optional($salesOrders->customer)->name }}</dd>
            <dt>Transport</dt>
            <dd>{{ $salesOrders->transport }}</dd>
            <dt>Term Of Payment</dt>
            <dd>{{ optional($salesOrders->termBilling)->id }}</dd>
            <dt>Created At</dt>
            <dd>{{ $salesOrders->created_at }}</dd>
            <dt>Updated At</dt>
            <dd>{{ $salesOrders->updated_at }}</dd>
            <dt>User</dt>
            <dd>{{ optional($salesOrders->user)->id }}</dd>

        </dl>

    </div>
</div>

@endsection