@extends('backend.layouts.app')
@section ('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
    @include('backend.sales_orders.includes.breadcrumb-links')
@endsection
@section('content')
<div class="card">
    <div class="card-body">
    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="fa fa-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">
            <form action="{{ url()->current() }}">
            <div class="col-md-offset-1 col-md-3 pull-left" role="group">                
                    <input type="text" name="keyword" class="form-control" placeholder="Search SO No...">                                    
            </div>
            <div class="col-md-offset-2 col-sm-4 pull-left">
                <div class="form-group row {{ $errors->has('customer_id') ? 'has-error' : '' }}">
                    <label class="col-md-3 col-form-label"  for="customer_id">Customer</label>
                    <div class="col-md-9">
                        <select class="form-control" id="customer_id" name="customer_id" >
                                <option value="" style="display: none;" disabled selected>Select customer</option>
                                @foreach ($customers as $key => $customer)
                                            <option value="{{ $key }}" >
                                                {{ $customer }}
                                            </option>
                                        @endforeach
                        </select>

                        {!! $errors->first('customer_id', '<p class="help-block">:message</p>') !!}
                    </div>

                </div>
            </div>
            <div class="col-md-offset-3 col-md-1 pull-left">
                <button type="submit" class="btn btn-primary">
                        Search
                    </button>  
                <br/><br/>
            </div>
            </form>    
            <div class="col-md-offset-4 btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('sales_orders.sales_orders.create') }}" class="btn btn-success" title="Create New Sales Orders">
                    <span class="fa fa-plus" aria-hidden="true"></span>
                </a>
            </div>

        </div>
        
        @if(count($salesOrdersObjects) == 0)
            <div class="panel-body text-center">
                <h4>No Sales Orders Available!</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Sales Order Date</th>
                            <th>Sales Person</th>
                            <th>Customer</th>
                            <th>Send Via</th>
                            <th>Total</th>                            
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($salesOrdersObjects as $salesOrders)
                        <tr>
                            <td>{{ $salesOrders->no }}</td>
                            <td>{{ date_format(date_create($salesOrders->sales_order_date), 'd/m/Y') }}</td>
                            <td>{{ optional($salesOrders->salesPerson)->name }}</td>                            
                            <td>{{ optional($salesOrders->customer)->name }}</td>
                            <td>{{ $salesOrders->transport }}</td>
                            <td align="right">{{ number_format($salesOrders->total,0,'.',',') }}</td>
                            
                            <td>

                                    <div class="btn-group btn-group-xs pull-right" role="group">
                                        <a target="_blank" href="{{ route('sales_orders.sales_orders.print', $salesOrders->id ) }}" class="btn btn-info" title="Print Sales Orders">
                                            <span class="fa fa-print" aria-hidden="true"></span>
                                        </a>
<!--                                        <a href="{{ route('sales_orders.sales_orders.show', $salesOrders->id ) }}" class="btn btn-info" title="Show Sales Orders">
                                            <span class="fa fa-search" aria-hidden="true"></span>
                                        </a>-->
                                        <a href="{{ route('sales_orders.sales_orders.edit', $salesOrders->id ) }}" class="btn btn-primary" title="Edit Sales Orders">
                                            <span class="fa fa-pencil" aria-hidden="true"></span>
                                        </a>                 
                                        @if ($salesOrders->status==0)
                                        <form method="POST" action="{!! route('sales_orders.sales_orders.complete', $salesOrders->id) !!}" accept-charset="UTF-8">
                                            <input name="_method" type="hidden" value="PUT">
                                        {{ csrf_field() }}
                                                <button type="submit" class="btn btn-success" title="Complete" onclick="return confirm(&quot;Complete Process?&quot;)">
                                                    <span class="fa fa-flag" aria-hidden="true"></span>
                                                </button>
                                        </form>
                                        @else
                                        <form method="POST" action="{!! route('sales_orders.sales_orders.uncomplete', $salesOrders->id) !!}" accept-charset="UTF-8">
                                            <input name="_method" type="hidden" value="PUT">
                                        {{ csrf_field() }}
                                                <button type="submit" class="btn btn-uncomplete" title="Uncomplete" onclick="return confirm(&quot;Uncomplete Process?&quot;)">
                                                    <span class="fa fa-expand" aria-hidden="true"></span>
                                                </button>
                                        </form>
                                        @endif
                                       <form method="POST" action="{!! route('sales_orders.sales_orders.destroy', $salesOrders->id) !!}" accept-charset="UTF-8">
                                        <input name="_method" value="DELETE" type="hidden">
                                        {{ csrf_field() }}     
                                        <button type="submit" class="btn btn-danger" title="Delete Sales Orders" onclick="return confirm(&quot;Delete Sales Orders?&quot;)">
                                            <span class="fa fa-trash" aria-hidden="true"></span>
                                        </button>
                                       </form>

                                    </div>

                                
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            {!! $salesOrdersObjects->render() !!}
        </div>
        
        @endif
    
    </div>
    </div>
</div>

@endsection