<div class="row">
    <div class="col-sm-6">        
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('no') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label"  class="col-md-3 col-form-label" for="no">No</label>
                        <div class="col-md-9">
                            <input class="form-control" name="no" type="text" id="no" value="{{ old('no', optional($salesOrders)->no) == '' ? 'SO-ABL/'.date('y').'/' : old('no', optional($salesOrders)->no) }}" minlength="1" maxlength="45" required="true" placeholder="Enter no here...">
                            {!! $errors->first('no', '<p class="help-block">:message</p>') !!}
                        </div>
                        
                    </div>
                </div>                
            </div>    
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('sales_person_id') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label"  for="sales_person_id" >Sales Person</label>
                        <div class="col-md-9">
                        <select class="form-control" id="sales_person_id" name="sales_person_id" required="true" >
                                    <option value="" style="display: none;" {{ old('sales_person_id', optional($salesOrders)->sales_person_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select sales person</option>
                                @foreach ($salesPeople as $key => $salesPerson)
                                            <option value="{{ $key }}" {{ old('sales_person_id', optional($salesOrders)->sales_person_id) == $key ? 'selected' : '' }}>
                                                {{ $salesPerson }}
                                            </option>
                                        @endforeach
                        </select>
                        {!! $errors->first('sales_person_id', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>                    
                </div>
            </div>    
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('sales_order_date') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label"  for="sales_order_date">SO Date</label>
                        <div class="col-md-9">
                            <div class="input-group">
                                <span class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                            </span>
                            <input class="form-control" name="sales_order_date" type="text" id="sales_order_date" data-provide="datepicker" value="{{ old('sales_order_date', optional($salesOrders)->getSalesOrderDateID()) }}" minlength="1" required="true" placeholder="Enter sales order date here...">
                                {!! $errors->first('sales_order_date', '<p class="help-block">:message</p>') !!}
                            </div>
                            <small class="text-muted">ex. 99/99/9999</small>
                        </div>
                    </div> 
                </div>            
            </div>    
            <div class="row">
                 <div class="col-sm-12">
                     <div class="form-group row {{ $errors->has('sales_order_delivery') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label"  for="sales_order_date">SO Delivery</label>
                        <div class="col-md-9">
                            <div class="input-group">
                                <span class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                            </span>
                            <input class="form-control" name="sales_order_delivery" type="text" id="sales_order_delivery" value="{{ old('sales_order_delivery', optional($salesOrders)->getSalesOrderDeliveryID()) }}" minlength="1" maxlength="45" required="true" placeholder="Enter sales order delivery here...">
                                {!! $errors->first('sales_order_delivery', '<p class="help-block">:message</p>') !!}
                            </div>
                            <small class="text-muted">ex. 99/99/9999</small>
                        </div>
                    </div>                      
                 </div>
            </div>                                                                   
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('delivery_address') ? 'has-error' : '' }}">
                        <label for="address" class="col-md-3 control-label">SO Address</label>
                        <div class="col-md-9">
                            <textarea id="delivery_address" name="delivery_address" rows="6" class="form-control" placeholder="">{{ old('delivery_address', optional($salesOrders)->delivery_address) }}</textarea>        
                            {!! $errors->first('delivery_address', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="col-sm-6">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('customer_id') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label"  for="customer_id">Customer</label>
                        <div class="col-md-9">
                            <select class="form-control" id="customer_id" name="customer_id" required="true">
                                    <option value="" style="display: none;" {{ old('customer_id', optional($salesOrders)->customer_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select customer</option>
                                    @foreach ($customers as $key => $customer)
                                                <option value="{{ $key }}" {{ old('customer_id', optional($salesOrders)->customer_id) == $key ? 'selected' : '' }}>
                                                    {{ $customer }}
                                                </option>
                                            @endforeach
                            </select>

                            {!! $errors->first('customer_id', '<p class="help-block">:message</p>') !!}
                        </div>
                        
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row{{ $errors->has('ref_po_customer') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label"  for="ref_po_customer">PO Customer</label>
                        <div class="col-md-9">
                            <input class="form-control" name="ref_po_customer" type="text" id="ref_po_customer" value="{{ old('ref_po_customer', optional($salesOrders)->ref_po_customer) }}" minlength="1" maxlength="45" required="true" placeholder="Enter ref po customer here...">
                                {!! $errors->first('ref_po_customer', '<p class="help-block">:message</p>') !!}
                        </div>    
                    </div>
                </div>
            </div>    
            
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('purchase_order_date') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label"  for="purchase_order_date">PO Date</label>
                        <div class="col-md-9">
                            <div class="input-group">
                                <span class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                            </span>
                            <input class="form-control" name="purchase_order_date" type="text" id="purchase_order_date" data-provide="datepicker" value="{{ old('purchase_order_date', optional($salesOrders)->getPurchaseOrderID()) }}" minlength="1" required="true" placeholder="Enter purchase order date here...">
                                {!! $errors->first('purchase_order_date', '<p class="help-block">:message</p>') !!}
                            </div>
                            <small class="text-muted">ex. 99/99/9999</small>
                        </div>
                            
                    </div> 
                </div>            
            </div>
            
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('transport') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label"  for="transport" class="col-md-2 control-label">Send Via</label>
                        <div class="col-md-9">
                            <input class="form-control" name="transport" type="text" id="transport" value="{{ old('transport', optional($salesOrders)->transport) }}" minlength="1" maxlength="45" required="true" placeholder="Enter Send Via here...">
                            {!! $errors->first('transport', '<p class="help-block">:message</p>') !!}
                        </div>                        
                    </div>
                </div>
            </div>    
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('term_billing_id') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label"  for="term_billing_id">TOC</label>
                        <div class="col-md-9">
                            <select class="form-control" id="term_billing_id" name="term_billing_id" required="true">
                                       <option value="" style="display: none;" {{ old('term_billing_id', optional($salesOrders)->term_billing_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select term billing</option>
                                   @foreach ($termBillings as $key => $termBilling)
                                               <option value="{{ $key }}" {{ old('term_billing_id', optional($salesOrders)->term_billing_id) == $key ? 'selected' : '' }}>
                                                   {{ $termBilling }}
                                               </option>
                                           @endforeach
                           </select>

                           {!! $errors->first('term_billing_id', '<p class="help-block">:message</p>') !!}
                        </div>                        
                    </div>
                </div>
            </div>   
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group row {{ $errors->has('tax') ? 'has-error' : '' }}">
                        <label class="col-md-3 col-form-label"  for="tax" class="col-md-2 control-label">Tax Persen</label>
                        <div class="col-md-9">
                        <input class="form-control" name="tax" type="text" id="tax" value="{{ old('tax', optional($salesOrders)->tax) == '' ? '0.11' : old('tax', optional($salesOrders)->tax) }}" minlength="1" maxlength="45" required="true" placeholder="Enter...">
                                {!! $errors->first('tax', '<p class="help-block">:message</p>') !!}          
                        </div>                        
                    </div>
                </div>
            </div>    
        </div>
    </div>
</div>
