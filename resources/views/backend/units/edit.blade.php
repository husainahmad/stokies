@extends('backend.layouts.app')
@section ('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
    @include('backend.units.includes.breadcrumb-links')
@endsection

@section('content')

    <div class="panel panel-default">
  
        <div class="panel-heading clearfix">

            <div class="btn-group btn-group-sm pull-right" role="group">

                <a href="{{ route('units.units.index') }}" class="btn btn-primary" title="Show All Units">
                    <span class="fa fa-th-list" aria-hidden="true"></span>
                </a>

                <a href="{{ route('units.units.create') }}" class="btn btn-success" title="Create New Units">
                    <span class="fa fa-plus" aria-hidden="true"></span>
                </a>

            </div>
        </div>

        <div class="panel-body">

            <form method="POST" action="{{ route('units.units.update', $units->id) }}" id="edit_units_form" name="edit_units_form" accept-charset="UTF-8" class="form-horizontal">
            {{ csrf_field() }}
            <input name="_method" type="hidden" value="PUT">
            @include ('backend.units.form', [
                                        'units' => $units,
                                      ])

                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <input class="btn btn-primary" type="submit" value="Update">
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection