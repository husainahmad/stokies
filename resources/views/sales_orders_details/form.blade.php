
<div class="form-group {{ $errors->has('sales_orders_id') ? 'has-error' : '' }}">
    <label for="sales_orders_id" class="col-md-2 control-label">Sales Orders</label>
    <div class="col-md-10">
        <select class="form-control" id="sales_orders_id" name="sales_orders_id" required="true">
        	    <option value="" style="display: none;" {{ old('sales_orders_id', optional($salesOrdersDetails)->sales_orders_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select sales orders</option>
        	@foreach ($salesOrders as $key => $salesOrder)
			    <option value="{{ $key }}" {{ old('sales_orders_id', optional($salesOrdersDetails)->sales_orders_id) == $key ? 'selected' : '' }}>
			    	{{ $salesOrder }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('sales_orders_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('items_id') ? 'has-error' : '' }}">
    <label for="items_id" class="col-md-2 control-label">Items</label>
    <div class="col-md-10">
        <select class="form-control" id="items_id" name="items_id" required="true">
        	    <option value="" style="display: none;" {{ old('items_id', optional($salesOrdersDetails)->items_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select items</option>
        	@foreach ($items as $key => $item)
			    <option value="{{ $key }}" {{ old('items_id', optional($salesOrdersDetails)->items_id) == $key ? 'selected' : '' }}>
			    	{{ $item }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('items_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('quantity') ? 'has-error' : '' }}">
    <label for="quantity" class="col-md-2 control-label">Quantity</label>
    <div class="col-md-10">
        <input class="form-control" name="quantity" type="number" id="quantity" value="{{ old('quantity', optional($salesOrdersDetails)->quantity) }}" min="-2147483648" max="2147483647" required="true" placeholder="Enter quantity here...">
        {!! $errors->first('quantity', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('price') ? 'has-error' : '' }}">
    <label for="price" class="col-md-2 control-label">Price</label>
    <div class="col-md-10">
        <input class="form-control" name="price" type="number" id="price" value="{{ old('price', optional($salesOrdersDetails)->price) }}" min="-9" max="9" required="true" placeholder="Enter price here...">
        {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
    </div>
</div>

