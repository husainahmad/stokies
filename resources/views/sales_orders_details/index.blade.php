@extends('layouts.app')

@section('content')

    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="fa fa-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 class="mt-5 mb-5">Sales Orders Details</h4>
            </div>

            <div class="btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('sales_orders_details.sales_orders_details.create') }}" class="btn btn-success" title="Create New Sales Orders Details">
                    <span class="fa fa-plus" aria-hidden="true"></span>
                </a>
            </div>

        </div>
        
        @if(count($salesOrdersDetailsObjects) == 0)
            <div class="panel-body text-center">
                <h4>No Sales Orders Details Available!</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>Sales Orders</th>
                            <th>Items</th>
                            <th>Quantity</th>
                            <th>Price</th>

                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($salesOrdersDetailsObjects as $salesOrdersDetails)
                        <tr>
                            <td>{{ optional($salesOrdersDetails->salesOrder)->no }}</td>
                            <td>{{ optional($salesOrdersDetails->item)->name }}</td>
                            <td>{{ $salesOrdersDetails->quantity }}</td>
                            <td>{{ $salesOrdersDetails->price }}</td>

                            <td>

                                <form method="POST" action="{!! route('sales_orders_details.sales_orders_details.destroy', $salesOrdersDetails->id) !!}" accept-charset="UTF-8">
                                <input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}

                                    <div class="btn-group btn-group-xs pull-right" role="group">
                                        <a target="_blank" href="{{ route('sales_orders_details.sales_orders_details.show', $salesOrdersDetails->id ) }}" class="btn btn-info" title="Show Sales Orders Details">
                                            <span class="fa fa-open" aria-hidden="true"></span>
                                        </a>
                                        <a href="{{ route('sales_orders_details.sales_orders_details.edit', $salesOrdersDetails->id ) }}" class="btn btn-primary" title="Edit Sales Orders Details">
                                            <span class="fa fa-pencil" aria-hidden="true"></span>
                                        </a>

                                        <button type="submit" class="btn btn-danger" title="Delete Sales Orders Details" onclick="return confirm(&quot;Delete Sales Orders Details?&quot;)">
                                            <span class="fa fa-trash" aria-hidden="true"></span>
                                        </button>
                                    </div>

                                </form>
                                
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            {!! $salesOrdersDetailsObjects->render() !!}
        </div>
        
        @endif
    
    </div>
@endsection