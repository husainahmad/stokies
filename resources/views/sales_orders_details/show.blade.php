@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Sales Orders Details' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('sales_orders_details.sales_orders_details.destroy', $salesOrdersDetails->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('sales_orders_details.sales_orders_details.index') }}" class="btn btn-primary" title="Show All Sales Orders Details">
                        <span class="fa fa-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('sales_orders_details.sales_orders_details.create') }}" class="btn btn-success" title="Create New Sales Orders Details">
                        <span class="fa fa-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('sales_orders_details.sales_orders_details.edit', $salesOrdersDetails->id ) }}" class="btn btn-primary" title="Edit Sales Orders Details">
                        <span class="fa fa-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Sales Orders Details" onclick="return confirm(&quot;Delete Sales Orders Details??&quot;)">
                        <span class="fa fa-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Sales Orders</dt>
            <dd>{{ optional($salesOrdersDetails->salesOrder)->no }}</dd>
            <dt>Items</dt>
            <dd>{{ optional($salesOrdersDetails->item)->name }}</dd>
            <dt>Quantity</dt>
            <dd>{{ $salesOrdersDetails->quantity }}</dd>
            <dt>Price</dt>
            <dd>{{ $salesOrdersDetails->price }}</dd>
            <dt>Created At</dt>
            <dd>{{ $salesOrdersDetails->created_at }}</dd>
            <dt>Updated At</dt>
            <dd>{{ $salesOrdersDetails->updated_at }}</dd>

        </dl>

    </div>
</div>

@endsection