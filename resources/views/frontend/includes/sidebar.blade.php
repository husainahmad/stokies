<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-title">
                {{ __('menus.backend.sidebar.general') }}
            </li>

            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('dashboard')) }}" href="{{ route('admin.dashboard') }}"><i class="icon-speedometer"></i> {{ __('menus.backend.sidebar.dashboard') }}</a>
            </li>

            <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/masterdata*'), 'open') }}">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="icon-list"></i> {{ __('menus.backend.masterdata.sales.index') }}
                </a>

                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/masterdata*')) }}" href="{{ route('sales_orders.sales_orders.index') }}">
                            {{ __('menus.backend.masterdata.sales-orders.index') }}
                        </a>
                    </li>                                       
                </ul>
            </li>
            
        </ul>
    </nav>
</div><!--sidebar-->