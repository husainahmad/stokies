<footer class="app-footer">
    <strong>{{ __('labels.general.copyright') }} &copy; {{ date('Y') }} {{ __('strings.backend.general.boilerplate_link') }}</strong>  &nbsp;{{ __('strings.backend.general.all_rights_reserved') }}

</footer>