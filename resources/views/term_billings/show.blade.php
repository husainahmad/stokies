@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Term Of Payments' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('term_billings.term_billings.destroy', $termBillings->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('term_billings.term_billings.index') }}" class="btn btn-primary" title="Show All Term Of Payments">
                        <span class="fa fa-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('term_billings.term_billings.create') }}" class="btn btn-success" title="Create New Term Of Payments">
                        <span class="fa fa-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('term_billings.term_billings.edit', $termBillings->id ) }}" class="btn btn-primary" title="Edit Term Of Payments">
                        <span class="fa fa-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Term Of Payments" onclick="return confirm(&quot;Delete Term Of Payments??&quot;)">
                        <span class="fa fa-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>No</dt>
            <dd>{{ $termBillings->no }}</dd>
            <dt>Sales Order Date</dt>
            <dd>{{ $termBillings->sales_order_date }}</dd>
            <dt>Sales Order Delivery</dt>
            <dd>{{ $termBillings->sales_order_delivery }}</dd>
            <dt>Sales Person</dt>
            <dd>{{ optional($termBillings->salesPerson)->id }}</dd>
            <dt>Ref Po Customer</dt>
            <dd>{{ $termBillings->ref_po_customer }}</dd>
            <dt>Customer</dt>
            <dd>{{ optional($termBillings->customer)->name }}</dd>
            <dt>Transport</dt>
            <dd>{{ $termBillings->transport }}</dd>
            <dt>Term Of Payment</dt>
            <dd>{{ optional($termBillings->termBilling)->id }}</dd>
            <dt>Created At</dt>
            <dd>{{ $termBillings->created_at }}</dd>
            <dt>Updated At</dt>
            <dd>{{ $termBillings->updated_at }}</dd>
            <dt>User</dt>
            <dd>{{ optional($termBillings->user)->id }}</dd>

        </dl>

    </div>
</div>

@endsection