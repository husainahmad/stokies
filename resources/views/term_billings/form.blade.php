
<div class="form-group {{ $errors->has('no') ? 'has-error' : '' }}">
    <label for="no" class="col-md-2 control-label">No</label>
    <div class="col-md-10">
        <input class="form-control" name="no" type="text" id="no" value="{{ old('no', optional($termBillings)->no) }}" minlength="1" maxlength="45" required="true" placeholder="Enter no here...">
        {!! $errors->first('no', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('sales_order_date') ? 'has-error' : '' }}">
    <label for="sales_order_date" class="col-md-2 control-label">Sales Order Date</label>
    <div class="col-md-10">
        <input class="form-control" name="sales_order_date" type="text" id="sales_order_date" value="{{ old('sales_order_date', optional($termBillings)->sales_order_date) }}" minlength="1" required="true" placeholder="Enter sales order date here...">
        {!! $errors->first('sales_order_date', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('sales_order_delivery') ? 'has-error' : '' }}">
    <label for="sales_order_delivery" class="col-md-2 control-label">Sales Order Delivery</label>
    <div class="col-md-10">
        <input class="form-control" name="sales_order_delivery" type="text" id="sales_order_delivery" value="{{ old('sales_order_delivery', optional($termBillings)->sales_order_delivery) }}" minlength="1" maxlength="45" required="true" placeholder="Enter sales order delivery here...">
        {!! $errors->first('sales_order_delivery', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('sales_person_id') ? 'has-error' : '' }}">
    <label for="sales_person_id" class="col-md-2 control-label">Sales Person</label>
    <div class="col-md-10">
        <select class="form-control" id="sales_person_id" name="sales_person_id" required="true">
        	    <option value="" style="display: none;" {{ old('sales_person_id', optional($termBillings)->sales_person_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select sales person</option>
        	@foreach ($salesPeople as $key => $salesPerson)
			    <option value="{{ $key }}" {{ old('sales_person_id', optional($termBillings)->sales_person_id) == $key ? 'selected' : '' }}>
			    	{{ $salesPerson }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('sales_person_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('ref_po_customer') ? 'has-error' : '' }}">
    <label for="ref_po_customer" class="col-md-2 control-label">Ref Po Customer</label>
    <div class="col-md-10">
        <input class="form-control" name="ref_po_customer" type="text" id="ref_po_customer" value="{{ old('ref_po_customer', optional($termBillings)->ref_po_customer) }}" minlength="1" maxlength="45" required="true" placeholder="Enter ref po customer here...">
        {!! $errors->first('ref_po_customer', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('customer_id') ? 'has-error' : '' }}">
    <label for="customer_id" class="col-md-2 control-label">Customer</label>
    <div class="col-md-10">
        <select class="form-control" id="customer_id" name="customer_id" required="true">
        	    <option value="" style="display: none;" {{ old('customer_id', optional($termBillings)->customer_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select customer</option>
        	@foreach ($customers as $key => $customer)
			    <option value="{{ $key }}" {{ old('customer_id', optional($termBillings)->customer_id) == $key ? 'selected' : '' }}>
			    	{{ $customer }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('customer_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('transport') ? 'has-error' : '' }}">
    <label for="transport" class="col-md-2 control-label">Transport</label>
    <div class="col-md-10">
        <input class="form-control" name="transport" type="text" id="transport" value="{{ old('transport', optional($termBillings)->transport) }}" minlength="1" maxlength="45" required="true" placeholder="Enter transport here...">
        {!! $errors->first('transport', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('term_billing_id') ? 'has-error' : '' }}">
    <label for="term_billing_id" class="col-md-2 control-label">Term Of Payment</label>
    <div class="col-md-10">
        <select class="form-control" id="term_billing_id" name="term_billing_id" required="true">
        	    <option value="" style="display: none;" {{ old('term_billing_id', optional($termBillings)->term_billing_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select Term Of Payment</option>
        	@foreach ($termBillings as $key => $termBilling)
			    <option value="{{ $key }}" {{ old('term_billing_id', optional($termBillings)->term_billing_id) == $key ? 'selected' : '' }}>
			    	{{ $termBilling }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('term_billing_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('user_id') ? 'has-error' : '' }}">
    <label for="user_id" class="col-md-2 control-label">User</label>
    <div class="col-md-10">
        <select class="form-control" id="user_id" name="user_id" required="true">
        	    <option value="" style="display: none;" {{ old('user_id', optional($termBillings)->user_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select user</option>
        	@foreach ($users as $key => $user)
			    <option value="{{ $key }}" {{ old('user_id', optional($termBillings)->user_id) == $key ? 'selected' : '' }}>
			    	{{ $user }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('user_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

