@extends('layouts.app')

@section('content')

    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="fa fa-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 class="mt-5 mb-5">Term Of Payments</h4>
            </div>

            <div class="btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('term_billings.term_billings.create') }}" class="btn btn-success" title="Create New Term Of Payments">
                    <span class="fa fa-plus" aria-hidden="true"></span>
                </a>
            </div>

        </div>
        
        @if(count($termBillingsObjects) == 0)
            <div class="panel-body text-center">
                <h4>No Term Of Payments Available!</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Sales Order Date</th>
                            <th>Sales Order Delivery</th>
                            <th>Sales Person</th>
                            <th>Ref Po Customer</th>
                            <th>Customer</th>
                            <th>Transport</th>
                            <th>Term Of Payment</th>
                            <th>User</th>

                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($termBillingsObjects as $termBillings)
                        <tr>
                            <td>{{ $termBillings->no }}</td>
                            <td>{{ $termBillings->sales_order_date }}</td>
                            <td>{{ $termBillings->sales_order_delivery }}</td>
                            <td>{{ optional($termBillings->salesPerson)->id }}</td>
                            <td>{{ $termBillings->ref_po_customer }}</td>
                            <td>{{ optional($termBillings->customer)->name }}</td>
                            <td>{{ $termBillings->transport }}</td>
                            <td>{{ optional($termBillings->termBilling)->id }}</td>
                            <td>{{ optional($termBillings->user)->id }}</td>

                            <td>

                                <form method="POST" action="{!! route('term_billings.term_billings.destroy', $termBillings->id) !!}" accept-charset="UTF-8">
                                <input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}

                                    <div class="btn-group btn-group-xs pull-right" role="group">
                                        <a href="{{ route('term_billings.term_billings.show', $termBillings->id ) }}" class="btn btn-info" title="Show Term Of Payments">
                                            <span class="fa fa-search" aria-hidden="true"></span>
                                        </a>
                                        <a href="{{ route('term_billings.term_billings.edit', $termBillings->id ) }}" class="btn btn-primary" title="Edit Term Of Payments">
                                            <span class="fa fa-pencil" aria-hidden="true"></span>
                                        </a>

                                        <button type="submit" class="btn btn-danger" title="Delete Term Of Payments" onclick="return confirm(&quot;Delete Term Of Payments?&quot;)">
                                            <span class="fa fa-trash" aria-hidden="true"></span>
                                        </button>
                                    </div>

                                </form>
                                
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            {!! $termBillingsObjects->render() !!}
        </div>
        
        @endif
    
    </div>
@endsection