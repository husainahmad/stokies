@extends('layouts.app')

@section('content')

    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="fa fa-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 class="mt-5 mb-5">Items Stock Ins</h4>
            </div>

            <div class="btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('items_stock_ins.items_stock_in.create') }}" class="btn btn-success" title="Create New Items Stock In">
                    <span class="fa fa-plus" aria-hidden="true"></span>
                </a>
            </div>

        </div>
        
        @if(count($itemsStockIns) == 0)
            <div class="panel-body text-center">
                <h4>No Items Stock Ins Available!</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>Items</th>
                            <th>Volume</th>
                            <th>Suppliers</th>
                            <th>Price</th>
                            <th>Amount Price</th>
                            <th>Users</th>
                            <th>Reference No</th>

                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($itemsStockIns as $itemsStockIn)
                        <tr>
                            <td>{{ optional($itemsStockIn->item)->name }}</td>
                            <td>{{ $itemsStockIn->volume }}</td>
                            <td>{{ optional($itemsStockIn->supplier)->name }}</td>
                            <td>{{ $itemsStockIn->price }}</td>
                            <td>{{ $itemsStockIn->amount_price }}</td>
                            <td>{{ optional($itemsStockIn->user)->id }}</td>
                            <td>{{ $itemsStockIn->reference_no }}</td>

                            <td>

                                <form method="POST" action="{!! route('items_stock_ins.items_stock_in.destroy', $itemsStockIn->id) !!}" accept-charset="UTF-8">
                                <input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}

                                    <div class="btn-group btn-group-xs pull-right" role="group">
                                        <a href="{{ route('items_stock_ins.items_stock_in.show', $itemsStockIn->id ) }}" class="btn btn-info" title="Show Items Stock In">
                                            <span class="fa fa-search" aria-hidden="true"></span>
                                        </a>
                                        <a href="{{ route('items_stock_ins.items_stock_in.edit', $itemsStockIn->id ) }}" class="btn btn-primary" title="Edit Items Stock In">
                                            <span class="fa fa-pencil" aria-hidden="true"></span>
                                        </a>

                                        <button type="submit" class="btn btn-danger" title="Delete Items Stock In" onclick="return confirm(&quot;Delete Items Stock In?&quot;)">
                                            <span class="fa fa-trash" aria-hidden="true"></span>
                                        </button>
                                    </div>

                                </form>
                                
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            {!! $itemsStockIns->render() !!}
        </div>
        
        @endif
    
    </div>
@endsection