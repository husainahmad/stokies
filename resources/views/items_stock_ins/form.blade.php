
<div class="form-group {{ $errors->has('items_id') ? 'has-error' : '' }}">
    <label for="items_id" class="col-md-2 control-label">Items</label>
    <div class="col-md-10">
        <select class="form-control" id="items_id" name="items_id" required="true">
        	    <option value="" style="display: none;" {{ old('items_id', optional($itemsStockIn)->items_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select items</option>
        	@foreach ($items as $key => $item)
			    <option value="{{ $key }}" {{ old('items_id', optional($itemsStockIn)->items_id) == $key ? 'selected' : '' }}>
			    	{{ $item }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('items_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('volume') ? 'has-error' : '' }}">
    <label for="volume" class="col-md-2 control-label">Volume</label>
    <div class="col-md-10">
        <input class="form-control" name="volume" type="number" id="volume" value="{{ old('volume', optional($itemsStockIn)->volume) }}" min="-2147483648" max="2147483647" required="true" placeholder="Enter volume here...">
        {!! $errors->first('volume', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('suppliers_id') ? 'has-error' : '' }}">
    <label for="suppliers_id" class="col-md-2 control-label">Suppliers</label>
    <div class="col-md-10">
        <select class="form-control" id="suppliers_id" name="suppliers_id" required="true">
        	    <option value="" style="display: none;" {{ old('suppliers_id', optional($itemsStockIn)->suppliers_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select suppliers</option>
        	@foreach ($suppliers as $key => $supplier)
			    <option value="{{ $key }}" {{ old('suppliers_id', optional($itemsStockIn)->suppliers_id) == $key ? 'selected' : '' }}>
			    	{{ $supplier }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('suppliers_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('price') ? 'has-error' : '' }}">
    <label for="price" class="col-md-2 control-label">Price</label>
    <div class="col-md-10">
        <input class="form-control" name="price" type="number" id="price" value="{{ old('price', optional($itemsStockIn)->price) }}" min="-9" max="9" required="true" placeholder="Enter price here...">
        {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('amount_price') ? 'has-error' : '' }}">
    <label for="amount_price" class="col-md-2 control-label">Amount Price</label>
    <div class="col-md-10">
        <input class="form-control" name="amount_price" type="number" id="amount_price" value="{{ old('amount_price', optional($itemsStockIn)->amount_price) }}" min="-9" max="9" required="true" placeholder="Enter amount price here...">
        {!! $errors->first('amount_price', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('users_id') ? 'has-error' : '' }}">
    <label for="users_id" class="col-md-2 control-label">Users</label>
    <div class="col-md-10">
        <select class="form-control" id="users_id" name="users_id" required="true">
        	    <option value="" style="display: none;" {{ old('users_id', optional($itemsStockIn)->users_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select users</option>
        	@foreach ($users as $key => $user)
			    <option value="{{ $key }}" {{ old('users_id', optional($itemsStockIn)->users_id) == $key ? 'selected' : '' }}>
			    	{{ $user }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('users_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('reference_no') ? 'has-error' : '' }}">
    <label for="reference_no" class="col-md-2 control-label">Reference No</label>
    <div class="col-md-10">
        <input class="form-control" name="reference_no" type="number" id="reference_no" value="{{ old('reference_no', optional($itemsStockIn)->reference_no) }}" min="-2147483648" max="2147483647" required="true" placeholder="Enter reference no here...">
        {!! $errors->first('reference_no', '<p class="help-block">:message</p>') !!}
    </div>
</div>

