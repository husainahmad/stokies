@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Items Stock In' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('items_stock_ins.items_stock_in.destroy', $itemsStockIn->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('items_stock_ins.items_stock_in.index') }}" class="btn btn-primary" title="Show All Items Stock In">
                        <span class="fa fa-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('items_stock_ins.items_stock_in.create') }}" class="btn btn-success" title="Create New Items Stock In">
                        <span class="fa fa-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('items_stock_ins.items_stock_in.edit', $itemsStockIn->id ) }}" class="btn btn-primary" title="Edit Items Stock In">
                        <span class="fa fa-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Items Stock In" onclick="return confirm(&quot;Delete Items Stock In??&quot;)">
                        <span class="fa fa-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Items</dt>
            <dd>{{ optional($itemsStockIn->item)->name }}</dd>
            <dt>Volume</dt>
            <dd>{{ $itemsStockIn->volume }}</dd>
            <dt>Suppliers</dt>
            <dd>{{ optional($itemsStockIn->supplier)->name }}</dd>
            <dt>Price</dt>
            <dd>{{ $itemsStockIn->price }}</dd>
            <dt>Amount Price</dt>
            <dd>{{ $itemsStockIn->amount_price }}</dd>
            <dt>Created At</dt>
            <dd>{{ $itemsStockIn->created_at }}</dd>
            <dt>Updated At</dt>
            <dd>{{ $itemsStockIn->updated_at }}</dd>
            <dt>Users</dt>
            <dd>{{ optional($itemsStockIn->user)->id }}</dd>
            <dt>Reference No</dt>
            <dd>{{ $itemsStockIn->reference_no }}</dd>

        </dl>

    </div>
</div>

@endsection