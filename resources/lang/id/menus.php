<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menus Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'access' => [
            'title' => 'Manajemen Akses',

            'roles' => [
                'all'        => 'Semua Peran',
                'create'     => 'Buat Peran',
                'edit'       => 'Edit Peran',
                'management' => 'Manajemen Peran',
                'main'       => 'Peran',
            ],

            'users' => [
                'all'             => 'Semua Pengguna',
                'change-password' => 'Ubah Sandi',
                'create'          => 'Buat Pengguna',
                'deactivated'     => 'Pengguna Dinonaktifkan',
                'deleted'         => 'Pengguna Dihapus',
                'edit'            => 'Edit Pengguna',
                'main'            => 'Pengguna',
                'view'            => 'Lihat Pengguna',
            ],
        ],
        
        'masterdata' => [
            'title'             => 'Data Master',
            'categories'        => [
                'index'         => 'Kategori',
                'create'        => 'Buat',
                'edit'          => 'Ubah',
                'show'          => 'Liat'
            ],
            'units'            => [
                'index'        => 'Satuan',
                'create'       => 'Buat',
                'edit'         => 'Ubah',
                'show'         => 'Liat'
            ],
            'customers'        => [
                'index'        => 'Pelanggan',
                'create'       => 'Buat',
                'edit'         => 'Ubah',
                'show'         => 'Liat'
            ],
            'suppliers'         => [
                'index'        => 'Pemasok',
                'create'       => 'Buat',
                'edit'         => 'Ubah',
                'show'         => 'Liat'
            ],
            'termbilling'      => [
                'index'        => 'Term Pembayaran',
                'create'       => 'Buat',
                'edit'         => 'Ubah',
                'show'         => 'Liat'
            ],
            'prices' => [
                'index'        => 'Harga',
                'create'       => 'Buat',
                'edit'         => 'Ubah',
                'show'         => 'Liat'
            ],
            'sales' => [
                'index'             => 'Penjualan',
                'create'       => 'Buat',
                'edit'         => 'Ubah',
                'show'         => 'Liat'
            ],
            'sales-persons' => [
                'index'             => 'Pramuniaga',
                'create'       => 'Buat',
                'edit'         => 'Ubah',
                'show'         => 'Liat'
            ],
            'sales-orders' => [
                'index'             => 'Pesanan Penjulan',
                'create'       => 'Buat',
                'edit'         => 'Ubah',
                'show'         => 'Liat'
            ],
            'stock' => [
                'index'             => 'Persediaan',
                'create'       => 'Buat',
                'edit'         => 'Ubah',
                'show'         => 'Liat'
            ],
            'items' => [
                'index'             => 'Barang',
                'create'       => 'Buat',
                'edit'         => 'Ubah',
                'show'         => 'Liat'
            ],
            'items_stocks' => [
                'index'             => 'Item stock',
                'create'       => 'Create',
                'edit'         => 'Edit',
                'show'         => 'Show'
            ],
            'order'    => [
                'index' => 'Pesanan'
            ],
            'purchase' => [
                'index'             => 'Pesanan Pembelian',
                'create'       => 'Buat',
                'edit'         => 'Ubah',
                'show'         => 'Liat'
            ],
            'purchase_invoice' => [
                'index'             => 'Faktur Masuk',
                'create'       => 'Buat',
                'edit'         => 'Ubah',
                'show'         => 'Liat'
            ],
            'incoming_invoices' => [
                'index'             => 'Faktur Masuk',
                'create'       => 'Buat',
                'edit'         => 'Ubah',
                'show'         => 'Liat'
            ],
        ],
        'transactions' => [
            'main'  =>  'Transaksi',
            'delivery_orders' => [
                'title' => 'Surat Jalan',
                'create' => 'Buat',
                'show' => 'Ubah',
                'edit' => 'Liat',
            ],
            'invoices' => [
                'title' => 'Faktur',
                'create' => 'Buat',
                'show' => 'Liat',
                'edit' => 'Ubah',
            ],
            'downpayment' => [
                'title' => 'Uang Muka',
                'create' => 'Create',
                'show' => 'Show',
                'edit' => 'Edit',
            ]
        ],
        'reports' => [
            'main'  =>  'Laporan',
            'delivery_orders' => [
                'title' => 'Laporan Surat Jalan',
                'create' => 'Buat',
                'show' => 'Liat',
                'edit' => 'Ubah',
            ],
            'sales_orders' => [
                'title' => 'Laporan Penjulan',
                'create' => 'Buat',
                'show' => 'Liat',
                'edit' => 'Ubah',
            ],
            'invoices' => [
                'title' => 'Laporan Faktur',
                'create' => 'Buat',
                'show' => 'Liat',
                'edit' => 'Ubah',
            ],
            'outstanding' => [
                'title' => 'Laporan Outstanding',
                'create' => 'Buat',
                'show' => 'Liat',
                'edit' => 'Ubah',
            ],
            'purchase' => [
                'title' => 'Laporan Pembelian',
                'create' => 'Buat',
                'show' => 'Liat',
                'edit' => 'Ubah',
            ],
            'incoming_invoice' => [
                'title' => 'Laporan Faktur Masuk',
                'create' => 'Buat',
                'show' => 'Liat',
                'edit' => 'Ubah',
            ]
        ],
        'log-viewer' => [
            'main'      => 'Pengamat Catatan',
            'dashboard' => 'Dasbor',
            'logs'      => 'Catatan',
        ],

        'sidebar' => [
            'dashboard' => 'Dasbor',
            'general'   => 'Umum',
            'system'    => 'Sistem',
        ],
    ],

    'language-picker' => [
        'language' => 'Bahasa',
        /*
         * Add the new language to this array.
         * The key should have the same language code as the folder name.
         * The string should be: 'Language-name-in-your-own-language (Language-name-in-English)'.
         * Be sure to add the new language in alphabetical order.
         */
        'langs' => [
            'en'    => 'Inggris (English)',
            'id'    => 'Bahasa Indonesia (Indonesian)',
            
        ],
    ],
];
