<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menus Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'access' => [
            'title' => 'Access Management',

            'roles' => [
                'all'        => 'All Roles',
                'create'     => 'Create Role',
                'edit'       => 'Edit Role',
                'management' => 'Role Management',
                'main'       => 'Roles',
            ],

            'users' => [
                'all'             => 'All Users',
                'change-password' => 'Change Password',
                'create'          => 'Create User',
                'deactivated'     => 'Deactivated Users',
                'deleted'         => 'Deleted Users',
                'edit'            => 'Edit User',
                'main'            => 'Users',
                'view'            => 'View User',
            ],
        ],

        'masterdata' => [
            'title'             => 'Master Data',
            'categories'        => [
                'index'         => 'Categories',
                'create'        => 'Create',
                'edit'          => 'Edit',
                'show'          => 'Show'
            ],
            'units'            => [
                'index'        => 'Units',
                'create'       => 'Create',
                'edit'         => 'Edit',
                'show'         => 'Show'
            ],
            'customers'        => [
                'index'        => 'Customers',
                'create'       => 'Create',
                'edit'         => 'Edit',
                'show'         => 'Show'
            ],
            'suppliers'         => [
                'index'        => 'Suppliers',
                'create'       => 'Create',
                'edit'         => 'Edit',
                'show'         => 'Show'
            ],
            'termbilling'      => [
                'index'        => 'Term Of Payment',
                'create'       => 'Create',
                'edit'         => 'Edit',
                'show'         => 'Show'
            ],
            'prices' => [
                'index'        => 'Prices',
                'create'       => 'Create',
                'edit'         => 'Edit',
                'show'         => 'Show'
            ],
            'sales' => [
                'index'             => 'Sales',
                'create'       => 'Create',
                'edit'         => 'Edit',
                'show'         => 'Show'
            ],
            'sales-persons' => [
                'index'             => 'Sales Person',
                'create'       => 'Create',
                'edit'         => 'Edit',
                'show'         => 'Show'
            ],
            'sales-orders' => [
                'index'             => 'Sales Orders',
                'create'       => 'Create',
                'edit'         => 'Edit',
                'show'         => 'Show'
            ],
            'stock' => [
                'index'             => 'Stock',
                'create'       => 'Create',
                'edit'         => 'Edit',
                'show'         => 'Show'
            ],
            'items' => [
                'index'             => 'Items',
                'create'       => 'Create',
                'edit'         => 'Edit',
                'show'         => 'Show'
            ],
            'items_stocks' => [
                'index'             => 'Item stock',
                'create'       => 'Create',
                'edit'         => 'Edit',
                'show'         => 'Show'
            ],
            'order'    => [
                'index' => 'Orders'
            ],
            'purchase' => [
                'index'             => 'Purchase Order',
                'create'       => 'Create',
                'edit'         => 'Edit',
                'show'         => 'Show'
            ],
            'purchase_invoice' => [
                'index'             => 'Incoming Invoice',
                'create'       => 'Create',
                'edit'         => 'Edit',
                'show'         => 'Show'
            ],
            'incoming_invoices' => [
                'index'             => 'Incoming Invoice',
                'create'       => 'Create',
                'edit'         => 'Edit',
                'show'         => 'Show'
            ],
        ],
        'transactions' => [
            'main'  =>  'Transactions',
            'delivery_orders' => [
                'title' => 'Delivery Order',
                'create' => 'Create',
                'show' => 'Show',
                'edit' => 'Edit',
            ],
            'invoices' => [
                'title' => 'Invoices',
                'create' => 'Create',
                'show' => 'Show',
                'edit' => 'Edit',
            ],
            'downpayment' => [
                'title' => 'Down Payment',
                'create' => 'Create',
                'show' => 'Show',
                'edit' => 'Edit',
            ]
        ],
        'reports' => [
            'main'  =>  'Reports',
            'delivery_orders' => [
                'title' => 'Delivery Order Report',
                'create' => 'Create',
                'show' => 'Show',
                'edit' => 'Edit',
            ],
            'sales_orders' => [
                'title' => 'Sales Order Report',
                'create' => 'Create',
                'show' => 'Show',
                'edit' => 'Edit',
            ],
            'invoices' => [
                'title' => 'Invoice Report',
                'create' => 'Create',
                'show' => 'Show',
                'edit' => 'Edit',
            ],
            'outstanding' => [
                'title' => 'Outstanding Report',
                'create' => 'Create',
                'show' => 'Show',
                'edit' => 'Edit',
            ],
            'purchase' => [
                'title' => 'Purchase Report',
                'create' => 'Create',
                'show' => 'Show',
                'edit' => 'Edit',
            ],
            'incoming_invoice' => [
                'title' => 'Incoming Invoice',
                'create' => 'Buat',
                'show' => 'Liat',
                'edit' => 'Ubah',
            ]
        ],
        'log-viewer' => [
            'main'      => 'Log Viewer',
            'dashboard' => 'Dashboard',
            'logs'      => 'Logs',
        ],

        'sidebar' => [
            'dashboard' => 'Dashboard',
            'general'   => 'General',
            'system'    => 'System',
        ],
    ],

    'language-picker' => [
        'language' => 'Language',
        /*
         * Add the new language to this array.
         * The key should have the same language code as the folder name.
         * The string should be: 'Language-name-in-your-own-language (Language-name-in-English)'.
         * Be sure to add the new language in alphabetical order.
         */
        'langs' => [
            
            'en'    => 'English',
            'id'    => 'Indonesian',
            
        ],
    ],
];
