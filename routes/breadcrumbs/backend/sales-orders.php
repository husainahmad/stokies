<?php

Breadcrumbs::register('sales_orders.sales_orders.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('menus.backend.masterdata.sales-orders.index'), url('sales_orders'));
});

Breadcrumbs::register('sales_orders.sales_orders.create', function ($breadcrumbs) {
    $breadcrumbs->parent('sales_orders.sales_orders.index');
    $breadcrumbs->push(__('menus.backend.masterdata.sales-orders.create'), url('sales_orders'));
});

Breadcrumbs::register('sales_orders.sales_orders.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('sales_orders.sales_orders.index');
    $breadcrumbs->push(__('menus.backend.masterdata.sales-orders.edit'), url('sales_orders'));
});

Breadcrumbs::register('sales_orders.sales_orders.show', function ($breadcrumbs) {
    $breadcrumbs->parent('sales_orders.sales_orders.index');
    $breadcrumbs->push(__('menus.backend.masterdata.sales-orders.show'), url('sales_orders'));
});

