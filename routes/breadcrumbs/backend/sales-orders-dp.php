<?php

Breadcrumbs::register('sales_orders_dps.sales_orders_dp.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('menus.backend.transactions.downpayment.title'), url('sales_orders_dps'));
});

Breadcrumbs::register('sales_orders_dps.sales_orders_dp.create', function ($breadcrumbs) {
    $breadcrumbs->parent('sales_orders_dps.sales_orders_dp.index');
    $breadcrumbs->push(__('menus.backend.transactions.downpayment.create'), url('sales_orders_dps'));
});

Breadcrumbs::register('sales_orders_dps.sales_orders_dp.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('sales_orders_dps.sales_orders_dp.index');
    $breadcrumbs->push(__('menus.backend.transactions.downpayment.edit'), url('sales_orders_dps'));
});

Breadcrumbs::register('sales_orders_dps.sales_orders_dp.show', function ($breadcrumbs) {
    $breadcrumbs->parent('sales_orders_dps.sales_orders_dp.index');
    $breadcrumbs->push(__('menus.backend.transactions.downpayment.show'), url('sales_orders_dps'));
});

