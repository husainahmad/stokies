<?php

Breadcrumbs::register('reports.reports.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('menus.backend.reports.main'), url('reports'));
});
Breadcrumbs::register('reports.reports.sales_order', function ($breadcrumbs) {
    $breadcrumbs->parent('reports.reports.index');
    $breadcrumbs->push(__('menus.backend.reports.sales_orders.title'), url('reports'));
});
Breadcrumbs::register('reports.reports.sales_order.result', function ($breadcrumbs) {
    $breadcrumbs->parent('reports.reports.index');
    $breadcrumbs->push(__('menus.backend.reports.sales_orders.title'), url('reports'));
});
Breadcrumbs::register('reports.reports.purchase_order', function ($breadcrumbs) {
    $breadcrumbs->parent('reports.reports.index');
    $breadcrumbs->push(__('menus.backend.reports.purchase.title'), url('reports'));
});
Breadcrumbs::register('reports.reports.purchase_order.result', function ($breadcrumbs) {
    $breadcrumbs->parent('reports.reports.index');
    $breadcrumbs->push(__('menus.backend.reports.purchase.title'), url('reports'));
});
Breadcrumbs::register('reports.reports.delivery_order', function ($breadcrumbs) {
    $breadcrumbs->parent('reports.reports.index');
    $breadcrumbs->push(__('menus.backend.reports.delivery_orders.title'), url('reports'));
});
Breadcrumbs::register('reports.reports.delivery_order.result', function ($breadcrumbs) {
    $breadcrumbs->parent('reports.reports.index');
    $breadcrumbs->push(__('menus.backend.reports.delivery_orders.title'), url('reports'));
});
Breadcrumbs::register('reports.reports.invoice', function ($breadcrumbs) {
    $breadcrumbs->parent('reports.reports.index');
    $breadcrumbs->push(__('menus.backend.reports.invoices.title'), url('reports'));
});
Breadcrumbs::register('reports.reports.invoice.result', function ($breadcrumbs) {
    $breadcrumbs->parent('reports.reports.index');
    $breadcrumbs->push(__('menus.backend.reports.invoices.title'), url('reports'));
});
Breadcrumbs::register('reports.reports.outstanding', function ($breadcrumbs) {
    $breadcrumbs->parent('reports.reports.index');
    $breadcrumbs->push(__('menus.backend.reports.outstanding.title'), url('reports'));
});
Breadcrumbs::register('reports.reports.outstanding.result', function ($breadcrumbs) {
    $breadcrumbs->parent('reports.reports.index');
    $breadcrumbs->push(__('menus.backend.reports.outstanding.title'), url('reports'));
});
Breadcrumbs::register('reports.reports.incoming_invoice', function ($breadcrumbs) {
    $breadcrumbs->parent('reports.reports.index');
    $breadcrumbs->push(__('menus.backend.reports.incoming_invoice.title'), url('reports'));
});
Breadcrumbs::register('reports.reports.incoming_invoice.result', function ($breadcrumbs) {
    $breadcrumbs->parent('reports.reports.index');
    $breadcrumbs->push(__('menus.backend.reports.incoming_invoice.title'), url('reports'));
});

