<?php

Breadcrumbs::register('delivery_orders.delivery_orders.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('menus.backend.transactions.delivery_orders.title'), url('delivery_orders'));
});

Breadcrumbs::register('delivery_orders.delivery_orders.create', function ($breadcrumbs) {
    $breadcrumbs->parent('delivery_orders.delivery_orders.index');
    $breadcrumbs->push(__('menus.backend.transactions.delivery_orders.create'), url('delivery_orders'));
});

Breadcrumbs::register('delivery_orders.delivery_orders.show', function ($breadcrumbs) {
    $breadcrumbs->parent('delivery_orders.delivery_orders.index');
    $breadcrumbs->push(__('menus.backend.transactions.delivery_orders.show'), url('delivery_orders'));
});

Breadcrumbs::register('delivery_orders.delivery_orders.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('delivery_orders.delivery_orders.index');
    $breadcrumbs->push(__('menus.backend.transactions.delivery_orders.edit'), url('delivery_orders'));
});


