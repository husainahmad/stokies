<?php

Breadcrumbs::register('items_stocks.items_stock.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('menus.backend.masterdata.items_stocks.index'), url('items_stocks'));
});

Breadcrumbs::register('items_stocks.items_stock.create', function ($breadcrumbs) {
    $breadcrumbs->parent('items_stocks.items_stock.index');
    $breadcrumbs->push(__('menus.backend.masterdata.items_stocks.create'), url('items_stocks'));
});

Breadcrumbs::register('items_stocks.items_stock.show', function ($breadcrumbs) {
    $breadcrumbs->parent('items_stocks.items_stock.index');
    $breadcrumbs->push(__('menus.backend.masterdata.items_stocks.show'), url('items_stocks'));
});

Breadcrumbs::register('items_stocks.items_stock.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('items_stocks.items_stock.index');
    $breadcrumbs->push(__('menus.backend.masterdata.items_stocks.edit'), url('items_stocks'));
});


