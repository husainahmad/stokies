<?php

Breadcrumbs::register('customers.customers.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('menus.backend.masterdata.customers.index'), url('customers'));
});

Breadcrumbs::register('customers.customers.create', function ($breadcrumbs) {
    $breadcrumbs->parent('customers.customers.index');
    $breadcrumbs->push(__('menus.backend.masterdata.customers.create'), url('customers'));
});

Breadcrumbs::register('customers.customers.show', function ($breadcrumbs) {
    $breadcrumbs->parent('customers.customers.index');
    $breadcrumbs->push(__('menus.backend.masterdata.customers.show'), url('customers'));
});

Breadcrumbs::register('customers.customers.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('customers.customers.index');
    $breadcrumbs->push(__('menus.backend.masterdata.customers.edit'), url('customers'));
});


