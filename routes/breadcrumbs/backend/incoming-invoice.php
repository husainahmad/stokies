<?php

Breadcrumbs::register('incoming_invoices.incoming_invoices.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('menus.backend.masterdata.incoming_invoices.index'), url('incoming_invoices'));
});

Breadcrumbs::register('incoming_invoices.incoming_invoices.create', function ($breadcrumbs) {
    $breadcrumbs->parent('incoming_invoices.incoming_invoices.index');
    $breadcrumbs->push(__('menus.backend.masterdata.incoming_invoices.create'), url('incoming_invoices'));
});

Breadcrumbs::register('incoming_invoices.incoming_invoices.show', function ($breadcrumbs) {
    $breadcrumbs->parent('incoming_invoices.incoming_invoices.index');
    $breadcrumbs->push(__('menus.backend.masterdata.incoming_invoices.show'), url('incoming_invoices'));
});

Breadcrumbs::register('incoming_invoices.incoming_invoices.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('incoming_invoices.incoming_invoices.index');
    $breadcrumbs->push(__('menus.backend.masterdata.incoming_invoices.edit'), url('incoming_invoices'));
});
