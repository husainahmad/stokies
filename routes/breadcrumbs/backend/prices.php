<?php

Breadcrumbs::register('prices.prices.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('menus.backend.masterdata.prices.index'), url('prices'));
});

Breadcrumbs::register('prices.prices.create', function ($breadcrumbs) {
    $breadcrumbs->parent('prices.prices.index');
    $breadcrumbs->push(__('menus.backend.masterdata.prices.create'), url('prices'));
});

Breadcrumbs::register('prices.prices.show', function ($breadcrumbs) {
    $breadcrumbs->parent('prices.prices.index');
    $breadcrumbs->push(__('menus.backend.masterdata.prices.show'), url('prices'));
});

Breadcrumbs::register('prices.prices.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('prices.prices.index');
    $breadcrumbs->push(__('menus.backend.masterdata.prices.edit'), url('prices'));
});

