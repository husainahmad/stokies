<?php

Breadcrumbs::register('purchase_orders.purchase_orders.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('menus.backend.masterdata.purchase.index'), url('purchase_orders'));
});

Breadcrumbs::register('purchase_orders.purchase_orders.create', function ($breadcrumbs) {
    $breadcrumbs->parent('purchase_orders.purchase_orders.index');
    $breadcrumbs->push(__('menus.backend.masterdata.purchase.create'), url('purchase_orders'));
});

Breadcrumbs::register('purchase_orders.purchase_orders.show', function ($breadcrumbs) {
    $breadcrumbs->parent('purchase_orders.purchase_orders.index');
    $breadcrumbs->push(__('menus.backend.masterdata.purchase.show'), url('purchase_orders'));
});

Breadcrumbs::register('purchase_orders.purchase_orders.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('purchase_orders.purchase_orders.index');
    $breadcrumbs->push(__('menus.backend.masterdata.purchase.edit'), url('purchase_orders'));
});
