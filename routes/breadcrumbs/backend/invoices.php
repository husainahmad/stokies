<?php

Breadcrumbs::register('invoices.invoices.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('menus.backend.transactions.invoices.title'), url('invoices'));
});

Breadcrumbs::register('invoices.invoices.create', function ($breadcrumbs) {
    $breadcrumbs->parent('invoices.invoices.index');
    $breadcrumbs->push(__('menus.backend.transactions.invoices.create'), url('invoices'));
});

Breadcrumbs::register('invoices.invoices.show', function ($breadcrumbs) {
    $breadcrumbs->parent('invoices.invoices.index');
    $breadcrumbs->push(__('menus.backend.transactions.invoices.show'), url('invoices'));
});

Breadcrumbs::register('invoices.invoices.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('invoices.invoices.index');
    $breadcrumbs->push(__('menus.backend.transactions.invoices.edit'), url('invoices'));
});


