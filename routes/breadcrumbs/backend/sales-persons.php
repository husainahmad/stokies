<?php

Breadcrumbs::register('sales_persons.sales_persons.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('menus.backend.masterdata.sales-persons.index'), url('sales_persons'));
});

Breadcrumbs::register('sales_persons.sales_persons.create', function ($breadcrumbs) {
    $breadcrumbs->parent('sales_persons.sales_persons.index');
    $breadcrumbs->push(__('menus.backend.masterdata.sales-persons.create'), url('sales_persons'));
});

Breadcrumbs::register('sales_persons.sales_persons.show', function ($breadcrumbs) {
    $breadcrumbs->parent('sales_persons.sales_persons.index');
    $breadcrumbs->push(__('menus.backend.masterdata.sales-persons.show'), url('sales_persons'));
});

Breadcrumbs::register('sales_persons.sales_persons.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('sales_persons.sales_persons.index');
    $breadcrumbs->push(__('menus.backend.masterdata.sales-persons.edit'), url('sales_persons'));
});


