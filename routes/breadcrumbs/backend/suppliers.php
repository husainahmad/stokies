<?php

Breadcrumbs::register('suppliers.suppliers.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('menus.backend.masterdata.suppliers.index'), url('suppliers'));
});

Breadcrumbs::register('suppliers.suppliers.create', function ($breadcrumbs) {
    $breadcrumbs->parent('suppliers.suppliers.index');
    $breadcrumbs->push(__('menus.backend.masterdata.suppliers.create'), url('suppliers'));
});

Breadcrumbs::register('suppliers.suppliers.show', function ($breadcrumbs) {
    $breadcrumbs->parent('suppliers.suppliers.index');
    $breadcrumbs->push(__('menus.backend.masterdata.suppliers.show'), url('suppliers'));
});

Breadcrumbs::register('suppliers.suppliers.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('suppliers.suppliers.index');
    $breadcrumbs->push(__('menus.backend.masterdata.suppliers.edit'), url('suppliers'));
});


