<?php

Breadcrumbs::register('admin.dashboard', function ($breadcrumbs) {
    $breadcrumbs->push(__('strings.backend.dashboard.title'), route('admin.dashboard'));
});

require __DIR__.'/auth.php';
require __DIR__.'/log-viewer.php';
require __DIR__.'/customers.php';
require __DIR__.'/suppliers.php';
require __DIR__.'/categories.php';
require __DIR__.'/units.php';
require __DIR__.'/items.php';
require __DIR__.'/items_stock.php';
require __DIR__.'/prices.php';
require __DIR__.'/sales-persons.php';
require __DIR__.'/sales-orders.php';
require __DIR__.'/term-billing.php';
require __DIR__.'/purchase-orders.php';
require __DIR__.'/delivery-orders.php';
require __DIR__.'/invoices.php';
require __DIR__.'/sales-orders-dp.php';
require __DIR__.'/incoming-invoice.php';
require __DIR__.'/reports.php';