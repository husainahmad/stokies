<?php

Breadcrumbs::register('categories.categories.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('menus.backend.masterdata.categories.index'), url('categories'));
});

Breadcrumbs::register('categories.categories.create', function ($breadcrumbs) {
    $breadcrumbs->parent('categories.categories.index');
    $breadcrumbs->push(__('menus.backend.masterdata.categories.create'), url('categories'));
});

Breadcrumbs::register('categories.categories.show', function ($breadcrumbs) {
    $breadcrumbs->parent('categories.categories.index');
    $breadcrumbs->push(__('menus.backend.masterdata.categories.show'), url('categories'));
});

Breadcrumbs::register('categories.categories.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('categories.categories.index');
    $breadcrumbs->push(__('menus.backend.masterdata.categories.edit'), url('categories'));
});
