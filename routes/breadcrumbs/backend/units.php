<?php

Breadcrumbs::register('units.units.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('menus.backend.masterdata.units.index'), url('units'));
});

Breadcrumbs::register('units.units.create', function ($breadcrumbs) {
    $breadcrumbs->parent('units.units.index');
    $breadcrumbs->push(__('menus.backend.masterdata.units.create'), url('units'));
});

Breadcrumbs::register('units.units.show', function ($breadcrumbs) {
    $breadcrumbs->parent('units.units.index');
    $breadcrumbs->push(__('menus.backend.masterdata.units.show'), url('units'));
});

Breadcrumbs::register('units.units.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('units.units.index');
    $breadcrumbs->push(__('menus.backend.masterdata.units.edit'), url('units'));
});


