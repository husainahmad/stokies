<?php

Breadcrumbs::register('items.items.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('menus.backend.masterdata.items.index'), url('items'));
});

Breadcrumbs::register('items.items.create', function ($breadcrumbs) {
    $breadcrumbs->parent('items.items.index');
    $breadcrumbs->push(__('menus.backend.masterdata.items.create'), url('items'));
});

Breadcrumbs::register('items.items.show', function ($breadcrumbs) {
    $breadcrumbs->parent('items.items.index');
    $breadcrumbs->push(__('menus.backend.masterdata.items.show'), url('items'));
});

Breadcrumbs::register('items.items.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('items.items.index');
    $breadcrumbs->push(__('menus.backend.masterdata.items.edit'), url('items'));
});


