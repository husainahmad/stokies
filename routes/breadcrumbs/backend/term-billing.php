<?php

Breadcrumbs::register('term_billings.term_billings.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('menus.backend.masterdata.termbilling.index'), url('term_billings'));
});

Breadcrumbs::register('term_billings.term_billings.create', function ($breadcrumbs) {
    $breadcrumbs->parent('term_billings.term_billings.index');
    $breadcrumbs->push(__('menus.backend.masterdata.termbilling.create'), url('term_billings'));
});

Breadcrumbs::register('term_billings.term_billings.show', function ($breadcrumbs) {
    $breadcrumbs->parent('term_billings.term_billings.index');
    $breadcrumbs->push(__('menus.backend.masterdata.termbilling.show'), url('term_billings'));
});

Breadcrumbs::register('term_billings.term_billings.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('term_billings.term_billings.index');
    $breadcrumbs->push(__('menus.backend.masterdata.termbilling.edit'), url('term_billings'));
});


