<?php

/**
 * Global Routes
 * Routes that are used between both frontend and backend.
 */

// Switch between the included languages
Route::get('lang/{lang}', 'LanguageController');

/*
 * Frontend Routes
 * Namespaces indicate folder structure
 */
Route::group(['namespace' => 'Frontend', 'as' => 'frontend.'], function () {
    include_route_files(__DIR__.'/frontend/');
});

/*
 * Backend Routes
 * Namespaces indicate folder structure
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
    /*
     * These routes need view-backend permission
     * (good if you want to allow more than one group in the backend,
     * then limit the backend features by different roles or permissions)
     *
     * Note: Administrator has all permissions so you do not have to specify the administrator role everywhere.
     * These routes can not be hit if the password is expired
     */
    include_route_files(__DIR__.'/backend/');
});

Route::group(
[
    'prefix' => 'categories',
], function () {

    Route::get('/', 'CategoriesController@index')
         ->name('categories.categories.index');

    Route::get('/create','CategoriesController@create')
         ->name('categories.categories.create');

    Route::get('/show/{categories}','CategoriesController@show')
         ->name('categories.categories.show')
         ->where('id', '[0-9]+');

    Route::get('/{categories}/edit','CategoriesController@edit')
         ->name('categories.categories.edit')
         ->where('id', '[0-9]+');

    Route::post('/', 'CategoriesController@store')
         ->name('categories.categories.store');
               
    Route::put('categories/{categories}', 'CategoriesController@update')
         ->name('categories.categories.update')
         ->where('id', '[0-9]+');

    Route::delete('/categories/{categories}','CategoriesController@destroy')
         ->name('categories.categories.destroy')
         ->where('id', '[0-9]+');

});

Route::group(
[
    'prefix' => 'customers',
], function () {

    Route::get('/', 'CustomersController@index')
         ->name('customers.customers.index');

    Route::get('/create','CustomersController@create')
         ->name('customers.customers.create');

    Route::get('/show/{customers}','CustomersController@show')
         ->name('customers.customers.show')
         ->where('id', '[0-9]+');

    Route::get('/{customers}/edit','CustomersController@edit')
         ->name('customers.customers.edit')
         ->where('id', '[0-9]+');

    Route::post('/', 'CustomersController@store')
         ->name('customers.customers.store');
               
    Route::put('customers/{customers}', 'CustomersController@update')
         ->name('customers.customers.update')
         ->where('id', '[0-9]+');

    Route::delete('/customers/{customers}','CustomersController@destroy')
         ->name('customers.customers.destroy')
         ->where('id', '[0-9]+');

});

Route::group(
[
    'prefix' => 'items',
], function () {

    Route::get('/', 'ItemsController@index')
         ->name('items.items.index');

    Route::get('/create','ItemsController@create')
         ->name('items.items.create');

    Route::get('/show/{items}','ItemsController@show')
         ->name('items.items.show')
         ->where('id', '[0-9]+');
    
    Route::get('/show/json/{items}','ItemsController@json')
         ->name('items.items.show.json')
         ->where('id', '[0-9]+');
    
    Route::get('/{items}/edit','ItemsController@edit')
         ->name('items.items.edit')
         ->where('id', '[0-9]+');
    
    Route::post('/', 'ItemsController@store')
         ->name('items.items.store');
               
    Route::put('items/{items}', 'ItemsController@update')
         ->name('items.items.update')
         ->where('id', '[0-9]+');

    Route::delete('/items/{items}','ItemsController@destroy')
         ->name('items.items.destroy')
         ->where('id', '[0-9]+');

});

Route::group(
[
    'prefix' => 'prices',
], function () {

    Route::get('/', 'PricesController@index')
         ->name('prices.prices.index');

    Route::get('/create','PricesController@create')
         ->name('prices.prices.create');

    Route::get('/show/{prices}','PricesController@show')
         ->name('prices.prices.show')
         ->where('id', '[0-9]+');

    Route::get('/{prices}/edit','PricesController@edit')
         ->name('prices.prices.edit')
         ->where('id', '[0-9]+');

    Route::post('/', 'PricesController@store')
         ->name('prices.prices.store');
               
    Route::put('prices/{prices}', 'PricesController@update')
         ->name('prices.prices.update')
         ->where('id', '[0-9]+');

    Route::delete('/prices/{prices}','PricesController@destroy')
         ->name('prices.prices.destroy')
         ->where('id', '[0-9]+');
    

});

Route::group(
[
    'prefix' => 'suppliers',
], function () {

    Route::get('/', 'SuppliersController@index')
         ->name('suppliers.suppliers.index');

    Route::get('/create','SuppliersController@create')
         ->name('suppliers.suppliers.create');

    Route::get('/show/{suppliers}','SuppliersController@show')
         ->name('suppliers.suppliers.show')
         ->where('id', '[0-9]+');

    Route::get('/{suppliers}/edit','SuppliersController@edit')
         ->name('suppliers.suppliers.edit')
         ->where('id', '[0-9]+');

    Route::post('/', 'SuppliersController@store')
         ->name('suppliers.suppliers.store');
               
    Route::put('suppliers/{suppliers}', 'SuppliersController@update')
         ->name('suppliers.suppliers.update')
         ->where('id', '[0-9]+');

    Route::delete('/suppliers/{suppliers}','SuppliersController@destroy')
         ->name('suppliers.suppliers.destroy')
         ->where('id', '[0-9]+');

});

Route::group(
[
    'prefix' => 'sales_orders',
], function () {

    Route::get('/', 'SalesOrdersController@index')
         ->name('sales_orders.sales_orders.index');

    Route::get('/create','SalesOrdersController@create')
         ->name('sales_orders.sales_orders.create');

    Route::get('/show/{salesOrders}','SalesOrdersController@show')
         ->name('sales_orders.sales_orders.show')
         ->where('id', '[0-9]+');

    Route::get('/print/{salesOrders}','SalesOrdersController@pdf')
         ->name('sales_orders.sales_orders.print')
         ->where('id', '[0-9]+');
    
    Route::get('/{salesOrders}/edit','SalesOrdersController@edit')
         ->name('sales_orders.sales_orders.edit')
         ->where('id', '[0-9]+');

    Route::get('/find','SalesOrdersController@find')
         ->name('sales_orders.sales_orders.find');
    
    Route::post('/', 'SalesOrdersController@store')
         ->name('sales_orders.sales_orders.store');
               
    Route::put('sales_orders/{salesOrders}', 'SalesOrdersController@update')
         ->name('sales_orders.sales_orders.update')
         ->where('id', '[0-9]+');

    Route::delete('/sales_orders/{salesOrders}','SalesOrdersController@destroy')
         ->name('sales_orders.sales_orders.destroy')
         ->where('id', '[0-9]+');
    
    Route::put('/complete/{salesOrders}','SalesOrdersController@complete')
         ->name('sales_orders.sales_orders.complete')
         ->where('id', '[0-9]+');
    
    Route::put('/uncomplete/{salesOrders}','SalesOrdersController@uncomplete')
         ->name('sales_orders.sales_orders.uncomplete')
         ->where('id', '[0-9]+');
   
});

Route::group(
[
    'prefix' => 'sales_persons',
], function () {

    Route::get('/', 'SalesPersonsController@index')
         ->name('sales_persons.sales_persons.index');

    Route::get('/create','SalesPersonsController@create')
         ->name('sales_persons.sales_persons.create');

    Route::get('/show/{salesPersons}','SalesPersonsController@show')
         ->name('sales_persons.sales_persons.show')
         ->where('id', '[0-9]+');

    Route::get('/{salesPersons}/edit','SalesPersonsController@edit')
         ->name('sales_persons.sales_persons.edit')
         ->where('id', '[0-9]+');

    Route::post('/', 'SalesPersonsController@store')
         ->name('sales_persons.sales_persons.store');
               
    Route::put('sales_persons/{salesPersons}', 'SalesPersonsController@update')
         ->name('sales_persons.sales_persons.update')
         ->where('id', '[0-9]+');

    Route::delete('/sales_persons/{salesPersons}','SalesPersonsController@destroy')
         ->name('sales_persons.sales_persons.destroy')
         ->where('id', '[0-9]+');

});

Route::group(
[
    'prefix' => 'units',
], function () {

    Route::get('/', 'UnitsController@index')
         ->name('units.units.index');

    Route::get('/create','UnitsController@create')
         ->name('units.units.create');

    Route::get('/show/{units}','UnitsController@show')
         ->name('units.units.show')
         ->where('id', '[0-9]+');

    Route::get('/{units}/edit','UnitsController@edit')
         ->name('units.units.edit')
         ->where('id', '[0-9]+');

    Route::post('/', 'UnitsController@store')
         ->name('units.units.store');
               
    Route::put('units/{units}', 'UnitsController@update')
         ->name('units.units.update')
         ->where('id', '[0-9]+');

    Route::delete('/units/{units}','UnitsController@destroy')
         ->name('units.units.destroy')
         ->where('id', '[0-9]+');

});

Route::group(
[
    'prefix' => 'term_billings',
], function () {

    Route::get('/', 'TermBillingsController@index')
         ->name('term_billings.term_billings.index');

    Route::get('/create','TermBillingsController@create')
         ->name('term_billings.term_billings.create');

    Route::get('/show/{termBillings}','TermBillingsController@show')
         ->name('term_billings.term_billings.show')
         ->where('id', '[0-9]+');

    Route::get('/{termBillings}/edit','TermBillingsController@edit')
         ->name('term_billings.term_billings.edit')
         ->where('id', '[0-9]+');

    Route::post('/', 'TermBillingsController@store')
         ->name('term_billings.term_billings.store');
               
    Route::put('term_billings/{termBillings}', 'TermBillingsController@update')
         ->name('term_billings.term_billings.update')
         ->where('id', '[0-9]+');

    Route::delete('/term_billings/{termBillings}','TermBillingsController@destroy')
         ->name('term_billings.term_billings.destroy')
         ->where('id', '[0-9]+');

});

Route::group(
[
    'prefix' => 'items_stocks',
], function () {

    Route::get('/', 'ItemsStocksController@index')
         ->name('items_stocks.items_stock.index');

    Route::get('/create','ItemsStocksController@create')
         ->name('items_stocks.items_stock.create');

    Route::get('/show/{itemsStock}','ItemsStocksController@show')
         ->name('items_stocks.items_stock.show')
         ->where('id', '[0-9]+');

    Route::get('/{itemsStock}/edit','ItemsStocksController@edit')
         ->name('items_stocks.items_stock.edit')
         ->where('id', '[0-9]+');

    Route::post('/', 'ItemsStocksController@store')
         ->name('items_stocks.items_stock.store');
               
    Route::put('items_stock/{itemsStock}', 'ItemsStocksController@update')
         ->name('items_stocks.items_stock.update')
         ->where('id', '[0-9]+');

    Route::delete('/items_stock/{itemsStock}','ItemsStocksController@destroy')
         ->name('items_stocks.items_stock.destroy')
         ->where('id', '[0-9]+');

});

Route::group(
[
    'prefix' => 'items_stock_ins',
], function () {

    Route::get('/', 'ItemsStockInsController@index')
         ->name('items_stock_ins.items_stock_in.index');

    Route::get('/create','ItemsStockInsController@create')
         ->name('items_stock_ins.items_stock_in.create');

    Route::get('/show/{itemsStockIn}','ItemsStockInsController@show')
         ->name('items_stock_ins.items_stock_in.show')
         ->where('id', '[0-9]+');

    Route::get('/{itemsStockIn}/edit','ItemsStockInsController@edit')
         ->name('items_stock_ins.items_stock_in.edit')
         ->where('id', '[0-9]+');

    Route::post('/', 'ItemsStockInsController@store')
         ->name('items_stock_ins.items_stock_in.store');
               
    Route::put('items_stock_in/{itemsStockIn}', 'ItemsStockInsController@update')
         ->name('items_stock_ins.items_stock_in.update')
         ->where('id', '[0-9]+');

    Route::delete('/items_stock_in/{itemsStockIn}','ItemsStockInsController@destroy')
         ->name('items_stock_ins.items_stock_in.destroy')
         ->where('id', '[0-9]+');

});

Route::group(
[
    'prefix' => 'items_stock_outs',
], function () {

    Route::get('/', 'ItemsStockOutsController@index')
         ->name('items_stock_outs.items_stock_out.index');

    Route::get('/create','ItemsStockOutsController@create')
         ->name('items_stock_outs.items_stock_out.create');

    Route::get('/show/{itemsStockOut}','ItemsStockOutsController@show')
         ->name('items_stock_outs.items_stock_out.show')
         ->where('id', '[0-9]+');

    Route::get('/{itemsStockOut}/edit','ItemsStockOutsController@edit')
         ->name('items_stock_outs.items_stock_out.edit')
         ->where('id', '[0-9]+');

    Route::post('/', 'ItemsStockOutsController@store')
         ->name('items_stock_outs.items_stock_out.store');
               
    Route::put('items_stock_out/{itemsStockOut}', 'ItemsStockOutsController@update')
         ->name('items_stock_outs.items_stock_out.update')
         ->where('id', '[0-9]+');

    Route::delete('/items_stock_out/{itemsStockOut}','ItemsStockOutsController@destroy')
         ->name('items_stock_outs.items_stock_out.destroy')
         ->where('id', '[0-9]+');

});

Route::group(
[
    'prefix' => 'sales_orders_details',
], function () {

    Route::get('/', 'SalesOrdersDetailsController@index')
         ->name('sales_orders_details.sales_orders_details.index');

    Route::get('/create','SalesOrdersDetailsController@create')
         ->name('sales_orders_details.sales_orders_details.create');

    Route::get('/show/{salesOrdersDetails}','SalesOrdersDetailsController@show')
         ->name('sales_orders_details.sales_orders_details.show')
         ->where('id', '[0-9]+');

    Route::get('/show/json/{salesOrdersDetails}','SalesOrdersDetailsController@json')
         ->name('sales_orders_details.sales_orders_details.show.json')
         ->where('id', '[0-9]+');
    
    Route::get('/{salesOrdersDetails}/edit','SalesOrdersDetailsController@edit')
         ->name('sales_orders_details.sales_orders_details.edit')
         ->where('id', '[0-9]+');

    Route::post('/', 'SalesOrdersDetailsController@store')
         ->name('sales_orders_details.sales_orders_details.store');
               
    Route::put('sales_orders_details/{salesOrdersDetails}', 'SalesOrdersDetailsController@update')
         ->name('sales_orders_details.sales_orders_details.update')
         ->where('id', '[0-9]+');

    Route::delete('/sales_orders_details/{salesOrdersDetails}','SalesOrdersDetailsController@destroy')
         ->name('sales_orders_details.sales_orders_details.destroy')
         ->where('id', '[0-9]+');

});

Route::group(
[
    'prefix' => 'purchase_orders',
], function () {

    Route::get('/', 'PurchaseOrdersController@index')
         ->name('purchase_orders.purchase_orders.index');

    Route::get('/create','PurchaseOrdersController@create')
         ->name('purchase_orders.purchase_orders.create');

    Route::get('/show/{purchaseOrders}','PurchaseOrdersController@show')
         ->name('purchase_orders.purchase_orders.show')
         ->where('id', '[0-9]+');
   
    Route::get('/show/json/{purchaseOrders}','PurchaseOrdersController@json')
         ->name('purchase_orders.purchase_orders.show.json')
         ->where('id', '[0-9]+');
    
    Route::get('/print/{purchaseOrders}','PurchaseOrdersController@pdf')
         ->name('purchase_orders.purchase_orders.print')
         ->where('id', '[0-9]+');
    
    Route::get('/{purchaseOrders}/edit','PurchaseOrdersController@edit')
         ->name('purchase_orders.purchase_orders.edit')
         ->where('id', '[0-9]+');
    
    Route::get('/find','PurchaseOrdersController@find')
         ->name('purchase_orders.purchase_orders.find');
    
    Route::post('/', 'PurchaseOrdersController@store')
         ->name('purchase_orders.purchase_orders.store');
               
    Route::put('purchase_orders/{purchaseOrders}', 'PurchaseOrdersController@update')
         ->name('purchase_orders.purchase_orders.update')
         ->where('id', '[0-9]+');

    Route::put('/complete/{purchaseOrders}','PurchaseOrdersController@complete')
         ->name('purchase_orders.purchase_orders.complete')
         ->where('id', '[0-9]+');
    
    Route::put('/uncomplete/{purchaseOrders}','PurchaseOrdersController@uncomplete')
         ->name('purchase_orders.purchase_orders.uncomplete')
         ->where('id', '[0-9]+');
    
    
    Route::delete('/purchase_orders/{purchaseOrders}','PurchaseOrdersController@destroy')
         ->name('purchase_orders.purchase_orders.destroy')
         ->where('id', '[0-9]+');

});

Route::group(
[
    'prefix' => 'purchase_orders_details',
], function () {

    Route::get('/', 'PurchaseOrdersDetailsController@index')
         ->name('purchase_orders_details.purchase_orders_details.index');

    Route::get('/create','PurchaseOrdersDetailsController@create')
         ->name('purchase_orders_details.purchase_orders_details.create');

    Route::get('/show/{purchaseOrdersDetails}','PurchaseOrdersDetailsController@show')
         ->name('purchase_orders_details.purchase_orders_details.show')
         ->where('id', '[0-9]+');
    
    Route::get('/print/{purchaseOrdersDetails}','PurchaseOrdersDetailsController@pdf')
         ->name('purchase_orders_details.purchase_orders_details.print')
         ->where('id', '[0-9]+');
    
    Route::get('/{purchaseOrdersDetails}/edit','PurchaseOrdersDetailsController@edit')
         ->name('purchase_orders_details.purchase_orders_details.edit')
         ->where('id', '[0-9]+');

    Route::post('/', 'PurchaseOrdersDetailsController@store')
         ->name('purchase_orders_details.purchase_orders_details.store');
               
    Route::put('purchase_orders_details/{purchaseOrdersDetails}', 'PurchaseOrdersDetailsController@update')
         ->name('purchase_orders_details.purchase_orders_details.update')
         ->where('id', '[0-9]+');

    Route::delete('/purchase_orders_details/{purchaseOrdersDetails}','PurchaseOrdersDetailsController@destroy')
         ->name('purchase_orders_details.purchase_orders_details.destroy')
         ->where('id', '[0-9]+');

});

Route::group(
[
    'prefix' => 'delivery_orders',
], function () {

    Route::get('/', 'DeliveryOrdersController@index')
         ->name('delivery_orders.delivery_orders.index');

    Route::get('/create','DeliveryOrdersController@create')
         ->name('delivery_orders.delivery_orders.create');

    Route::get('/show/{deliveryOrders}','DeliveryOrdersController@show')
         ->name('delivery_orders.delivery_orders.show')
         ->where('id', '[0-9]+');
    
    Route::get('/print/{deliveryOrders}','DeliveryOrdersController@pdf')
         ->name('delivery_orders.delivery_orders.print')
         ->where('id', '[0-9]+');

    Route::get('/{deliveryOrders}/edit','DeliveryOrdersController@edit')
         ->name('delivery_orders.delivery_orders.edit')
         ->where('id', '[0-9]+');

    Route::post('/', 'DeliveryOrdersController@store')
         ->name('delivery_orders.delivery_orders.store');
               
    Route::put('delivery_orders/{deliveryOrders}', 'DeliveryOrdersController@update')
         ->name('delivery_orders.delivery_orders.update')
         ->where('id', '[0-9]+');

    Route::put('/complete/{deliveryOrders}','DeliveryOrdersController@complete')
         ->name('delivery_orders.delivery_orders.complete')
         ->where('id', '[0-9]+');
    
    Route::put('/uncomplete/{deliveryOrders}','DeliveryOrdersController@uncomplete')
         ->name('delivery_orders.delivery_orders.uncomplete')
         ->where('id', '[0-9]+');
    
    Route::get('/find','DeliveryOrdersController@find')
         ->name('delivery_orders.delivery_orders.find');
        
    Route::delete('/delivery_orders/{deliveryOrders}','DeliveryOrdersController@destroy')
         ->name('delivery_orders.delivery_orders.destroy')
         ->where('id', '[0-9]+');

});

Route::group(
[
    'prefix' => 'delivery_orders_details',
], function () {

    Route::get('/', 'DeliveryOrdersDetailsController@index')
         ->name('delivery_orders_details.delivery_orders_details.index');

    Route::get('/create','DeliveryOrdersDetailsController@create')
         ->name('delivery_orders_details.delivery_orders_details.create');

    Route::get('/show/{deliveryOrdersDetails}','DeliveryOrdersDetailsController@show')
         ->name('delivery_orders_details.delivery_orders_details.show')
         ->where('id', '[0-9]+');
    
    Route::get('/show/json/{salesOrdersDetails}','DeliveryOrdersDetailsController@json')
         ->name('delivery_orders_details.delivery_orders_details.show.json')
         ->where('id', '[0-9]+');
    
    Route::get('/print/{deliveryOrdersDetails}','DeliveryOrdersDetailsController@pdf')
         ->name('delivery_orders_details.delivery_orders_details.print')
         ->where('id', '[0-9]+');
    
    Route::get('/{deliveryOrdersDetails}/edit','DeliveryOrdersDetailsController@edit')
         ->name('delivery_orders_details.delivery_orders_details.edit')
         ->where('id', '[0-9]+');

    Route::post('/', 'DeliveryOrdersDetailsController@store')
         ->name('delivery_orders_details.delivery_orders_details.store');
               
    Route::put('delivery_orders_details/{deliveryOrdersDetails}', 'DeliveryOrdersDetailsController@update')
         ->name('delivery_orders_details.delivery_orders_details.update')
         ->where('id', '[0-9]+');

    Route::delete('/delivery_orders_details/{deliveryOrdersDetails}','DeliveryOrdersDetailsController@destroy')
         ->name('delivery_orders_details.delivery_orders_details.destroy')
         ->where('id', '[0-9]+');

});

Route::group(
[
    'prefix' => 'invoices',
], function () {

    Route::get('/', 'InvoicesController@index')
         ->name('invoices.invoices.index');

    Route::get('/create','InvoicesController@create')
         ->name('invoices.invoices.create');

    Route::get('/show/{invoices}','InvoicesController@show')
         ->name('invoices.invoices.show')
         ->where('id', '[0-9]+');
    
    Route::get('/print/{invoices}','InvoicesController@pdf')
         ->name('invoices.invoices.print')
         ->where('id', '[0-9]+');

    Route::get('/{invoices}/edit','InvoicesController@edit')
         ->name('invoices.invoices.edit')
         ->where('id', '[0-9]+');

    Route::get('/find','InvoicesController@find')
         ->name('invoices.invoices.find');
        
    Route::post('/', 'InvoicesController@store')
         ->name('invoices.invoices.store');
               
    Route::put('invoices/{invoices}', 'InvoicesController@update')
         ->name('invoices.invoices.update')
         ->where('id', '[0-9]+');

    Route::delete('/invoices/{invoices}','InvoicesController@destroy')
         ->name('invoices.invoices.destroy')
         ->where('id', '[0-9]+');

});

Route::group(
[
    'prefix' => 'invoices_details',
], function () {

    Route::get('/', 'InvoicesDetailsController@index')
         ->name('invoices_details.invoices_details.index');

    Route::get('/create','InvoicesDetailsController@create')
         ->name('invoices_details.invoices_details.create');

    Route::get('/show/{invoicesDetails}','InvoicesDetailsController@show')
         ->name('invoices_details.invoices_details.show')
         ->where('id', '[0-9]+');

    Route::get('/{invoicesDetails}/edit','InvoicesDetailsController@edit')
         ->name('invoices_details.invoices_details.edit')
         ->where('id', '[0-9]+');

    Route::post('/', 'InvoicesDetailsController@store')
         ->name('invoices_details.invoices_details.store');
               
    Route::put('invoices_details/{invoicesDetails}', 'InvoicesDetailsController@update')
         ->name('invoices_details.invoices_details.update')
         ->where('id', '[0-9]+');

    Route::delete('/invoices_details/{invoicesDetails}','InvoicesDetailsController@destroy')
         ->name('invoices_details.invoices_details.destroy')
         ->where('id', '[0-9]+');

});

Route::group(
[
    'prefix' => 'invoice_delivery_orders',
], function () {

    Route::get('/', 'InvoiceDeliveryOrdersController@index')
         ->name('invoice_delivery_orders.invoice_delivery_orders.index');

    Route::get('/create','InvoiceDeliveryOrdersController@create')
         ->name('invoice_delivery_orders.invoice_delivery_orders.create');

    Route::get('/show/{invoiceDeliveryOrders}','InvoiceDeliveryOrdersController@show')
         ->name('invoice_delivery_orders.invoice_delivery_orders.show')
         ->where('id', '[0-9]+');
    
    Route::get('/{invoiceDeliveryOrders}/edit','InvoiceDeliveryOrdersController@edit')
         ->name('invoice_delivery_orders.invoice_delivery_orders.edit')
         ->where('id', '[0-9]+');

    Route::post('/', 'InvoiceDeliveryOrdersController@store')
         ->name('invoice_delivery_orders.invoice_delivery_orders.store');
               
    Route::put('invoice_delivery_orders/{invoiceDeliveryOrders}', 'InvoiceDeliveryOrdersController@update')
         ->name('invoice_delivery_orders.invoice_delivery_orders.update')
         ->where('id', '[0-9]+');

    Route::delete('/invoice_delivery_orders/{invoiceDeliveryOrders}','InvoiceDeliveryOrdersController@destroy')
         ->name('invoice_delivery_orders.invoice_delivery_orders.destroy')
         ->where('id', '[0-9]+');

});

Route::group(
[
    'prefix' => 'sales_orders_dps',
], function () {

    Route::get('/', 'SalesOrdersDpsController@index')
         ->name('sales_orders_dps.sales_orders_dp.index');

    Route::get('/create','SalesOrdersDpsController@create')
         ->name('sales_orders_dps.sales_orders_dp.create');

    Route::get('/show/{salesOrdersDp}','SalesOrdersDpsController@show')
         ->name('sales_orders_dps.sales_orders_dp.show')
         ->where('id', '[0-9]+');
    
    Route::get('/print/{salesOrdersDp}','SalesOrdersDpsController@pdf')
         ->name('sales_orders_dps.sales_orders_dp.print')
         ->where('id', '[0-9]+');

    Route::get('/{salesOrdersDp}/edit','SalesOrdersDpsController@edit')
         ->name('sales_orders_dps.sales_orders_dp.edit')
         ->where('id', '[0-9]+');

    Route::post('/', 'SalesOrdersDpsController@store')
         ->name('sales_orders_dps.sales_orders_dp.store');
               
    Route::put('sales_orders_dp/{salesOrdersDp}', 'SalesOrdersDpsController@update')
         ->name('sales_orders_dps.sales_orders_dp.update')
         ->where('id', '[0-9]+');

    Route::delete('/sales_orders_dp/{salesOrdersDp}','SalesOrdersDpsController@destroy')
         ->name('sales_orders_dps.sales_orders_dp.destroy')
         ->where('id', '[0-9]+');

});

Route::group(
[
    'prefix' => 'incoming_invoices',
], function () {

    Route::get('/', 'IncomingInvoicesController@index')
         ->name('incoming_invoices.incoming_invoices.index');

    Route::get('/create','IncomingInvoicesController@create')
         ->name('incoming_invoices.incoming_invoices.create');

    Route::get('/show/{incomingInvoices}','IncomingInvoicesController@show')
         ->name('incoming_invoices.incoming_invoices.show')
         ->where('id', '[0-9]+');

    Route::get('/{incomingInvoices}/edit','IncomingInvoicesController@edit')
         ->name('incoming_invoices.incoming_invoices.edit')
         ->where('id', '[0-9]+');

    Route::post('/', 'IncomingInvoicesController@store')
         ->name('incoming_invoices.incoming_invoices.store');
               
    Route::put('incoming_invoices/{incomingInvoices}', 'IncomingInvoicesController@update')
         ->name('incoming_invoices.incoming_invoices.update')
         ->where('id', '[0-9]+');

    Route::delete('/incoming_invoices/{incomingInvoices}','IncomingInvoicesController@destroy')
         ->name('incoming_invoices.incoming_invoices.destroy')
         ->where('id', '[0-9]+');

});

Route::group(
[
    'prefix' => 'incoming_invoices_details',
], function () {

    Route::get('/', 'IncomingInvoicesDetailsController@index')
         ->name('incoming_invoices_details.incoming_invoices_details.index');

    Route::get('/create','IncomingInvoicesDetailsController@create')
         ->name('incoming_invoices_details.incoming_invoices_details.create');

    Route::get('/show/{incomingInvoicesDetails}','IncomingInvoicesDetailsController@show')
         ->name('incoming_invoices_details.incoming_invoices_details.show')
         ->where('id', '[0-9]+');

    Route::get('/{incomingInvoicesDetails}/edit','IncomingInvoicesDetailsController@edit')
         ->name('incoming_invoices_details.incoming_invoices_details.edit')
         ->where('id', '[0-9]+');

    Route::post('/', 'IncomingInvoicesDetailsController@store')
         ->name('incoming_invoices_details.incoming_invoices_details.store');
               
    Route::put('incoming_invoices_details/{incomingInvoicesDetails}', 'IncomingInvoicesDetailsController@update')
         ->name('incoming_invoices_details.incoming_invoices_details.update')
         ->where('id', '[0-9]+');

    Route::delete('/incoming_invoices_details/{incomingInvoicesDetails}','IncomingInvoicesDetailsController@destroy')
         ->name('incoming_invoices_details.incoming_invoices_details.destroy')
         ->where('id', '[0-9]+');

});

Route::group(
[
    'prefix' => 'reports',
], function () {

    Route::get('/', 'ReportsController@index')
         ->name('reports.reports.index');

    Route::get('/outstanding','ReportsController@outstanding')
         ->name('reports.reports.outstanding');
    
    Route::any('/outstanding/result','ReportsController@outstandingresult')
         ->name('reports.reports.outstanding.result');
    
    Route::get('/sales_order','ReportsController@salesorder')
         ->name('reports.reports.sales_order');
    
    Route::any('/sales_order/result','ReportsController@salesorderresult')
         ->name('reports.reports.sales_order.result');
    
    Route::get('/purchase_order','ReportsController@purchaseorder')
         ->name('reports.reports.purchase_order');
    
    Route::any('/purchase_order/result','ReportsController@purchaseorderresult')
         ->name('reports.reports.purchase_order.result');
    
    Route::get('/delivery_order','ReportsController@deliveryorder')
         ->name('reports.reports.delivery_order');
    
    Route::any('/delivery_order/result','ReportsController@deliveryorderresult')
         ->name('reports.reports.delivery_order.result');
    
    Route::get('/invoice','ReportsController@invoice')
         ->name('reports.reports.invoice');
    
    Route::any('/invoice/result','ReportsController@invoiceresult')
         ->name('reports.reports.invoice.result');
    
    Route::get('/incoming_invoice','ReportsController@incominginvoice')
         ->name('reports.reports.incoming_invoice');
    
    Route::any('/incoming_invoice/result','ReportsController@incominginvoiceresult')
         ->name('reports.reports.incoming_invoice.result');
        
    
});
